const LOAD_MEMBERS = 'redux-example/addMapMember/LOAD_MEMBERS';
const LOAD_MEMBERS_SUCCESS = 'redux-example/addMapMember/LOAD_MEMBERS_SUCCESS';
const LOAD_MEMBERS_FAIL = 'redux-example/addMapMember/LOAD_MEMBERS_FAIL';

const SAVE = 'redux-example/addMapMember/SAVE';
const SAVE_SUCCESS = 'redux-example/addMapMember/SAVE_SUCCESS';
const SAVE_FAIL = 'redux-example/addMapMember/SAVE_FAIL';

const initialState = {
  loaded: false,
  saveError: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_MEMBERS:
      return {
        ...state,
        loading: true
      };
    case LOAD_MEMBERS_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result,
        error: null
      };
    case LOAD_MEMBERS_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case SAVE:
      return state; // 'saving' flag handled by redux-form
    case SAVE_SUCCESS:
      return {
        ...state,
        saveError: {
          ...state.saveError
        }
      };
    case SAVE_FAIL:
      return typeof action.error === 'string' ? {
        ...state,
        saveError: {
          ...state.saveError,
          [action.id]: action.error
        }
      } : state;
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.addMapMember && globalState.addMapMember.loaded;
}

export function load(id) {
  return {
    types: [LOAD_MEMBERS, LOAD_MEMBERS_SUCCESS, LOAD_MEMBERS_FAIL],
    promise: (client) => client.get(`/api/member/${id}`)
  };
}

export function save(data, parentId, isLeft, isRight) {
  console.info(data, parentId, isLeft, isRight);
  return {
    types: [SAVE, SAVE_SUCCESS, SAVE_FAIL],
    promise: (client) => client.post(`/api/member/${parentId}/ancestor`, {
      data: {
        ...data,
        isLeft,
        isRight
      }
    })
  };
}
