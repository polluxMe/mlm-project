const initialState = {
  data: [
    {
      id: 1,
      title: 'Announcement Title',
      text: 'Text text text text text text text text text text',
      date: '2016.00.00'
    },
    {
      id: 2,
      title: 'Announcement Title',
      text: 'Text text text text text text text text text text',
      date: '2016.00.00'
    },
    {

      id: 3,
      title: 'Announcement Title',
      text: 'Text text text text text text text text text text',
      date: '2016.00.00'
    }
  ]
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    default:
      return state;
  }
}
