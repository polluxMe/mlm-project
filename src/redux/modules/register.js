const SET_TEMP_USER = 'redux-example/register/SET_TEMP_USER';

const initialState = {};

export default function registerReducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_TEMP_USER:
      return {
        ...state,
        tempUser: action.user
      };
    default:
      return state;
  }
}

export function setTempUser(user) {
  return {
    type: SET_TEMP_USER,
    user
  };
}
