const LOAD = 'redux-example/vs_config_frame_detail/LOAD';
const LOAD_SUCCESS = 'redux-example/vs_config_frame_detail/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/vs_config_frame_detail/LOAD_FAIL';
const EDIT_START = 'redux-example/vs_config_frame_detail/EDIT_START';
const EDIT_STOP = 'redux-example/vs_config_frame_detail/EDIT_STOP';
const SAVE = 'redux-example/vs_config_frame_detail/SAVE';
const SAVE_SUCCESS = 'redux-example/vs_config_frame_detail/SAVE_SUCCESS';
const SAVE_FAIL = 'redux-example/vs_config_frame_detail/SAVE_FAIL';
const SET_DATA = 'redux-example/vs_config_frame_detail/SET_DATA';
const DELETE_LAST_FRAME = 'redux-example/vs_config_frame_detail/DELETE_LAST_FRAME';
const DELETE_LAST_FRAME_SUCCESS = 'redux-example/vs_config_frame_detail/DELETE_LAST_FRAME_SUCCESS';
const DELETE_LAST_FRAME_FAIL = 'redux-example/vs_config_frame_detail/DELETE_LAST_FRAME_FAIL';

const initialState = {
  loaded: false,
  editing: {},
  sizePerPage: 10,
  totalDataSize: 10,
  currentPage: 1,
  filter: '',
  sortOrder: '',
  frames: {from: [], to: []},
  frameBlocks: [],
  frame: null,
  deleting: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      // console.log('action.result', action.result);
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.frames,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case EDIT_START:
      return {
        ...state,
        editing: {
          ...state.editing,
          [action.id]: true
        }
      };
    case EDIT_STOP:
      return {
        ...state,
        editing: {
          ...state.editing,
          [action.id]: false
        }
      };
    case DELETE_LAST_FRAME:
      return {
        ...state,
        deleting: true
      };
    case DELETE_LAST_FRAME_SUCCESS:
      return {
        ...state,
        deleting: false,
        data: state.data.slice(0, -1),
      };
    case DELETE_LAST_FRAME_FAIL:
      return {
        ...state,
        deleting: false,
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.vsConfigFrameDetails && globalState.vsConfigFrameDetails.loaded;
}

export function load(id) {
  console.log('globalState', id);
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/api/vs_frame_config/' + id + '/frame')
  };
}

export function set(data) {
  return { type: SET_DATA, data };
}

export function save(widget) {
  return {
    types: [SAVE, SAVE_SUCCESS, SAVE_FAIL],
    id: widget.id,
    promise: (client) => client.post('/api/vs_frame_config', {
      data: widget
    })
  };
}

export function editStart(id) {
  return { type: EDIT_START, id };
}

export function editStop(id) {
  return { type: EDIT_STOP, id };
}

export function removeLastFrame(id) {
  return {
    types: [DELETE_LAST_FRAME, DELETE_LAST_FRAME_SUCCESS, DELETE_LAST_FRAME_FAIL],
    promise: (client) => client.del('/api/vs_frame_config/' + id + '/lastFrame')
  };
}
