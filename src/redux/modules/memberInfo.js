import _ from 'lodash' // eslint-disable-line

const LOAD_INFO = 'redux-example/memberInfo/LOAD_INFO';
const LOAD_INFO_SUCCESS = 'redux-example/memberInfo/LOAD_INFO_SUCCESS';
const LOAD_INFO_FAIL = 'redux-example/memberInfo/LOAD_INFO_FAIL';
const LOAD_USER_INFO = 'redux-example/memberInfo/LOAD_USER_INFO';
const LOAD_USER_INFO_SUCCESS = 'redux-example/memberInfo/LOAD_USER_INFO_SUCCESS';
const LOAD_USER_INFO_FAIL = 'redux-example/memberInfo/LOAD_USER_INFO_FAIL';
const LOAD_REPORT = 'redux-example/memberInfo/LOAD_REPORT';
const LOAD_REPORT_SUCCESS = 'redux-example/memberInfo/LOAD_REPORT_SUCCESS';
const LOAD_REPORT_FAIL = 'redux-example/memberInfo/LOAD_REPORT_FAIL';

const initialState = {
  infoLoaded: false,
  reportLoaded: false,
  infoLoading: false,
  reportLoading: false,
  wallet: null,
  memberID: null,
  info: null,
  report: null,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_INFO:
      return {
        ...state,
        infoLoading: true
      };
    case LOAD_INFO_SUCCESS:
      return {
        ...state,
        infoLoading: false,
        infoLoaded: true,
        wallet: action.result,
        error: null
      };
    case LOAD_INFO_FAIL:
      return {
        ...state,
        infoLoading: false,
        infoLoaded: false,
        wallet: null,
        error: action.error
      };
    case LOAD_USER_INFO:
      return {
        ...state,
        infoLoading: true
      };
    case LOAD_USER_INFO_SUCCESS:
      return {
        ...state,
        infoLoading: false,
        infoLoaded: true,
        info: action.result,
        error: null
      };
    case LOAD_USER_INFO_FAIL:
      return {
        ...state,
        infoLoading: false,
        infoLoaded: false,
        info: null,
        error: action.error
      };
    case LOAD_REPORT:
      return {
        ...state,
        reportLoading: true
      };
    case LOAD_REPORT_SUCCESS:
      return {
        ...state,
        reportLoading: false,
        reportLoaded: true,
        report: action.result,
        error: null
      };
    case LOAD_REPORT_FAIL:
      return {
        ...state,
        reportLoading: false,
        reportLoaded: false,
        report: null,
        error: action.error
      };
    default:
      return state;
  }
}

export function loadWallet(id) {
  return {
    types: [LOAD_INFO, LOAD_INFO_SUCCESS, LOAD_INFO_FAIL],
    promise: (client) => client.get('/api/member/' + id + '/wallet')
  };
}

export function loadUserInfo(id, isGuess) {
  isGuess = isGuess || false; // eslint-disable-line
  return {
    types: [LOAD_USER_INFO, LOAD_USER_INFO_SUCCESS, LOAD_USER_INFO_FAIL],
    promise: (client) => client.get('/api/member/' + id + '/commission?isGuess=' + isGuess)
  };
}

export function loadReport(id, params) {
  console.log(params, 'params');
  const queryString = '?' + _.toPairs(params).map((param) => {
    return param[0] + '=' + param[1];
  }).join('&');
  return {
    types: [LOAD_REPORT, LOAD_REPORT_SUCCESS, LOAD_REPORT_FAIL],
    promise: (client) => client.get('/api/member/' + id + '/commission' + queryString)
  };
}
