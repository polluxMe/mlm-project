import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import {reducer as reduxAsyncConnect} from 'redux-async-connect';

import auth from './auth';
import {reducer as form} from 'redux-form';
import vsPurchases from './vsPurchases';
import vsPurchasesHistory from './vsPurchasesHistory';
import events from './events';
import news from './news';
import membership from './membership';
import register from './register';
import map from './map';
import addMapMember from './addMapMember';
import vsConfig from './vsConfig';
import vsConfigFrame from './vsConfigFrame';
import vsConfigFrameDetails from './vsConfigFrameDetails';
import vsRate from './vsRate';
import invitation from './invitation';
import orders from './orders';
import adminMap from './adminMap';
import adminAddMapMember from './adminAddMapMember';
import moneyWithdraw from './moneyWithdraw';
import adminMemberList from './adminMemberList';
import mainCommissionRequests from './mainCommissionRequests';
import withdrawRequests from './withdrawRequests';
import matchingBonusRequests from './matchingBonusRequests';
import cashRewardRequests from './cashRewardRequests';
import mainConfig from './mainConfig';
import withdrawalConfig from './withdrawalConfig';
import withdrawalRate from './withdrawalRate';
import vsPurchaseProcess from './vsPurchaseProcess';
import memberInfo from './memberInfo';
import adminMemberDetails from './adminMemberDetails';
import cashRewardsRequestDetails from './cashRewardsRequestDetails';
import mainCommissionRequestDetails from './mainCommissionRequestDetails';
import matchingBonusRequestDetails from './matchingBonusRequestDetails';
import unilevelMap from './unilevelMap';
import adminUnilevelMap from './adminUnilevelMap';
import account from './account';
import ticketRequests from './ticketRequests';
import ticketRequestDetails from './ticketRequestDetails';
import brave1WayRequests from './brave1WayRequests';
import brave2WayRequests from './brave2WayRequests';
import brave3WayRequests from './brave3WayRequests';
import brave1WayHistory from './brave1WayHistory';
import brave2WayHistory from './brave2WayHistory';
import brave3WayHistory from './brave3WayHistory';
import mainCommission from './mainCommission';
import rankCommission from './rankCommission';
import megaCommission from './megaCommission';
import cashRewardCommission from './cashRewardCommission';
import mainCommissionDividend from './mainCommissionDividend';
import mainCommissionPurchased from './mainCommissionPurchased';

export default combineReducers({
  routing: routerReducer,
  reduxAsyncConnect,
  auth,
  form,
  vsPurchases,
  vsPurchasesHistory,
  events,
  news,
  membership,
  map,
  addMapMember,
  register,
  vsConfig,
  vsConfigFrame,
  vsRate,
  invitation,
  vsConfigFrameDetails,
  orders,
  adminMap,
  adminAddMapMember,
  moneyWithdraw,
  adminMemberList,
  mainCommissionRequests,
  withdrawRequests,
  matchingBonusRequests,
  cashRewardRequests,
  mainConfig,
  withdrawalConfig,
  withdrawalRate,
  vsPurchaseProcess,
  memberInfo,
  adminMemberDetails,
  cashRewardsRequestDetails,
  mainCommissionRequestDetails,
  matchingBonusRequestDetails,
  unilevelMap,
  adminUnilevelMap,
  account,
  ticketRequests,
  ticketRequestDetails,
  brave1WayRequests,
  brave2WayRequests,
  brave3WayRequests,
  brave1WayHistory,
  brave2WayHistory,
  brave3WayHistory,
  mainCommission,
  rankCommission,
  megaCommission,
  cashRewardCommission,
  mainCommissionDividend,
  mainCommissionPurchased
});
