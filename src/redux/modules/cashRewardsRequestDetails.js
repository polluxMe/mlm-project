const LOAD = 'redux-example/cashRewardsRequestDetails/LOAD';
const LOAD_SUCCESS = 'redux-example/cashRewardsRequestDetails/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/cashRewardsRequestDetails/LOAD_FAIL';


const initialState = {
  loaded: false,
  loading: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      console.log('ACTION RESULT', action.result);
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        purchase: null,
        error: action.error
      };
    default:
      return state;
  }
}

export function load(id) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: client => client.get(`/api/cash_reward_request/` + id + '/master/')
  };
}
