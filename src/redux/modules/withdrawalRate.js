const LOAD_RATE = 'redux-example/withdrawalRate/LOAD_RATE';
const LOAD_RATE_SUCCESS = 'redux-example/withdrawalRate/LOAD_RATE_SUCCESS';
const LOAD_RATE_FAIL = 'redux-example/withdrawalRate/LOAD_RATE_FAIL';

const initialState = {
  rateLoaded: false,
  rateLoading: false,
  rate: null,
  memberID: null,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_RATE:
      return {
        ...state,
        rateLoading: true
      };
    case LOAD_RATE_SUCCESS:
      return {
        ...state,
        rateLoading: false,
        rateLoaded: true,
        rate: action.result.rate,
        error: null
      };
    case LOAD_RATE_FAIL:
      return {
        ...state,
        rateLoading: false,
        rateLoaded: false,
        rate: null,
        error: action.error
      };
    default:
      return state;
  }
}

export function loadRate() {
  console.log('loadRate');
  return {
    types: [LOAD_RATE, LOAD_RATE_SUCCESS, LOAD_RATE_FAIL],
    promise: (client) => client.get('/api/withdrawal_config/rate')
  };
}
