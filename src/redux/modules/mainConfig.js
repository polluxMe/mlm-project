const SAVE = 'redux-example/mainConfig/SAVE';
const SAVE_SUCCESS = 'redux-example/mainConfig/SAVE_SUCCESS';
const SAVE_FAIL = 'redux-example/mainConfig/SAVE_FAIL';
const LOAD = 'redux-example/mainConfig/LOAD';
const LOAD_SUCCESS = 'redux-example/mainConfig/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/mainConfig/LOAD_FAIL';

const initialState = {
  data: null,
  loaded: false,
  loading: false,

};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case SAVE:
      return {
        ...state,
        saving: true
      };
    case SAVE_SUCCESS:
      return {
        ...state,
        saving: false,
        saved: true,
        data: action.result,
        error: null
      };
    case SAVE_FAIL:
      return {
        ...state,
        saving: false,
        saved: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.mainConfig && globalState.mainConfig.loaded;
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/api/main_config')
  };
}

export function save(data) {
  return {
    types: [SAVE, SAVE_SUCCESS, SAVE_FAIL],
    promise: (client) => client.put('/api/main_config', {
      data: {
        ...data
      }
    })
  };
}
