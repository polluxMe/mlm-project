const LOAD_HISTORY = 'redux-example/vsPurchasesHistory/LOAD_HISTORY';
const LOAD_HISTORY_SUCCESS = 'redux-example/vsPurchasesHistory/LOAD_HISTORY_SUCCESS';
const LOAD_HISTORY_FAIL = 'redux-example/vsPurchasesHistory/LOAD_HISTORY_FAIL';
const LOAD_FRAME = 'redux-example/vsPurchasesHistory/LOAD_FRAME';
const LOAD_FRAME_SUCCESS = 'redux-example/vsPurchasesHistory/LOAD_FRAME_SUCCESS';
const LOAD_FRAME_FAIL = 'redux-example/vsPurchasesHistory/LOAD_FRAME_FAIL';
const UPDATE_FRAME = 'redux-example/vsPurchases/UPDATE_FRAME';
const UPDATE_FRAME_SUCCESS = 'redux-example/vsPurchases/UPDATE_FRAME_SUCCESS';
const UPDATE_FRAME_FAIL = 'redux-example/vsPurchases/UPDATE_FRAME_FAIL';
const END_FRAME = 'redux-example/vsPurchasesHistory/END_FRAME';
const END_FRAME_SUCCESS = 'redux-example/vsPurchasesHistory/END_FRAME_SUCCESS';
const END_FRAME_FAIL = 'redux-example/vsPurchasesHistory/END_FRAME_FAIL';
const CHANGE_HISTORY_PAGE = 'redux-example/vsPurchasesHistory/CHANGE_HISTORY_PAGE';
const CHANGE_HISTORY_PAGE_SUCCESS = 'redux-example/vsPurchasesHistory/CHANGE_HISTORY_PAGE_SUCCESS';
const CHANGE_HISTORY_PAGE_FAIL = 'redux-example/vsPurchasesHistory/CHANGE_HISTORY_PAGE_FAIL';
const CHANGE_HISTORY_FILTER = 'redux-example/vsPurchasesHistory/CHANGE_HISTORY_FILTER';
const CHANGE_HISTORY_FILTER_SUCCESS = 'redux-example/vsPurchasesHistory/CHANGE_HISTORY_FILTER_SUCCESS';
const CHANGE_HISTORY_FILTER_FAIL = 'redux-example/vsPurchasesHistory/CHANGE_HISTORY_FILTER_FAIL';
const CHANGE_HISTORY_SORT = 'redux-example/vsPurchasesHistory/CHANGE_HISTORY_SORT';
const CHANGE_HISTORY_SORT_SUCCESS = 'redux-example/vsPurchasesHistory/CHANGE_HISTORY_SORT_SUCCESS';
const CHANGE_HISTORY_SORT_FAIL = 'redux-example/vsPurchasesHistory/CHANGE_HISTORY_SORT_FAIL';
const CHANGE_HISTORY_SIZE_PER_PAGE = 'redux-example/vsPurchasesHistory/CHANGE_HISTORY_SIZE_PER_PAGE';
const CHANGE_HISTORY_SIZE_PER_PAGE_SUCCESS = 'redux-example/vsPurchasesHistory/CHANGE_HISTORY_SIZE_PER_PAGE_SUCCESS';
const CHANGE_HISTORY_SIZE_PER_PAGE_FAIL = 'redux-example/vsPurchasesHistory/CHANGE_HISTORY_SIZE_PER_PAGE_FAIL';

const initialState = {
  loaded: false,
  sizePerPage: 10,
  totalDataSize: 10,
  currentPage: 1,
  filter: '',
  sortOrder: '{"name":"createDate","order":-1}',
  frameId: null
};


const getStateParams = (state) => ({
  page: state.currentPage,
  sort: state.sortOrder,
  filter: state.filter,
  limit: state.sizePerPage,
});

const createLoadPromise = (reqParams) => (client, getState) => {
  const state = getState().vsPurchasesHistory;
  const params = Object.assign({}, getStateParams(state), reqParams);
  console.log('params state', state);
  return client.get('/api/vs_purchase', {params});
};

export default function reducer(state = initialState, action = {}) {
  // console.log('action', action.type);
  switch (action.type) {
    case LOAD_HISTORY:
      return {
        ...state,
        loading: true
      };
    case LOAD_HISTORY_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        totalDataSize: action.result.count,
        error: null
      };
    case LOAD_HISTORY_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case UPDATE_FRAME:
      return {
        ...state,
        loading: true
      };
    case UPDATE_FRAME_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: null
      };
    case UPDATE_FRAME_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case LOAD_FRAME:
      return {
        ...state,
        loading: true
      };
    case LOAD_FRAME_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        frameData: action.result,
        error: null
      };
    case LOAD_FRAME_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        frameData: null,
        error: action.error
      };
    case END_FRAME:
      return {
        ...state,
        loading: true
      };
    case END_FRAME_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        success: action.result,
        error: null
      };
    case END_FRAME_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        success: null,
        error: action.error
      };
    case CHANGE_HISTORY_PAGE:
      return {
        ...state,
        loading: true
      };
    case CHANGE_HISTORY_PAGE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        error: null,
        currentPage: action.page
      };
    case CHANGE_HISTORY_PAGE_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case CHANGE_HISTORY_SIZE_PER_PAGE:
      return {
        ...state,
        loading: true
      };
    case CHANGE_HISTORY_SIZE_PER_PAGE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        error: null,
        sizePerPage: action.limit
      };
    case CHANGE_HISTORY_SIZE_PER_PAGE_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case CHANGE_HISTORY_FILTER:
      return {
        ...state,
        loading: true
      };
    case CHANGE_HISTORY_FILTER_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        totalDataSize: action.result.count,
        error: null,
        filter: action.filter
      };
    case CHANGE_HISTORY_FILTER_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case CHANGE_HISTORY_SORT:
      return {
        ...state,
        loading: true
      };
    case CHANGE_HISTORY_SORT_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        error: null,
        sortOrder: action.sort
      };
    case CHANGE_HISTORY_SORT_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.vsPurchasesHistory && globalState.vsPurchasesHistory.loaded;
}

export function loadHistory(frameId) {
  return {
    types: [LOAD_HISTORY, LOAD_HISTORY_SUCCESS, LOAD_HISTORY_FAIL],
    promise: createLoadPromise({frameId})
  };
}

export function loadFrameData(frameId) {
  return {
    types: [LOAD_FRAME, LOAD_FRAME_SUCCESS, LOAD_FRAME_FAIL],
    promise: client => client.get(`/api/vs_frame_config/${frameId}/frameItem`)
  };
}

export function endFrame(frameId) {
  return {
    types: [END_FRAME, END_FRAME_SUCCESS, END_FRAME_FAIL],
    promise: client => client.post(`/api/vs_frame_config/${frameId}/frameEnd`)
  };
}

export function updateFrame(frameParams) {
  console.log('frame', frameParams);
  return {
    types: [UPDATE_FRAME, UPDATE_FRAME_SUCCESS, UPDATE_FRAME_FAIL],
    promise: client => client.put(`/api/vs_frame_config/${frameParams._id}/update`, {data: frameParams})
  };
}

export function changePage(page, limit, frameId) {
  console.log('limit', limit, frameId);
  return {
    types: [CHANGE_HISTORY_PAGE, CHANGE_HISTORY_PAGE_SUCCESS, CHANGE_HISTORY_PAGE_FAIL],
    promise: createLoadPromise({page, frameId}),
    page
  };
}

export function changeSizePerPage(limit, frameId) {
  return {
    types: [CHANGE_HISTORY_SIZE_PER_PAGE, CHANGE_HISTORY_SIZE_PER_PAGE_SUCCESS, CHANGE_HISTORY_SIZE_PER_PAGE_FAIL],
    promise: createLoadPromise({limit, frameId}),
    limit
  };
}

export function changeSort(sort, frameId) {
  return {
    types: [CHANGE_HISTORY_SORT, CHANGE_HISTORY_SORT_SUCCESS, CHANGE_HISTORY_SORT_FAIL],
    promise: createLoadPromise({sort, frameId}),
    sort
  };
}

export function changeFilter(filter, frameId) {
  return {
    types: [CHANGE_HISTORY_FILTER, CHANGE_HISTORY_FILTER_SUCCESS, CHANGE_HISTORY_FILTER_FAIL],
    promise: createLoadPromise({filter, frameId}),
    filter
  };
}
