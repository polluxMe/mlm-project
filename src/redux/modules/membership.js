const initialState = {
  data: {
    memberId: 1111,
    rank: 'R5',
    ownedStock: '100,000,000',
    brave: 'text',
    braveCoin: 'text',
    ticket: 'text'
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    default:
      return state;
  }
}
