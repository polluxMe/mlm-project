const LOAD = 'redux-example/vsPurchaseProcess/LOAD';
const LOAD_SUCCESS = 'redux-example/vsPurchaseProcess/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/vsPurchaseProcess/LOAD_FAIL';
const UPDATE = 'redux-example/vsPurchaseProcess/UPDATE';
const UPDATE_SUCCESS = 'redux-example/vsPurchaseProcess/UPDATE_SUCCESS';
const UPDATE_FAIL = 'redux-example/vsPurchaseProcess/UPDATE_FAIL';
const CANCEL = 'redux-example/vsPurchaseProcess/CANCEL                                                                                                                                                                  ';
const CANCEL_SUCCESS = 'redux-example/vsPurchaseProcess/CANCEL_SUCCESS';
const CANCEL_FAIL = 'redux-example/vsPurchaseProcess/CANCEL_FAIL';

const initialState = {
  loaded: false,
  loading: false,
  error: ''
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        purchase: action.result,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        purchase: null,
        error: action.error
      };
    case CANCEL:
      return {
        ...state,
        canceling: true
      };
    case CANCEL_SUCCESS:
      return {
        ...state,
        canceling: false,
        loaded: true,
        purchase: action.result,
        error: null
      };
    case CANCEL_FAIL:
      return {
        ...state,
        canceling: false,
        loaded: false,
        error: action.error
      };
    case UPDATE:
      return {
        ...state,
        updating: true
      };
    case UPDATE_SUCCESS:
      return {
        ...state,
        updating: false,
        error: null
      };
    case UPDATE_FAIL:
      return {
        ...state,
        updating: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.vsPurchaseProcess && globalState.vsPurchaseProcess.loaded;
}

export function load(id) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: client => client.get(`/api/vs_purchase/master/` + id + '/')
  };
}

export function update(item) {
  return {
    types: [UPDATE, UPDATE_SUCCESS, UPDATE_FAIL],
    promise: client => client.put(`/api/vs_purchase/`, {data: item})
  };
}

export function cancel(id) {
  return {
    types: [CANCEL, CANCEL_SUCCESS, CANCEL_FAIL],
    promise: client => client.post(`/api/vs_purchase/${id}/cancel`)
  };
}
