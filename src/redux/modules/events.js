const initialState = {
  data: [
    {
      id: 1,
      title: '○ moon ○ day Tokyo seminar held in',
      text: 'Tokyo: Seminazu Nishi campus Shinjuku Nishi 8-4-2 Nomura Real Estate Nishi building 4F ※ 9-minute walk ' +
      'from JR Shinjuku Station, a 1-minute walk from the Marunouchi Line Nishi-Shinjuku Station'
    },
    {
      id: 2,
      title: '○ moon ○ day Tokyo seminar held in',
      text: 'Tokyo: Seminazu Nishi campus Shinjuku Nishi 8-4-2 Nomura Real Estate Nishi building 4F ※ 9-minute walk ' +
      'from JR Shinjuku Station, a 1-minute walk from the Marunouchi Line Nishi-Shinjuku Station'
    },
    {

      id: 3,
      title: '○ moon ○ day Tokyo seminar held in',
      text: 'Tokyo: Seminazu Nishi campus Shinjuku Nishi 8-4-2 Nomura Real Estate Nishi building 4F ※ 9-minute walk ' +
      'from JR Shinjuku Station, a 1-minute walk from the Marunouchi Line Nishi-Shinjuku Station'
    }
  ]
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    default:
      return state;
  }
}
