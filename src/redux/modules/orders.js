const LOAD_ORDERS = 'redux-example/orders/LOAD_ORDERS';
const LOAD_ORDERS_SUCCESS = 'redux-example/orders/LOAD_ORDERS_SUCCESS';
const LOAD_ORDERS_FAIL = 'redux-example/orders/LOAD_ORDERS_FAIL';

const CANCEL_PURCHASE = 'redux-example/orders/CANCEL_PURCHASE';
const CANCEL_PURCHASE_SUCCESS = 'redux-example/orders/CANCEL_PURCHASE_SUCCESS';
const CANCEL_PURCHASE_FAIL = 'redux-example/orders/CANCEL_PURCHASE_FAIL';

const initialState = {
  loaded: false,
  data: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_ORDERS:
      return {
        ...state,
        loading: true
      };
    case LOAD_ORDERS_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result,
        error: null
      };
    case LOAD_ORDERS_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case CANCEL_PURCHASE:
      return state;
    case CANCEL_PURCHASE_SUCCESS:
      return state;
    case CANCEL_PURCHASE_FAIL:
      return state;
    default:
      return state;
  }
}


export function loadOrders(memberID) {
  return {
    types: [LOAD_ORDERS, LOAD_ORDERS_SUCCESS, LOAD_ORDERS_FAIL],
    promise: (client) => client.get(`/api/vs_purchase/${memberID}`)
  };
}

export function cancelPurchase(transactionId) {
  return {
    types: [CANCEL_PURCHASE, CANCEL_PURCHASE_SUCCESS, CANCEL_PURCHASE_FAIL],
    promise: (client) => client.post(`/api/vs_purchase/${transactionId}/cancel`)
  };
}

