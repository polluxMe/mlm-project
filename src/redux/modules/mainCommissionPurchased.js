const LOAD = 'redux-example/mainCommissionPurchased/LOAD';
const LOAD_SUCCESS = 'redux-example/mainCommissionPurchased/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/mainCommissionPurchased/LOAD_FAIL';

const initialState = {
  loaded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        item: action.result,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        item: null,
        error: action.error
      };
    default:
      return state;
  }
}

export function load(memberId, params = {}) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: client => client.get(`/api/vs_purchase/${memberId}/sum`, {params})
  };
}
