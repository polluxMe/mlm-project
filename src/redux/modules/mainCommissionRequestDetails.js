const LOAD = 'redux-example/mainCommissionRequestDetails/LOAD';
const LOAD_SUCCESS = 'redux-example/mainCommissionRequestDetails/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/mainCommissionRequestDetails/LOAD_FAIL';


const initialState = {
  loaded: false,
  loading: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        purchase: null,
        error: action.error
      };
    default:
      return state;
  }
}

export function load(id) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: client => client.get(`/api/commission_request/` + id + '/master/')
  };
}
