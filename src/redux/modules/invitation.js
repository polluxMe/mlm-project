const SEND = 'redux-example/invitation/SEND';
const SEND_SUCCESS = 'redux-example/invitation/SEND_SUCCESS';
const SEND_FAIL = 'redux-example/invitation/SEND_FAIL';

const initialState = {
  loading: false,
  response: null
};

export default function info(state = initialState, action = {}) {
  switch (action.type) {
    case SEND:
      return {
        ...state,
        loading: true
      };
    case SEND_SUCCESS:
      return {
        ...state,
        loading: false,
        response: action.result
      };
    case SEND_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function send(message) {
  return {
    types: [SEND, SEND_SUCCESS, SEND_FAIL],
    promise: (client) => client.post('api/member/invitation', {data: message})
  };
}

