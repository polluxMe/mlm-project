const SHIFT_LEFT = 'redux-example/adminUnilevelMap/SHIFT_LEFT';
const SHIFT_RIGHT = 'redux-example/adminUnilevelMap/SHIFT_RIGHT';
const SHIFT_TOP = 'redux-example/adminUnilevelMap/SHIFT_TOP';
const SHIFT_BOTTOM = 'redux-example/adminUnilevelMap/SHIFT_BOTTOM';

const LOAD_MEMBERS = 'redux-example/adminUnilevelMap/LOAD_MEMBERS';
const LOAD_MEMBERS_SUCCESS = 'redux-example/adminUnilevelMap/LOAD_MEMBERS_SUCCESS';
const LOAD_MEMBERS_FAIL = 'redux-example/adminUnilevelMap/LOAD_MEMBERS_FAIL';

const initialState = {
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,

  loading: true,
  loaded: false,
  data: []
};

const step = 100;

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SHIFT_LEFT:
      return {
        ...state,
        left: state.left + step
      };

    case SHIFT_RIGHT:
      return {
        ...state,
        left: state.left - step
      };

    case SHIFT_TOP:
      return {
        ...state,
        top: state.top + step
      };

    case SHIFT_BOTTOM:
      return {
        ...state,
        top: state.top - step
      };

    case LOAD_MEMBERS:
      return {
        ...state,
        loading: true
      };
    case LOAD_MEMBERS_SUCCESS:
      console.log(action.result);
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result,
        error: null
      };
    case LOAD_MEMBERS_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };

    default:
      return state;
  }
}

export function shiftLeft() {
  return {
    type: SHIFT_LEFT
  };
}

export function shiftRight() {
  return {
    type: SHIFT_RIGHT
  };
}

export function shiftTop() {
  return {
    type: SHIFT_TOP
  };
}

export function shiftBottom() {
  return {
    type: SHIFT_BOTTOM
  };
}

export function load(id) {
  return {
    types: [LOAD_MEMBERS, LOAD_MEMBERS_SUCCESS, LOAD_MEMBERS_FAIL],
    promise: (client) => client.get(`/api/member/${id}/unilevelMap`)
  };
}
