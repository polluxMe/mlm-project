const LOAD = 'redux-example/adminMemberDetails/LOAD';
const LOAD_SUCCESS = 'redux-example/adminMemberDetails/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/adminMemberDetails/LOAD_FAIL';
const LOAD_RANK_HISTORY = 'redux-example/adminMemberDetails/LOAD_RANK_HISTORY';
const LOAD_RANK_HISTORY_SUCCESS = 'redux-example/adminMemberDetails/LOAD_RANK_HISTORY_SUCCESS';
const LOAD_RANK_HISTORY_FAIL = 'redux-example/adminMemberDetails/LOAD_RANK_HISTORY_FAIL';
const SAVE = 'redux-example/adminMemberDetails/SAVE';
const SAVE_SUCCESS = 'redux-example/adminMemberDetails/SAVE_SUCCESS';
const SAVE_FAIL = 'redux-example/adminMemberDetails/SAVE_FAIL';
const SAVE_RANK_HISTORY = 'redux-example/adminMemberDetails/SAVE_RANK_HISTORY';
const SAVE_RANK_HISTORY_SUCCESS = 'redux-example/adminMemberDetails/SAVE_RANK_HISTORY_SUCCESS';
const SAVE_RANK_HISTORY_FAIL = 'redux-example/adminMemberDetails/SAVE_RANK_HISTORY_FAIL';

const initialState = {
  loaded: false,
  member: {},
  saving: false,
  saved: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        member: action.result,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        member: {},
        error: action.error
      };
    case LOAD_RANK_HISTORY:
      return {
        ...state,
        loadingRankHistory: true
      };
    case LOAD_RANK_HISTORY_SUCCESS:
      return {
        ...state,
        loadingRankHistory: false,
        loadedRankHistory: true,
        rankHistory: action.result,
        error: null
      };
    case LOAD_RANK_HISTORY_FAIL:
      return {
        ...state,
        loadingRankHistory: false,
        loadedRankHistory: false,
        rankHistory: {},
        error: action.error
      };
    case SAVE:
      return {
        ...state,
        saving: true,
        saved: false
      };
    case SAVE_SUCCESS:
      return {
        ...state,
        saving: false,
        saved: true,
        data: action.result,
        error: null
      };
    case SAVE_FAIL:
      return {
        ...state,
        saving: false,
        saved: false,
        error: action.error
      };
    case SAVE_RANK_HISTORY:
      return {
        ...state,
        savingRankHistory: true,
        savedRankHistory: false
      };
    case SAVE_RANK_HISTORY_SUCCESS:
      return {
        ...state,
        savingRankHistory: false,
        savedRankHistory: true,
        // data: action.result,
        error: null
      };
    case SAVE_RANK_HISTORY_FAIL:
      return {
        ...state,
        savingRankHistory: false,
        savedRankHistory: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.adminMemberDetails && globalState.adminMemberDetails.loaded;
}

export function load(id) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: client => client.get(`/api/member/item/${id}/`)
  };
}

export function loadRankHistory(id) {
  return {
    types: [LOAD_RANK_HISTORY, LOAD_RANK_HISTORY_SUCCESS, LOAD_RANK_HISTORY_FAIL],
    promise: client => client.get(`/api/member/item/${id}/rank_history`)
  };
}

export function save(data) {
  return {
    types: [SAVE, SAVE_SUCCESS, SAVE_FAIL],
    promise: (client) => client.put('/api/member', {
      data: {
        ...data
      }
    })
  };
}

export function saveRankHistory(memberID, data) {
  return {
    types: [SAVE_RANK_HISTORY, SAVE_RANK_HISTORY_SUCCESS, SAVE_RANK_HISTORY_FAIL],
    promise: (client) => client.put(`/api/member/item/${memberID}/rank_history`, {
      data: {
        ...data
      }
    })
  };
}
