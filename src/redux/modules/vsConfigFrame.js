const LOAD = 'redux-example/vs_config_frame/LOAD';
const LOAD_SUCCESS = 'redux-example/vs_config_frame/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/vs_config_frame/LOAD_FAIL';
const LOAD_FRAME = 'redux-example/vs_config_frame/LOAD_FRAME';
const LOAD_FRAME_SUCCESS = 'redux-example/vs_config_frame/LOAD_FRAME_SUCCESS';
const LOAD_FRAME_FAIL = 'redux-example/vs_config_frame/LOAD_FRAME_FAIL';
const LOAD_FRAME_RECORD = 'redux-example/vs_config_frame/LOAD_FRAME_RECORD';
const LOAD_FRAME_RECORD_SUCCESS = 'redux-example/vs_config_frame/LOAD_FRAME_RECORD_SUCCESS';
const LOAD_FRAME_RECORD_FAIL = 'redux-example/vs_config_frame/LOAD_FRAME_RECORD_FAIL';
const LOAD_DELETE = 'redux-example/vs_config_frame/LOAD_DELETE';
const LOAD_DELETE_SUCCESS = 'redux-example/vs_config_frame/LOAD_DELETE_SUCCESS';
const LOAD_DELETE_FAIL = 'redux-example/vs_config_frame/LOAD_DELETE_FAIL';
const LOAD_DELETE_RECORD = 'redux-example/vs_config_frame/LOAD_DELETE_RECORD';
const LOAD_DELETE_RECORD_SUCCESS = 'redux-example/vs_config_frame/LOAD_DELETE_RECORD_SUCCESS';
const LOAD_DELETE_RECORD_FAIL = 'redux-example/vs_config_frame/LOAD_DELETE_RECORD_FAIL';
const SAVE_FAIL = 'redux-example/vs_config_frame/SAVE_FAIL';
const SAVE_SUCCESS = 'redux-example/vs_config_frame/SAVE_SUCCESS';
const SAVE = 'redux-example/vs_config_frame/SAVE';
const UPDATE_FAIL = 'redux-example/vs_config_frame/UPDATE_FAIL';
const UPDATE_SUCCESS = 'redux-example/vs_config_frame/UPDATE_SUCCESS';
const UPDATE = 'redux-example/vs_config_frame/UPDATE';
const CHANGE_PAGE = 'redux-example/vs_config_frame/CHANGE_PAGE';
const CHANGE_PAGE_SUCCESS = 'redux-example/vs_config_frame/CHANGE_PAGE_SUCCESS';
const CHANGE_PAGE_FAIL = 'redux-example/vs_config_frame/CHANGE_PAGE_FAIL';
const CHANGE_ORDER = 'redux-example/vs_config_frame/CHANGE_ORDER';
const CHANGE_ORDER_SUCCESS = 'redux-example/vs_config_frame/CHANGE_ORDER_SUCCESS';
const CHANGE_ORDER_FAIL = 'redux-example/vs_config_frame/CHANGE_ORDER_FAIL';

const initialState = {
  loaded: false,
  editing: {},
  sizePerPage: 10,
  totalDataSize: 10,
  currentPage: 1,
  filter: '',
  sortOrder: '',
  frames: {from: [], to: []},
  frameBlocks: [],
  frame: null,
  loadedData: false,
  saving: false,
  deleting: false
};

const getStateParams = (state) => ({
  page: state.currentPage,
  limit: state.sizePerPage,
});

const createLoadPromise = (reqParams) => (client, getState) => {
  const state = getState().vsPurchases;
  const params = Object.assign({}, getStateParams(state), reqParams);
  return client.get('/api/vs_frame_config/', {params});
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        totalDataSize: action.result.count,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case CHANGE_PAGE:
      return {
        ...state,
        loading: true
      };
    case CHANGE_PAGE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        totalDataSize: action.result.count,
        error: null,
        currentPage: action.page
      };
    case CHANGE_PAGE_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case LOAD_FRAME:
      return {
        ...state,
        loading: true
      };
    case LOAD_FRAME_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        frames: action.result,
        error: null
      };
    case LOAD_FRAME_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        frames: null,
        error: action.error
      };
    case LOAD_FRAME_RECORD:
      return {
        ...state,
        loading: true
      };
    case LOAD_FRAME_RECORD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        frame: action.result,
        loadedData: true,
        error: null
      };
    case LOAD_FRAME_RECORD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        frame: null,
        error: action.error
      };
    case LOAD_DELETE:
      return {
        ...state,
        loading: true
      };
    case LOAD_DELETE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        loadedData: true,
        error: null
      };
    case LOAD_DELETE_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case LOAD_DELETE_RECORD:
      return {
        ...state,
        loading: true
      };
    case LOAD_DELETE_RECORD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        frame: action.result,
        loadedData: true,
        error: null
      };
    case LOAD_DELETE_RECORD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        frame: null,
        error: action.error
      };
    case SAVE:
      return {
        ...state,
        saving: true,
      };
    case SAVE_SUCCESS:
      return {
        ...state,
        saving: false,
        error: null
      };
    case SAVE_FAIL:
      return {
        ...state,
        saving: false,
        error: action.error
      };
    case UPDATE:
      return {
        ...state,
        saving: true,
      };
    case UPDATE_SUCCESS:
      return {
        ...state,
        saving: false,
        error: null
      };
    case UPDATE_FAIL:
      return {
        ...state,
        saving: false,
        error: action.error
      };
    case CHANGE_ORDER:
      return {
        ...state,
        loading: true
      };
    case CHANGE_ORDER_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: null
      };
    case CHANGE_ORDER_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.vsConfigFrame && globalState.vsConfigFrame.loaded;
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: createLoadPromise()
  };
}

export function changePage(page) {
  return {
    types: [CHANGE_PAGE, CHANGE_PAGE_SUCCESS, CHANGE_PAGE_FAIL],
    promise: createLoadPromise({page}),
    page
  };
}

export function loadFrame(id) {
  console.log('globalState', id);
  return {
    types: [LOAD_FRAME_RECORD, LOAD_FRAME_RECORD_SUCCESS, LOAD_FRAME_RECORD_FAIL],
    promise: (client) => client.get('/api/vs_frame_config/' + id + '/frame')
  };
}

export function loadFrames() {
  return {
    types: [LOAD_FRAME, LOAD_FRAME_SUCCESS, LOAD_FRAME_FAIL],
    promise: (client) => client.get('/api/vs_frame_config/frames')
  };
}

export function deleteFrames(id) {
  console.log('delete', id);
  return {
    types: [LOAD_DELETE, LOAD_DELETE_SUCCESS, LOAD_DELETE_FAIL],
    promise: (client) => client.del('/api/vs_frame_config/' + id)
  };
}

export function deleteFrame(id) {
  return {
    types: [LOAD_DELETE_RECORD, LOAD_DELETE_RECORD_SUCCESS, LOAD_DELETE_RECORD_FAIL],
    promise: (client) => client.del('/api/vs_frame_config/frame/' + id)
  };
}

export function changeOrder(id, id2) {
  return {
    types: [CHANGE_ORDER, CHANGE_ORDER_SUCCESS, CHANGE_ORDER_FAIL],
    promise: (client) => client.put('/api/vs_frame_config/' + id + '/change_order', {
      data: {
        id: id2
      }
    })
  };
}

export function save(config) {
  return {
    types: [SAVE, SAVE_SUCCESS, SAVE_FAIL],
    promise: (client) => client.post('/api/vs_frame_config', {
      data: config
    })
  };
}

export function update(config) {
  return {
    types: [UPDATE, UPDATE_SUCCESS, UPDATE_FAIL],
    promise: (client) => client.put('/api/vs_frame_config', {
      data: config
    })
  };
}


