const SAVE = 'redux-example/account/LOAD';
const SAVE_SUCCESS = 'redux-example/account/LOAD_SUCCESS';
const SAVE_FAIL = 'redux-example/account/LOAD_FAIL';

const initialState = {
  saving: false,
  saved: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SAVE:
      return {
        ...state,
        saving: true,
        saved: false
      };
    case SAVE_SUCCESS:
      return {
        ...state,
        saving: false,
        saved: true,
        data: action.result,
        error: null
      };
    case SAVE_FAIL:
      return {
        ...state,
        saving: false,
        saved: false,
        error: action.error
      };
    default:
      return state;
  }
}


export function save(data) {
  return {
    types: [SAVE, SAVE_SUCCESS, SAVE_FAIL],
    promise: (client) => client.put('/api/member', {
      data: {
        ...data
      }
    })
  };
}
