const LOAD_RATE = 'redux-example/vsRate/LOAD_RATE';
const LOAD_RATE_SUCCESS = 'redux-example/vsRate/LOAD_RATE_SUCCESS';
const LOAD_RATE_FAIL = 'redux-example/vsRate/LOAD_RATE_FAIL';
const LOAD_CASH_RATE = 'redux-example/vsRate/LOAD_CASH_RATE';
const LOAD_CASH_RATE_SUCCESS = 'redux-example/vsRate/LOAD_CASH_RATE_SUCCESS';
const LOAD_CASH_RATE_FAIL = 'redux-example/vsRate/LOAD_CASH_RATE_FAIL';

const initialState = {
  rateLoaded: false,
  rateLoading: false,
  rate: null,
  cashRate: null,
  memberID: null,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_RATE:
      return {
        ...state,
        rateLoading: true
      };
    case LOAD_RATE_SUCCESS:
      return {
        ...state,
        rateLoading: false,
        rateLoaded: true,
        rate: action.result.rate,
        error: null
      };
    case LOAD_RATE_FAIL:
      return {
        ...state,
        rateLoading: false,
        rateLoaded: false,
        rate: null,
        error: action.error
      };
    case LOAD_CASH_RATE:
      return {
        ...state,
        rateLoading: true
      };
    case LOAD_CASH_RATE_SUCCESS:
      return {
        ...state,
        rateLoading: false,
        rateLoaded: true,
        cashRate: action.result.rate,
        error: null
      };
    case LOAD_CASH_RATE_FAIL:
      return {
        ...state,
        rateLoading: false,
        rateLoaded: false,
        rate: null,
        error: action.error
      };
    default:
      return state;
  }
}

export function loadRate() {
  return {
    types: [LOAD_RATE, LOAD_RATE_SUCCESS, LOAD_RATE_FAIL],
    promise: (client) => client.get('/api/vs_config/rate')
  };
}

export function loadCashRate() {
  return {
    types: [LOAD_CASH_RATE, LOAD_CASH_RATE_SUCCESS, LOAD_CASH_RATE_FAIL],
    promise: (client) => client.get('/api/vs_frame_config/activeRate')
  };
}
