const LOAD = 'redux-example/ticketRequests/LOAD';
const LOAD_SUCCESS = 'redux-example/ticketRequests/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/ticketRequests/LOAD_FAIL';
const CHANGE_STATUS = 'redux-example/ticketRequests/UPDATE';
const CHANGE_STATUS_SUCCESS = 'redux-example/ticketRequests/UPDATE';
const CHANGE_STATUS_FAIL = 'redux-example/ticketRequests/UPDATE';
const CHANGE_PAGE = 'redux-example/ticketRequests/CHANGE_PAGE';
const CHANGE_PAGE_SUCCESS = 'redux-example/ticketRequests/CHANGE_PAGE_SUCCESS';
const CHANGE_PAGE_FAIL = 'redux-example/ticketRequests/CHANGE_PAGE_FAIL';
const CHANGE_FILTER = 'redux-example/ticketRequests/CHANGE_FILTER';
const CHANGE_FILTER_SUCCESS = 'redux-example/ticketRequests/CHANGE_FILTER_SUCCESS';
const CHANGE_FILTER_FAIL = 'redux-example/ticketRequests/CHANGE_FILTER_FAIL';
const CHANGE_SORT = 'redux-example/ticketRequests/CHANGE_SORT';
const CHANGE_SORT_SUCCESS = 'redux-example/ticketRequests/CHANGE_SORT_SUCCESS';
const CHANGE_SORT_FAIL = 'redux-example/ticketRequests/CHANGE_SORT_FAIL';
const CHANGE_SIZE_PER_PAGE = 'redux-example/ticketRequests/CHANGE_SIZE_PER_PAGE';
const CHANGE_SIZE_PER_PAGE_SUCCESS = 'redux-example/ticketRequests/CHANGE_SIZE_PER_PAGE_SUCCESS';
const CHANGE_SIZE_PER_PAGE_FAIL = 'redux-example/ticketRequests/CHANGE_SIZE_PER_PAGE_FAIL';

const initialState = {
  loaded: false,
  sizePerPage: 10,
  totalDataSize: 10,
  currentPage: 1,
  filter: '',
  sortOrder: '{"name":"createDate","order":-1}'
};


const getStateParams = (state) => ({
  page: state.currentPage,
  sort: state.sortOrder,
  filter: state.filter,
  limit: state.sizePerPage,
});

const createLoadPromise = (reqParams) => (client, getState) => {
  const state = getState().ticketRequests;
  const params = Object.assign({}, getStateParams(state), reqParams);
  return client.get('/api/ticket_requests', {params});
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        totalDataSize: action.result.count,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case CHANGE_STATUS:
      return {
        ...state,
        loading: true
      };
    case CHANGE_STATUS_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: state.data.map((item) => {
          if (item._id === action.result._id) return action.result;
          return item;
        }),
        totalDataSize: action.result.count,
        error: null
      };
    case CHANGE_STATUS_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case CHANGE_PAGE:
      return {
        ...state,
        loading: true
      };
    case CHANGE_PAGE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        error: null,
        currentPage: action.page
      };
    case CHANGE_PAGE_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case CHANGE_SIZE_PER_PAGE:
      return {
        ...state,
        loading: true
      };
    case CHANGE_SIZE_PER_PAGE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        error: null,
        sizePerPage: action.limit
      };
    case CHANGE_SIZE_PER_PAGE_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case CHANGE_FILTER:
      return {
        ...state,
        loading: true
      };
    case CHANGE_FILTER_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        totalDataSize: action.result.count,
        error: null,
        filter: action.filter
      };
    case CHANGE_FILTER_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case CHANGE_SORT:
      return {
        ...state,
        loading: true
      };
    case CHANGE_SORT_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        error: null,
        sortOrder: action.sort
      };
    case CHANGE_SORT_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.ticketRequests && globalState.ticketRequests.loaded;
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: createLoadPromise()
  };
}

export function changeStatus(item) {
  return {
    types: [CHANGE_STATUS, CHANGE_STATUS_SUCCESS, CHANGE_STATUS_FAIL],
    promise: client => client.put(`/api/ticket_requests/${item._id}/status`, {data: item})
  };
}

export function changePage(page, limit, frameId) {
  return {
    types: [CHANGE_PAGE, CHANGE_PAGE_SUCCESS, CHANGE_PAGE_FAIL],
    promise: createLoadPromise({page, frameId}),
    page
  };
}

export function changeSizePerPage(limit, frameId) {
  return {
    types: [CHANGE_SIZE_PER_PAGE, CHANGE_SIZE_PER_PAGE_SUCCESS, CHANGE_SIZE_PER_PAGE_FAIL],
    promise: createLoadPromise({limit, frameId}),
    limit
  };
}

export function changeSort(sort, frameId) {
  return {
    types: [CHANGE_SORT, CHANGE_SORT_SUCCESS, CHANGE_SORT_FAIL],
    promise: createLoadPromise({sort, frameId}),
    sort
  };
}

export function changeFilter(filter, frameId) {
  return {
    types: [CHANGE_FILTER, CHANGE_FILTER_SUCCESS, CHANGE_FILTER_FAIL],
    promise: createLoadPromise({filter, frameId}),
    filter
  };
}
