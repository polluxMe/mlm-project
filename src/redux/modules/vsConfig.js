const LOAD = 'redux-example/vs_config/LOAD';
const LOAD_SUCCESS = 'redux-example/vs_config/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/vs_config/LOAD_FAIL';
const DELETE = 'redux-example/vs_config/DELETE';
const DELETE_SUCCESS = 'redux-example/vs_config/DELETE_SUCCESS';
const DELETE_FAIL = 'redux-example/vs_config/DELETE_FAIL';
const CHANGE_PAGE = 'redux-example/vs_config/CHANGE_PAGE';
const CHANGE_PAGE_SUCCESS = 'redux-example/vs_config/CHANGE_PAGE_SUCCESS';
const CHANGE_PAGE_FAIL = 'redux-example/vs_config/CHANGE_PAGE_FAIL';
const CHANGE_FILTER = 'redux-example/vs_config/CHANGE_FILTER';
const CHANGE_FILTER_SUCCESS = 'redux-example/vs_config/CHANGE_FILTER_SUCCESS';
const CHANGE_FILTER_FAIL = 'redux-example/vs_config/CHANGE_FILTER_FAIL';
const CHANGE_SORT = 'redux-example/vs_config/CHANGE_SORT';
const CHANGE_SORT_SUCCESS = 'redux-example/vs_config/CHANGE_SORT_SUCCESS';
const CHANGE_SORT_FAIL = 'redux-example/vs_config/CHANGE_SORT_FAIL';
const CHANGE_SIZE_PER_PAGE = 'redux-example/vs_config/CHANGE_SIZE_PER_PAGE';
const CHANGE_SIZE_PER_PAGE_SUCCESS = 'redux-example/vs_config/CHANGE_SIZE_PER_PAGE_SUCCESS';
const CHANGE_SIZE_PER_PAGE_FAIL = 'redux-example/vs_config/CHANGE_SIZE_PER_PAGE_FAIL';
const EDIT_START = 'redux-example/vs_config/EDIT_START';
const EDIT_STOP = 'redux-example/vs_config/EDIT_STOP';
const SAVE = 'redux-example/vs_config/SAVE';
const SAVE_SUCCESS = 'redux-example/vs_config/SAVE_SUCCESS';
const SAVE_FAIL = 'redux-example/vs_config/SAVE_FAIL';
const SET_DATA = 'redux-example/vs_config/SET_DATA';

const initialState = {
  loaded: false,
  editing: {},
  sizePerPage: 10,
  totalDataSize: 10,
  currentPage: 1,
  filter: '',
  sortOrder: '{"name":"startDate","order":1}',
  selected: []
};


const getStateParams = (state) => ({
  page: state.currentPage,
  sort: state.sortOrder,
  filter: state.filter,
  limit: state.sizePerPage,
});

const createLoadPromise = (reqParams) => (client, getState) => {
  const state = getState().vsConfig;
  const params = Object.assign({}, getStateParams(state), reqParams);
  return client.get('/api/vs_config', {params});
};

export default function reducer(state = initialState, action = {}) {
  // console.log('action', action.type);
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        totalDataSize: action.result.count,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case CHANGE_PAGE:
      return {
        ...state,
        loading: true
      };
    case CHANGE_PAGE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        error: null,
        currentPage: action.page
      };
    case CHANGE_PAGE_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case CHANGE_SIZE_PER_PAGE:
      return {
        ...state,
        loading: true
      };
    case CHANGE_SIZE_PER_PAGE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        error: null,
        sizePerPage: action.limit
      };
    case CHANGE_SIZE_PER_PAGE_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case CHANGE_FILTER:
      return {
        ...state,
        loading: true
      };
    case CHANGE_FILTER_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        currentPage: action.page,
        error: null,
        filter: action.filter
      };
    case CHANGE_FILTER_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case CHANGE_SORT:
      return {
        ...state,
        loading: true
      };
    case CHANGE_SORT_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        error: null,
        sortOrder: action.sort
      };
    case CHANGE_SORT_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case DELETE:
      return {
        ...state,
        loading: true
      };
    case DELETE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.items,
        loadedData: true,
        error: null
      };
    case DELETE_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case EDIT_START:
      return {
        ...state,
        editing: {
          ...state.editing,
          [action.id]: true
        }
      };
    case EDIT_STOP:
      return {
        ...state,
        editing: {
          ...state.editing,
          [action.id]: false
        }
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.vsConfig && globalState.vsConfig.loaded;
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: createLoadPromise({}),
  };
}

export function changePage(page /* , limit */) {
  return {
    types: [CHANGE_PAGE, CHANGE_PAGE_SUCCESS, CHANGE_PAGE_FAIL],
    promise: createLoadPromise({page}),
    page
  };
}

export function changeSizePerPage(limit) {
  return {
    types: [CHANGE_SIZE_PER_PAGE, CHANGE_SIZE_PER_PAGE_SUCCESS, CHANGE_SIZE_PER_PAGE_FAIL],
    promise: createLoadPromise({limit}),
    limit
  };
}

export function changeSort(sort) {
  return {
    types: [CHANGE_SORT, CHANGE_SORT_SUCCESS, CHANGE_SORT_FAIL],
    promise: createLoadPromise({sort}),
    sort
  };
}

export function changeFilter(filter) {
  return {
    types: [CHANGE_FILTER, CHANGE_FILTER_SUCCESS, CHANGE_FILTER_FAIL],
    promise: createLoadPromise({filter}),
    filter
  };
}

export function deleteConfig(id) {
  console.log('delete', id);
  return {
    types: [DELETE, DELETE_SUCCESS, DELETE_FAIL],
    promise: (client) => client.del('/api/vs_config/' + id)
  };
}

export function set(data) {
  return { type: SET_DATA, data };
}

export function save(widget) {
  return {
    types: [SAVE, SAVE_SUCCESS, SAVE_FAIL],
    id: widget.id,
    promise: (client) => client.post('/api/vs_config', {
      data: widget
    })
  };
}

export function editStart(id) {
  return { type: EDIT_START, id };
}

export function editStop(id) {
  return { type: EDIT_STOP, id };
}
