/**
 * THIS IS THE ENTRY POINT FOR THE CLIENT, JUST LIKE server.js IS THE ENTRY POINT FOR THE SERVER.
 */
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import createStore from './redux/create';
import ApiClient from './helpers/ApiClient';
// import io from 'socket.io-client';
import {Provider} from 'react-redux';
import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { ReduxAsyncConnect } from 'redux-async-connect';
import useScroll from 'scroll-behavior/lib/useStandardScroll';

if (!window.Intl) {
  window.Intl = require('intl');
}

import { IntlProvider, addLocaleData } from 'react-intl';
import enLocaleData from 'react-intl/locale-data/en';
import jaLocaleData from 'react-intl/locale-data/ja';

import getRoutes from './routes';

const client = new ApiClient();
const _browserHistory = useScroll(() => browserHistory)();
const dest = document.getElementById('content');
const store = createStore(_browserHistory, client, window.__data);
const history = syncHistoryWithStore(_browserHistory, store);

// function initSocket() {
//   const socket = io('', {path: '/ws'});
//   socket.on('news', (data) => {
//     console.log(data);
//     socket.emit('my other event', { my: 'data from client' });
//   });
//   socket.on('msg', (data) => {
//     console.log(data);
//   });
//
//   return socket;
// }

// This example app only uses English. A fake `"en-UPPER"` locale is created so
// translations can be emulated.
addLocaleData(enLocaleData);
addLocaleData({
  locale: 'en-UPPER',
  parentLocale: 'en',
});
addLocaleData(jaLocaleData);

const {locale, messages} = window.App;

// global.socket = initSocket();

const component = (
  <Router render={(props) =>
        <ReduxAsyncConnect {...props} helpers={{client}} filter={item => !item.deferred} />
      } history={history} routes={getRoutes(store)} />
);

ReactDOM.render(
  <Provider store={store} key="provider">
    <IntlProvider locale={locale} messages={messages}>
      {component}
    </IntlProvider>
  </Provider>,
  dest
);

if (process.env.NODE_ENV !== 'production') {
  window.React = React; // enable debugger

  if (!dest || !dest.firstChild || !dest.firstChild.attributes || !dest.firstChild.attributes['data-react-checksum']) {
    console.error('Server-side React render was discarded. Make sure that your initial render does not contain any client-side code.');
  }
}

// if (__DEVTOOLS__ && !window.devToolsExtension) {
//   const DevTools = require('./containers/DevTools/DevTools');
//   ReactDOM.render(
//     <Provider store={store} key="provider">
//       <div>
//         <IntlProvider locale={locale} messages={messages}>
//          {component}
//         </IntlProvider>
//         <DevTools />
//       </div>
//     </Provider>,
//     dest
//   );
// }
