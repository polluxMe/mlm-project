const isEmpty = value => value === undefined || value === null || value === '';
const join = (rules) => (value, data) => rules.map(rule => rule(value, data)).filter(error => !!error)[0 /* first error */ ];

export function email(value) {
  // Let's not start a debate on email regex. This is just for an example app!
  if (!isEmpty(value) && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    return 'Invalid email address';
  }
}

export function requiredCash(value, data) {
  if (data.purchaseType === 'cash' && isEmpty(value)) {
    return '必須';
  }
}

export function requiredACTTransfer(value, data) {
  if (data.purchaseType === 'ACTTransfer' && isEmpty(value)) {
    return '必須';
  }
}

export function requiredBitcoin(value, data) {
  if (data.purchaseType === 'bitcoin' && isEmpty(value)) {
    return '必須';
  }
}

export function required(value) {
  if (isEmpty(value)) {
    return '必須';
  }
}

export function requiredAnd(predicate) {
  return (value, data) => {
    if (predicate(data)) {
      required(value);
    }
  };
}

export function minLength(min) {
  return value => {
    if (!isEmpty(value) && value.length < min) {
      return `Must be at least ${min} characters`;
    }
  };
}

export function maxLength(max) {
  return value => {
    if (!isEmpty(value) && value.length > max) {
      return `Must be no more than ${max} characters`;
    }
  };
}

export function integer(value) {
  if (!Number.isInteger(Number(value))) {
    return 'Must be an integer';
  }
}

export function minValue(min) {
  return value => {
    if (isEmpty(value) || value < min) {
      return `${min}以上は必須です。`;
    }
  };
}
export function maxValue(max) {
  return value => {
    if (isEmpty(value) || value > max) {
      return `${max}以下は必須です。`;
    }
  };
}

export function oneOf(enumeration) {
  return value => {
    if (!~enumeration.indexOf(value)) {
      return `Must be one of: ${enumeration.join(', ')}`;
    }
  };
}

export function match(field) {
  return (value, data) => {
    if (data) {
      if (value !== data[field]) {
        return 'Do not match';
      }
    }
  };
}

export function createValidator(rules) {
  return (data = {}) => {
    const errors = {};
    Object.keys(rules).forEach((key) => {
      const rule = join([].concat(rules[key])); // concat enables both functions and arrays of functions
      const error = rule(data[key], data);
      if (error) {
        errors[key] = error;
      }
    });
    return errors;
  };
}

export function parseServerValidationError(serverError) {
  return Object.keys(serverError).reduce((errors, serverErrorKey) => {
    errors[serverErrorKey] = '検証に失敗しました';
    return errors;
  }, {});
}
