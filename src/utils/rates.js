const rates = {
  ticketPurchaseAmounts: [100, 1000, 2000, 4000, 8000, 16000, 32000, 64000, 128000, 256000],
  ticketCashAmounts: [555, 5555, 11111, 22222, 44444, 88888, 177777, 355555, 711111, 1422222],
  ticketVSAmounts: [1300, 13000, 260000, 520000, 1040000, 2080000, 4160000, 8320000, 16640000, 33280000],
  getTicketPurchaseAmount(rank) {
    return this.ticketPurchaseAmounts[rank - 1];
  },
  getTicketVSAmount(rank) {
    return this.ticketVSAmounts[rank - 1];
  },
  getTicketCashAmount(rank) {
    return this.ticketCashAmounts[rank - 1];
  }
};

export default rates;
