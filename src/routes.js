import { isLoaded as isAuthLoaded, load as loadAuth } from 'redux/modules/auth';
import {
    App,
    // Home,
    VSPurchases,
    VSPurchasesAdd,
    About,
    LoginSuccess,
    Login,
    NotFound,
    MyAccount,
    Ticket,
    BraveRegistration,
    GCloudRegistration,
    Brave1Way,
    Brave1WayTickets,
    Brave1WayHistory,
    Brave2Way,
    Brave2WayHistory,
    Brave3Way,
    Brave3WayTickets,
    Brave3WayHistory,
    VirtualStock,
    Orders,
    Map,
    AddMapMember,
    Commission,
    Report,
    RegisterCompletion,
    ChangeID,
    TempLogin,
    VSConfig,
    VSConfigForm,
    VSConfigFrame,
    VSConfigFrameForm,
    MyPage,
    Invitation,
    InvitationSent,
    vsConfigFrameDetails,
    VSConfigFrameView,
    AdminMap,
    AdminAddMapMember,
    MoneyWithdraw,
    AdminMemberList,
    MainCommissionRequests,
    WithdrawRequests,
    MatchingBonusRequests,
    CashRewardRequests,
    AdminMemberListCsv,
    MainConfig,
    WithdrawalConfig,
    WithdrawalConfigForm,
    VSPurchaseProcess,
    AdminMemberDetails,
    CashRewardsRequestDetails,
    MainCommissionRequestDetails,
    MatchingBonusRequestDetails,
    UnilevelMap,
    AdminUnilevelMap,
    EditAccountInfo,
    TicketRequests,
    TicketRequestDetails,
    Brave1WayRequests,
    Brave2WayRequests,
    Brave3WayRequests,
    MemberRankHistory,
    ForgetPassword,
    ChangePassword,
    Commissions,
    RankCommission,
    MegaCommission,
    CashRewardCommission
} from 'containers';

export default (store) => {
  const requireLogin = (nextState, replace, cb) => {
    function checkAuth() {
      const { auth: { user }} = store.getState();
      if (!user) {
        // oops, not logged in, so can't be here!
        replace('/');
      }
      cb();
    }

    if (!isAuthLoaded(store.getState())) {
      store.dispatch(loadAuth()).then(checkAuth);
    } else {
      checkAuth();
    }
  };

  const requireOfficialStatus = (nextState, replace, cb) => {
    function checkAuth() {
      const { auth: { user }} = store.getState();

      if (!user || !user.isChangeId || !user.status || user.status !== 'official') {
        // oops, not logged in, so can't be here!
        replace('/');
      }
      cb();
    }

    if (!isAuthLoaded(store.getState())) {
      store.dispatch(loadAuth()).then(checkAuth);
    } else {
      checkAuth();
    }
  };

  const requireAdmin = (nextState, replace, cb) => {
    function checkAuth() {
      const { auth: { user }} = store.getState();

      if (!user || !user.status || user.status !== 'official' || !user.isAdmin) {
        // oops, not logged in, so can't be here!
        replace('/');
      }
      cb();
    }

    if (!isAuthLoaded(store.getState())) {
      store.dispatch(loadAuth()).then(checkAuth);
    } else {
      checkAuth();
    }
  };

  return [
    {
      path: '/',
      component: App,
      indexRoute: {component: Login},
      childRoutes: [
        {
          path: 'about',
          component: About,
        },
        {
          onEnter: requireLogin,
          childRoutes: [
            {
              path: 'loginSuccess',
              component: LoginSuccess,
            },
            {
              path: 'my_page',
              component: MyPage,
            },
          ]
        },
        {
          path: 'registration/brave',
          component: BraveRegistration,
        },
        {
          path: 'registration/gcloud',
          component: GCloudRegistration,
        },
        {
          path: 'registration/completion',
          component: RegisterCompletion,
        },
        {
          path: 'registration/temp_login',
          component: TempLogin,
        },
        {
          path: 'registration/change_id',
          component: ChangeID,
        },
        {
          path: 'registration/forget_password',
          component: ForgetPassword,
        },
        {
          path: 'registration/change_password',
          component: ChangePassword,
        },
        {
          onEnter: requireOfficialStatus,
          childRoutes: [
            {
              path: 'my_account',
              component: MyAccount,
            },
            {
              path: 'my_account/edit',
              component: EditAccountInfo,
            },
            {
              path: 'my_account/ticket',
              component: Ticket,
            },
            {
              path: 'my_account/brave1way',
              component: Brave1Way,
            },
            {
              path: 'my_account/brave1way/tickets',
              component: Brave1WayTickets,
            },
            {
              path: 'my_account/brave1way/history',
              component: Brave1WayHistory,
            },
            {
              path: 'my_account/brave2way',
              component: Brave2Way,
            },
            {
              path: 'my_account/brave2way/history',
              component: Brave2WayHistory,
            },
            {
              path: 'my_account/brave3way',
              component: Brave3Way,
            },
            {
              path: 'my_account/brave3way/tickets',
              component: Brave3WayTickets,
            },
            {
              path: 'my_account/brave3way/history',
              component: Brave3WayHistory,
            },
            {
              path: 'my_account/virtual_stock',
              component: VirtualStock,
            },
            {
              path: 'my_account/virtual_stock/orders',
              component: Orders,
            },
            {
              path: 'my_account/commissions',
              component: Commissions,
            },
            {
              path: 'my_account/commissions/main_commission',
              component: Commission,
            },
            {
              path: 'my_account/commissions/rank_commission',
              component: RankCommission,
            },
            {
              path: 'my_account/commissions/mega_commission',
              component: MegaCommission,
            },
            {
              path: 'my_account/commissions/cash_reward_commission',
              component: CashRewardCommission,
            },
            {
              path: 'report',
              component: Report,
            },
            {
              path: 'invitation',
              component: Invitation,
            },
            {
              path: 'invitation/sent',
              component: InvitationSent,
            },
            {
              path: 'my_account/commission/map',
              component: Map,
            },
            {
              path: 'my_account/commission/unilevelMap',
              component: UnilevelMap,
            },
            {
              path: 'my_account/commission/map/add_member',
              component: AddMapMember,
            },
            {
              path: 'my_account/withdrawal',
              component: MoneyWithdraw
            },
          ]
        },
        {
          onEnter: requireAdmin,
          childRoutes: [
            {
              path: 'vs_purchases',
              component: VSPurchases,
            },
            {
              path: 'commission_requests',
              component: MainCommissionRequests,
            },
            {
              path: 'commission_requests/:id',
              component: MainCommissionRequestDetails,
            },
            {
              path: 'matching_bonus_requests',
              component: MatchingBonusRequests,
            },
            {
              path: 'matching_bonus_requests/:id',
              component: MatchingBonusRequestDetails,
            },
            {
              path: 'cash_reward_requests',
              component: CashRewardRequests,
            },
            {
              path: 'cash_reward_requests/:id',
              component: CashRewardsRequestDetails,
            },
            {
              path: 'withdraw_requests',
              component: WithdrawRequests,
            },
            {
              path: 'vs_purchases/add/:memberID',
              component: VSPurchasesAdd,
            },
            {
              path: 'vs_purchases/process/:id',
              component: VSPurchaseProcess,
            },
            {
              path: 'vs_config',
              component: VSConfig,
            },
            {
              path: 'main_config',
              component: MainConfig,
            },
            {
              path: 'vs_config/form',
              component: VSConfigForm,
            },
            {
              path: 'vs_config/form/:id',
              component: VSConfigForm,
            },
            {
              path: 'vs_config_frame',
              component: VSConfigFrame,
            },
            {
              path: 'vs_config_frame/form',
              component: VSConfigFrameForm,
            },
            {
              path: 'vs_config_frame/form/:id',
              component: VSConfigFrameForm,
            },
            {
              path: 'vs_config_frame/details/:id',
              component: vsConfigFrameDetails,
            },
            {
              path: 'vs_config_frame/details/:id/history',
              component: VSConfigFrameView,
            },
            {
              path: 'withdrawal_config',
              component: WithdrawalConfig,
            },
            {
              path: 'withdrawal_config/form',
              component: WithdrawalConfigForm,
            },
            {
              path: 'withdrawal_config/form/:id',
              component: WithdrawalConfigForm,
            },
            {
              path: 'map',
              component: AdminMap,
            },
            {
              path: 'unilevelMap',
              component: AdminUnilevelMap,
            },
            {
              path: 'map/add_member',
              component: AdminAddMapMember,
            },
            {
              path: 'members',
              component: AdminMemberList
            },
            {
              path: 'members/import_csv',
              component: AdminMemberListCsv
            },
            {
              path: 'members/:id',
              component: AdminMemberDetails
            },
            {
              path: 'members/:id/rank_history',
              component: MemberRankHistory
            },
            {
              path: 'tickets',
              component: TicketRequests
            },
            {
              path: 'tickets/:id',
              component: TicketRequestDetails,
            },
            {
              path: 'brave1way_requests',
              component: Brave1WayRequests
            },
            {
              path: 'brave2way_requests',
              component: Brave2WayRequests
            },
            {
              path: 'brave3way_requests',
              component: Brave3WayRequests
            }
          ]
        },
        {
          path: 'login',
          component: Login,
        },
        {
          path: '*',
          component: NotFound,
          status: 404
        }
      ]
    }
  ];
};
