import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Helmet from 'react-helmet';
import { Board } from 'components';
import {loadOrders, cancelPurchase} from 'redux/modules/orders';
import moment from 'moment';
import {Button} from 'react-bootstrap';
import { browserHistory } from 'react-router';
import {FormattedNumber, FormattedMessage} from 'react-intl';

const stockBtnCansul = require('theme/mlm/images/stock-btn-cansul.png');

@connect(
  state => ({
    user: state.auth.user,
    orders: state.orders.data,
    loaded: state.orders.loaded
  }),
  dispatch => bindActionCreators({loadOrders, cancelPurchase}, dispatch)
)
export default class Orders extends Component {
  static propTypes = {
    params: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    orders: PropTypes.array,
    loaded: PropTypes.bool.isRequired,
    loadOrders: PropTypes.func.isRequired,
    cancelPurchase: PropTypes.func.isRequired
  };

  componentDidMount() {
    const memberID = this.props.user && this.props.user._id;
    this.props.loadOrders(memberID);
  }

  render() {
    const {orders, loaded} = this.props;

    const styles = require('./Orders.scss');

    return (
      <div id="wrap">
        <Helmet title="Orders"/>
        <div className="stock" id="contents">
          <Board>
            <div className={styles.background}>
              <h3>未成立買い注文</h3>
              <div className={styles.inner}>
                {
                  loaded && orders && orders.filter(item =>
                    item.status === 'created' ||
                    item.status === 'btc_order' ||
                    item.status === 'atc_order' ||
                    item.status === 'cash_order'
                  ).map(item =>
                  <div className={styles.order} key={item._id}>
                    <div className="date">
                      {moment(item.createDate).format('YYYY/M/D')}
                    </div>
                    <table className="kai">
                      <tbody>
                      <tr>
                        <th>購入額</th>
                        <td>
                          <FormattedNumber value={item.purchaseAmount || 0} />円
                        </td>
                      </tr>
                      <tr>
                        <th>注文時レート</th>
                        <td>{item.purchaseRate || item.purchaseRateCreated} 円/B$</td>
                      </tr>
                      <tr>
                        <th>購入予定数</th>
                        <td>{item.purchaseQuantityCreated} VS</td>
                      </tr>
                      </tbody>
                    </table>

                    <div className="tac mt15 mb15">
                      <Button
                        onClick={() => confirm('キャンセルして宜しいですか？') && this.props.cancelPurchase(item.masterID) && browserHistory.push('/my_account/virtual_stock/')}
                        className={styles.button}
                      >
                        <input type="image" src={stockBtnCansul} width="117" alt="キャンセル" />
                      </Button>
                    </div>
                  </div>
                  )
                }
                {!orders && (
                  <h3>
                    <FormattedMessage
                      id="OrdersList.Empty"
                      defaultMessage="You dont have orders yet"
                    />
                  </h3>)}
              </div>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}


