import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import * as brave3WayHistoryActions from 'redux/modules/brave3WayHistory';
import Helmet from 'react-helmet';
import { Board } from 'components';
import {bindActionCreators} from 'redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import {FormattedMessage, FormattedNumber} from 'react-intl';
import moment from 'moment';

// require('../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css');

const brave3WayTitleImage = require('theme/mlm/images/3way-title.png');

@connect(state => ({
  user: state.auth.user,
  requests: state.brave3WayHistory.data,
  error: state.brave3WayHistory.error,
  loading: state.brave3WayHistory.loading,
  loaded: state.brave3WayHistory.loaded,
  totalDataSize: state.brave3WayHistory.totalDataSize,
  currentPage: state.brave3WayHistory.currentPage,
  sizePerPage: state.brave3WayHistory.sizePerPage,
}),
  dispatch => bindActionCreators({...brave3WayHistoryActions}, dispatch)
)
export default class Brave3WayHistory extends Component {
  static propTypes = {
    user: PropTypes.objrect,
    wallet: PropTypes.object,
    requests: PropTypes.array,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    load: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    totalDataSize: PropTypes.number,
    sizePerPage: PropTypes.number,
  };

  componentWillMount() {
    this.props.load();
  }

  render() {
    function dateFormatter(cell) {
      return moment(cell).format('YYYY/MM/DD');
    }

    function numberFormatter(cell) {
      return (<span><FormattedNumber value={cell}/>&nbsp;B$</span>);
    }


    return (
      <div id="wrap">
        <Helmet title="Brave 3 way"/>
        <div className="three-way" id="contents">
          <Board>
            <h2 className="mb10"><img src={brave3WayTitleImage} alt="BRAVE$ 3Way"/></h2>
            <BootstrapTable
              data={this.props.requests || []}
              remote
              hover
              condensed
              pagination
              fetchInfo={ { dataTotalSize: this.props.totalDataSize } }
              options={ { sizePerPage: this.props.sizePerPage,
                onPageChange: this.onPageChange,
                sizePerPageList: [ 5, 10, 20],
                pageStartIndex: 1,
                page: this.props.currentPage,
                // onSearchChange: this.onSearchChange,
                onSortChange: this.onSortChange,
                // onFilterChange: this.onFilterChange,
                onSizePerPageList: this.onSizePerPageList,
                hideSizePerPage: true,
                noDataText: '表示されるデータがございません。'
              }}
            >
              <TableHeaderColumn dataField="createDate" isKey dataFormat={dateFormatter} dataAlign="center" editable={false}>
                <FormattedMessage
                  id="Brave3WayHistory.CreationDate"
                  defaultMessage="Creation date"
                />
              </TableHeaderColumn>
              <TableHeaderColumn dataField="status" dataAlign="center" editable={false}>
                <FormattedMessage
                  id="Brave3WayHistory.Status"
                  defaultMessage="Status"
                />
              </TableHeaderColumn>
              <TableHeaderColumn dataField="brave3WayPurchaseAmount" dataFormat={numberFormatter} dataAlign="center" editable={false}>
                <FormattedMessage
                  id="Brave3WayHistory.brave3WayPurchaseAmount"
                  defaultMessage="Purchase amount"
                />
              </TableHeaderColumn>
            </BootstrapTable>
          </Board>
        </div>
      </div>
    );
  }
}

