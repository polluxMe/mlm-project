import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import { VSConfigAdd } from 'components';
import { browserHistory } from 'react-router';
import { initialize } from 'redux-form';
import {FormattedMessage} from 'react-intl';

@connect(
  () => ({}),
  {
    initialize
  })
export default class VSConfigForm extends Component {
  static propTypes = {
    initialize: PropTypes.func.isRequired
  }

  componentWillMount() {
    this.handleInitialize();
  }

  handleSubmit = () => {
    console.log('handle');
    browserHistory.push('/vs_config');
  }

  handleInitialize = () => {
    this.props.initialize('VSConfigForm', {

    });
  }

  render() {
    return (
      <div className="container">
        <h1>
          <FormattedMessage
            id="VSConfigForm.Header"
            defaultMessage="VS configuration form"
          />
        </h1>
        <Helmet title="VSConfig"/>
        <VSConfigAdd {...this.props} onSubmit={this.handleSubmit}/>
      </div>
    );
  }
}
