import React, {Component, PropTypes} from 'react';
// import Helmet from 'react-helmet';
import {connect} from 'react-redux';
import * as vsConfigActions from 'redux/modules/vsConfig';
// import * as purchasesActions from 'redux/modules/vsPurchases';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
// import { WidgetForm } from 'components';
import {FormattedMessage} from 'react-intl';
import { push } from 'react-router-redux';
import { Link } from 'react-router';

require('../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css');

@connect(
  state => ({
    vsConfig: state.vsConfig.data,
    editing: state.vsConfig.editing,
    error: state.vsConfig.error,
    loading: state.vsConfig.loading,
    loaded: state.vsConfig.loaded,
    totalDataSize: state.vsConfig.totalDataSize,
    currentPage: state.vsConfig.currentPage,
    sizePerPage: state.vsConfig.sizePerPage,
    pager: state.vsConfig.pager,
    onSizePerPageList: state.vsConfig.onSizePerPageList,
    search: state.vsConfig.search,
    sorter: state.vsConfig.sorter,
    selected: state.vsConfig.selected
  }),
  {...vsConfigActions, pushState: push })
export default class VSConfig extends Component {
  static propTypes = {
    vsConfig: PropTypes.array,
    error: PropTypes.string,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    editing: PropTypes.object.isRequired,
    editStart: PropTypes.func.isRequired,
    set: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    totalDataSize: PropTypes.number,
    sizePerPage: PropTypes.number,
    changeSort: PropTypes.func.isRequired,
    changePage: PropTypes.func.isRequired,
    changeSizePerPage: PropTypes.func.isRequired,
    changeFilter: PropTypes.func.isRequired,
    pushState: PropTypes.func.isRequired,
    selected: PropTypes.array,
    deleteConfig: PropTypes.func.isRequired,
    load: PropTypes.func
  };

  componentWillMount() {
    this.props.load();
  }

  onSortChange = (name, order) => {
    const sortOrder = {name: name, order: (order === 'asc') ? 1 : -1};
    this.props.changeSort(JSON.stringify(sortOrder));
  };

  onPageChange = (page, limit) => {
    this.props.changePage(page, limit);
  };

  onAddRow = () => {
    this.props.pushState('/vs_config/form');
  }
  onFilterChange = (filterObj) => {
    this.props.changeFilter(JSON.stringify(filterObj));
  };

  onSizePerPageList = (sizePerPage) => {
    this.props.changeSizePerPage(sizePerPage);
  };

  render() {
    const now = new Date();

    function isActive(config) {
      if (new Date(config.startDate) < now && new Date(config.endDate) > now) return true;
      return false;
    }

    function dateFormatter(cell) {
      const date = new Date(cell);
      return ('0' + date.getDate()).slice(-2) + '-' +
        ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
        date.getFullYear();
    }

    const remove = (_id) => {
      this.props.deleteConfig(_id);
    };

    const editFormatter = (cell, row) => {
      if (isActive(row)) {
        return null;
      }
      return (
        <Link to={'/vs_config/form/' + cell}>
          <FormattedMessage
            id="VSConfig.Edit"
            defaultMessage="Edit"
          />
        </Link>
      );
    };

    function deleteFormatter(cell, row) {
      if (isActive(row)) {
        return null;
      }
      return (
        <a href="#" onClick={ () => remove(cell) }>
          <FormattedMessage
            id="VSConfig.Delete"
            defaultMessage="Delete"
          />
        </a>
      );
    }

    let result;
    // console.log(this.props.vsConfig);
    /* ReactDataGrid uses the document object, which is not available on server side */
    result = (
        <div>
          <div className="container">
          <h2>
            <FormattedMessage
              id="VSConfig.Header"
              defaultMessage="Purchase rate setting"
            />
          </h2>

          <div className="row">
            <div className="col-xs-12 col-sm-6 col-md-6 col-lg-8">
              <div className="btn-group btn-group-sm">
                <button type="button" onClick={this.onAddRow} className="btn btn-success react-bs-table-add-btn">
                  <i className="glyphicon glyphicon-plus"></i>
                  <span>
                      <FormattedMessage
                        id="VSConfig.Add"
                        defaultMessage="Add new"
                      />
                  </span>
                </button>
              </div>
            </div>
          </div>
          </div>
          <BootstrapTable
            data={this.props.vsConfig || []}
            remote
            hover
            condensed
            pagination
            fetchInfo={ { dataTotalSize: this.props.totalDataSize } }
            options={ { sizePerPage: this.props.sizePerPage,
                        onPageChange: this.onPageChange,
                        sizePerPageList: [ 5, 10, 20],
                        pageStartIndex: 1,
                        page: this.props.currentPage,
                        onSortChange: this.onSortChange,
                        onSizePerPageList: this.onSizePerPageList,
                        hideSizePerPage: true,
                        noDataText: '表示されるデータがございません。'
                       }
                    }
          >
            <TableHeaderColumn dataField="startDate" isKey key dataSort dataFormat={dateFormatter} dataAlign="center">
              <FormattedMessage
                id="VSConfig.StartDate"
                defaultMessage="Start date"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="endDate" dataSort dataFormat={dateFormatter} dataAlign="center">
              <FormattedMessage
                id="VSConfig.EndDate"
                defaultMessage="End date"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="purchaseRate" dataSort dataAlign="center">
              <FormattedMessage
                id="VSConfig.PurchaseRate"
                defaultMessage="Purchase rate"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="status" dataAlign="center">
              <FormattedMessage
                id="VSConfig.Status"
                defaultMessage="Status"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="_id" dataFormat={ editFormatter }>
              <FormattedMessage
                id="VSConfig.Edit"
                defaultMessage="Edit"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="_id" dataFormat={ deleteFormatter }>
              <FormattedMessage
                id="VSConfig.Delete"
                defaultMessage="Delete"
              />
            </TableHeaderColumn>
          </BootstrapTable>
        </div>
    );

    return result;
  }
}

