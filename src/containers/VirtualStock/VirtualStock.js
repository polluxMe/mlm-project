import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {reduxForm, Field} from 'redux-form';
import VirtualStockFormValidation from './VirtualStockFormValidation';
import {Form, FormControl, InputGroup, Button} from 'react-bootstrap';
import {loadRate, loadCashRate} from 'redux/modules/vsRate';
import Helmet from 'react-helmet';
import { Board } from 'components';
import { Link } from 'react-router';
import config from '../../config.js';
import { Spinner } from 'components';

const Input = ({type, placeholder, input, readOnly, suffix, min, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <InputGroup>
        <FormControl type={type} {...input} placeholder={placeholder} readOnly={readOnly} min={min} />
        {suffix && <InputGroup.Addon>{suffix}</InputGroup.Addon>}
      </InputGroup>
    </div>
    {!readOnly && error && touched && <div className="text-danger">{error}</div>}
  </div>;

const Radio = ({input, radioValue, children}) =>
  <label>
    <input type="radio" {...input} value={radioValue}/>&nbsp;{children}
  </label>;

const stockTitleImage = require('theme/mlm/images/stock-title.png');
const stockButtonKeisanImage = require('theme/mlm/images/stock-btn-keisan.png');
const stockButtonKaiImage = require('theme/mlm/images/stock-btn-kai.png');
const stockButtonMiseirituImage = require('theme/mlm/images/stock-btn-miseiritu.png');

@reduxForm({
  form: 'virtualStock',
  validate: VirtualStockFormValidation,
  enableReinitialize: true
})
@connect(
  state => ({
    form: state.form.virtualStock,
    purchaseRate: state.vsRate.rate,
    cashRate: state.vsRate.cashRate,
    user: state.auth.user
  }),
  dispatch => bindActionCreators({loadRate, loadCashRate}, dispatch)
)
export default class VirtualStock extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    error: PropTypes.string,
    valid: PropTypes.bool,
    pristine: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
    form: PropTypes.object.isRequired,
    purchaseRate: PropTypes.number,
    cashRate: PropTypes.number,
    onSubmit: PropTypes.func,
    loadRate: PropTypes.func.isRequired,
    loadCashRate: PropTypes.func.isRequired,
    user: PropTypes.object,
    initialize: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {orderListEnabled: false};
  }

  componentWillMount() {
    this.props.loadRate();
    this.props.loadCashRate();
  }

  onCalculate = () => {
    const amount = this.getPurchaseAmount();
    const quantity = this.calculatePurchaseQuantity(amount);

    if (quantity && amount) {
      this.props.initialize({
        ...this.props.form.values,
        purchaseQuantity: quantity
      });
    }
  };

  getPurchaseAmount = () => this.props.form.values && this.props.form.values.purchaseAmount || 0;

  calculatePurchaseQuantity = (amount) => {
    return Math.floor(amount / this.props.purchaseRate / this.props.cashRate);
  };

  submitPurchase = (data) => {
    data.purchaseRate = this.props.purchaseRate;
    data.cashRate = this.props.cashRate;
    // data.purchaseQuantity = this.state.purchaseQuantity;
    data.memberID = this.props.user._id;

    const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);
    return fetch(apiPath + '/api/vs_purchase', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({...data})
    })
      .then((response) => response.json())
      .then(() => {
        this.setState({orderListEnabled: true});
        this.props.initialize({});
        alert('買い注文を受け付けました。');
        // browserHistory.push('/my_account/virtual_stock/orders/' + data.memberID);
      })
      .catch(error => {
        console.error(error);
      });
  };

  render() {
    const styles = require('./VirtualStock.scss');

    const { handleSubmit, valid, error, cashRate, purchaseRate, submitting } = this.props;

    return (
      <div id="wrap">
        <Helmet title="Virtual Stock"/>
        <div className="stock" id="contents">
          <Form horizontal>
            <Board>
              <h2 className="mb05"><img src={stockTitleImage} alt="バーチャルストック"/></h2>
                <div className="box">
                  <h3>VS購入</h3>
                  <div className="inner">
                    <dl>
                      <dt>現在のレート</dt>
                      <dd>1VS={cashRate}c</dd>
                    </dl>

                    <table>
                      <tbody>
                      <tr>
                        <th>購入通貨</th>
                        <td>
                          <Field name="purchaseType" component={Radio} radioValue="cash">円</Field>&nbsp;
                          <Field name="purchaseType" component={Radio} radioValue="BTC">Bitcoin</Field>&nbsp;
                          <Field name="purchaseType" component={Radio} radioValue="ATC">ATC</Field>
                        </td>
                      </tr>
                      <tr>
                        <th className="no-border">購入額</th>
                        <td className="no-border">
                          <Field name="purchaseAmount" type="number" component={Input} min={1} suffix="円" />
                        </td>
                      </tr>
                      </tbody>
                    </table>
                    <div>※購入レート：BS$={purchaseRate}円</div>

                    <div className="tac mt10 mb10">
                      <Button
                        onClick={this.onCalculate}
                        className={styles.button}
                      >
                        <img src={stockButtonKeisanImage} width="117" alt="計算" />
                      </Button>
                    </div>

                    <table>
                      <tbody>
                      <tr>
                        <th>購入予定数</th>
                        <td className="tar">
                          <Field name="purchaseQuantity" type="number" component={Input} readOnly min={1} suffix="VS" />
                        </td>
                      </tr>
                      </tbody>
                    </table>
                    <p className="tac">
                      ※入金確認時点での設定レートにより実際の購入数は変動します。
                    </p>

                    {error && <div className="text-danger">{error}</div>}

                    <div className="tac mt10 mb10">
                      <Button
                        disabled={!valid || submitting}
                        onClick={handleSubmit(this.submitPurchase)}
                        className={styles.button}
                      >
                        <Spinner loading={submitting} size={25} exclusive>
                          <img src={stockButtonKaiImage} width="100%" alt="買い注文"/>
                        </Spinner>
                      </Button>
                    </div>

                    <div className="tac mt10">
                      <Link to="/my_account/virtual_stock/orders/">
                        <Button
                          className={styles.button}
                        >
                          <img src={stockButtonMiseirituImage} width="157" alt="未成立買い注文"/>
                        </Button>
                      </Link>
                    </div>
                  </div>
                </div>
            </Board>
          </Form>
        </div>
      </div>
    );
  }
}

