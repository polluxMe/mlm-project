import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import * as brave1WayHistoryActions from 'redux/modules/brave1WayHistory';
import Helmet from 'react-helmet';
import { Board } from 'components';
import {bindActionCreators} from 'redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import {FormattedMessage, FormattedNumber} from 'react-intl';
import moment from 'moment';

// require('../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css');

const brave1WayTitleImage = require('theme/mlm/images/1way-title.png');

@connect(state => ({
  user: state.auth.user,
  requests: state.brave1WayHistory.data,
  error: state.brave1WayHistory.error,
  loading: state.brave1WayHistory.loading,
  loaded: state.brave1WayHistory.loaded,
  totalDataSize: state.brave1WayHistory.totalDataSize,
  currentPage: state.brave1WayHistory.currentPage,
  sizePerPage: state.brave1WayHistory.sizePerPage,
}),
  dispatch => bindActionCreators({...brave1WayHistoryActions}, dispatch)
)
export default class Brave1WayHistory extends Component {
  static propTypes = {
    user: PropTypes.objrect,
    wallet: PropTypes.object,
    requests: PropTypes.array,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    load: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    totalDataSize: PropTypes.number,
    sizePerPage: PropTypes.number,
  };

  componentWillMount() {
    this.props.load();
  }

  render() {
    function dateFormatter(cell) {
      return moment(cell).format('YYYY/MM/DD');
    }

    function numberFormatter(cell) {
      return (<span><FormattedNumber value={cell}/>&nbsp;B$</span>);
    }


    return (
      <div id="wrap">
        <Helmet title="Brave 1 way"/>
        <div className="one-way" id="contents">
          <Board>
            <h2 className="mb10"><img src={brave1WayTitleImage} alt="BRAVE$ 1WAY"/></h2>
            <BootstrapTable
              data={this.props.requests || []}
              remote
              hover
              condensed
              pagination
              fetchInfo={ { dataTotalSize: this.props.totalDataSize } }
              options={ { sizePerPage: this.props.sizePerPage,
                onPageChange: this.onPageChange,
                sizePerPageList: [ 5, 10, 20],
                pageStartIndex: 1,
                page: this.props.currentPage,
                // onSearchChange: this.onSearchChange,
                onSortChange: this.onSortChange,
                // onFilterChange: this.onFilterChange,
                onSizePerPageList: this.onSizePerPageList,
                hideSizePerPage: true,
                noDataText: '表示されるデータがございません。'
              }}
            >
              <TableHeaderColumn dataField="createDate" isKey dataFormat={dateFormatter} dataAlign="center" editable={false}>
                <FormattedMessage
                  id="Brave1WayHistory.CreationDate"
                  defaultMessage="Creation date"
                />
              </TableHeaderColumn>
              <TableHeaderColumn dataField="status" dataAlign="center" editable={false}>
                <FormattedMessage
                  id="Brave1WayHistory.Status"
                  defaultMessage="Status"
                />
              </TableHeaderColumn>
              <TableHeaderColumn dataField="brave1WayPurchaseAmount" dataFormat={numberFormatter} dataAlign="center" editable={false}>
                <FormattedMessage
                  id="Brave1WayHistory.brave1WayPurchaseAmount"
                  defaultMessage="Purchase amount"
                />
              </TableHeaderColumn>
            </BootstrapTable>
          </Board>
        </div>
      </div>
    );
  }
}

