import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {injectIntl, intlShape, defineMessages, FormattedMessage, FormattedNumber} from 'react-intl';
import { load as loadPurchase, update, cancel } from 'redux/modules/vsPurchaseProcess';
import { load as loadPurchases } from 'redux/modules/vsPurchases';
import { loadRate, loadCashRate} from 'redux/modules/vsRate';
import {Grid, Col, Row, FormGroup, ControlLabel, FormControl, Button} from 'react-bootstrap';
import {push} from 'react-router-redux';
import { Spinner } from 'components';
// import { asyncConnect } from 'redux-async-connect';

const styles = require('./VSPurchaseProcess.scss');

const messages = defineMessages({
  AreYouSureYouWantToCancelPurchase: {
    id: 'VSPurchaseProcess.AreYouSureYouWantToCancelPurchase',
    defaultMessage: 'Are you sure you want to cancel purchase?'
  }
});

// @asyncConnect([
//   {
//     deferred: true,
//     promise: ({store: {dispatch}, params: {id}}) => {
//       return dispatch(loadPurchase(id));
//     }
//   },
//   {
//     deferred: true,
//     promise: ({store: {dispatch}}) => {
//       return dispatch(loadRate());
//     }
//   },
//   {
//     deferred: true,
//     promise: ({store: {dispatch}}) => {
//       return dispatch(loadCashRate());
//     }
//   }
// ])
@connect(state => ({
  purchase: state.vsPurchaseProcess.purchase,
  canceling: state.vsPurchaseProcess.canceling,
  updating: state.vsPurchaseProcess.updating,
  error: state.vsPurchaseProcess.error,
  cashRate: state.vsRate.cashRate,
  cashPurchaseRate: 0,
  rate: state.vsRate.rate
}),
  {pushState: push, cancel, update, loadPurchase, loadRate, loadCashRate, loadPurchases})
class VSPurchaseProcess extends Component {
  static propTypes = {
    purchase: PropTypes.object,
    params: PropTypes.object,
    load: PropTypes.func,
    cashRate: PropTypes.number,
    cashPurchaseRate: PropTypes.number,
    rate: PropTypes.number,
    update: PropTypes.func,
    cancel: PropTypes.func,
    pushState: PropTypes.func,
    loadRate: PropTypes.func,
    loadCashRate: PropTypes.func,
    loadPurchases: PropTypes.func,
    loadPurchase: PropTypes.func,
    intl: intlShape.isRequired,
    canceling: PropTypes.bool,
    updating: PropTypes.bool,
    error: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {amount: 0, cashRate: 0, cashPurchaseRate: 0};
  }

  componentWillMount() {
    Promise.all([
      this.props.loadPurchase(this.props.params.id),
      this.props.loadRate(),
      this.props.loadCashRate(),
    ]).then(results => this.setState({amount: results[0].purchaseAmount, cashRate: results[0].cashRateCreated, cashPurchaseRate: results[2].rate}));
  }

  onCashRateChange = (event) => {
    if (event.target.value < 0) {
      return;
    }
    this.setState({cashPurchaseRate: event.target.value});
  }

  onQuantityChange = (event) => {
    this.setState({quantity: event.target.value});
  }

  onCalculate = () => {
    this.setState({quantity: Math.floor(this.state.amount / this.state.cashPurchaseRate / this.props.rate)});
  }

  onProcess = () => {
    // if (!confirm('Are you sure you want to process purchase?')) return;
    const purchasedStatuses = {'cash': 'cash_purchased', 'ATC': 'atc_purchased', 'BTC': 'btc_purchased'};
    this.props.update({
      ...this.props.purchase,
      purchaseQuantity: this.state.quantity,
      cashRate: parseFloat(this.state.cashPurchaseRate),
      purchaseRate: this.props.rate,
      purchaseBC: this.props.purchase.purchaseAmount / this.props.rate,
      status: purchasedStatuses[this.props.purchase.purchaseType]
    })
    .then(() => this.props.loadPurchases())
    .then(() => {
      // this.props.pushState('/vs_purchases');
      alert('購入確定処理を実行しました。');
    })
    .catch(error => {
      let errorMsg = error;
      if (error instanceof Object) {
        errorMsg = error.response ? error.response.text : 'Error on processing request.';
      }

      console.log('error', errorMsg, error);
    });
  }

  onCancel = () => {
    if (!confirm(this.props.intl.formatMessage(messages.AreYouSureYouWantToCancelPurchase))) return;
    this.props.cancel(this.props.purchase._id)
        .then(() => this.props.loadPurchases())
        .then(() => {
          this.props.pushState('/vs_purchases');
        });
  }

  onBack = () => {
    this.props.pushState('/vs_purchases');
  }

  render() {
    let result;
    result = (
      <Grid>
        <h2>
          <FormattedMessage
            id="VSConfig.Header"
            defaultMessage="Virtual frame configuration"
          />
        </h2>

        <div className={styles.container}>
          <div className={styles.block} style={{border: '1px solid black'}}>
            <Row>
              <Col xs={8} >
                <p className="pull-right">
                  <FormattedMessage
                    id="VSPurchaseProcess.VsRateWhenWantToPurchase"
                    defaultMessage="Vs rate when want to purchase:"
                  />
                </p>
              </Col>
              <Col xs={3}>
                <p className="pull-right">
                  {this.props.purchase && this.props.purchase.cashRateCreated}
                </p>
              </Col>
              <Col xs={1}>
                <p>
                  B$
                </p>
              </Col>
            </Row>
            <Row>
              <Col xs={8}>
                <p className="pull-right">
                  <FormattedMessage
                    id="VSPurchaseProcess.PurchaseAmount"
                    defaultMessage="Purchase amount:"
                  />
                </p>
              </Col>
              <Col xs={3}>
                <p className="pull-right">
                  <FormattedNumber value={this.props.purchase && this.props.purchase.purchaseAmount || 0}/>
                </p>
              </Col>
              <Col xs={1}>
                <p>
                  円
                </p>
              </Col>
            </Row>
            <Row>
              <Col xs={8}>
                <p className="pull-right">
                  <FormattedMessage
                    id="VSPurchaseProcess.PurchaseRate"
                    defaultMessage="Purchase rate:"
                  />
                </p>
              </Col>
              <Col xs={3}>
                <p className="pull-right">
                  {this.props.purchase && this.props.purchase.purchaseRateCreated}
                </p>
              </Col>
              <Col xs={1}>
                <p>
                  円
                </p>
              </Col>
            </Row>
            <Row>
              <Col xs={8}>
                <p className="pull-right">
                  <FormattedMessage
                    id="VSPurchaseProcess.PlannedPurchaseQuantity"
                    defaultMessage="Planned purchase quantity:"
                  />
                </p>
              </Col>
              <Col xs={3}>
                <p className="pull-right">
                  <FormattedNumber value={this.props.purchase && this.props.purchase.purchaseQuantityCreated || 0}/>
                </p>
              </Col>
              <Col xs={1}>
                <p>
                  VS
                </p>
              </Col>
            </Row>
            <div>
              <hr style={{borderTop: '1px solid black'}} />
            </div>
            <Row>
              <Col xs={8}>
                <p className="pull-right">
                  <FormattedMessage
                    id="VSPurchaseProcess.CurrentRate"
                    defaultMessage="Current rate:"
                  />
                </p>
              </Col>
              <Col xs={3}>
                <p className="pull-right">
                  {this.props.cashRate}
                </p>
              </Col>
              <Col xs={1}>
                <p>
                  B$
                </p>
              </Col>
            </Row>
            <Row>
              <Col xs={8}>
                <p className="pull-right">
                  <FormattedMessage
                    id="VSPurchaseProcess.PurchaseAmount"
                    defaultMessage="Purchase amount:"
                  />
                </p>
              </Col>
              <Col xs={3}>
                <p className="pull-right">
                  <FormattedNumber value={this.props.purchase && this.props.purchase.purchaseAmount || 0}/>
                </p>
              </Col>
              <Col xs={1}>
                <p>
                  円
                </p>
              </Col>
            </Row>
            <Row>
              <Col xs={8}>
                <p className="pull-right">
                  <FormattedMessage
                    id="VSPurchaseProcess.PurchaseRate"
                    defaultMessage="Purchase rate:"
                  />
                </p>
              </Col>
              <Col xs={3}>
                <p className="pull-right">
                  {this.props.rate}
                </p>
              </Col>
              <Col xs={1}>
                <p>
                  円
                </p>
              </Col>
            </Row>
            <Row>
              <Col xs={8}>
                <p className="pull-right">
                  <FormattedMessage
                    id="VSPurchaseProcess.PlannedPurchaseQuantity"
                    defaultMessage="Planned purchase quantity:"
                  />
                </p>
              </Col>
              <Col xs={3}>
                <p className="pull-right">
                  <FormattedNumber value={this.props.purchase && Math.floor(this.props.purchase.purchaseAmount / this.props.rate / this.props.cashRate) || 0}/>
                </p>
              </Col>
              <Col xs={1}>
                <p>
                  VS
                </p>
              </Col>
            </Row>
          </div>

          <Col className={styles.block}>
            <Row>
              <Col xs={4}>
                <ControlLabel className="pull-right">
                  <FormattedMessage
                    id="VSPurchaseProcess.VSRate"
                    defaultMessage="VS rate:"
                  />
                </ControlLabel>
              </Col>
              <Col xs={7}>
                <FormControl type="number" value={this.state.cashPurchaseRate} onChange={this.onCashRateChange}/>
              </Col>
              <Col xs={1}>
                <p>
                  B$
                </p>
              </Col>
            </Row>
            <Row>
              <Col xs={4}>
                <p className="pull-right">
                  <FormattedMessage
                    id="VSPurchaseProcess.PurchaseAmount"
                    defaultMessage="Purchase amount:"
                  />
                </p>
              </Col>
              <Col xs={7}>
                <p className="pull-right">
                  <FormattedNumber value={this.props.purchase && this.props.purchase.purchaseAmount || 0}/>
                </p>
              </Col>
              <Col xs={1}>
                <p>
                  円
                </p>
              </Col>
            </Row>
            <Row>
              <Col xs={4}>
                <p className="pull-right">
                  <FormattedMessage
                    id="VSPurchaseProcess.PurchaseRate"
                    defaultMessage="Purchase rate:"
                  />
                </p>
              </Col>
              <Col xs={7}>
                <p className="pull-right">
                  {this.props.rate}
                </p>
              </Col>
              <Col xs={1}>
                <p>
                  円
                </p>
              </Col>
            </Row>
            <FormGroup>
              <Col xs={7} xsOffset={4} className={styles.mbottom + ' text-center'}>
                <Button onClick={this.onCalculate} disabled={!+this.state.cashRate}>
                  <FormattedMessage
                    id="VSPurchaseProcess.Calculate"
                    defaultMessage="Calculate"
                  />
                </Button>
              </Col>
            </FormGroup>
            <Row>
              <Col xs={4}>
                <ControlLabel className="pull-right">
                  <FormattedMessage
                    id="VSPurchaseProcess.PurchaseQuantity"
                    defaultMessage="Purchase quanity:"
                  />
                </ControlLabel>
              </Col>
              <Col xs={7}>
                <FormControl type="number" value={this.state.quantity} onChange={this.onQuantityChange}/>
              </Col>
              <Col xs={1}>
                <p>
                  VS
                </p>
              </Col>
            </Row>
            <FormGroup>
              <Col xs={1} xsOffset={6} className={styles.mTopSm}>
                <Button onClick={this.onBack}>
                  <FormattedMessage
                    id="VSPurchaseProcess.Back"
                    defaultMessage="Back"
                  />
                </Button>
              </Col>
              <Col xs={3} className={styles.mTopSm}>
                <Button onClick={this.onProcess} disabled={!+this.state.quantity || this.props.updating}>
                  <Spinner loading={this.props.updating} size={25} exclusive>
                    <FormattedMessage
                      id="VSPurchaseProcess.Process"
                      defaultMessage="Process"
                    />
                  </Spinner>
                </Button>
              </Col>
            </FormGroup>
            <FormGroup>
              <Col xs={5} xsOffset={3} className={styles.mTopSm + ' ' + styles.mTop}>
                <span style={{color: 'red'}}>
                  <FormattedMessage
                    id="VSPurchaseProcess.ToCancelTheBuyOrder"
                    defaultMessage="To cancel the buy order"
                  />
                </span>
              </Col>
              <Col xs={2} className={styles.mTopSm + ' ' + styles.mTop}>
                <Button onClick={this.onCancel} disabled={this.props.canceling}>
                  <Spinner loading={this.props.canceling} size={25} exclusive>
                    <FormattedMessage
                      id="VSPurchaseProcess.Cancel"
                      defaultMessage="Cancel"
                    />
                  </Spinner>
                </Button>
              </Col>
            </FormGroup>
          </Col>
        </div>
        {this.props.error && <div className={styles['m-sm'] + ' alert alert-danger'} role="alert">{this.props.error.response.text}</div>}
      </Grid>
    );

    return result;
  }
}

export default injectIntl(VSPurchaseProcess);
