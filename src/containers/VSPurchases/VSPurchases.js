import React, {Component, PropTypes} from 'react';
// import Helmet from 'react-helmet';
import {connect} from 'react-redux';
import * as purchasesActions from 'redux/modules/vsPurchases';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import {injectIntl, intlShape, defineMessages, FormattedMessage, FormattedNumber} from 'react-intl';
import { VSPurchasesSearchForm } from 'components';
import classNames from 'classnames';
import { Glyphicon } from 'react-bootstrap';
import { push } from 'react-router-redux';
import { Link } from 'react-router';
import moment from 'moment';

const messages = defineMessages({
  AreYouSureYouWantToCancelPurchase: {
    id: 'PurchaseList.AreYouSureYouWantToCancelPurchase',
    defaultMessage: 'Are you sure you want to cancel purchase?'
  },
  AreYouSureYouWantToChange: {
    id: 'PurchaseList.AreYouSureYouWantToChange',
    defaultMessage: `Are you sure you want to change {fieldName} to {fieldValue}?`
  }
});

require('../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css');

@connect(
  state => ({
    purchases: state.vsPurchases.data,
    editing: state.vsPurchases.editing,
    error: state.vsPurchases.error,
    loading: state.vsPurchases.loading,
    loaded: state.vsPurchases.loaded,
    totalDataSize: state.vsPurchases.totalDataSize,
    currentPage: state.vsPurchases.currentPage,
    sizePerPage: state.vsPurchases.sizePerPage,
    // search: state.vsPurchases.search,
    sorter: state.vsPurchases.sorter,
  }),
  {...purchasesActions, pushState: push})
class VSPurchases extends Component {
  static propTypes = {
    purchases: PropTypes.array,
    error: PropTypes.string,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    load: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    totalDataSize: PropTypes.number,
    sizePerPage: PropTypes.number,
    changeSort: PropTypes.func.isRequired,
    changePage: PropTypes.func.isRequired,
    changeSizePerPage: PropTypes.func.isRequired,
    changeFilter: PropTypes.func.isRequired,
    pushState: PropTypes.func,
    cancel: PropTypes.func,
    update: PropTypes.func,
    intl: intlShape.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {searchVisible: false};
  }

  componentWillMount() {
    this.props.load();
  }

  onSearch = (fields) =>
    this.onFilterChange(fields);

  onSortChange = (name, order) => {
    const sortOrder = {name: name, order: (order === 'asc') ? 1 : -1};
    this.props.changeSort(JSON.stringify(sortOrder));
  };

  onPageChange = (page, limit) => {
    this.props.changePage(page, limit);
  };

  onFilterChange = (filterObj) =>
    this.props.changeFilter(JSON.stringify(filterObj));

  onSizePerPageList = (sizePerPage) => {
    this.props.changeSizePerPage(sizePerPage);
  };

  onCancel = (id) => {
    if (!confirm(this.props.intl.formatMessage(messages.AreYouSureYouWantToCancelPurchase))) {
      return;
    }

    this.props.cancel(id)
      .then(() => {
        this.props.load();
      });
  };

  purchaseIDFormatter = (cell, row) => {
    let result = cell;
    const removeStatuses = ['removed', 'removed_order_admin', 'removed_order_user', 'removed_purchase_admin'];
    const purchasedStatuses = ['cash_purchased', 'atc_purchased', 'btc_purchased', 'purchased'];

    if (removeStatuses.indexOf(row.status) === -1) {
      result = (<Link to={'vs_purchases/process/' + cell}><i className="glyphicon glyphicon-pencil"></i> {cell}</Link>);
    }

    if (purchasedStatuses.indexOf(row.status) !== -1) {
      result = (<div style={{cursor: 'pointer'}} onClick={() => {this.onCancel(cell);}}>
        <i className="glyphicon glyphicon-trash"></i>
        &nbsp;
        {cell}
      </div>);
    }

    return result;
  };

  render() {
    const styles = require('./VSPurchases.scss');

    const {intl} = this.props;

    function dateFormatter(cell, row) {
      if (!cell) {
        return <span>{'-'}</span>;
      }
      if (row.isImported) {
        return moment(cell).format('DD-MM-YYYY 00:00:00');
      }
      return moment(cell).format('DD-MM-YYYY HH:mm:ss');
    }

    function memberIDFormatter(cell, row) {
      return <Link to={'/members/' + cell}>{row.member && row.member.applicantID}</Link>;
    }

    function dollarFormatter(cell) {
      if (!cell) {
        return <span>{'-'}</span>;
      }
      return (
        <span>
          <FormattedNumber value={Math.floor(cell)}/>
          {' B$'}
        </span>
      );
    }

    function vsFormatter(cell) {
      if (!cell) {
        return <span>{'-'}</span>;
      }
      return (
        <span>
          <FormattedNumber value={Math.floor(cell)}/>
          {'VS'}
        </span>);
    }

    function centFormatter(cell) {
      if (!cell) {
        return <span>{'-'}</span>;
      }
      return <span>{(cell) + ' B$'}</span>;
    }

    function cashFormatter(cell, row) {
      if (!cell) {
        return <span>{'-'}</span>;
      }

      const currency = {'cash': '￥'};
      const purchaseType = row.purchaseType;
      return (
        <span>
          <FormattedNumber value={Math.floor(cell)}/>
          {currency[purchaseType] || purchaseType}
        </span>);
    }

    function statusFormatter(cell) {
      const statuses = {
        cash_order: '現金買い注文',
        atc_order: 'ATC買い注文',
        btc_order: 'Bitcoin買い注文',
        cash_purchased: '現金購入',
        atc_purchased: 'ATC購入',
        btc_purchased: 'Bitcoin購入',
        removed_order_admin: '注文キャンセル（管理）',
        removed_order_user: '注文キャンセル',
        removed_purchase_admin: '購入キャンセル（管理）'
      };
      return statuses[cell] ? statuses[cell] : cell;
    }

    function trClassFormat(rowData) {
      if (rowData.status === 'removed_order_admin' ||
        rowData.status === 'removed_order_user' ||
        rowData.status === 'removed_purchase_admin') {
        return styles.removedPurchase;
      }
    }

    return (
        <div>
          <div className="container">
            <h2>
              <FormattedMessage
                id="PurchaseList.Header"
                defaultMessage="Purchase list"
              />
            </h2>
            <div className={classNames(styles.searchForm, {[styles.hidden]: !this.state.searchVisible })}>
              <div className={styles.wrapper}>
                <VSPurchasesSearchForm onSubmit={this.onSearch}/>
              </div>
            </div>
            <div style={{textAlign: 'center'}}>
              <button className={styles.hideButton} onClick={()=> this.setState({searchVisible: !this.state.searchVisible})}>
                <Glyphicon glyph={this.state.searchVisible ? 'arrow-up' : 'arrow-down'}/>
                &nbsp;
                {
                  this.state.searchVisible ?
                    <FormattedMessage
                      id="General.HideSearch"
                      defaultMessage="Hide search"
                    /> :
                    <FormattedMessage
                      id="General.ShowSearch"
                      defaultMessage="Show search"
                    />
                }
                </button>
            </div>
          </div>
          <BootstrapTable
            cellEdit = {{
              mode: 'click',
              blurToSave: true,
              afterSaveCell: (item, fieldName, fieldValue) => {
                if (fieldValue === 'created') return;
                const updatedItem = Object.assign({}, item);
                item[fieldName] = fieldValue;
                if (fieldValue === 'purchased') {
                  updatedItem.status = 'purchased';
                  this.props.update(updatedItem);
                } else if (fieldValue === 'removed') {
                  updatedItem.status = 'removed';
                  this.props.cancel(updatedItem);
                }
              },
              beforeSaveCell: (item, fieldName, fieldValue) => {
                if (item[fieldName] === fieldValue) return true;
                return confirm(intl.formatMessage(messages.AreYouSureYouWantToChange, {fieldName, fieldValue}));
              }
            }}
            trClassName={trClassFormat}
            data={this.props.purchases || []}
            remote
            hover
            condensed
            pagination
            fetchInfo={ { dataTotalSize: this.props.totalDataSize } }
            options={ { sizePerPage: this.props.sizePerPage,
                        onPageChange: this.onPageChange,
                        sizePerPageList: [ 5, 10, 20],
                        pageStartIndex: 1,
                        page: this.props.currentPage,
                        // onSearchChange: this.onSearchChange,
                        onSortChange: this.onSortChange,
                        // onFilterChange: this.onFilterChange,
                        onSizePerPageList: this.onSizePerPageList,
                        hideSizePerPage: true,
                        noDataText: '表示されるデータがございません。'
                       }
                    }
          >
            <TableHeaderColumn dataField="createDate" dataSort dataFormat={dateFormatter} dataAlign="center" editable={false}>
              <FormattedMessage
                id="PurchaseList.CreationDate"
                defaultMessage="Creation date"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="createOrderDate" dataSort dataFormat={dateFormatter} dataAlign="center" editable={false}>
              <FormattedMessage
                id="PurchaseList.OrderCreationDate"
                defaultMessage="Order creation date"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="status" dataSort dataFormat={statusFormatter} dataAlign="center" editable={false}>
              <FormattedMessage
                id="PurchaseList.Status"
                defaultMessage="Status"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="purchaseAmount" dataSort dataFormat={cashFormatter} dataAlign="center" editable={false} width="150">
              <FormattedMessage
                id="PurchaseList.PurchaseAmount"
                defaultMessage="Purchase amount"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="purchaseBCCreated" dataSort dataFormat={dollarFormatter} dataAlign="center" width="120">
              <FormattedMessage
                id="VSConfigFrameView.PurchaseBraveCreated$"
                defaultMessage="Purchase BRAVE$ created"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="purchaseBC" dataSort dataFormat={dollarFormatter} dataAlign="center" width="120">
              <FormattedMessage
                id="VSConfigFrameView.PurchaseBrave$"
                defaultMessage="Purchase BRAVE$"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="purchaseType" dataSort dataAlign="center" editable={false} width="100">
              <FormattedMessage
                id="PurchaseList.PurchaseType"
                defaultMessage="Purchase type"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="cashRateCreated" dataSort dataAlign="center" editable={false} dataFormat={centFormatter} width="100">
              <FormattedMessage
                id="PurchaseList.СashRateCreated"
                defaultMessage="Created Rate"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="purchaseQuantityCreated" dataSort dataAlign="center" dataFormat={vsFormatter} editable={false} width="100">
              <FormattedMessage
                id="PurchaseList.PurchaseQuantityCreated"
                defaultMessage="Created quantity"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="cashRate" dataSort dataAlign="center" editable={false} dataFormat={centFormatter} width="100">
              <FormattedMessage
                id="PurchaseList.СashRate"
                defaultMessage="Rate"
              />
            </TableHeaderColumn>
            {/* <TableHeaderColumn dataField="purchaseRate" dataSort dataAlign="center" editable={false}> */}
              {/* <FormattedMessage */}
                {/* id="PurchaseList.PurchaseRate" */}
                {/* defaultMessage="Purchase rate" */}
              {/* /> */}
            {/* </TableHeaderColumn> */}
            <TableHeaderColumn dataField="purchaseQuantity" dataSort dataAlign="center" dataFormat={vsFormatter} editable={false} width="100">
              <FormattedMessage
                id="PurchaseList.PurchaseQuantity"
                defaultMessage="Quantity"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="memberID" dataSort dataAlign="center" editable={false} dataFormat={memberIDFormatter}>
              <FormattedMessage
                id="PurchaseList.applicantID"
                defaultMessage="Member ID"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="masterID" dataSort isKey dataAlign="center" dataFormat={this.purchaseIDFormatter} editable={false}>
              <FormattedMessage
                id="PurchaseList.PurchaseID"
                defaultMessage="Purchase ID"
              />
            </TableHeaderColumn>
          </BootstrapTable>
        </div>
    );
  }
}

export default injectIntl(VSPurchases);
