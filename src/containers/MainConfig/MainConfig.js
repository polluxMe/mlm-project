import React, {Component, PropTypes} from 'react';
// import Helmet from 'react-helmet';
import {connect} from 'react-redux';
import * as mainConfigActions from 'redux/modules/mainConfig';
import {isLoaded, load as loadConfig} from 'redux/modules/mainConfig';
// import * as purchasesActions from 'redux/modules/vsPurchases';
// import { WidgetForm } from 'components';
import { asyncConnect } from 'redux-async-connect';
import {FormattedMessage} from 'react-intl';
import { push } from 'react-router-redux';
import { MainConfigForm } from 'components';

@asyncConnect([{
  deferred: false,
  promise: ({store: {dispatch, getState}}) => {
    if (!isLoaded(getState())) {
      return dispatch(loadConfig());
    }
  }
}])
@connect(
  state => ({
    config: state.mainConfig.data,
    error: state.mainConfig.error,
    loading: state.mainConfig.loading,
    loaded: state.mainConfig.loaded,
  }),
  {...mainConfigActions, pushState: push })
export default class VSConfig extends Component {
  static propTypes = {
    error: PropTypes.string,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    load: PropTypes.func.isRequired,
    pushState: PropTypes.func.isRequired,
    selected: PropTypes.array,
    config: PropTypes.object
  };
  render() {
    let result;
    result = (
        <div className="container">
          <h2>
            <FormattedMessage
              id="MainConfig.Header"
              defaultMessage="Main Config"
            />
          </h2>
              <MainConfigForm initialValues={this.props.config}/>
        </div>
    );

    return result;
  }
}

