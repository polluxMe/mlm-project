import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
// import config from '../../config';
import Helmet from 'react-helmet';
import { LinkContainer } from 'react-router-bootstrap';
import {FormattedMessage} from 'react-intl';
import { Board } from 'components';
import {Tabs, Tab} from 'react-bootstrap';
import {bindActionCreators} from 'redux';
import {loadWallet} from 'redux/modules/memberInfo';

import 'theme/mlm/css/style.css';

const myAccountButtonImage = require('theme/mlm/images/btn-myaccount.png');

const memberOnlyButtonImage = require('theme/mlm/images/btn-memberonly.png');
const gameButtonImage = require('theme/mlm/images/btn-game.png');
const shoppingButtonImage = require('theme/mlm/images/btn-shopping.png');
const reinvestButtonImage = require('theme/mlm/images/btn-reinvest.png');
const exchangeButtonkImage = require('theme/mlm/images/btn-exchange.png');
const marketButtonImage = require('theme/mlm/images/btn-market.png');

const tabStock = require('theme/mlm/images/tab-stock.png');
const tab3way = require('theme/mlm/images/tab-3way.png');
const tab2way = require('theme/mlm/images/tab-2way.png');
const tabTicket = require('theme/mlm/images/tab-ticket.png');

const tabBodyStock = require('theme/mlm/images/tab-body-stock.png');
const tabBody3Way = require('theme/mlm/images/tab-body-3way.png');
const tabBody2Way = require('theme/mlm/images/tab-body-2way.png');
const tabBodyTicket = require('theme/mlm/images/tab-body-ticket.png');

@connect(state => ({
  user: state.auth.user,
  wallet: state.memberInfo.wallet
}),
  dispatch => bindActionCreators({loadWallet}, dispatch)
)
export default class MyPage extends Component {
  static propTypes = {
    user: PropTypes.object,
    wallet: PropTypes.object,
    loadWallet: PropTypes.func
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  componentWillMount() {
    this.props.loadWallet(this.props.user._id);
  }

  render() {
    const styles = require('./MyPage.scss');
    require('./MyPage.css');

    const {user} = this.props;

    const isUserTypeOfficial = user && user.status && user.status === 'official';

    return (
      <div id="wrap">
      <div id="contents">
        <Helmet title="MyAccount"/>
          <div id="my-account" className="clearfix">
            <dl id="my-id" style={{width: '40%', fontSize: '14px'}}>
              <dt>ID</dt>
              <dd>{this.props.user && this.props.user.username || 0}</dd>
            </dl>
            <dl id="my-rank" style={{width: '22%', fontSize: '14px'}}>
              <dt>
                <FormattedMessage
                  id="MyPage.RANK"
                  defaultMessage="RANK"
                />
              </dt>
              <dd>{this.props.user && this.props.user.rank || 0}</dd>
            </dl>
              <LinkContainer to={isUserTypeOfficial ? '/my_account' : '/my_page'}>
                <img src={myAccountButtonImage} alt="MyAccount" className={styles.myAccountBtn} />
              </LinkContainer>
          </div>
        <Tabs defaultActiveKey={1} id="mypage_tabs">
          <Tab eventKey={1} title={<img src={tabStock} />}>
            <img src={tabBodyStock} />
            <dl>
              <dt>保有ストック</dt>
              <dd>{this.props.wallet ? this.props.wallet.vsAmount : 0}</dd>
            </dl>
          </Tab>
          <Tab eventKey={2} title={<img src={tab3way} />}>
            <img src={tabBody3Way} />
            <dl>
              <dt>3way</dt>
              <dd>{this.props.user && this.props.user.commision ? Math.floor(this.props.user.commision / 2) + ' B$' : 0 + ' B$'}</dd>
            </dl>
          </Tab>
          <Tab eventKey={3} title={<img src={tab2way} />}>
            <img src={tabBody2Way} />
            <dl>
              <dt>2way</dt>
              <dd>0</dd>
            </dl>
          </Tab>
          <Tab eventKey={5} title={<img src={tabTicket} />}>
            <img src={tabBodyTicket} />
            <dl>
              <dt>ticket</dt>
              <dd>0 $</dd>
            </dl>
          </Tab>
        </Tabs>
          <Board >
            <div className={styles.menu}>
              <div className={styles.menuItem}>
                <img src={memberOnlyButtonImage} alt="MyAccount"/>
              </div>
              <div className={styles.menuItem}>
                <img src={gameButtonImage} alt="MyAccount"/>
              </div>
              <div className={styles.menuItem}>
                <img src={shoppingButtonImage} alt="MyAccount"/>
              </div>
              <div className={styles.menuItem}>
                <img src={reinvestButtonImage} alt="MyAccount"/>
              </div>
              <div className={styles.menuItem}>
                <img src={exchangeButtonkImage} alt="MyAccount"/>
              </div>
              <div className={styles.menuItem}>
                <img src={marketButtonImage} alt="MyAccount"/>
              </div>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}

