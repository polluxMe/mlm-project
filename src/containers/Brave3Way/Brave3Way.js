import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import { Board } from 'components';
import { LinkContainer } from 'react-router-bootstrap';
import {bindActionCreators} from 'redux';
import {loadWallet} from 'redux/modules/memberInfo';

const brave3WayTitleImage = require('theme/mlm/images/3way-title.png');
const brave3WayTicketImage = require('theme/mlm/images/3way-ticket.png');
const brave3WayGameImage = require('theme/mlm/images/3way-game.png');
const brave3WayShoppingImage = require('theme/mlm/images/3way-shopping.png');
const brave3WayExchangeImage = require('theme/mlm/images/3way-exchange.png');
const brave3WayHistoryImage = require('theme/mlm/images/3way-history.png');

@connect(state => ({
  user: state.auth.user,
  wallet: state.memberInfo.wallet
}),
  dispatch => bindActionCreators({loadWallet}, dispatch)
)
export default class Brave3Way extends Component {
  static propTypes = {
    user: PropTypes.object,
    wallet: PropTypes.object,
    loadWallet: PropTypes.func,
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  componentWillMount() {
    this.props.loadWallet(this.props.user._id);
  }

  render() {
    return (
      <div id="wrap">
        <Helmet title="Brave 3 way"/>
        <div className="three-way" id="contents">
          <Board>
            <h2 className="mb10"><img src={brave3WayTitleImage} alt="BRAVE$ 3WAY"/></h2>
            <dl>
              <dt>保有3WAY</dt>
              <dd>{this.props.wallet ? Math.floor(this.props.wallet.commission / 2) + ' B$' : 0 + ' B$'}</dd>
            </dl>
            <div className="mb10">
              <LinkContainer to="/my_account/brave3way/tickets">
                <img src={brave3WayTicketImage} alt="チケットへ変換" />
              </LinkContainer>
            </div>
            <div className="mb10"><a href="#"><img src={brave3WayGameImage} alt="懸賞ゲーム"/></a></div>
            <div className="mb10"><a href="#"><img src={brave3WayShoppingImage} alt="ショッピング"/></a></div>
            <div className="mb10">
              <LinkContainer to="/my_account/withdrawal">
                <img src={brave3WayExchangeImage} alt="円へ両替"/>
              </LinkContainer>
            </div>
            <div>
              <LinkContainer to="/my_account/brave3way/history">
                <img src={brave3WayHistoryImage} alt="使用履歴"/>
              </LinkContainer>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}

/*

 */
