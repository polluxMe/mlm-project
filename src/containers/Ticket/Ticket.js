import React, { Component, PropTypes } from 'react';
// import config from '../../config';
import Helmet from 'react-helmet';
import { Board } from 'components';

const ticketTitleImage = require('theme/mlm/images/ticket-title.png');
const ticketButtomTranceferImage = require('theme/mlm/images/ticket-btn-trancefer.png');

export default class Ticket extends Component {
  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  render() {
    return (
      <div id="wrap">
        <Helmet title="Ticket"/>
        <div className="ticket" id="contents">
          <Board>
            <h3 className="mb10"><img src={ticketTitleImage} alt="チケット"/></h3>
            <dl>
              <dt>保有チケット</dt>
              <dd>0 $</dd>
            </dl>
            <a href="#"><img src={ticketButtomTranceferImage} alt="トランスファー"/></a>
          </Board>
        </div>
      </div>
    );
  }
}

/*

 */
