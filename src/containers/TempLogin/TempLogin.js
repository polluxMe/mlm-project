import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {bindActionCreators} from 'redux';
import * as authActions from 'redux/modules/auth';
import { replace as replaceRoute } from 'react-router-redux';
import { TempLoginForm } from 'components';
import {FormattedMessage} from 'react-intl';


@connect(
  state => ({
    form: state.form.tempLoginForm,
  }),
  dispatch => bindActionCreators({...authActions, replaceRoute}, dispatch)
)
export default class TempLogin extends Component {
  static propTypes = {
    tempUser: PropTypes.object,
    saveUser: PropTypes.func,
    replaceRoute: PropTypes.func
  }

  onSubmit = (response) => {
    this.props.saveUser(response);
    this.props.replaceRoute('/registration/change_id');
  };

  render() {
    /* eslint-disable */
    const {
      tempUser,
      asyncValidating,
      dirty,
      active,
      handleSubmit,
      reset,
      invalid,
      pristine,
      valid,
      form
    } = this.props;
    /* eslint-disable */

    const styles = require('./TempLogin.scss');
    return (
      <div className={styles.loginPage + ' container'}>
        <Helmet title="Temp Login"/>
        <div className={'row ' + styles.mTLg}>
          <div className="col-sm-12">
            <div className="panel panel-default panel-border-color panel-border-color-warning">
              <div className="panel-heading panel-heading-divider">
                <h1>
                  <FormattedMessage
                    id="TempLogin.Header"
                    defaultMessage="Temp Login"
                  />
                </h1>
              </div>
              <div className="panel-body">
                <div>
                  <TempLoginForm onSubmit={this.onSubmit} tempUser={this.props.tempUser}/>
                </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    );
  }
}
