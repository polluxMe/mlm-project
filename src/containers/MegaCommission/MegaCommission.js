import React, { Component, PropTypes } from 'react';
// import config from '../../config';
import _ from 'lodash'; // eslint-disable-line
import {connect} from 'react-redux';
import {FormattedMessage, FormattedNumber} from 'react-intl';
import {Board} from 'components';
import Helmet from 'react-helmet';
import * as megaCommissionActions from 'redux/modules/megaCommission';
import * as mainCommissionDividendActions from 'redux/modules/mainCommissionDividend';
import moment from 'moment';
import { LinkContainer } from 'react-router-bootstrap';

const backImg = require('../../theme/mlm/images/btn-double-back.png');

const currYear = moment().format('YYYY');
const currMonth = +moment().format('MM');
const months = [];
const years = [];
for (let ii = currYear; ii >= currYear - 3; ii -= 1) {
  years.push({key: ii, value: ii + '年'});
}
for (let ii = 1; ii <= 12; ii += 1) {
  months.push({key: ii, value: ii + '月'});
}

const Select = ({options, ...props}) =>
  <select {...props}>
    <option key="0" value="0">...</option>
    {options && options.map((item, index) => <option key={index} value={item.key}>{item.value}</option>)}
  </select>;

@connect(state => ({
  user: state.auth.user,
  commission: state.megaCommission.item,
  dividend: state.mainCommissionDividend.item
}),
  {
    loadMegaCommission: megaCommissionActions.load,
    loadDividend: mainCommissionDividendActions.load
  }
)
export default class MegaCommission extends Component {
  static propTypes = {
    user: PropTypes.object,
    commission: PropTypes.object,
    dividend: PropTypes.object,
    purchased: PropTypes.object,
    mainConfig: PropTypes.object,
    loadMegaCommission: PropTypes.func,
    loadDividend: PropTypes.func
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  state = {
    year: currYear,
    month: currMonth
  };

  componentWillMount() {
    const {loadMegaCommission, loadDividend, user} = this.props;
    loadMegaCommission(user._id, {year: currYear, month: currMonth});
    loadDividend(user._id, {year: currYear, month: currMonth, type: 'mega'});
  }

  onMonthChange = ({target: {value}}) => {
    const {loadMegaCommission, loadDividend, user} = this.props;
    this.setState({month: value});
    loadMegaCommission(user._id, {year: this.state.year, month: value});
    loadDividend(user._id, {year: this.state.year, month: value, type: 'mega'});
  };

  onYearChange = ({target: {value}}) => {
    const {loadMegaCommission, loadDividend, user} = this.props;
    this.setState({year: value});
    loadMegaCommission(user._id, {year: value, month: this.state.month});
    loadDividend(user._id, {year: value, month: this.state.month, type: 'mega'});
  };

  render() {
    const {commission, user} = this.props;
    const createDate = commission && commission.createDate ? new Date(commission.createDate) : null;

    return (
      <div id="wrap">
        <Helmet title="Mega matching bonus"/>
        <div id="contents" className="comission_detail">
          <Board>
            <div>
              <div className="main_mega">
                <h2>
                  <FormattedMessage
                    id="MegaCommission.MegaCommission"
                    defaultMessage="Mega matching bonus"
                  />
                </h2>
                <div>
                  <div className="select-wrapL">
                    <Select
                      value={this.state.year}
                      options={years}
                      onChange={this.onYearChange}
                      className="year"
                    />
                  </div>
                  <div className="select-wrapR">
                    <Select
                      value={this.state.month}
                      options={months}
                      onChange={this.onMonthChange}
                      className="month"
                    />
                  </div>
                </div>
                <div style={{'clear': 'both'}}></div>
                <div>
                  <h3>
                    <FormattedMessage
                      id="MegaCommission.NumberOfDividenedVS"
                      defaultMessage="Number of dividened VS"
                    />
                  </h3>
                  <p className="tac">
                    <FormattedNumber value={(commission && commission.amount) || 0} />
                    &nbsp;VS
                  </p>
                </div>
                <div>
                  <h3>
                    <FormattedMessage
                      id="MegaCommission.NumberOfDevidend"
                      defaultMessage="Number of devidend VS"
                    />
                  </h3>
                  <p className="tac">
                    <FormattedNumber value={(commission && commission.amount) || 0} />
                    &nbsp;VS
                  </p>
                </div>
                <div>
                  <h3>ランク／直紹介者人数</h3>
                  <table>
                    <tr>
                      <td>R{(user && user.rank) || 1}</td>
                      <td>{commission && commission.exactChildren}人</td>
                    </tr>
                  </table>
                  <table>
                    {commission && commission.children && _.map(commission.children, (item, key) => (
                      <tr key={key}>
                        <td>{+key + 1}段目</td>
                        <td>{item}人</td>
                      </tr>
                    ))}
                  </table>
                </div>
                {createDate && <div>
                  <h3>
                    <FormattedMessage
                      id="MegaCommission.DayPlanningToPay"
                      defaultMessage="Day planning to pay:"
                    />
                    {createDate && createDate.getMonth() + 1}月 {createDate && createDate.getDate()}日
                  </h3>
                </div>}
                <div style={{padding: '10px'}}>
                  <LinkContainer to="/my_account/commissions/">
                    <img src={backImg} width="100%" />
                  </LinkContainer>
                </div>
              </div>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}
