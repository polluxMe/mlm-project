import React, {Component, PropTypes} from 'react';
// import Helmet from 'react-helmet';
import {connect} from 'react-redux';
// import * as vsConfigFrameActions from 'redux/modules/vsConfigFrame';
import * as vsConfigFrameActions from 'redux/modules/vsConfigFrame';
// import * as purchasesActions from 'redux/modules/vsPurchases';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
// import { WidgetForm } from 'components';
import {FormattedMessage, FormattedNumber} from 'react-intl';
// import { push } from 'react-router-redux';
import { Link } from 'react-router';
import {bindActionCreators} from 'redux';
import {Button, ButtonGroup} from 'react-bootstrap';

require('../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css');

@connect(
  state => ({
    vsConfigFrame: state.vsConfigFrame.data,
    error: state.vsConfigFrame.error,
    loading: state.vsConfigFrame.loading,
    loaded: state.vsConfigFrame.loaded,
    totalDataSize: state.vsConfigFrame.totalDataSize,
    currentPage: state.vsConfigFrame.currentPage,
    sizePerPage: state.vsConfigFrame.sizePerPage,
    pager: state.vsConfigFrame.pager,
    onSizePerPageList: state.vsConfigFrame.onSizePerPageList,
    search: state.vsConfigFrame.search,
    sorter: state.vsConfigFrame.sorter,
  }),
  dispatch => bindActionCreators({...vsConfigFrameActions}, dispatch))
export default class VSConfigFrame extends Component {
  static propTypes = {
    vsConfigFrame: PropTypes.array,
    error: PropTypes.object,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    load: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    totalDataSize: PropTypes.number,
    sizePerPage: PropTypes.number,
    changeSort: PropTypes.func,
    changePage: PropTypes.func,
    changeSizePerPage: PropTypes.func,
    changeFilter: PropTypes.func,
    deleteFrames: PropTypes.func.isRequired,
    changeOrder: PropTypes.func.isRequired,
    pushState: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.props.load();
  }

  onSortChange = (name, order) => {
    const sortOrder = {name: name, order: (order === 'asc') ? 1 : -1};
    this.props.changeSort(JSON.stringify(sortOrder));
  };

  onPageChange = (page, limit) => {
    this.props.changePage(page, limit);
  };

  onFilterChange = (filterObj) => {
    this.props.changeFilter(JSON.stringify(filterObj));
  };

  onSizePerPageList = (sizePerPage) => {
    this.props.changeSizePerPage(sizePerPage);
  };

  changeOrder = (id1, id2) => {
    this.props.changeOrder(id1, id2)
      .then(() => this.props.load());
  };
  // edit = (_id) => {
  //   this.props.pushState('/vs_config_frame/form/' + _id);
  //   console.log('_id', _id);
  // }

  render() {
    // const handleEdit = (widget) => {
    //   const {editStart} = this.props; // eslint-disable-line no-shadow
    //   return () => editStart(String(widget.id));
    // };

    function detailFormatter(cell) {
      return (
        <Link to={'/vs_config_frame/details/' + cell}>
          <FormattedMessage
            id="VSConfig.Details"
            defaultMessage="Details"
          />
        </Link>
      );
    }

    function editFormatter(cell) {
      return (
        <Link to={'/vs_config_frame/form/' + cell}>
          <FormattedMessage
            id="VSConfig.Edit"
            defaultMessage="Edit"
          />
        </Link>
      );
    }

    const deleteFormatter = (cell, row) => {
      if (row.status === 'active') {
        return (<div></div>);
      }
      const deleteFrame = (_cell) => {
        this.props.deleteFrames(_cell)
          .then(() => this.props.load());
      };
      return (
        <a href="#" onClick={ () => confirm('Are you sure to delete this frame?') && deleteFrame(cell) }>
          <FormattedMessage
            id="VSConfig.Delete"
            defaultMessage="Delete"
          />
        </a>
      );
    };

    function statusFormatter(cell) {
      const statuses = {
        finished: '販売中',
        active: '販売中',
        created: '未販売'
      };
      return statuses[cell] ? statuses[cell] : cell;
    }

    const numberFormatter = (cell) => {
      return <FormattedNumber value={cell}/>;
    };

    const levelFormatter = (cell, row) => {
      return (
        <div>
          <ButtonGroup>
            <Button
              disabled={row.status !== 'created' || !row.hasPrevLevel || this.props.loading}
              onClick={() => this.changeOrder(row._id, row.prevItemId)}
            >
              <i className="glyphicon glyphicon-triangle-top"></i>
            </Button>
            <Button
              disabled={row.status !== 'created' || !row.hasNextLevel || this.props.loading}
              onClick={() => this.changeOrder(row._id, row.nextItemId)}
            >
              <i className="glyphicon glyphicon-triangle-bottom"></i>
            </Button>
          </ButtonGroup>
          <span>&nbsp;</span>
          <span>
            {cell}
          </span>
        </div>
      );
    };

    let result;
    // console.log(this.props.vsConfigFrame);
    /* ReactDataGrid uses the document object, which is not available on server side */
    result = (
        <div>
          <div className="container">
          <h2>
            <FormattedMessage
              id="VSConfig.Header"
              defaultMessage="Virtual frame configuration"
            />
          </h2>

          <div className="row">
            <div className="col-xs-12 col-sm-6 col-md-6 col-lg-8">
              <div className="btn-group btn-group-sm">
                <a type="button" href="/vs_config_frame/form" className="btn btn-info react-bs-table-add-btn">
                  <i className="glyphicon glyphicon-plus"></i>
                  <span>
                      <FormattedMessage
                        id="VSConfig.Add"
                        defaultMessage="Add new"
                      />
                  </span>
                </a>
              </div>
            </div>
          </div>
          </div>
          <BootstrapTable
            data={this.props.vsConfigFrame || []}
            remote
            hover
            condensed
            pagination
            fetchInfo={ { dataTotalSize: this.props.totalDataSize } }
            options={ { sizePerPage: this.props.sizePerPage,
              onPageChange: this.onPageChange,
              sizePerPageList: [ 5, 10, 20],
              pageStartIndex: 1,
              page: this.props.currentPage,
              onSortChange: this.onSortChange,
              onSizePerPageList: this.onSizePerPageList,
              hideSizePerPage: true,
              noDataText: '表示されるデータがございません。'
            }
            }
          >
            <TableHeaderColumn dataField="level" isKey dataAlign="center" dataFormat={ levelFormatter }>
              #
            </TableHeaderColumn>
            <TableHeaderColumn dataField="startRate" dataAlign="center">
              <FormattedMessage
                id="VSConfig.StartRate"
                defaultMessage="Start rate"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="endRate" dataAlign="center">
              <FormattedMessage
                id="VSConfig.EndRate"
                defaultMessage="End rate"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="purchaseAmmount" dataAlign="center" dataFormat={ numberFormatter }>
              <FormattedMessage
                id="VSConfig.PurchaseAmmount"
                defaultMessage="Purchase amount"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="status" dataAlign="center" dataFormat={ statusFormatter }>
              <FormattedMessage
                id="VSConfig.Status"
                defaultMessage="Status"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="_id" dataFormat={ detailFormatter }>
              <FormattedMessage
                id="VSConfig.Details"
                defaultMessage="Details"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="_id" dataFormat={ editFormatter }>
              <FormattedMessage
                id="VSConfig.Edit"
                defaultMessage="Edit"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="_id" dataFormat={ deleteFormatter }>
              <FormattedMessage
                id="VSConfig.Delete"
                defaultMessage="Delete"
              />
            </TableHeaderColumn>
          </BootstrapTable>
        </div>
    );

    return result;
  }
}

