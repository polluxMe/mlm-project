import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import { initialize } from 'redux-form';
import { VSConfigFrameAdd } from 'components';
import { browserHistory } from 'react-router';
import {FormattedMessage} from 'react-intl';

@connect(
  () => ({}),
  {
    initialize
  })
export default class VSConfigFrameForm extends Component {
  static propTypes = {
    initialize: PropTypes.func.isRequired,
    location: PropTypes.object
  }

  componentWillMount() {
    this.handleInitialize();
  }

  handleSubmit = () => {
    browserHistory.push('/vs_config_frame');
  }

  handleInitialize = () => {

  }

  render() {
    return (
      <div className="container">
        <h1>
          <FormattedMessage
            id="VSConfigFrameAdd.VsFrameForm"
            defaultMessage="VS frame form"
          />
        </h1>
        <Helmet title="VSConfigFrame"/>
        <VSConfigFrameAdd {...this.props} onSubmit={this.handleSubmit}/>
      </div>
    );
  }
}
