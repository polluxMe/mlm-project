import React, {Component, PropTypes} from 'react';
// import Helmet from 'react-helmet';
import {connect} from 'react-redux';
import * as purchasesActions from 'redux/modules/vsPurchasesHistory';
import {cancel} from 'redux/modules/vsPurchases';
// import { loadHistory, loadFrameData, endFrame } from 'redux/modules/vsPurchasesHistory';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
// import { asyncConnect } from 'redux-async-connect';
import {Form, FormGroup, Col, Button, ControlLabel, FormControl} from 'react-bootstrap';
import {injectIntl, intlShape, defineMessages, FormattedMessage, FormattedNumber} from 'react-intl';
import { VSFramePurchaseHistoryListSearchForm } from 'components';
import classNames from 'classnames';
import { Glyphicon } from 'react-bootstrap';
import {bindActionCreators} from 'redux';
import {push} from 'react-router-redux';
import { Link } from 'react-router';
import moment from 'moment';
import {reduxForm} from 'redux-form';

const messages = defineMessages({
  AreYouSureYouWantToCancelPurchase: {
    id: 'VSConfigFrameView.AreYouSureYouWantToCancelPurchase',
    defaultMessage: 'Are you sure you want to cancel purchase?'
  },
  DoYouWantToEndThisFrame: {
    id: 'VSConfigFrameView.DoYouWantToEndThisFrame',
    defaultMessage: 'Do you want to end this frame?'
  },
  Purchased: {
    id: 'VSConfigFrameView.Purchased',
    defaultMessage: 'Purchased'
  }
});


require('../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css');


// const Input = ({max, type, placeholder, input, disabled, meta: {touched, error}}) =>
//   <div>
//     <div className={(error && touched ? ' has-error' : '')}>
//       <FormControl max={max} type={type} {...input} placeholder={placeholder} disabled={disabled} />
//     </div>
//     {error && touched && <div className="text-danger">{error}</div>}
//   </div>;

@reduxForm({
  form: 'vsConfigFrameDetailsEdit'
})
@connect(
  state => ({
    form: state.form.vsConfigFrameDetailsEdit,
    purchases: state.vsPurchasesHistory.data,
    editing: state.vsPurchasesHistory.editing,
    frameId: state.vsPurchasesHistory.frameId,
    frameData: state.vsPurchasesHistory.frameData,
    error: state.vsPurchasesHistory.error,
    loading: state.vsPurchasesHistory.loading,
    loaded: state.vsPurchasesHistory.loaded,
    totalDataSize: state.vsPurchasesHistory.totalDataSize,
    currentPage: state.vsPurchasesHistory.currentPage,
    sizePerPage: state.vsPurchasesHistory.sizePerPage,
    // search: state.vsPurchasesHistory.search,
    sorter: state.vsPurchasesHistory.sorter,
  }),
  dispatch => bindActionCreators({pushState: push, ...purchasesActions, cancel}, dispatch))
class VSConfigFrameView extends Component {
  static propTypes = {
    purchases: PropTypes.array,
    frameData: PropTypes.object,
    error: PropTypes.string,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    editing: PropTypes.object.isRequired,
    editStart: PropTypes.func.isRequired,
    load: PropTypes.func.isRequired,
    set: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    totalDataSize: PropTypes.number,
    sizePerPage: PropTypes.number,
    changeSort: PropTypes.func.isRequired,
    changePage: PropTypes.func.isRequired,
    changeSizePerPage: PropTypes.func.isRequired,
    changeFilter: PropTypes.func.isRequired,
    pushState: PropTypes.func,
    loadHistory: PropTypes.func,
    loadFrameData: PropTypes.func,
    cancel: PropTypes.func,
    updateFrame: PropTypes.func,
    endFrame: PropTypes.func,
    params: PropTypes.object,
    intl: intlShape.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {searchVisible: false, frameId: props.params.id};
  }

  componentWillMount() {
    this.props.loadHistory(this.props.params.id);
    this.props.loadFrameData(this.props.params.id)
      .then(frame => {
        this.setState({
          frameAmount: frame.purchaseFrame,
          framePurchased: frame.purchaseQuantity,
          frameRemain: frame.remainQuantity
        });
      });
  }

  onSearch = (fields) =>
    this.onFilterChange(fields, this.props.params.id);

  onSortChange = (name, order) => {
    const sortOrder = {name: name, order: (order === 'asc') ? 1 : -1};
    this.props.changeSort(JSON.stringify(sortOrder), this.props.params.id);
  };

  onPageChange = (page, limit) => {
    this.props.changePage(page, limit, this.props.params.id);
  };

  // onSearchChange = () => {
  //
  // };

  onFilterChange = (filterObj) =>
    this.props.changeFilter(JSON.stringify(filterObj), this.props.params.id);

  onSizePerPageList = (sizePerPage) => {
    this.props.changeSizePerPage(sizePerPage, this.props.params.id);
  };

  onCancel = (id) => {
    if (!confirm(this.props.intl.formatMessage(messages.AreYouSureYouWantToCancelPurchase))) {
      return;
    }

    this.props.cancel(id).
    then(() => {
      this.props.loadHistory(this.props.params.id);
      this.props.loadFrameData(this.props.params.id)
        .then(frame => {
          this.setState({
            frameAmount: frame.purchaseFrame,
            framePurchased: frame.purchaseQuantity,
            frameRemain: frame.remainQuantity
          });
        });
    });
  };

  onAmountChange = (event) => {
    const remainDiff = parseInt(event.target.value, 10) - parseInt(this.state.framePurchased, 10);
    this.setState({frameAmount: event.target.value, frameRemain: remainDiff});
  }

  purchaseIDFormatter = (cell, row) => {
    let result = cell;
    const removeStatuses = ['removed', 'removed_order_admin', 'removed_order_user', 'removed_purchase_admin'];
    const purchasedStatuses = ['cash_purchased', 'atc_purchased', 'btc_purchased', 'purchased'];

    if (removeStatuses.indexOf(row.status) === -1) {
      result = (<Link to={'vs_purchases/process/' + cell}>{cell}</Link>);
    }

    if (purchasedStatuses.indexOf(row.status) !== -1) {
      result = (<div style={{cursor: 'pointer'}} onClick={() => {this.onCancel(cell);}}>
        <i className="glyphicon glyphicon-trash"></i>
        &nbsp;
        {cell}
      </div>);
    }

    return result;
  };

  endImideatly = () => {
    if (!confirm(this.props.intl.formatMessage(messages.DoYouWantToEndThisFrame))) {
      return;
    }
    this.props.endFrame(this.props.params.id)
      .then(() => {
        this.props.pushState('/vs_config_frame');
      })
      .catch((err) => {
        window.alert(err.response.text);
      });
  };

  saveFrame = (state) => {
    if (state.remainQuantity < 0) {
      return;
    }

    this.props.updateFrame({
      _id: this.props.params.id,
      frameAmount: state.frameAmount,
      framePurchased: state.framePurchased,
      frameRemain: state.frameRemain
    })
      .then((response) => {
        console.log('response', response);
      });
  };

  render() {
    function dateFormatter(cell, row) {
      if (!cell) {
        return <span>{'-'}</span>;
      }
      if (row.isImported) {
        return moment(cell).format('DD-MM-YYYY 00:00:00');
      }
      return moment(cell).format('DD-MM-YYYY HH:mm:ss');
    }

    function memberIDFormatter(cell, row) {
      return <Link to={'/members/' + cell}>{row.member && row.member.applicantID}</Link>;
    }

    function dollarFormatter(cell) {
      if (!cell) {
        return <span>{'-'}</span>;
      }
      return (
        <span>
          <FormattedNumber value={cell}/>
          {' B$'}
        </span>
      );
    }

    function vsFormatter(cell) {
      if (!cell) {
        return <span>{'-'}</span>;
      }
      return (
        <span>
          <FormattedNumber value={cell}/>
          {' VS'}
        </span>);
    }

    function cashFormatter(cell, row) {
      if (!cell) {
        return <span>{'-'}</span>;
      }

      const currency = {'cash': '￥'};
      const purchaseType = row.purchaseType;
      return (
        <span>
          <FormattedNumber value={cell}/>
          {currency[purchaseType] || purchaseType}
        </span>);
    }

    function centFormatter(cell) {
      if (!cell) {
        return <span>{'-'}</span>;
      }
      return <span>{(cell * 100) + ' ¢'}</span>;
    }

    function statusFormatter(cell) {
      const statuses = {
        cash_order: '現金買い注文',
        atc_order: 'ATC買い注文',
        btc_order: 'Bitcoin買い注文',
        cash_purchased: '現金購入',
        atc_purchased: 'ATC購入',
        btc_purchased: 'Bitcoin購入',
        removed_order_admin: '注文キャンセル（管理）',
        removed_order_user: '注文キャンセル',
        removed_purchase_admin: '購入キャンセル（管理）'
      };
      return statuses[cell] ? statuses[cell] : cell;
    }

    const styles = require('../VSPurchases/VSPurchases.scss');

    const {intl} = this.props;

    return (
      <div>
        <div className="container">
        <h2>
          <FormattedMessage
            id="VSConfigFrameView.Header"
            defaultMessage="Frame purchase history list"
          />
        </h2>
        <div className={classNames(styles.searchForm, {[styles.hidden]: !this.state.searchVisible })}>
          <div className={styles.wrapper}>
            <VSFramePurchaseHistoryListSearchForm onSubmit={this.onSearch} />
          </div>
        </div>
        <div style={{textAlign: 'center'}}>
          <button className={styles.hideButton} onClick={()=> this.setState({searchVisible: !this.state.searchVisible})}>
            <Glyphicon glyph={this.state.searchVisible ? 'arrow-up' : 'arrow-down'}/>
            &nbsp;
            {
              this.state.searchVisible ?
                <FormattedMessage
                  id="General.HideSearch"
                  defaultMessage="Hide search"
                /> :
                <FormattedMessage
                  id="General.ShowSearch"
                  defaultMessage="Show search"
                />
            }
          </button>
        </div>
          <div className={styles.blockContainer}>
            <Form horizontal>
              <FormGroup
                controlId="mainConfigFormCommisionCycle"
              >
                <Col xs={6}>
                  <FormGroup
                    controlId="endImideatly"
                  >
                    <Col xs={6}>
                      <FormattedMessage
                        id="General.EndImideatly"
                        defaultMessage="End immediately"
                      />
                    </Col>
                    <Col xs={6}>
                      <Button
                        disabled={this.props.frameData && this.props.frameData.status === 'finished'}
                        bsSize="large"
                        onClick={this.endImideatly}
                      >
                        <FormattedMessage
                          id="General.Execute"
                          defaultMessage="Execute"
                        />
                      </Button>
                    </Col>
                  </FormGroup>
                </Col>
                <Col xs={6}>
                  <FormGroup
                    controlId="ammount"
                  >
                    <Col xs={3}>
                      <ControlLabel>
                        <FormattedMessage
                          id="General.Ammount"
                          defaultMessage="Amount"
                        />
                      </ControlLabel>
                    </Col>
                    <Col xs={9}>
                      <FormControl value={this.state.frameAmount}
                                   type="number"
                                   disabled={this.props.frameData && this.props.frameData.status === 'finished'}
                                   placeholder={
                                     intl.formatMessage({id: 'General.Ammount', defaultMessage: 'Amount'})
                                   }
                                   onChange={this.onAmountChange}/>
                    </Col>
                  </FormGroup>
                  <FormGroup
                    controlId="purchased"
                  >
                    <Col xs={3}>
                      <ControlLabel>
                        <FormattedMessage
                          id="VSConfigFrameView.PurchasedAmount"
                          defaultMessage="Purchased amount"
                        />
                      </ControlLabel>
                    </Col>
                    <Col xs={9}>
                      <FormControl value={this.state.framePurchased}
                                   type="number"
                                   disabled
                                   placeholder={
                                     intl.formatMessage(messages.Purchased)
                                   } />
                    </Col>
                  </FormGroup>
                  <FormGroup
                    controlId="remain"
                  >
                  <Col xs={3}>
                      <ControlLabel>
                        <FormattedMessage
                          id="VSConfigFrameView.Remain"
                          defaultMessage="Remain amount"
                        />
                      </ControlLabel>
                    </Col>
                    <Col xs={9}>
                      <FormControl value={this.state.frameRemain}
                                   type="number"
                                   disabled
                                   placeholder={
                                     intl.formatMessage(messages.Purchased)
                                   } />
                    </Col>
                  </FormGroup>
                  <FormGroup
                    controlId="confirm"
                  >
                    <Col xs={12}>
                      <Button
                      disabled={this.props.frameData && this.props.frameData.status === 'finished'}
                      bsSize="large"
                      onClick={() => {this.saveFrame(this.state);}}
                      >
                        <FormattedMessage
                          id="General.Save"
                          defaultMessage="Save"
                        />
                      </Button>
                    </Col>
                  </FormGroup>
                </Col>
              </FormGroup>
            </Form>
          </div>
        </div>
            <BootstrapTable
          data={this.props.purchases || []}
          remote
          hover
          condensed
          pagination
          fetchInfo={ { dataTotalSize: this.props.totalDataSize } }
          options={ { sizePerPage: this.props.sizePerPage,
                        onPageChange: this.onPageChange,
                        sizePerPageList: [ 5, 10, 20],
                        pageStartIndex: 1,
                        page: this.props.currentPage,
                        // onSearchChange: this.onSearchChange,
                        onSortChange: this.onSortChange,
                        // onFilterChange: this.onFilterChange,
                        onSizePerPageList: this.onSizePerPageList,
                        hideSizePerPage: true,
                        noDataText: '表示されるデータがございません。'
                       }
                    }
        >
          <TableHeaderColumn dataField="createOrderDate" dataSort dataFormat={dateFormatter} dataAlign="center">
            <FormattedMessage
              id="VSConfigFrameView.CreationOrderDate"
              defaultMessage="Creation date"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="approveDate" dataSort dataFormat={dateFormatter} dataAlign="center">
            <FormattedMessage
              id="VSConfigFrameView.CreationApproveDate"
              defaultMessage="Approve date"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="status" dataSort dataFormat={statusFormatter} dataAlign="center">
            <FormattedMessage
              id="VSConfigFrameView.Status"
              defaultMessage="Status"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="purchaseAmount" dataSort dataFormat={cashFormatter} dataAlign="center" width="150">
            <FormattedMessage
              id="VSConfigFrameView.PurchaseAmount"
              defaultMessage="Purchase amount"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="purchaseBC" dataSort dataFormat={dollarFormatter} dataAlign="center" width="150">
            <FormattedMessage
              id="VSConfigFrameView.PurchaseBrave$"
              defaultMessage="Purchase BRAVE$"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="cashRateCreated" dataSort dataFormat={centFormatter} dataAlign="center" width="100">
            <FormattedMessage
              id="VSConfigFrameView.СashRateCreated"
              defaultMessage="Create Rate"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="purchaseQuantityCreated" dataSort dataFormat={vsFormatter} dataAlign="center" width="100">
            <FormattedMessage
              id="VSConfigFrameView.PurchaseVSCreated"
              defaultMessage="Create VS amount"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="cashRate" dataSort dataFormat={centFormatter} dataAlign="center" width="100">
            <FormattedMessage
              id="VSConfigFrameView.СashRate"
              defaultMessage="Approved Rate"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="purchaseQuantity" dataSort dataFormat={vsFormatter} dataAlign="center" width="100">
            <FormattedMessage
              id="VSConfigFrameView.PurchaseQuantity"
              defaultMessage="Approved VS amount"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="applicantID" dataSort dataAlign="center" dataFormat={memberIDFormatter}>
            <FormattedMessage
              id="VSConfigFrameView.MemberID"
              defaultMessage="Member ID"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="masterID" dataSort isKey dataAlign="center" dataFormat={this.purchaseIDFormatter}>
            <FormattedMessage
              id="VSConfigFrameView.PurchaseID"
              defaultMessage="Purchase ID"
            />
          </TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

export default injectIntl(VSConfigFrameView);
