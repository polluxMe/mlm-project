import React, {Component, PropTypes} from 'react';
// import Helmet from 'react-helmet';
import {connect} from 'react-redux';
import {load as loadFrames, removeLastFrame} from 'redux/modules/vsConfigFrameDetails';
// import * as purchasesActions from 'redux/modules/vsPurchases';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
// import { WidgetForm } from 'components';
// import { asyncConnect } from 'redux-async-connect';
import {injectIntl, intlShape, defineMessages, FormattedMessage, FormattedNumber} from 'react-intl';
import {bindActionCreators} from 'redux';
import { Link } from 'react-router';
// import { push } from 'react-router-redux';

const styles = require('./VSPurchaseDetails.scss');

require('../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css');

const messages = defineMessages({
  AreYouSureToDeleteThisFrame: {
    id: 'VSConfigFrameDetail.AreYouSureToDeleteThisFrame',
    defaultMessage: `Are you sure to delete this frame?`
  }
});

// const MIN_END_RATE = 35;

// @asyncConnect([{
//   deferred: true,
//   promise: ({store: {dispatch, getState}}) => {
//     if (!isLoaded(getState())) {
//       return dispatch(loadFrames(getState()));
//     }
//   }
// }])
@connect(
  state => ({
    data: state.vsConfigFrameDetails.data,
    editing: state.vsConfigFrameDetails.editing,
    error: state.vsConfigFrameDetails.error,
    loading: state.vsConfigFrameDetails.loading,
    loaded: state.vsConfigFrameDetails.loaded
  }),
  dispatch => bindActionCreators({loadFrames, removeLastFrame}, dispatch))
class VSConfigFrameDetails extends Component {
  static propTypes = {
    data: PropTypes.array,
    error: PropTypes.object,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    editing: PropTypes.object.isRequired,
    editStart: PropTypes.func.isRequired,
    load: PropTypes.func.isRequired,
    set: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    totalDataSize: PropTypes.number,
    sizePerPage: PropTypes.number,
    changeSort: PropTypes.func.isRequired,
    changePage: PropTypes.func.isRequired,
    changeSizePerPage: PropTypes.func.isRequired,
    changeFilter: PropTypes.func.isRequired,
    pushState: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    loadFrames: PropTypes.func.isRequired,
    removeLastFrame: PropTypes.func.isRequired,
    intl: intlShape.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {id: props.params && props.params.id ? props.params.id : null};
  }

  componentWillMount() {
    this.props.loadFrames(this.props.params.id);
  }

  // edit = (_id) => {
  //   this.props.pushState('/vs_config_frame/form/' + _id);
  //   console.log('_id', _id);
  // }

  onDeleteClick = () => {
    if (!confirm(this.props.intl(messages.AreYouSureToDeleteThisFrame))) return;
    this.props.removeLastFrame(this.props.params.id);
  };

  editFormatter(cell, row) {
    const statuses = {
      'selling': '販売中',
      'created': '未販売',
      'finished': '売買終了',
      'extra_finished': '売買終了'
    };

    if (row.status === 'created') {
      return (<span>{statuses[cell]}</span>);
    }

    return (
      <Link to={'/vs_config_frame/details/' + row._id + '/history'}>{statuses[cell]}</Link>
    );
  }

  render() {
    function dateFormatter(cell) {
      if (!cell) {
        return '-';
      }
      const date = new Date(cell);
      return ('0' + date.getDate()).slice(-2) + '-' +
        ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
        date.getFullYear();
    }

    function revenueFormatter(cell, row) {
      if (!row.purchaseQuantity) {
        return 0;
      }
      const res = row.purchaseQuantity * (row.purchaseRate / 100);
      return <span><FormattedNumber value={Math.floor(res)}/> {' B$'}</span>;
    }

    function centFormatter(cell) {
      if (!cell) {
        return <span>-</span>;
      }
      return <span>{cell + ' ¢'}</span>;
    }

    const deleteFormatter = (rate, row, additional, index) => {
      if (row.status !== 'created' || index !== this.props.data.length - 1 || index < 1 /* || +rate <= MIN_END_RATE */) {
        return null;
      }
      return (
        <a href="#" onClick={this.onDeleteClick}>
          <FormattedMessage
            id="VSConfig.Delete"
            defaultMessage="Delete"
          />
        </a>
      );
    };

    const numberFormatter = (cell) => {
      return <FormattedNumber value={cell}/>;
    };

    const trClassFormat = (rowData) => {
      return rowData.status === 'finished' ? styles.gray : '';
    };

    let result;
    // console.log(this.props.vsConfigFrameDetails);
    /* ReactDataGrid uses the document object, which is not available on server side */
    result = (
        <div>
          <div className="container">
            <h2>
              <FormattedMessage
                id="VSConfig.Header"
                defaultMessage="Manage purchase frame"
              />
            </h2>
          </div>
          <BootstrapTable
            data={this.props.data || []}
            remote
            hover
            condensed
            trClassName={trClassFormat}
            options={ {
              hideSizePerPage: true,
              noDataText: '表示されるデータがございません。'
            }}
          >
            <TableHeaderColumn dataField="startDate" dataSort dataFormat={dateFormatter} dataAlign="center">
              <FormattedMessage
              id="VSConfigFrameDetail.StartDate"
              defaultMessage="Start date"
              />
            </TableHeaderColumn>
              <TableHeaderColumn dataField="endDate" dataSort dataFormat={dateFormatter} dataAlign="center">
              <FormattedMessage
              id="VSConfigFrameDetail.EndDate"
              defaultMessage="End date"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="purchaseRate" isKey dataSort dataFormat={centFormatter} dataAlign="center">
              <FormattedMessage
              id="VSConfigFrameDetail.PurchaseRate"
              defaultMessage="Purchase Rate"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="purchaseFrame" dataFormat={numberFormatter} dataSort dataAlign="center">
              <FormattedMessage
              id="VSConfigFrameDetail.PurchaseFrame"
              defaultMessage="Purchase Frame"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="purchaseQuantity" dataFormat={numberFormatter} dataSort dataAlign="center">
              <FormattedMessage
              id="VSConfigFrameDetail.PurchaseQuantity"
              defaultMessage="Purchase Quantity"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="remainQuantity" dataFormat={numberFormatter} dataSort dataAlign="center">
              <FormattedMessage
              id="VSConfigFrameDetail.RemainQuantity"
              defaultMessage="Remain Quantity"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="revenue" dataSort dataFormat={revenueFormatter} dataAlign="center">
              <FormattedMessage
              id="VSConfigFrameDetail.Revenue"
              defaultMessage="Revenue"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="status" dataSort dataFormat={ this.editFormatter } dataAlign="center">
              <FormattedMessage
              id="VSConfigFrameDetail.Status"
              defaultMessage="Status"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="purchaseRate" dataFormat={ deleteFormatter } dataAlign="center">
              <FormattedMessage
                id="VSConfigFrameDetail.Delete"
                defaultMessage="Delete"
              />
            </TableHeaderColumn>
          </BootstrapTable>
        </div>
    );

    return result;
  }
}

export default injectIntl(VSConfigFrameDetails);

