import React, { Component, PropTypes } from 'react';
// import config from '../../config';
import {connect} from 'react-redux';
import {Board} from 'components';
import Helmet from 'react-helmet';
import { LinkContainer } from 'react-router-bootstrap';
import * as mainCommissionActions from 'redux/modules/mainCommission';
import * as rankCommissionActions from 'redux/modules/rankCommission';
import * as megaCommissionActions from 'redux/modules/megaCommission';
import * as cashRewardCommissionActions from 'redux/modules/cashRewardCommission';
import moment from 'moment';
import {FormattedNumber, FormattedMessage} from 'react-intl';

const commissionMapButtonImage = require('theme/mlm/images/commission-btn-map.png');

@connect(state => ({
  user: state.auth.user,
  mainCommission: state.mainCommission.item,
  rankCommission: state.rankCommission.item,
  megaCommission: state.megaCommission.item,
  cashRewardCommission: state.cashRewardCommission.item,
}),
  {
    loadMainCommission: mainCommissionActions.load,
    loadRankCommission: rankCommissionActions.load,
    loadMegaCommission: megaCommissionActions.load,
    loadCashRewardCommission: cashRewardCommissionActions.load,
  }
)
export default class Commissions extends Component {
  static propTypes = {
    user: PropTypes.object,
    mainCommission: PropTypes.object,
    rankCommission: PropTypes.object,
    megaCommission: PropTypes.object,
    cashRewardCommission: PropTypes.object,
    loadMainCommission: PropTypes.func,
    loadRankCommission: PropTypes.func,
    loadMegaCommission: PropTypes.func,
    loadCashRewardCommission: PropTypes.func,
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  componentDidMount() {
    const {user, loadMainCommission, loadRankCommission, loadMegaCommission, loadCashRewardCommission} = this.props;
    const year = moment().format('YYYY');
    const month = +moment().format('MM');
    loadMainCommission(user._id, {year, month});
    loadRankCommission(user._id, {year, month});
    loadMegaCommission(user._id, {year, month});
    loadCashRewardCommission(user._id, {year, month});
  }

  render() {
    const {mainCommission, rankCommission, megaCommission, cashRewardCommission} = this.props;
    const month = +moment().format('MM');
    return (
      <div id="wrap">
        <Helmet title="Commission"/>
        <div className="comission_detail" id="contents">
          <Board>
            <div>
              <LinkContainer to="/my_account/commissions/main_commission">
                <div className="main_com note_main">
                  <h2>
                    <FormattedMessage
                      id="Commissions.MainCommission"
                      defaultMessage="Main commission"
                    />
                  </h2>
                  <p className="tal">
                    {month}&nbsp;
                    <FormattedMessage
                      id="Commissions.Month"
                      defaultMessage="month"
                    />
                    <span className="val">
                      <FormattedNumber value={(mainCommission && mainCommission.amount) || 0} />
                      &nbsp;
                      VS
                    </span>
                  </p>
                </div>
              </LinkContainer>
              <LinkContainer to="/my_account/commissions/rank_commission">
                <div className="main_rank note_rank">
                  <h2>
                    <FormattedMessage
                      id="Commissions.RankCommission"
                      defaultMessage="Rank commission"
                    />
                  </h2>
                  <p className="tal">{month}&nbsp;
                    <FormattedMessage
                      id="Commissions.Month"
                      defaultMessage="month"
                    />
                    <span className="val">
                        <FormattedNumber value={(rankCommission && rankCommission.amount) || 0} />
                      &nbsp;
                      VS
                      </span>
                  </p>
                </div>
              </LinkContainer>
              <LinkContainer to="/my_account/commissions/mega_commission">
                <div className="main_mega note_mega">
                  <h2>
                    <FormattedMessage
                      id="Commissions.MegaMatchingBonus"
                      defaultMessage="Mega matching bonus"
                    />
                  </h2>
                  <p className="tal">{month}&nbsp;
                    <FormattedMessage
                      id="Commissions.Month"
                      defaultMessage="month"
                    />
                    <span className="val">
                        <FormattedNumber value={(megaCommission && megaCommission.amount) || 0} />
                      &nbsp;VS
                      </span>
                  </p>
                </div>
              </LinkContainer>
              <LinkContainer to="/my_account/commissions/cash_reward_commission">
                <div className="main_cash note_cash">
                  <h2>
                    <FormattedMessage
                      id="Commissions.CashReward"
                      defaultMessage="Cash reward"
                    />
                  </h2>
                  <p className="tal">{month}&nbsp;
                    <FormattedMessage
                      id="Commissions.Month"
                      defaultMessage="month"
                    />
                    <span className="val">
                        <FormattedNumber value={(cashRewardCommission && cashRewardCommission.amount) || 0} />
                      &nbsp;
                      <FormattedMessage
                        id="General.Yen"
                        defaultMessage="￥"
                      />
                      </span>
                  </p>
                </div>
              </LinkContainer>
              <div className="btn">
                <LinkContainer to="/my_account/commission/unilevelMap">
                  <img src={commissionMapButtonImage} width="100%" alt="買い注文"/>
                </LinkContainer>
              </div>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}
