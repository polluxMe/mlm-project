import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
// import config from '../../config';
import Helmet from 'react-helmet';
import { LinkContainer } from 'react-router-bootstrap';
// import {FormattedMessage} from 'react-intl';
import { Board } from 'components';
import {bindActionCreators} from 'redux';
import {loadWallet} from 'redux/modules/memberInfo';
import {FormattedMessage} from 'react-intl';

import 'theme/mlm/css/style.css';

const commissionImage = require('theme/mlm/images/account-btn-comition.png');
const basicInformationImage = require('theme/mlm/images/account-btn-info.png');

const virtualStockImage = require('theme/mlm/images/account-stock.png');
const ticketImage = require('theme/mlm/images/account-ticket.png');
const brave3WayStockImage = require('theme/mlm/images/account-3way.png');
const brave2WayStockImage = require('theme/mlm/images/account-2way.png');

@connect(state => ({
  user: state.auth.user,
  wallet: state.memberInfo.wallet
}),
  dispatch => bindActionCreators({loadWallet}, dispatch)
)
export default class MyAccount extends Component {
  static propTypes = {
    user: PropTypes.object,
    wallet: PropTypes.object,
    loadWallet: PropTypes.func,
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  componentWillMount() {
    this.props.loadWallet(this.props.user._id);
  }

  render() {
    return (
      <div id="wrap">
      <div className="account" id="contents">
        <Helmet title="MyAccount"/>
          <div id="my-account" className="clearfix">
            <dl id="my-id">
              <dt>ID</dt>
              <dd>{this.props.user && this.props.user.username || ''}</dd>
            </dl>
            <dl id="my-rank">
              <dt>
                <FormattedMessage
                  id="MyPage.RANK"
                  defaultMessage="RANK"
                />
              </dt>
              <dd>{this.props.user && this.props.user.rank || 0}</dd>
            </dl>
          </div>
          <div className="crearfix mb10 btn">
            <div>
              <LinkContainer to="my_account/commissions">
                <img src={commissionImage} alt="Commission"/>
              </LinkContainer>
            </div>
            <div>
              <LinkContainer to="my_account/edit">
                <img src={basicInformationImage} alt="Edit"/>
              </LinkContainer>
            </div>
          </div>

          <Board >
              <LinkContainer to="/my_account/virtual_stock">
                <div className="stock base">
                  <img src={virtualStockImage} alt="Virtual Stock"/>
                  <span>{this.props.wallet ? this.props.wallet.vsAmount : 0}</span>
                </div>
              </LinkContainer>
              <div className="stock base">
                <img src={ticketImage} alt="Ticket"/>
                0 $
              </div>
              <div className="stock base">
                <img src={brave3WayStockImage} alt="Brave 3 way"/>
                {this.props.user.commision ? Math.floor(this.props.user.commision / 2) + ' B$' : 0 + ' B$'}
              </div>
              <div className="stock base">
                <img src={brave2WayStockImage} alt="Brave 2 way"/>
                <span>0 B$</span>
              </div>
          </Board>
        </div>
      </div>
    );
  }
}

/*

 */
