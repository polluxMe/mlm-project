import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
// import Helmet from 'react-helmet';
import D3Tree from './D3Tree';
import {bindActionCreators} from 'redux';
import * as mapActions from 'redux/modules/map';
import dimensions from 'react-dimensions';
import {
  Button
} from 'react-bootstrap';
import {FormattedMessage} from 'react-intl';
import { Board } from 'components';

const images = {
  'blue': require('./img/blue_map_member.png'),
  'gray': require('./img/gray_map_member.png'),
  'red': require('./img/red_map_member.png'),
  'yellow': require('./img/yellow_map_member.png'),
  'orange': require('./img/orange_map_member.png')
};

@connect(
  state => ({
    user: state.auth.user,
    map: state.map.data,
    loaded: state.map.loaded,
    left: state.map.left,
    right: state.map.right,
    top: state.map.top,
    bottom: state.map.bottom
  }),
  dispatch => bindActionCreators(mapActions, dispatch)
)
class Map extends Component {
  static propTypes = {
    user: PropTypes.object,

    map: PropTypes.array,
    loaded: PropTypes.bool.isRequired,
    load: PropTypes.func.isRequired,

    left: PropTypes.number.isRequired,
    right: PropTypes.number.isRequired,
    top: PropTypes.number.isRequired,
    bottom: PropTypes.number.isRequired,

    shiftLeft: PropTypes.func.isRequired,
    shiftRight: PropTypes.func.isRequired,
    shiftTop: PropTypes.func.isRequired,
    shiftBottom: PropTypes.func.isRequired,

    containerWidth: PropTypes.number.isRequired,
    containerHeight: PropTypes.number.isRequired,
    location: PropTypes.object.isRequired
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  componentDidMount() {
    const {user, load} = this.props;
    const userId = user && user._id;
    load(userId);

    setTimeout(() => {
      document.querySelector('.parentBlock').scrollLeft += 100000;
      const scroll = document.querySelector('.parentBlock').scrollLeft;
      document.querySelector('.parentBlock').scrollLeft = (scroll / 2);
    }, 1000);
  }

  render() {
    require('./Map.css');
    const styles = require('../UnilevelMap/UnilevelMap.scss');
    const {map, right, top, bottom, loaded, user} = this.props;

    // const containerWidth = this.props.containerWidth - 60;
    // const containerHeight = this.props.containerHeight - 60;

    const xCount = map && map[0] && map[0].maxWidth || 8;
    const yCount = map && map[0] && map[0].maxDepth || 4;

    let multiplier = xCount / yCount;
    let multiplierY = 1;
    if (multiplier < 1) {
      multiplierY = yCount / xCount;
      multiplier = 1;
    }


    const svgWidth = xCount * multiplierY * 150;
    const svgHeight = yCount * multiplier * 150;

    const stl = {
      // left,
      right,
      top,
      bottom,
      width: svgWidth,
      height: svgHeight
    };

    // if (containerWidth < svgWidth) {
    //   stl.left -= (svgWidth - containerWidth) / 2;
    // }

    // const isShiftRightButtonDisabled = (svgWidth + stl.left) < containerWidth;
    // const isShiftBottomButtonDisabled = (svgHeight + stl.top) < containerHeight;
    //
    // const isShiftLeftButtonDisabled = stl.left >= 0;
    // const isShiftTopButtonDisabled = stl.top >= 0;
    const userId = user && user._id;

    return (
      <div id="wrap">
        <div id="contents">
          <Board>
            <div className={'parentBlock ' + styles.parent}>
              <p className="text-center" style={{'marginLeft': (svgWidth / 2) - 30}}>
                <Button bsStyle="primary" href={'/my_account/commission/unilevelMap?id=' + userId}>
                  <FormattedMessage
                    id="Map.SwitchMap"
                    defaultMessage="Switch Map"
                  />
                </Button>
              </p>
              <div className="block" style={stl}>
                {loaded ? <D3Tree treeData={map} svgWidth={svgWidth} svgHeight={svgHeight} images={images} rootId={userId} /> :
                  <FormattedMessage
                    id="General.Loading"
                    defaultMessage="Loading..."
                  />}
              </div>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}

module.exports = dimensions()(Map);
