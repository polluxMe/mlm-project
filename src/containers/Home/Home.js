import React, { Component } from 'react';

import {
  EventsBlock,
  AnnouncementNewsBlock,
  MembershipBlock
} from '../../components';
import Helmet from 'react-helmet';
import {
  Grid,
  Col
} from 'react-bootstrap';

export default class Home extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const styles = require('./Home.scss');
    // require the logo image both from client and server
    // const logoImage = require('./logo.png');
    return (
      <div className={styles.home}>
        <Helmet title="Home"/>
        <div className={styles.masthead}></div>

        <div className="container">
          <div className="col-md-4">
            <EventsBlock />
          </div>
          <div className="col-md-4">
            <AnnouncementNewsBlock />
          </div>
          <div className="col-md-4">
            <MembershipBlock />
          </div>
        </div>

        <Grid>
          <Col xs={12}>
            <section id="affiriate">
              <h2><img src="./img/icon-yen.png" alt="" />The next generation affiliate</h2>
              <div>
                <div><img src="./img/affiriate.jpg" alt="The next generation affiliate" /></div>
                <div>
                  <span>End-user </span><span>120 million people </span><span>expanding profit as possible because to charge each user</span><br />
                </div>
              </div>
              <div>
                <img src="./img/sp-affiriate.jpg" alt="Expanding profit as possible because to charge the end-user 120 million people each user" />
              </div>
              <p>Virtual stock and is intended to share the ownership of the BRAVE business issued by the TOP SQUARE.</p>

              <div id="hikaku">
                <div id="kabu">
                  <h3>★ comparison with the stock</h3>
                  <table>
                    <thead>
                    <tr>
                      <th></th>
                      <th><img src="./img/aff-th01.png" alt="Virtual currency" /></th>
                      <th><img src="./img/aff-th02.png" alt="stock" /></th>
                      <th><img src="./img/aff-th03.png" alt="FX" /></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <th>type</th>
                      <td><b>About 700 species</b></td>
                      <td>About 3,500</td>
                      <td>About 50 to 60</td>
                    </tr>
                    <tr>
                      <th>Traded funds</th>
                      <td>Small</td>
                      <td>Big</td>
                      <td>Small</td>
                    </tr>
                    <tr>
                      <th>Trading hours</th>
                      <td>24hours</td>
                      <td>9:00 to 5</td>
                      <td>24hours</td>
                    </tr>
                    <tr>
                      <th>Fluctuation range</th>
                      <td>Big</td>
                      <td>small</td>
                      <td>small</td>
                    </tr>
                    <tr>
                      <th>Profit</th>
                      <td>Since the price action is more violent stock · FX chance there. <br />Easy profitable idea to buy the presale period</td>
                      <td>Since the price action is more violent FX chance there. <br />If you leave purchase before publishing easy profitable.</td>
                      <td>Price action is unlikely to issue a profit and not to apply if funds are less leveraged because of the stable.</td>
                    </tr>
                    </tbody>
                  </table>
                </div>

                <div id="system">
                  <h3>★ millionaire graduates system</h3>
                  <div><img src="./img/aff-image.png" alt="Usually, increases profits to take the commission to the securities company of intermediation is taken by the TOP SQUARE" /></div>
                </div>
              </div>
            </section>
          </Col>
        </Grid>
      </div>
    );
  }
}
