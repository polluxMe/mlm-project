import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import * as brave2WayHistoryActions from 'redux/modules/brave2WayHistory';
import Helmet from 'react-helmet';
import { Board } from 'components';
import {bindActionCreators} from 'redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import {FormattedMessage, FormattedNumber} from 'react-intl';
import moment from 'moment';

// require('../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css');

const brave2WayTitleImage = require('theme/mlm/images/3way-title.png');

@connect(state => ({
  user: state.auth.user,
  requests: state.brave2WayHistory.data,
  error: state.brave2WayHistory.error,
  loading: state.brave2WayHistory.loading,
  loaded: state.brave2WayHistory.loaded,
  totalDataSize: state.brave2WayHistory.totalDataSize,
  currentPage: state.brave2WayHistory.currentPage,
  sizePerPage: state.brave2WayHistory.sizePerPage,
}),
  dispatch => bindActionCreators({...brave2WayHistoryActions}, dispatch)
)
export default class Brave2WayHistory extends Component {
  static propTypes = {
    user: PropTypes.objrect,
    wallet: PropTypes.object,
    requests: PropTypes.array,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    load: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    totalDataSize: PropTypes.number,
    sizePerPage: PropTypes.number,
  };

  componentWillMount() {
    this.props.load();
  }

  render() {
    function dateFormatter(cell) {
      return moment(cell).format('YYYY/MM/DD');
    }

    function numberFormatter(cell) {
      return (<span><FormattedNumber value={cell}/>&nbsp;B$</span>);
    }


    return (
      <div id="wrap">
        <Helmet title="Brave 2 way"/>
        <div className="one-way" id="contents">
          <Board>
            <h2 className="mb10"><img src={brave2WayTitleImage} alt="BRAVE$ 2Way"/></h2>
            <BootstrapTable
              data={this.props.requests || []}
              remote
              hover
              condensed
              pagination
              fetchInfo={ { dataTotalSize: this.props.totalDataSize } }
              options={ { sizePerPage: this.props.sizePerPage,
                onPageChange: this.onPageChange,
                sizePerPageList: [ 5, 10, 20],
                pageStartIndex: 1,
                page: this.props.currentPage,
                // onSearchChange: this.onSearchChange,
                onSortChange: this.onSortChange,
                // onFilterChange: this.onFilterChange,
                onSizePerPageList: this.onSizePerPageList,
                hideSizePerPage: true,
                noDataText: '表示されるデータがございません。'
              }}
            >
              <TableHeaderColumn dataField="createDate" isKey dataFormat={dateFormatter} dataAlign="center" editable={false}>
                <FormattedMessage
                  id="Brave2WayHistory.CreationDate"
                  defaultMessage="Creation date"
                />
              </TableHeaderColumn>
              <TableHeaderColumn dataField="status" dataAlign="center" editable={false}>
                <FormattedMessage
                  id="Brave2WayHistory.Status"
                  defaultMessage="Status"
                />
              </TableHeaderColumn>
              <TableHeaderColumn dataField="brave2WayPurchaseAmount" dataFormat={numberFormatter} dataAlign="center" editable={false}>
                <FormattedMessage
                  id="Brave2WayHistory.brave2WayPurchaseAmount"
                  defaultMessage="Purchase amount"
                />
              </TableHeaderColumn>
            </BootstrapTable>
          </Board>
        </div>
      </div>
    );
  }
}

