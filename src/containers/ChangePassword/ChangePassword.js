import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {FormattedMessage} from 'react-intl';
import {Form, FormGroup, FormControl, ControlLabel, Button, HelpBlock} from 'react-bootstrap';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import ChangePasswordValidation from './ChangePasswordValidation';
import { replace as replaceRoute } from 'react-router-redux';
import {bindActionCreators} from 'redux';
import config from '../../config.js';

const Input = ({type, placeholder, input, disabled, help, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <FormControl type={type} {...input} placeholder={placeholder} disabled={disabled} />
    </div>
    {help && <HelpBlock>{help}</HelpBlock> }
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;


@reduxForm({
  form: 'changePassword',
  validate: ChangePasswordValidation
})
@connect(
  state => ({user: state.auth.user}),
  dispatch => bindActionCreators({replaceRoute}, dispatch)
)
export default class ChangePassword extends Component {
  static propTypes = {
    location: PropTypes.object,
    replaceRoute: PropTypes.func
  };

  submit = (data) => {
    const {location} = this.props;
    return Promise.resolve()
      .then(() => {
        data.token = location.query.token;

        const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);

        return fetch(apiPath + '/api/auth/change_password', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          credentials: 'include',
          mode: 'cors',
          body: JSON.stringify(data)
        })
          .then((response) => response.json())
          .then((response) => {
            if (response.code === 500) {
              throw new SubmissionError({_error: response.msg});
            } else {
              this.props.replaceRoute('/login');
            }
          })
          .catch((err) => {
            throw new SubmissionError({_error: (err.errors && err.errors._error)});
          });
      });
  };

  render() {
    /* eslint-disable */
    const {
      handleSubmit,
      valid,
      error
    } = this.props;
    /* eslint-disable */

    const styles = require('./ChangePassword.scss');
    return (
      <div className={styles.loginPage + ' container'}>
        <Helmet title="Login"/>
        <div className={'row ' + styles.mTLg}>
          <div className="col-sm-12">
            <div className="panel panel-default panel-border-color panel-border-color-warning">
              <div className="panel-heading panel-heading-divider">
                  <p>
                    <FormattedMessage
                      id="ChangePassword.ChangePassword"
                      defaultMessage="Please select your new password"
                    />
                  </p>
                </div>
                <div className="panel-body">
                  <Form onSubmit={handleSubmit(this.submit)}>
                    <FormGroup
                      controlId="password"
                    >
                      <ControlLabel>
                        <FormattedMessage
                          id="LoginFlow.Password"
                          defaultMessage="Password"
                        />
                      </ControlLabel>
                      <Field name="password"
                             component={Input}
                             type="password"
                             help={
                               <FormattedMessage
                                 id="LoginFlow.PasswordHelp"
                                 defaultMessage="* Password, 8 to 16-digit alphanumeric"
                               />
                             }
                      />
                      <FormControl.Feedback />
                    </FormGroup>
                    <FormGroup
                      controlId="password"
                    >
                      <ControlLabel>
                        <FormattedMessage
                          id="LoginFlow.RepeatRassword"
                          defaultMessage="Repeat password"
                        />
                      </ControlLabel>
                      <Field
                        name="repeatPassword"
                        component={Input}
                        type="password"
                      />
                    </FormGroup>
                    <div style={{textAlign: 'center'}}>
                      {error && <div className="text-danger">{error}</div>}
                      <Button
                        bsSize="large"
                        disabled={!valid}
                        onClick={handleSubmit(this.submit)}
                      >
                        <FormattedMessage
                          id="ChangePassword.Submit"
                          defaultMessage="Submit"
                        />
                      </Button>
                    </div>
                  </Form>
                </div>
              </div>
            </div>
          </div>
      </div>
    );
  }
}
