const validate = (values) => {
  const errors = {};

  if (!values.password) {
    errors.password = '入力してください。';
  } else if (values.password.length < 8 || values.password.length > 16) {
    errors.password = 'Password must contain from 8 to 16 characters';
  } else if (!/[a-ｚ]/.test(values.password)) {
    errors.password = 'Password must contain only alphabet half-size characters';
  }

  if (!values.repeatPassword) {
    errors.repeatPassword = '入力してください。';
  } else if (values.password !== values.repeatPassword) {
    errors.repeatPassword = 'Password must match';
  }

  return errors;
};


export default validate;
