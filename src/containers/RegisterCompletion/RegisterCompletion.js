import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import config from '../../config';

import {FormattedMessage} from 'react-intl';

@connect(
  (state) => ({
    tempUser: state.register.tempUser
  })
)
export default class RegisterCompletion extends Component {
  static propTypes = {
    tempUser: PropTypes.object,
  };

  render() {
    const styles = require('./RegisterCompletion.scss');
    const tempUser = this.props.tempUser || {username: 'tmp', id: 'tmp', password: 'tmp'};
    return (
      <div className={styles.wrapper}>
        <div className="row">
          <div className="col-sm-12">
            <div className="panel panel-default panel-border-color panel-border-color-warning">
              <div className="panel-heading panel-heading-divider">
                <h3>
                  {` ${tempUser.username}`}
                  <FormattedMessage
                    id="LoginFlow.DearMr"
                    defaultMessage="Dear Mr."
                  />
                </h3>
              </div>
              <div className="panel-body">
                <p>
                  <FormattedMessage
                    id="LoginFlow.ThankYouForRegisteringThusURLBelowWillBeYoureMyPage"
                    defaultMessage="Thank you for registering, this URL below will be you're login page"
                  />
                </p>
                <a href={config.siteUrl + '/registration/temp_login'} target="_blank">
                  http://brave.works/temp-login
                </a>
                <p>BRAVE ID: {tempUser.id}</p>
                <p>仮パスワード: {tempUser.password}</p>

                <div className={styles.qr}></div>
            </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
