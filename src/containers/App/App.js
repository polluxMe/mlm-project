import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { isLoaded as isAuthLoaded, load as loadAuth, logout } from 'redux/modules/auth';
import { push } from 'react-router-redux';
import config from '../../config';
import { asyncConnect } from 'redux-async-connect';
import {FormattedMessage} from 'react-intl';
import cookie from 'react-cookie';
import {default as Sidebar} from 'react-sidebar';
import SidebarContent from './sidebar_content';
import MaterialTitlePanel from './material_title_panel';
import {loadWallet} from 'redux/modules/memberInfo';

const logo = require('../Home/logo.png');

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];

    if (!isAuthLoaded(getState())) {
      promises.push(dispatch(loadAuth()));
    }

    return Promise.all(promises);
  }
}])
@connect(
  state => ({user: state.auth.user}),
  {logout, pushState: push, loadWallet})
export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    user: PropTypes.object,
    logout: PropTypes.func.isRequired,
    pushState: PropTypes.func.isRequired,
    location: PropTypes.object.isRequired,
    loadWallet: PropTypes.func,
  };

  static contextTypes = {
    store: PropTypes.object.isRequired,
    intl: PropTypes.object.isRequired
  };

  static childContextTypes = {
    intl: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {sidebarOpen: false};
  }

  getChildContext() {
    return {
      intl: this.context.intl
    };
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.user && nextProps.user) {
      console.info('login', nextProps.user);

      this.props.loadWallet(nextProps.user._id)
        .then((response) => {
          cookie.save('vsAmount', response.vsAmount, {path: '/', domain: '.brave.works'});
          cookie.save('username', nextProps.user.username, {path: '/', domain: '.brave.works'});
          cookie.save('rank', nextProps.user.rank, {path: '/', domain: '.brave.works'});

          // login
          let redirectTo = '/my_page';
          if (!nextProps.user.isChangeId) {
            redirectTo = '/registration/change_id';
            if (window) {
              console.log('redirectTo', redirectTo);
              window.location = redirectTo;
            } else {
              this.props.pushState(redirectTo);
            }
          } else {
            if (window) {
              window.location = redirectTo;
            }
          }
        });
    } else if (this.props.user && !nextProps.user) {
      console.info('logout', nextProps.user);

      cookie.remove('vsAmount', {path: '/', domain: '.brave.works'});
      cookie.remove('username', {path: '/', domain: '.brave.works'});
      cookie.remove('rank', {path: '/', domain: '.brave.works'});
      // logout
      this.props.pushState('/');
    }
  }

  render() {
    require('theme/css/style.css');

    const {user, location} = this.props;

    const styles = require('./App.scss');

    const self = this;

    function onSetOpen(open) {
      self.setState({sidebarOpen: open});
    }

    function menuButtonClick(ev) {
      ev.preventDefault();
      onSetOpen(!self.state.sidebarOpen);
    }

    const sidebar = <SidebarContent user={user} logout={this.props.logout} location={location} onSetOpen={onSetOpen} />;

    const contentHeader = (
      <div>
        {!this.state.docked &&
        <a onClick={menuButtonClick} href="#" className={styles.navbarToggle}>
        </a>}
        <h1 className={styles.headerImg}><img src={logo} width="125" alt="BRAVE" /></h1>
      </div>);

    const sidebarProps = {
      sidebar: sidebar,
      docked: this.state.docked,
      sidebarClassName: 'custom-sidebar-class',
      open: this.state.sidebarOpen,
      touch: false,
      shadow: true,
      pullRight: false,
      touchHandleWidth: 50,
      dragToggleDistance: 20,
      transitions: true,
      onSetOpen: onSetOpen
    };

    return (
      <div className={styles.app}>
        <Helmet {...config.app.head}/>
        <Sidebar {...sidebarProps}>
          <MaterialTitlePanel title={contentHeader}>
            <div className={styles.appContent}>
              {this.props.children}
            </div>

            <div className="well text-center" style={{marginBottom: 0}}>
              <FormattedMessage
                id="App.Copyright"
                defaultMessage="Copyright (C) 2016 BRAVE portal. All Rights Reserved."
              />
            </div>
          </MaterialTitlePanel>
        </Sidebar>
      </div>
    );
  }
}
