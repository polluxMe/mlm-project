import React from 'react';
import MaterialTitlePanel from './material_title_panel';
import { LinkContainer } from 'react-router-bootstrap';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import {FormattedMessage} from 'react-intl';

const navi04icon = require('../../theme/mlm/images/navi04icon.png');
const navi05icon = require('../../theme/mlm/images/navi05icon.png');
const navi06icon = require('../../theme/mlm/images/navi06icon.png');
const navi07icon = require('../../theme/mlm/images/navi07icon.png');
const navi08icon = require('../../theme/mlm/images/navi08icon.png');
const navi09icon = require('../../theme/mlm/images/navi09icon.png');

const styles = {
  sidebar: {
    width: 256,
    height: 'calc(100vh - 100px)',
  },
  sidebarLink: {
    display: 'block',
    padding: '16px 0px',
    color: '#757575',
    textDecoration: 'none',
  },
  divider: {
    margin: '8px 0',
    height: 1,
    backgroundColor: '#757575',
  },
  content: {
    padding: '0',
    minHeight: '100%',
    backgroundColor: 'white',
  }
};

const SidebarContent = (props) => {
  const style = props.style ? {...styles.sidebar, ...props.style} : styles.sidebar;

  const {user, logout, location, onSetOpen} = props;

  const handleLogout = (event) => {
    event.preventDefault();
    console.log(user);
    logout();
  };

  const isLoginAndRegistrationLinksEnabled = location && location.pathname !== '/registration/temp_login' && location.pathname !== '/registration/change_id';

  return (
    <MaterialTitlePanel title="" style={style}>
      <div style={styles.content} className="navigation">
        <Nav stacked>
          {!user && isLoginAndRegistrationLinksEnabled &&
          <LinkContainer to="/login" onClick={() => onSetOpen(false)}>
            <NavItem eventKey={1}>
              <FormattedMessage
                id="App.Login"
                defaultMessage="Login"
              />
            </NavItem>
          </LinkContainer>}
          {!user && isLoginAndRegistrationLinksEnabled &&
          <LinkContainer to="/registration/brave" onClick={() => onSetOpen(false)}>
            <NavItem eventKey={2}>
              <FormattedMessage
                id="App.BRAVERegistration"
                defaultMessage="BRAVE registration"
              />
            </NavItem>
          </LinkContainer>}
          {user && user.isChangeId &&
          <LinkContainer to="/my_page" onClick={() => onSetOpen(false)}>
            <NavItem eventKey={6}>
              <FormattedMessage
                id="App.TOP"
                defaultMessage="TOP"
              />
            </NavItem>
          </LinkContainer>}
          {user && user.isChangeId &&
          <LinkContainer to="/my_account/edit" onClick={() => onSetOpen(false)}>
            <NavItem eventKey={6}>
              <FormattedMessage
                id="App.BasicInformation"
                defaultMessage="Basic information"
              />
            </NavItem>
          </LinkContainer>}
          {user && user.isChangeId &&
          <LinkContainer to="/my_account/commissions" onClick={() => onSetOpen(false)}>
            <NavItem eventKey={7}>
              <FormattedMessage
                id="App.Commissions"
                defaultMessage="Commissions"
              />
            </NavItem>
          </LinkContainer>
          }
          {user && user.isChangeId &&
          <LinkContainer to="/my_account" onClick={() => onSetOpen(false)}>
            <NavItem eventKey={8}>
              <FormattedMessage
                id="App.MyAccount"
                defaultMessage="My Account"
              />
            </NavItem>
          </LinkContainer>}
          {user && user.isChangeId &&
            <NavItem eventKey={9}>
              <img src={navi04icon} width={40} />
              <FormattedMessage
                id="App.GameMember"
                defaultMessage="Game member"
              />
            </NavItem>}
          {user && user.isChangeId &&
            <NavItem eventKey={10}>
              <img src={navi05icon} width={40} />
              <FormattedMessage
                id="App.GamePlatform"
                defaultMessage="Game platform"
              />
            </NavItem>}
          {user && user.isChangeId &&
            <NavItem eventKey={11}>
              <img src={navi06icon} width={40} />
              <FormattedMessage
                id="App.Shopping"
                defaultMessage="Shopping"
              />
            </NavItem>}
          {user && user.isChangeId &&
            <NavItem eventKey={12}>
              <img src={navi07icon} width={40} />
              <FormattedMessage
                id="App.Reinvest"
                defaultMessage="Re-Invest"
              />
            </NavItem>}
          {user && user.isChangeId &&
            <NavItem eventKey={13}>
              <img src={navi08icon} width={40} />
              <FormattedMessage
                id="App.MoneyRequest"
                defaultMessage="Money request"
              />
            </NavItem>
          }
          {user && user.isChangeId &&
            <NavItem eventKey={14}>
              <img src={navi09icon} width={40} />
              <FormattedMessage
                id="App.Market"
                defaultMessage="Market"
              />
            </NavItem>}
          {user && user.isChangeId &&
          <LinkContainer to="/invitation" onClick={() => onSetOpen(false)}>
            <NavItem eventKey={4}>
              <FormattedMessage
                id="App.Invitation"
                defaultMessage="Invitation"
              />
            </NavItem>
          </LinkContainer>}
          {user &&
          <LinkContainer to="/logout" onClick={() => onSetOpen(false)}>
            <NavItem eventKey={5} className="logout-link" onClick={handleLogout}>
              <FormattedMessage
                id="App.Logout"
                defaultMessage="Logout"
              />
            </NavItem>
          </LinkContainer>}
        </Nav>
      </div>
    </MaterialTitlePanel>
  );
};

SidebarContent.propTypes = {
  user: React.PropTypes.object,
  logout: React.PropTypes.func.isRequired,
  onSetOpen: React.PropTypes.func.isRequired,
  style: React.PropTypes.object,
  location: React.PropTypes.object,
  children: React.PropTypes.array
};

export default SidebarContent;
