import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {bindActionCreators} from 'redux';
import * as authActions from 'redux/modules/auth';
import {Button} from 'react-bootstrap';
// import { browserHistory } from 'react-router';
// import { saveUser } from 'redux/modules/auth';
import { push as replaceRoute } from 'react-router-redux';
import { LoginForm } from 'components';
import {FormattedMessage} from 'react-intl';
import cookie from 'react-cookie';
import config from '../../config';

@connect(
  state => ({user: state.auth.user}),
  dispatch => bindActionCreators({...authActions, replaceRoute}, dispatch)
)
export default class Login extends Component {
  static propTypes = {
    user: PropTypes.object,
    login: PropTypes.func,
    logout: PropTypes.func,
    saveUser: PropTypes.func,
    replaceRoute: PropTypes.func,
    location: PropTypes.object
  };

  componentWillMount() {
    const {location} = this.props;
    const query = location && location.query;

    if (query && query.introducerID && query.introducerName) {
      cookie.save('introducerID', query.introducerID, {path: '/', domain: config.apiHost});
      cookie.save('introducerName', query.introducerName, {path: '/', domain: config.apiHost});
    }

    if (this.props.user) {
      this.props.replaceRoute('/my_page?reload=true');
    }
  }

  onSubmit = (response) => {
    this.props.saveUser(response);
    this.props.replaceRoute('/my_page?reload=true');
  };

  render() {
    const {user, logout, saveUser} = this.props;
    const styles = require('./Login.scss');
    return (
      <div className={styles.loginPage + ' container'}>
        <Helmet title="Login"/>
        <div className={'row ' + styles.mTLg}>
          <div className="col-sm-12">
            <div className="panel panel-default panel-border-color panel-border-color-warning">
              <div className="panel-heading panel-heading-divider">
                <h1>
                  <FormattedMessage
                    id="Login.Header"
                    defaultMessage="Login"
                  />
                </h1>
              </div>
              <div className="panel-body">
                {!user &&
                <LoginForm onSubmit={saveUser}/>
                }
                {user &&
                <div>
                  <p>You are currently logged in as {user && user.username}.</p>

                  <div style={{textAlign: 'center'}}>
                    <Button onClick={logout}><i className="fa fa-sign-out"/>{' '}Log Out</Button>
                  </div>
                </div>
                }
                </div>
              </div>
            </div>
          </div>
      </div>
    );
  }
}
