import memoize from 'lru-memoize';

const validate = (values) => {
  const errors = {};
  if (!values.bravePurchased || values.bravePurchased <= 0) {
    errors.bravePurchased = '必須';
  }
  if (!values.brave3WayPurchaseAmount || values.brave3WayPurchaseAmount <= 0) {
    errors.brave3WayPurchaseAmount = '必須';
  }
  if (values.brave3WayPurchaseAmount > values.bravePurchased) {
    errors.brave3WayPurchaseAmount = '最初のフィールドを超えることはできません';
  }

  return errors;
};

export default memoize(10)(validate);
