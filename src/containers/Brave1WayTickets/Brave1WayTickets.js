import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import { Board } from 'components';
import {bindActionCreators} from 'redux';
import {loadWallet} from 'redux/modules/memberInfo';
import {Form, FormControl, InputGroup, Button} from 'react-bootstrap';
import {reduxForm, Field} from 'redux-form';
import Brave1WayTicketsValidation from './Brave1WayTicketsValidation';
import config from '../../config.js';
import { Spinner } from 'components';

const brave1WayTitleImage = require('theme/mlm/images/1way-title.png');
const brave1WayBtnTransfer = require('theme/mlm/images/1way-btn-trancefer.png');

const Input = ({type, placeholder, input, readOnly, suffix, min, max, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <InputGroup>
        <FormControl type={type} {...input} placeholder={placeholder} readOnly={readOnly} min={min} max={max} />
        {suffix && <InputGroup.Addon>{suffix}</InputGroup.Addon>}
      </InputGroup>
    </div>
    {!readOnly && error && touched && <div className="text-danger">{error}</div>}
  </div>;


@reduxForm({
  form: 'brave1WayTickets',
  validate: Brave1WayTicketsValidation,
  enableReinitialize: true
})
@connect(state => ({
  user: state.auth.user,
  wallet: state.memberInfo.wallet
}),
  dispatch => bindActionCreators({loadWalletAction: loadWallet}, dispatch)
)
export default class Brave1WayTickets extends Component {
  static propTypes = {
    user: PropTypes.object,
    wallet: PropTypes.object,
    loadWalletAction: PropTypes.func,
    initialize: PropTypes.func,
    valid: PropTypes.bool,
    submitting: PropTypes.bool,
    pristine: PropTypes.bool,
    handleSubmit: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {isSent: false};
  }

  componentWillMount() {
    const {loadWalletAction, user} = this.props;
    loadWalletAction(user._id)
      .then(() => {
        this.props.initialize({
          bravePurchased: this.props.wallet && this.props.wallet.bravePurchased
        });
      });
  }

  submit = (data) => {
    this.setState({...this.state, isSent: false});

    data.memberID = this.props.user._id;

    const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);
    return fetch(apiPath + '/api/brave1way_requests', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({...data})
    })
      .then((response) => response.json())
      .then(() => {
        this.setState({...this.state, isSent: true});
        this.props.initialize({
          bravePurchased: this.props.wallet && this.props.wallet.bravePurchased
        });
      })
      .catch(error => {
        console.error(error);
      });
  };

  render() {
    const styles = require('./Brave1WayTickets.scss');

    const {wallet, valid, submitting, handleSubmit, pristine} = this.props;

    return (
      <div id="wrap">
        <Helmet title="Brave 1 way"/>
        <div className="one-way" id="contents">
          <Board>
            <h2 className="mb10"><img src={brave1WayTitleImage} alt="BRAVE$ 1WAY"/></h2>
            <div className="box">
              <h3>チケットへ変換</h3>
              <div className="inner">

                <Form horizontal>
                  <table>
                    <tbody>
                    <tr>
                      <th>保有1WAY</th>
                      <td className="tar">
                        <Field name="bravePurchased" type="number" readOnly component={Input} min={1} placeholder="0" suffix="B$" />
                      </td>
                    </tr>
                    <tr>
                      <th className="no-border">変換額</th>
                      <td className="no-border">
                        <Field name="brave1WayPurchaseAmount" type="number" component={Input} min={1} max={wallet && wallet.bravePurchased} placeholder="0" suffix="B$" />
                      </td>
                    </tr>
                    </tbody>
                  </table>

                  <div className="tac mt10 mb10">
                    <Button
                      disabled={!valid || submitting}
                      className={styles.button}
                      onClick={handleSubmit(this.submit)}
                    >
                      <Spinner loading={submitting} size={25} exclusive>
                        <img src={brave1WayBtnTransfer} width="79" alt="変換" />
                      </Spinner>
                    </Button>
                    {this.state.isSent && pristine && <p style={{color: 'green'}}>
                      リクエストが送信されました。ありがとうございます。
                    </p>}
                  </div>

                </Form>
              </div>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}

