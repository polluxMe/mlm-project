import memoize from 'lru-memoize';

const validate = (values) => {
  const errors = {};
  if (!values.bravePurchased || values.bravePurchased <= 0) {
    errors.bravePurchased = '必須';
  }
  if (!values.brave1WayPurchaseAmount || values.brave1WayPurchaseAmount <= 0) {
    errors.brave1WayPurchaseAmount = '必須';
  }
  if (values.brave1WayPurchaseAmount > values.bravePurchased) {
    errors.brave1WayPurchaseAmount = 'Cannot be more than the first field';
  }

  return errors;
};

export default memoize(10)(validate);
