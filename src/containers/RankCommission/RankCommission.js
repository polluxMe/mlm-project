import React, { Component, PropTypes } from 'react';
// import config from '../../config';
import {connect} from 'react-redux';
import {FormattedMessage, FormattedNumber} from 'react-intl';
import {Board} from 'components';
import Helmet from 'react-helmet';
import * as rankCommissionActions from 'redux/modules/rankCommission';
import * as mainCommissionDividendActions from 'redux/modules/mainCommissionDividend';
import moment from 'moment';
import { LinkContainer } from 'react-router-bootstrap';

const backImg = require('../../theme/mlm/images/btn-double-back.png');

const currYear = moment().format('YYYY');
const currMonth = +moment().format('MM');
const months = [];
const years = [];
for (let ii = currYear; ii >= currYear - 3; ii -= 1) {
  years.push({key: ii, value: ii + '年'});
}
for (let ii = 1; ii <= 12; ii += 1) {
  months.push({key: ii, value: ii + '月'});
}

const Select = ({options, ...props}) =>
  <select {...props}>
    <option key="0" value="0">...</option>
    {options && options.map((item, index) => <option key={index} value={item.key}>{item.value}</option>)}
  </select>;

@connect(state => ({
  user: state.auth.user,
  commission: state.rankCommission.item,
  dividend: state.mainCommissionDividend.item
}),
  {
    loadRankCommission: rankCommissionActions.load,
    loadDividend: mainCommissionDividendActions.load
  }
)
export default class RankCommission extends Component {
  static propTypes = {
    user: PropTypes.object,
    commission: PropTypes.object,
    dividend: PropTypes.object,
    purchased: PropTypes.object,
    mainConfig: PropTypes.object,
    loadRankCommission: PropTypes.func,
    loadDividend: PropTypes.func
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  state = {
    year: currYear,
    month: currMonth
  };

  componentWillMount() {
    const {loadRankCommission, loadDividend, user} = this.props;
    loadRankCommission(user._id, {year: currYear, month: currMonth});
    loadDividend(user._id, {year: currYear, month: currMonth, type: 'rank'});
  }

  onMonthChange = ({target: {value}}) => {
    const {loadRankCommission, loadDividend, user} = this.props;
    this.setState({month: value});
    loadRankCommission(user._id, {year: this.state.year, month: value});
    loadDividend(user._id, {year: this.state.year, month: value, type: 'rank'});
  };

  onYearChange = ({target: {value}}) => {
    const {loadRankCommission, loadDividend, user} = this.props;
    this.setState({year: value});
    loadRankCommission(user._id, {year: value, month: this.state.month});
    loadDividend(user._id, {year: value, month: this.state.month, type: 'rank'});
  };

  render() {
    const {commission} = this.props;

    return (
      <div id="wrap">
        <Helmet title="Rank commission"/>
        <div id="contents" className="comission_detail">
          <Board>
            <div>
              <div className="main_rank">
                <h2>
                  <FormattedMessage
                    id="RankCommission.RankCommission"
                    defaultMessage="Rank commission"
                  />
                </h2>
                <div>
                  <div className="select-wrapL">
                    <Select
                      value={this.state.year}
                      options={years}
                      onChange={this.onYearChange}
                      className="year"
                    />
                  </div>
                  <div className="select-wrapR">
                    <Select
                      value={this.state.month}
                      options={months}
                      onChange={this.onMonthChange}
                      className="month"
                    />
                  </div>
                </div>
                <div style={{'clear': 'both'}}></div>
                <div>
                  <h3>
                    <FormattedMessage
                      id="RankCommission.NumberOfDividenedVS"
                      defaultMessage="Number of dividened VS"
                    />
                  </h3>
                  <p className="tac">
                    <FormattedNumber value={(commission && commission.amount) || 0} />
                    &nbsp;VS
                  </p>
                  <h3>
                    <FormattedMessage
                      id="RankCommission.NumberOfPayedVS"
                      defaultMessage="Number of payed VS"
                    />
                  </h3>
                  <p className="tac">
                    <FormattedNumber value={(commission && commission.amount) || 0} />
                    &nbsp;VS
                  </p>
                  <div style={{padding: '10px'}}>
                    <LinkContainer to="/my_account/commissions/">
                      <img src={backImg} width="100%" />
                    </LinkContainer>
                  </div>
                </div>
              </div>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}
