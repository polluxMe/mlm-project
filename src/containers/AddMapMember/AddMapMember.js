import React, {Component, PropTypes} from 'react';
import Helmet from 'react-helmet';
import {connect} from 'react-redux';
import * as addMapMemberActions from 'redux/modules/addMapMember';
import {reduxForm, Field} from 'redux-form';
import { push } from 'react-router-redux';
import { Link } from 'react-router';
import {
  Form,
  FormControl,
  Button
} from 'react-bootstrap';
import {FormattedMessage} from 'react-intl';
import { Board } from 'components';

const Select = ({suffix, placeholder, input, disabled, options, meta: {touched, error}}) =>
  <div>
    <div className={error && touched ? ' has-error' : ''}>
      <FormControl componentClass="select" {...input} placeholder={placeholder} disabled={disabled}>
        <option key="0" value="0">...</option>
        {
          options && options.map((item) => <option key={item._id} value={item._id}>{item.applicantName}</option>)
        }
      </FormControl>
      <span>{suffix && <span style={{paddingLeft: 5, width: 40}}>{suffix}</span>}</span>
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

@reduxForm({
  form: 'AddMapMember',
})
@connect(
  state => ({
    user: state.auth.user,
    members: state.addMapMember.data,
    form: state.form.AddMapMember
  }),
  {...addMapMemberActions, pushRoute: push })
export default class AddMapMember extends Component {
  static propTypes = {
    members: PropTypes.array,
    valid: PropTypes.bool.isRequired,
    load: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    form: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    pushRoute: PropTypes.func.isRequired,
    user: PropTypes.object,
    location: PropTypes.object.isRequired
  };

  componentDidMount() {
    const {user, load} = this.props;

    const rootId = user && user._id;

    load(rootId);
  }

  render() {
    const {members, form, handleSubmit, save, location, pushRoute } = this.props;

    const {parentId, isRight, isLeft} = location.query;

    const submitForm = () => {
      return save(form && form.values, parentId, isLeft, isRight)
      .then(result => {
        if (result) {
          if (typeof result.error === 'object') {
            return Promise.reject(result.error);
          }

          pushRoute('/my_account/commission/map');
        }
      });
    };

    return (
      <div id="wrap">
        <div id="contents" style={{overflowX: 'hidden'}}>
          <Board>
            <Helmet title="Add map member"/>
            <h1>
              <FormattedMessage
                id="AddMapMember.Header"
                defaultMessage="Select member to place"
              />
            </h1>
            <Form horizontal>

              <div className="form-group">
                <Field name="_id" component={Select} options={members} />
              </div>

              <div className="form-group">
                <div className="text-center">
                  <Link to="/my_account/commission/map">
                    <Button
                      bsSize="large"
                    >
                      <FormattedMessage
                        id="AddMapMember.Back"
                        defaultMessage="Back"
                      />
                    </Button>
                  </Link>
                  &nbsp;
                  <Button
                    bsSize="large"
                    bsStyle="success"
                    onClick={handleSubmit(submitForm)}
                  >
                    <FormattedMessage
                      id="AddMapMember.Submit"
                      defaultMessage="Submit"
                    />
                  </Button>
                </div>
              </div>
            </Form>
          </Board>
        </div>
      </div>
    );
  }
}
