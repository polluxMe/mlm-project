import React, {PropTypes} from 'react';
import d3 from 'd3';
import Faux from 'react-faux-dom';
import __ from 'lodash';
import config from '../../config.js';
// import { Router } from 'react-router';

const renderTree = (treeData, svgWidth, svgHeight, svgDomNode, images, rootId) => {
  const margin = {top: 50, right: 10, bottom: 10, left: 10};
  const width = svgWidth;
  const height = svgHeight;

  let ii = 0;
  let root;

  // Cleans up the SVG on re-render
  d3.select(svgDomNode).selectAll('*').remove();

  const tree = d3.layout.tree()
    .size([height, width]);

  const diagonal = d3.svg.diagonal()
    .projection(dd => [dd.x, dd.y]);

  const svg = d3.select(svgDomNode)
    .attr('width', width + margin.right + margin.left)
    .attr('height', height + margin.top + margin.bottom)
    .append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

  // Define the div for the tooltip
  const tip = d3.select('body').append('div')
    .attr('class', 'tooltip')
    .style('opacity', 0);

  const appendVirtualNodes = (node) => {
    if (!node.children) {
      node.children = [];

      node.children.push({
        isLeft: true,
        parentId: node._id,
        depth: node.depth + 1
      });

      node.children.push({
        isRight: true,
        parentId: node._id,
        depth: node.depth + 1
      });

      return node;
    } else if (node.children) {
      if (node.children.length === 1) {
        if (node.children[0].isLeft) {
          node.children.push({
            isRight: true,
            parentId: node._id,
            depth: node.depth + 1
          });

          appendVirtualNodes(node.children[0]);
        } else if (node.children[0].isRight) {
          node.children.unshift({
            isLeft: true,
            parentId: node._id,
            depth: node.depth + 1
          });

          appendVirtualNodes(node.children[1]);
        }
      } else {
        __.each(node.children, appendVirtualNodes);
      }
    }
  };

  __.each(treeData, appendVirtualNodes);

  root = treeData[0];
  root.x0 = 0;
  root.y0 = 0;

  function update(source) {
    // Compute the new tree layout.
    const nodes = tree.nodes(root).reverse();
    const links = tree.links(nodes);

    // Normalize for fixed-depth.
    nodes.forEach((dd) => { dd.y = dd.depth * 80; });

    // Update the nodes…
    const node = svg.selectAll('g.node')
      .data(nodes, dd => dd.id || (dd.id = ++ii));

    // Enter any new nodes at the parent's previous position.
    const nodeEnter = node.enter().append('g')
      .attr('class', 'node')
      .attr('transform', () => 'translate(' + source.x0 + ',' + source.y0 + ')');
    // .on('click', click);

    const color = (dd) => {
      if (dd._id === rootId) return images.gray;
      if (dd.children) return images.blue;
      // if (dd.introducerID && dd.applicantID && dd.introducerID === applicantID) {
      //   return images.yellow;
      // }
      return images.orange;
    };

    nodeEnter
      .append('svg:a')
      .attr('xlinkHref', dd =>
        dd.children ? `/map?id=${dd._id}` : `/map/add_member?rootId=${rootId}&parentId=${dd.parentId}&isLeft=${!!dd.isLeft}&isRight=${!!dd.isRight}`)
      .append('image')
      .attr('xlinkHref', color)
      .attr('x', -16)
      .attr('y', -16)
      .attr('width', 32)
      .attr('height', 32)
      .on('mouseover', (dd) => {
        tip.transition()
          .duration(200)
          .style('opacity', 0.9);

        const tipHtml = dd.children ? `
          applicantID: ${dd.applicantID} <br />
          applicantName: ${dd.applicantName} <br />
          email: ${dd.email} <br />
          commision: ${dd.commision} <br />
        ` : 'add';

        tip.html(tipHtml)
          .style('left', (d3.event.pageX) + 'px')
          .style('top', (d3.event.pageY - 28) + 'px');
      })
      .on('mouseout', () => {
        tip.transition()
          .duration(500)
          .style('opacity', 0);
      });

    nodeEnter
      .append('text')
      .attr('y', () => 30)
      .text((dd) => dd._id ? 'x' : '')
      .on('click', (dd) => {
        if (!dd._id) {
          return;
        }

        const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);
        fetch(apiPath + `/api/member/${dd._id}/remove`, {
          method: 'DELETE',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          mode: 'cors',
          credentials: 'include',
        })
          .then((response) => response.json())
          .then(() =>{
            window.location = document.URL;
          })
          .catch(error => {
            console.error(error);
          });
      });

    nodeEnter.append('text')
      .attr('y', (dd) => dd.children || dd._children ? -30 : 30)
      .attr('dy', '.35em')
      .attr('text-anchor', 'middle')
      .text(dd => dd.commision !== undefined ? `${dd.commision}` : '')
      .style('fill-opacity', 1);

    // Transition nodes to their new position.
    const nodeUpdate = node
    // .duration(duration)
      .attr('transform', dd => 'translate(' + dd.x + ',' + dd.y + ')');

    nodeUpdate.select('circle')
      .attr('r', 16)
      .style('stroke', () => '#000000')
      .style('fill', dd => dd.children ? '#FFFFFF' : '#008000');

    nodeUpdate.select('text')
      .style('fill-opacity', 1);

    // Transition exiting nodes to the parent's new position.
    const nodeExit = node.exit()
      .attr('transform', () => 'translate(' + source.x + ',' + source.y + ')')
      .remove();

    nodeExit.select('circle')
      .attr('r', 1e-6);

    nodeExit.select('text')
      .style('fill-opacity', 1e-6);

    // Update the links…
    const link = svg.selectAll('path.link')
      .data(links, dd => dd.target.id);

    // Enter any new links at the parent's previous position.
    link.enter().insert('path', 'g')
      .attr('class', 'link')
      // .style('stroke', (dd) => dd.target.level)
      .attr('d', () => {
        const oo = {x: source.x0, y: source.y0};
        return diagonal({source: oo, target: oo});
      });

    // Transition links to their new position.
    link
      .attr('d', diagonal);

    // Transition exiting nodes to the parent's new position.
    link.exit().transition()
    // .duration(duration)
      .attr('d', () => {
        const oo = {x: source.x, y: source.y};
        return diagonal({source: oo, target: oo});
      })
      .remove();

    // Stash the old positions for transition.
    nodes.forEach(dd => {
      dd.x0 = dd.x;
      dd.y0 = dd.y;
    });
  }

  update(root);
};

const D3Tree = React.createClass({
  propTypes: {
    treeData: PropTypes.array.isRequired,
    svgWidth: PropTypes.number.isRequired,
    svgHeight: PropTypes.number.isRequired,
    images: PropTypes.object.isRequired,
    rootId: PropTypes.string.isRequired
  },

  mixins: [
    Faux.mixins.core,
    Faux.mixins.anim
  ],

  getInitialState() {
    return {
      chart: 'loading...'
    };
  },

  componentDidMount() {
    const faux = this.connectFauxDOM('svg', 'chart');
    // Render the tree usng d3 after first component mount
    renderTree(this.props.treeData, this.props.svgWidth, this.props.svgHeight, faux, this.props.images, this.props.rootId);

    this.animateFauxDOM(800);
  },

  /* shouldComponentUpdate: function(nextProps) {
   const faux = this.connectFauxDOM('svg', 'chart');
   // Delegate rendering the tree to a d3 function on prop change
   renderTree(nextProps.treeData, this.props.svgWidth, this.props.svgHeight, faux);

   // Do not allow react to render the component on prop change
   return false;
   }*/

  render() {
    return (
      <div>
        {this.state.chart}
      </div>
    );
  }
});

export default D3Tree;
