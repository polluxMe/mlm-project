import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {
  Grid,
  Col,
  Button,
  Glyphicon
} from 'react-bootstrap';
import {FormattedMessage} from 'react-intl';

@connect(
  state => ({
    data: state.brave1Way.data,
  }))
export default class Brave2Way extends Component {
  static propTypes = {
    data: PropTypes.array
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  render() {
    const styles = require('./Brave2Way.scss');

    const {data} = this.props;

    const tableBody = data.map(item => (
      <tr key={item.id}>
        <td className="text-right">{item.date}</td>
        <td>{item.description}</td>
        <td className="text-right">{item.sum}</td>
      </tr>
    ));

    return (
      <div className={styles.home}>
        <Helmet title="Brave2Way"/>

        <div className={styles.wrapper}>
          <div className={styles.content}>
            <Grid>
              <Col xs={12}>
                <div className={styles.brave2Way}>
                  <div className={styles.braveImg}></div>
                  <div className={styles.inlineFlex}>
                    <div>
                      <FormattedMessage
                        id="General.Brave$2Way"
                        defaultMessage="Brave$ 2way"
                      />
                    </div>
                  </div>
                </div>

                <div id="table1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap no-footer">
                  <div className="be-datatable-body">
                    <table id="table1" className="table table-bordered table-striped table-hover table-fw-widget dataTable no-footer" role="grid" aria-describedby="table1_info">
                      <thead>
                        <tr>
                          <th>
                            <FormattedMessage
                              id="Brave2Way.Date"
                              defaultMessage="Date"
                            />
                          </th>
                          <th>
                            <FormattedMessage
                              id="Brave2Way.Description"
                              defaultMessage="Description"
                            />
                          </th>
                          <th>
                            <FormattedMessage
                              id="Brave2Way.Sum"
                              defaultMessage="Sum"
                            />
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {tableBody}
                      </tbody>
                    </table>
                  </div>
                </div>
              </Col>
            </Grid>
            <Grid className={styles.buttonBar}>
              <Col xs={6}>
                <Button
                  className={styles.btn}
                  block
                  bsStyle="primary">
                  <Glyphicon glyph="chevron-left" />
                  <span> </span>
                  <FormattedMessage
                    id="Brave2Way.Prev"
                    defaultMessage="Prev"
                  />
                </Button>
              </Col>
              <Col xs={6}>
                <Button
                  className={styles.btn}
                  block
                  bsStyle="primary">
                  <FormattedMessage
                    id="Brave2Way.Next"
                    defaultMessage="Next"
                  />
                  <span> </span>
                  <Glyphicon glyph="chevron-right" />
                </Button>
              </Col>
            </Grid>
          </div>
        </div>
      </div>
    );
  }
}

/*

 */
