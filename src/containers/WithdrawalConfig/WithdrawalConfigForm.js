import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import { initialize } from 'redux-form';
import { WithdrawalConfigAdd } from 'components';
import { browserHistory } from 'react-router';
import {FormattedMessage} from 'react-intl';

@connect(
  () => ({}),
  {
    initialize
  })
export default class WithdrawalConfigForm extends Component {
  static propTypes = {
    initialize: PropTypes.func.isRequired
  }

  componentWillMount() {
    this.handleInitialize();
  }

  handleSubmit = () => {
    console.log('handle');
    browserHistory.push('/withdrawal_config');
  };

  handleInitialize = () => {
    this.props.initialize('WithdrawalConfigForm', {

    });
  };

  render() {
    return (
      <div className="container">
        <h1>
          <FormattedMessage
            id="WithdrawalConfigForm.Header"
            defaultMessage="Withdrawal configuration form"
          />
        </h1>
        <Helmet title="WithdrawalConfig"/>
        <WithdrawalConfigAdd {...this.props} onSubmit={this.handleSubmit}/>
      </div>
    );
  }
}
