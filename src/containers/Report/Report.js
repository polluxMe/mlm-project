import React, { Component, PropTypes } from 'react';
// import config from '../../config';
import {connect} from 'react-redux';
import {FormattedMessage, FormattedNumber} from 'react-intl';
import {Board} from 'components';
import Helmet from 'react-helmet';
import moment from 'moment';
import momentLocalizer from 'react-widgets/lib/localizers/moment';
// import { Link } from 'react-router';
import { loadReport } from 'redux/modules/memberInfo';
import { toPairs } from 'lodash';
// import { createLocation } from 'history'
import { push as pushRoute} from 'react-router-redux';

momentLocalizer(moment);

const arrowLeftImg = require('theme/mlm/images/arrow-left-blue.png');
const arrowRightImg = require('theme/mlm/images/arrow-right-blue.png');

let currentDate = moment();
let prevMonth = moment().subtract(1, 'month');
let nextMonth = moment().add(1, 'month');

@connect(state => ({
  user: state.auth.user,
  report: state.memberInfo.report
}),
  {pushRoute, loadReport}
)
export default class Report extends Component {
  static propTypes = {
    user: PropTypes.object,
    report: PropTypes.object,
    location: PropTypes.object,
    history: PropTypes.object,
    loadReport: PropTypes.func,
    pushRoute: PropTypes.func
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  componentWillMount() {
    const {query} = this.props.location;
    const {paramMonth, paramYear} = query;
    const isLoad = paramMonth ? true : false;

    this.buildRequest(paramMonth, paramYear, isLoad);
  }

  buildRequest = (month, year, isLoad) => {
    const date = new Date();

    if (month && '' + month.length === 1) {
      month = '0' + month; // eslint-disable-line
    }
    const paramMonth = month || (date.getMonth() + 1);
    const paramYear = year || date.getFullYear();
    const queryString = '?' + toPairs({paramMonth, paramYear}).map((param) => {
      return param[0] + '=' + param[1];
    }).join('&');
    if (isLoad) {
      this.props.pushRoute(this.props.location.pathname + queryString);
    }
    this.props.loadReport(this.props.user._id, {paramMonth, paramYear});
  }

  render() {
    const styles = require('./Report.scss');

    console.log('this.props.location', this.props.location);
    const {query} = this.props.location;
    const {paramMonth, paramYear} = query;

    if (paramMonth && paramYear) {
      currentDate = moment(paramYear + '-' + paramMonth);
      prevMonth = moment(paramYear + '-' + paramMonth).subtract(1, 'month');
      nextMonth = moment(paramYear + '-' + paramMonth).add(1, 'month');
    }

    console.log('report', this.props.report);

    /* eslint-disable */
    return (
      <div id="wrap">
        <Helmet title="Commission"/>
        <div className="report" id="contents">
          <Board>
            <div className={styles.background}>
                <div className={styles.inner}>
                  <ul className="month-select">
                    <li className="prev">
                      <a href="javascript:void(0)" onClick={() => {this.buildRequest(prevMonth.format('M'), prevMonth.format('YYYY'), true);}}>
                        <img src={arrowLeftImg} />
                      </a>
                    </li>
                    <li>{currentDate.format('YYYY年M月')}</li>
                    <li className="next">
                      <a href="javascript:void(0)" onClick={() => {this.buildRequest(nextMonth.format('M'), nextMonth.format('YYYY'), true);}}>
                        <img src={arrowRightImg} />
                      </a>
                    </li>
                  </ul>

                  <dl className="main-comission">
                    <dt>
                      <FormattedMessage
                        id="Report.MainCommission"
                        defaultMessage="Main commission"
                      />
                    </dt>
                    <dd>
                      <p className="payment-amount">
                        <FormattedNumber value={this.props.report ? this.props.report.commission : 0} />
                        &nbsp;B$
                      </p>
                    </dd>
                  </dl>

                  <table>
                    <tr>
                      <th>
                        <FormattedMessage
                          id="Report.Right"
                          defaultMessage="Right"
                        />
                      </th>
                      <td>
                        {this.props.report ? this.props.report.right.count : 0 + ' '}
                        <FormattedMessage id="General.People" defaultMessage="People" />
                      </td>
                    </tr>
                    <tr className="purchase">
                      <th>
                        <FormattedMessage
                          id="Report.PurchaseAmount"
                          defaultMessage="Purchase amount"
                        />
                      </th>
                      <td>
                        <FormattedNumber value={this.props.report ? this.props.report.right.sum : 0} />
                        &nbsp;B$
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <FormattedMessage
                          id="Report.Left"
                          defaultMessage="Left"
                        />
                      </th>
                      <td>
                        {this.props.report ? this.props.report.left.count : 0 + ' '}
                        <FormattedMessage id="General.People" defaultMessage="People" />
                      </td>
                    </tr>
                    <tr className="purchase">
                      <th>
                        <FormattedMessage
                          id="Report.PurchaseAmount"
                          defaultMessage="Purchase amount"
                        />
                      </th>
                      <td>
                        <FormattedNumber value={this.props.report ? this.props.report.left.sum : 0} />
                        &nbsp;B$
                      </td>
                    </tr>
                  </table>

                  <dl className="maching-bonus">
                    <dt>
                      <FormattedMessage
                        id="Report.MatchingBonus"
                        defaultMessage="Matching Bonus"
                      />
                    </dt>
                    <dd>
                      <FormattedNumber value={0}/>
                      &nbsp;
                      <FormattedMessage
                        id="General.Yen"
                        defaultMessage="￥"
                      />
                    </dd>
                  </dl>
                  <table>
                    <tr>
                      <th>
                        <FormattedMessage
                          id="Report.R4OrMorePeople"
                          defaultMessage="R4 or more persons"
                        />
                      </th>
                      <td>
                        <FormattedNumber value={this.props.report ? this.props.report.higherRank || 0 : 0}/>
                        <FormattedMessage id="General.People" defaultMessage="People" />
                      </td>
                    </tr>
                  </table>
                  <dl>
                    <dt>
                      <FormattedMessage
                        id="Report.PaidCashRewards"
                        defaultMessage="Paid Cash Rewards"
                      />
                    </dt>
                    <dd>
                      <FormattedNumber value={this.props.report ? this.props.report.cashReward || 0 : 0}/>
                      &nbsp;
                      <FormattedMessage
                        id="General.Yen"
                        defaultMessage="￥"
                      />
                    </dd>
                    <dt>
                      <FormattedMessage
                        id="Report.SelfPurchasePrice"
                        defaultMessage="Self purchase price"
                      />
                    </dt>
                    <dd>
                      <FormattedNumber value={this.props.report ? this.props.report.selfPurchase || 0 : 0}/>
                      &nbsp;
                      <FormattedMessage
                        id="General.Yen"
                        defaultMessage="￥"
                      />
                    </dd>
                  </dl>
                </div>
            </div>
        </Board>
      </div>
    </div>
    );
    /* eslint-disable */
  }
}
