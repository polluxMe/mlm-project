import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {injectIntl, intlShape, defineMessages, FormattedMessage} from 'react-intl';
import { load } from 'redux/modules/matchingBonusRequestDetails';
import {Grid} from 'react-bootstrap';
// import { asyncConnect } from 'redux-async-connect';
import {DetailsTable} from 'components';

const messages = defineMessages({
  RequestID: {
    id: 'MatchingBonusRequestDetails.RequestID',
    defaultMessage: 'Request ID'
  },
  MemberID: {
    id: 'MatchingBonusRequestDetails.MemberID',
    defaultMessage: 'Member ID'
  },
  CreationDate: {
    id: 'MatchingBonusRequestDetails.CreationDate',
    defaultMessage: 'Creation date'
  },
  Status: {
    id: 'MatchingBonusRequestDetails.Status',
    defaultMessage: 'Status'
  },
  Amount: {
    id: 'MatchingBonusRequestDetails.Amount',
    defaultMessage: 'Amount'
  }
});

@connect(state => ({
  request: state.matchingBonusRequestDetails.data
}),
  {load})
class MatchingBonusRequestDetails extends Component {
  static propTypes = {
    request: PropTypes.object,
    load: PropTypes.func,
    params: PropTypes.object,
    intl: intlShape.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {amount: 0, cashRate: 0};
  }

  componentWillMount() {
    this.props.load(this.props.params.id);
  }

  render() {
    const {intl, request} = this.props;
    let result;
    result = (
      <Grid>
        <h2>
          <FormattedMessage
            id="MatchingBonusRequestDetails.Header"
            defaultMessage="Matching bonus request details"
          />
        </h2>
        <DetailsTable data={request && [
          {
            label: intl.formatMessage(messages.RequestID),
            value: request._id
          },
          {
            label: intl.formatMessage(messages.MemberID),
            value: request.memberID
          },
          {
            label: intl.formatMessage(messages.CreationDate),
            value: request.createDate
          },
          {
            label: intl.formatMessage(messages.Status),
            value: request.status
          },
          {
            label: intl.formatMessage(messages.Amount),
            value: request.amount
          }
        ]}/>
      </Grid>
    );

    return result;
  }
}

export default injectIntl(MatchingBonusRequestDetails);
