import React, {Component, PropTypes} from 'react';
// import Helmet from 'react-helmet';
import {connect} from 'react-redux';
import * as memberDetailsActions from 'redux/modules/adminMemberDetails';
import {FormattedMessage, FormattedNumber} from 'react-intl';
import { push } from 'react-router-redux';
import {Table, Button} from 'react-bootstrap';
import moment from 'moment';
import countries from 'utils/countries';
import {SwitchableInput, Spinner} from '../../components';

const rankOptions = [];

for (let ii = 0; ii <= 10; ii += 1) {
  rankOptions.push({value: ii, label: ii});
}

const countryOptions = countries.map(item => ({label: item.name, value: item.name}));

const currentYear = +moment().format('YYYY');
const years = [];
const months = [];
const days = [];

for (let ii = currentYear; ii > currentYear - 100; ii -= 1) {
  years.push({_id: ii, name: ii});
}

for (let ii = 1; ii <= 12; ii += 1) {
  months.push({_id: ii, name: ii});
}

for (let ii = 1; ii <= 31; ii += 1) {
  days.push({_id: ii, name: ii});
}

const DatePicker = React.createClass({
  propTypes: {
    value: PropTypes.string,
    onChange: PropTypes.func,
    onBlur: PropTypes.func
  },

  onYearChange(event) {
    this.props.onChange(moment(this.props.value).year(event.target.value)._d);
  },

  onMonthChange(event) {
    this.props.onChange(moment(this.props.value).month(event.target.value + 1)._d);
  },

  onDateChange(event) {
    this.props.onChange(moment(this.props.value).date(event.target.value)._d);
  },

  render() {
    return (
      <div>
        <select value={moment(this.props.value).year()} onChange={this.onYearChange} onBlur={this.props.onBlur}>
          {years && years.map((item, index) => <option key={index} value={item._id}>{item.name}</option>)}
        </select>
        <select value={moment(this.props.value).month() + 1} onChange={this.onMonthChange} onBlur={this.props.onBlur}>
          {months && months.map((item, index) => <option key={index} value={item._id}>{item.name}</option>)}
        </select>
        <select value={moment(this.props.value).date()} onChange={this.onDateChange} onBlur={this.props.onBlur}>
          {days && days.map((item, index) => <option key={index} value={item._id}>{item.name}</option>)}
        </select>
      </div>
    );
  }
});

const Select = ({value, onChange, options, onBlur}) =>
  <select autoFocus onChange={event => onChange(event.target.value)} value={value} onBlur={onBlur}>
    {
      options && options.map((item, index) => <option key={index} value={item.value}>{item.label}</option>)
    }
  </select>;

const Textarea = ({value, onChange, onBlur}) =>
  <textarea onChange={event => onChange(event.target.value)} value={value} onBlur={onBlur}>
  </textarea>;

@connect(
  state => ({
    member: state.adminMemberDetails.member,
    loaded: state.adminMemberDetails.loaded,
    loading: state.adminMemberDetails.loading,
  }),
  {...memberDetailsActions, pushState: push})
export default class AdminMemberList extends Component {
  static propTypes = {
    member: PropTypes.object,
    loaded: PropTypes.bool,
    loading: PropTypes.bool,
    params: PropTypes.object,
    load: PropTypes.func,
    save: PropTypes.func,
    saved: PropTypes.bool,
    saving: PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.state = {user: {}, saved: false};
  }

  componentWillMount() {
    this.props.load(this.props.params.id)
      .then(() => {
        this.setState({user: {...this.props.member, ...this.props.member.profile}});
      });
  }

  onFieldChange = (fieldName) => {
    return (value) => {
      this.setState({user: {...this.state.user, [fieldName]: value}});
    };
  };

  onSave = () => {
    this.props.save({
      _id: this.props.member._id,
      applicantName: this.state.user.applicantName,
      applicantKatakana: this.state.user.applicantKatakana,
      nickname: this.state.user.nickname,
      email: this.state.user.email,
      note: this.state.user.note,
      bankInformation: this.state.user.bankInformation,
      rank: this.state.user.rank,
      profile: {
        nationality: this.state.user.nationality,
        dateOfBirth: this.state.user.dateOfBirth,
        address: this.state.user.address,
        phone: this.state.user.phone,
        mobilePhone: this.state.user.mobilePhone,
        gender: this.state.user.gender,
        passport: this.state.user.passport,
      }
    })
      .then(() => {
        this.setState({saved: true});
      });
  };

  render() {
    function noticeFormatter(value) {
      if (value) return '通知済み';
      return '未通知';
    }

    function birthDateFormatter(value) {
      return moment(value).format('YYYY/MM/DD');
    }

    function dateFormatter(value) {
      return moment(value).format('DD-MM-YYYY');
    }

    function genderFormatter(value) {
      if (value === 'male') return '男';
      return '女';
    }

    function statusFormatter(value) {
      let status;
      switch (value) {
        case 'temp':
          status = '仮会員';
          break;
        case 'official':
          status = '本会員';
          break;
        case 'deactivated':
          status = '無効';
          break;
        default:
          status = '';
          break;
      }
      return status;
    }

    const {loaded, member, loading, saving} = this.props;
    return (
        <div className="container">
          <h2>
            <FormattedMessage
              id="MemberDetails.Header"
              defaultMessage="Member details"
            />
          </h2>
          {
            loading && <span><FormattedMessage
              id="General.Loading"
              defaultMessage="Loading..."
            /></span>
          }
          {
            loaded &&
            <div>
              <Table striped bordered condensed>
                <tbody>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.BraveID"
                        defaultMessage="Brave ID"
                      />
                    </th>
                    <td>
                      <SwitchableInput value={this.state.user.applicantID}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.LoginID"
                        defaultMessage="Login ID"
                      />
                    </th>
                    <td>
                      <SwitchableInput value={this.state.user.username}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.TempPass"
                        defaultMessage="Temporary Password"
                      />
                    </th>
                    <td>{member.tempPassword}</td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.Name"
                        defaultMessage="Name"
                      />
                    </th>
                    <td>
                      <SwitchableInput editable value={this.state.user.applicantName} onChange={this.onFieldChange('applicantName')}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.applicantKatakana"
                        defaultMessage="Name (Furigana)"
                      />
                    </th>
                    <td>
                      <SwitchableInput editable value={this.state.user.applicantKatakana} onChange={this.onFieldChange('applicantKatakana')}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.Nickname"
                        defaultMessage="Nickname"
                      />
                    </th>
                    <td>
                      <SwitchableInput editable value={this.state.user.nickname} onChange={this.onFieldChange('nickname')}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.BraveIntroducerID"
                        defaultMessage="Brave Introducer ID"
                      />
                    </th>
                    <td>
                      {member.accountType === 'brave' && <SwitchableInput value={this.state.user.introducerID}/>}
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.GCloudID"
                        defaultMessage="GCloud ID"
                      />
                    </th>
                    <td>{member.gCloudID}</td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.GCloudIntroducerID"
                        defaultMessage="GCloud Introducer ID"
                      />
                    </th>
                    <td>
                      {member.accountType === 'gcloud' && <SwitchableInput value={this.state.user.introducerID}/>}
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.National"
                        defaultMessage="National"
                      />
                    </th>
                    <td>
                      <SwitchableInput editable
                                       value={this.state.user.nationality}
                                       onChange={this.onFieldChange('nationality')}
                                       inputComponent={Select}
                                       options={countryOptions}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.Sex"
                        defaultMessage="Sex"
                      />
                    </th>
                    <td>
                      <SwitchableInput
                        editable
                        value={this.state.user.gender}
                        onChange={this.onFieldChange('gender')}
                        inputComponent={Select}
                        options={[{label: '男', value: 'male'}, {label: '女', value: 'female'}]}
                        valueFormat={genderFormatter}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.Birthday"
                        defaultMessage="Birthday"
                      />
                    </th>
                    <td>
                      <SwitchableInput editable value={this.state.user.dateOfBirth} inputComponent={DatePicker} valueFormat={birthDateFormatter} onChange={this.onFieldChange('dateOfBirth')}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.Address"
                        defaultMessage="Address"
                      />
                    </th>
                    <td>
                      <SwitchableInput editable value={this.state.user.address || '-'} onChange={this.onFieldChange('address')}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.Postal"
                        defaultMessage="Postal code"
                      />
                    </th>
                    <td>
                      <SwitchableInput editable value={this.state.user.postal || '-'} onChange={this.onFieldChange('postal')}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.Phone"
                        defaultMessage="Phone number"
                      />
                    </th>
                    <td>
                      <SwitchableInput editable value={this.state.user.phone || '-'} onChange={this.onFieldChange('phone')}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.MobilePhone"
                        defaultMessage="Mobile number"
                      />
                    </th>
                    <td>
                      <SwitchableInput editable value={this.state.user.mobilePhone || '-'} onChange={this.onFieldChange('mobilePhone')}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.Email"
                        defaultMessage="Email"
                      />
                    </th>
                    <td>
                      <SwitchableInput editable value={this.state.user.email || '-'}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.RegisterDate"
                        defaultMessage="Register date"
                      />
                    </th>
                    <td><SwitchableInput value={this.state.user.createDate} valueFormat={dateFormatter}/></td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.Passport"
                        defaultMessage="Identity verification doc(passport, License)"
                      />
                    </th>
                    <td>
                      <SwitchableInput editable value={this.state.user.passport || '-'} onChange={this.onFieldChange('passport')}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.Note"
                        defaultMessage="Note"
                      />
                    </th>
                    <td>
                      <SwitchableInput editable value={this.state.user.note || '-'} onChange={this.onFieldChange('note')}/>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.BelongTo"
                        defaultMessage="Belong to"
                      />
                    </th>
                    <td>{member.accountType}</td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.LastLoginTime"
                        defaultMessage="Last login time"
                      />
                    </th>
                    <td>{member.lastLoginTime && moment(member.lastLoginTime).format('YYYY-MM-DD HH:mm:ss')}</td>
                </tr>
                <tr>
                  <th>
                    <FormattedMessage
                      id="AdminMemberDetails.PurchaseMoneyAmount"
                      defaultMessage="Purchase money amount (￥)"
                    />
                  </th>
                  <td><FormattedNumber value={member.purchasePrice || 0}/></td>
                </tr>
                <tr>
                  <th>
                    <FormattedMessage
                      id="AdminMemberDetails.PurchaseMoneyAmountATC"
                      defaultMessage="Purchase money amount (ATC)"
                    />
                  </th>
                  <td><FormattedNumber value={member.purchasePriceATC || 0}/></td>
                </tr>
                <tr>
                  <th>
                    <FormattedMessage
                      id="AdminMemberDetails.PurchaseMoneyAmountBTC"
                      defaultMessage="Purchase money amount (BTC)"
                    />
                  </th>
                  <td><FormattedNumber value={member.purchasePriceBTC || 0}/></td>
                </tr>
                <tr>
                  <th>
                    <FormattedMessage
                      id="AdminMemberDetails.BraveRank"
                      defaultMessage="Brave rank"
                    />
                  </th>
                  <td>
                    <SwitchableInput
                      editable
                      value={this.state.user.rank || 0}
                      onChange={this.onFieldChange('rank')}
                      inputComponent={Select}
                      options={rankOptions}/>
                  </td>
                </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.GCloudRank"
                        defaultMessage="G-Cloud rank"
                      />
                    </th>
                    <td>
                      <SwitchableInput value={this.state.user.gCloudRank} />
                    </td>
                  </tr>
                <tr>
                  <th>
                    <FormattedMessage
                      id="AdminMemberDetails.Status"
                      defaultMessage="Member status"
                    />
                  </th>
                  <td>
                    <SwitchableInput value={this.state.user.status} valueFormat={statusFormatter}/>
                  </td>
                </tr>
                <tr>
                  <th>
                    <FormattedMessage
                      id="AdminMemberDetails.MailNoticeStatus"
                      defaultMessage="Mail notice status"
                    />
                  </th>
                  <td>
                    <SwitchableInput value={this.state.user.emailNotice} valueFormat={noticeFormatter}/>
                  </td>
                </tr>
                <tr>
                  <th>
                    <FormattedMessage
                      id="AdminMemberDetails.DocumentNoticeStatus"
                      defaultMessage="Document notice status"
                    />
                  </th>
                  <td>
                    <SwitchableInput value={this.state.user.writtenNotice} valueFormat={noticeFormatter}/>
                  </td>
                </tr>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="AdminMemberDetails.BankInformation"
                        defaultMessage="Payment (Bank transfer/cash)"
                      />
                    </th>
                    <td>
                      <SwitchableInput editable inputComponent={Textarea} value={this.state.user.bankInformation || '-'} onChange={this.onFieldChange('bankInformation')}/>
                    </td>
                  </tr>
                </tbody>
              </Table>
              {this.state.saved && (
                <p>
                  <FormattedMessage
                    id="EditAccountInfo.Saved"
                    defaultMessage="Account info successfuly saved"
                  />
                </p>)}
              <div className="text-center">
                <Button bsStyle="success" onClick={this.onSave}>
                  <Spinner loading={saving} size={25} exclusive>
                    <FormattedMessage
                      id="General.Save"
                      defaultMessage="Save"
                    />
                  </Spinner>
                </Button>
                <Button bsStyle="primary" href={'/map?id=' + member._id}>
                  <FormattedMessage
                    id="AdminMemberDetails.Map"
                    defaultMessage="Map"
                  />
                </Button>
                <Button bsStyle="primary" href={'/vs_purchases/add/' + member._id}>
                  <FormattedMessage
                    id="AdminMemberDetails.Purchase"
                    defaultMessage="Purchase"
                  />
                </Button>
                <Button bsStyle="primary" href={'/members/' + member._id + '/rank_history'}>
                  <FormattedMessage
                    id="AdminMemberDetails.RankHistory"
                    defaultMessage="Rank history"
                  />
                </Button>
              </div>
            </div>
          }
        </div>
    );
  }
}
