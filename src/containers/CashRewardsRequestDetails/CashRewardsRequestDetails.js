import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {injectIntl, intlShape, defineMessages, FormattedMessage} from 'react-intl';
import { load } from 'redux/modules/cashRewardsRequestDetails';
import {Grid} from 'react-bootstrap';
// import { asyncConnect } from 'redux-async-connect';
import {DetailsTable} from 'components';

const messages = defineMessages({
  RequestID: {
    id: 'CashRewardsRequestDetails.RequestID',
    defaultMessage: 'Request ID'
  },
  MemberID: {
    id: 'CashRewardsRequestDetails.MemberID',
    defaultMessage: 'Member ID'
  },
  CreationDate: {
    id: 'CashRewardsRequestDetails.CreationDate',
    defaultMessage: 'Creation date'
  },
  Status: {
    id: 'CashRewardsRequestDetails.Status',
    defaultMessage: 'Status'
  },
  Amount: {
    id: 'CashRewardsRequestDetails.Amount',
    defaultMessage: 'Amount'
  }
});

@connect(state => ({
  request: state.cashRewardsRequestDetails.data
}),
  {load})
class CashRewardsRequestDetails extends Component {
  static propTypes = {
    request: PropTypes.object,
    load: PropTypes.func,
    params: PropTypes.object,
    intl: intlShape.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {amount: 0, cashRate: 0};
  }

  componentWillMount() {
    this.props.load(this.props.params.id);
  }

  render() {
    const {intl, request} = this.props;

    let result;
    result = (
      <Grid>
        <h2>
          <FormattedMessage
            id="CashRewardsRequestDetails.Header"
            defaultMessage="Cash reward request details"
          />
        </h2>
        <DetailsTable data={request && [
          {
            label: intl.formatMessage(messages.RequestID),
            value: request._id
          },
          {
            label: intl.formatMessage(messages.MemberID),
            value: request.memberID
          },
          {
            label: intl.formatMessage(messages.CreationDate),
            value: request.createDate
          },
          {
            label: intl.formatMessage(messages.Status),
            value: request.status
          },
          {
            label: intl.formatMessage(messages.Amount),
            value: request.amount
          }
        ]}/>
      </Grid>
    );

    return result;
  }
}

export default injectIntl(CashRewardsRequestDetails);
