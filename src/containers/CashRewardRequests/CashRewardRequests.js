import React, {Component, PropTypes} from 'react';
// import Helmet from 'react-helmet';
import {connect} from 'react-redux';
import * as cashRewardRequestsActions from 'redux/modules/cashRewardRequests';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import {FormattedMessage, FormattedNumber} from 'react-intl';
import { CashRewardRequestsSearchForm } from 'components';
import classNames from 'classnames';
import { Glyphicon } from 'react-bootstrap';
import { push } from 'react-router-redux';
import { Link } from 'react-router';
import moment from 'moment';

// require('../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css');

@connect(
  state => ({
    requests: state.cashRewardRequests.data,
    error: state.cashRewardRequests.error,
    loading: state.cashRewardRequests.loading,
    loaded: state.cashRewardRequests.loaded,
    totalDataSize: state.cashRewardRequests.totalDataSize,
    currentPage: state.cashRewardRequests.currentPage,
    sizePerPage: state.cashRewardRequests.sizePerPage,
    // search: state.vsPurchases.search,
    sorter: state.cashRewardRequests.sorter,
  }),
  {...cashRewardRequestsActions, pushState: push})
export default class cashRewardRequests extends Component {
  static propTypes = {
    requests: PropTypes.array,
    error: PropTypes.string,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    load: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    totalDataSize: PropTypes.number,
    sizePerPage: PropTypes.number,
    changeSort: PropTypes.func.isRequired,
    changePage: PropTypes.func.isRequired,
    changeSizePerPage: PropTypes.func.isRequired,
    changeFilter: PropTypes.func.isRequired,
    pushState: PropTypes.func,
    changeStatus: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {searchVisible: false};
  }

  componentWillMount() {
    this.props.load();
  }

  onSearch = (fields) =>
    this.onFilterChange(fields);

  onSortChange = (name, order) => {
    const sortOrder = {name: name, order: (order === 'asc') ? 1 : -1};
    this.props.changeSort(JSON.stringify(sortOrder));
  };

  onPageChange = (page, limit) => {
    this.props.changePage(page, limit);
  };

  // onSearchChange = () => {
  //
  // };

  onFilterChange = (filterObj) =>
    this.props.changeFilter(JSON.stringify(filterObj));

  onSizePerPageList = (sizePerPage) => {
    this.props.changeSizePerPage(sizePerPage);
  };

  render() {
    function dateFormatter(cell) {
      return moment(cell).format('DD-MM-YYYY HH:mm:ss');
    }

    function idFormatter(cell) {
      return (<Link to={'cash_reward_requests/' + cell}>{cell}</Link>);
    }

    function memberIDFormatter(cell, row) {
      return <Link to={'/members/' + cell}>{row.member && row.member.applicantID}</Link>;
    }

    function memberNameFormatter(cell, row) {
      return row.member && row.member.applicantName;
    }

    function numberFormatter(cell) {
      return <FormattedNumber value={cell}/>;
    }

    function statusFormatter(cell) {
      const statuses = {
        removed: '現金買い注文',
        created: '未払い',
        purchased: '支払い済み'
      };
      return statuses[cell] ? statuses[cell] : cell;
    }

    const styles = require('./CashRewardRequests.scss');

    return (
        <div>
          <div className="container">
          <h2>
            <FormattedMessage
              id="cashRewardRequests.Header"
              defaultMessage="Cash reward requests list"
            />
          </h2>
          <div className={classNames(styles.searchForm, {[styles.hidden]: !this.state.searchVisible })}>
            <div className={styles.wrapper}>
              <CashRewardRequestsSearchForm onSubmit={this.onSearch}/>
            </div>
          </div>
          <div style={{textAlign: 'center'}}>
            <button className={styles.hideButton} onClick={()=> this.setState({searchVisible: !this.state.searchVisible})}>
              <Glyphicon glyph={this.state.searchVisible ? 'arrow-up' : 'arrow-down'}/>
              &nbsp;
              {
                this.state.searchVisible ?
                  <FormattedMessage
                    id="General.HideSearch"
                    defaultMessage="Hide search"
                  /> :
                  <FormattedMessage
                    id="General.ShowSearch"
                    defaultMessage="Show search"
                  />
              }
              </button>
          </div>
          </div>
          <BootstrapTable
            cellEdit = {{
              mode: 'click',
              blurToSave: true,
              afterSaveCell: (item, fieldName, fieldValue) => {
                item[fieldName] = fieldValue;
                this.props.changeStatus(item);
              },
              beforeSaveCell: (item, fieldName, fieldValue) => {
                if (item[fieldName] === fieldValue) return true;
                if (fieldValue === 'removed') return confirm(`Are you sure you want to change ${fieldName} to ${fieldValue}?`);
              }
            }}
            data={this.props.requests || []}
            remote
            hover
            condensed
            pagination
            fetchInfo={ { dataTotalSize: this.props.totalDataSize } }
            options={ { sizePerPage: this.props.sizePerPage,
                        onPageChange: this.onPageChange,
                        sizePerPageList: [ 5, 10, 20],
                        pageStartIndex: 1,
                        page: this.props.currentPage,
                        // onSearchChange: this.onSearchChange,
                        onSortChange: this.onSortChange,
                        // onFilterChange: this.onFilterChange,
                        onSizePerPageList: this.onSizePerPageList,
                        hideSizePerPage: true,
                        noDataText: '表示されるデータがございません。'
                       }
                    }
          >
            <TableHeaderColumn dataField="createDate" dataSort dataFormat={dateFormatter} dataAlign="center" editable={false}>
              <FormattedMessage
                id="cashRewardRequests.CreationDate"
                defaultMessage="Creation date"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="updateDate" dataSort dataFormat={dateFormatter} dataAlign="center" editable={false}>
              <FormattedMessage
                id="cashRewardRequests.UpdateDate"
                defaultMessage="Update date"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="status" dataSort dataAlign="center" dataFormat={statusFormatter} editable={{type: 'select', options: {values: ['removed', 'created', 'purchased']}}}>
              <FormattedMessage
                id="cashRewardRequests.Status"
                defaultMessage="Status"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="_id" dataAlign="center" dataFormat={memberNameFormatter} editable={false}>
              <FormattedMessage
                id="WithdrawRequests.Name"
                defaultMessage="Name"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="amount" dataSort dataAlign="center" dataFormat={numberFormatter} editable={false}>
              <FormattedMessage
                id="cashRewardRequests.CashRewardAmount"
                defaultMessage="Cash reward amount"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="memberID" dataAlign="center" editable={false} dataFormat={memberIDFormatter}>
              <FormattedMessage
                id="cashRewardRequests.MemberID"
                defaultMessage="Member ID"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="masterID" dataSort isKey dataAlign="center" dataFormat={idFormatter} editable={false}>
              <FormattedMessage
                id="cashRewardRequests.CashRewardRequestID"
                defaultMessage="Cash reward request ID"
              />
            </TableHeaderColumn>
          </BootstrapTable>
        </div>
    );
  }
}

