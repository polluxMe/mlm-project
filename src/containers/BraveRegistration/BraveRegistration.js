import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {initialize} from 'redux-form';
import {BraveRegistrationForm, FormHeader} from 'components';
import { browserHistory } from 'react-router';
import { setTempUser } from 'redux/modules/register';
import moment from 'moment';
import cookie from 'react-cookie';
import config from '../../config';

const BRAVE_FORM_COLOR = '#f58220';

@connect(
  () => ({}),
  {initialize, setTempUser})
export default class BraveRegistration extends Component {
  static propTypes = {
    initialize: PropTypes.func.isRequired,
    location: PropTypes.object
  };

  componentWillMount() {
    this.handleInitialize();
  }

  handleSubmit = (response) => {
    this.props.setTempUser({
      username: response.applicantName,
      id: response.applicantID,
      password: response.pwd
    });
    browserHistory.push('/registration/completion');
  }

  handleInitialize = () => {
    const {location} = this.props;
    const currentYear = +moment().format('YYYY');
    const currentMonth = +moment().format('M');
    const currentDay = +moment().format('D');

    const query = location && location.query;

    if (query && query.introducerID && query.introducerName) {
      cookie.save('introducerID', query.introducerID, {path: '/', domain: config.apiHost});
      cookie.save('introducerName', query.introducerName, {path: '/', domain: config.apiHost});
    }

    const introducerID = cookie.load('introducerID');
    const introducerName = cookie.load('introducerName');

    this.props.initialize('braveRegistration', {
      // applicantName: '中嶋 愛子',
      // applicantKatakana: 'ナカシマ アイコ',
      introducerName,
      // introducerKatakana: 'ナカシマ アイコ',
      introducerID,
      // email: 'naka@i.softbank.jp',
      purchaseType: 'cash',
      profile: {
        // phone: '12341',
        // nationality: '日本',
        // mobilePhone: '11111111',
        // passport: '免許証',
        // address: '京都',
        yearOfBirth: currentYear,
        monthOfBirth: currentMonth,
        dayOfBirth: currentDay,
        nationality: 'Japan'
      },
      // introducerPhone: '123465789',
      // nickname: '愛子',
    });
  }

  render() {
    return (
      <div className="container" style={{background: 'white'}}>
        <h1>BRAVE</h1>
        <Helmet title="Brave"/>
        <FormHeader color={BRAVE_FORM_COLOR}/>
        <BraveRegistrationForm {...this.props} onSubmit={this.handleSubmit}/>
      </div>
    );
  }
}

BraveRegistration.propTypes = {
  setTempUser: React.PropTypes.func
};
