import memoize from 'lru-memoize';

const validate = (values) => {
  const errors = {};
  if (!values.title) {
    errors.title = '必須';
  }
  if (!values.message) {
    errors.message = '必須';
  }
  if (values.emails) {
    errors.emails = [];
    values.emails.forEach((item, index) => {
      if (!item || !/@/.test(item)) {
        errors.emails[index] = '必須';
      }
    });
  }
  return errors;
};

export default memoize(10)(validate);
