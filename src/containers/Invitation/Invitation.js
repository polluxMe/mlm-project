import React, { Component, PropTypes } from 'react';
// import config from '../../config';
import {connect} from 'react-redux';
import {reduxForm, Field, FieldArray} from 'redux-form';
import {injectIntl, intlShape, FormattedMessage} from 'react-intl';
import {Board} from 'components';
import Helmet from 'react-helmet';
import {send as sendInvitation} from 'redux/modules/invitation';
import QRCode from 'qrcode.react';
import validate from './InvitationValidation';
import {push as pushState} from 'react-router-redux';

const invitationAddButtonImage = require('theme/mlm/images/invitation-btn-add.png');
const invitationSendButtonImage = require('theme/mlm/images/invitation-btn-send.png');

const styles = require('./Invitation.scss');

const Input = ({input, meta: {touched, error}, ...rest}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <input {...input} {...rest} />
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

const Textarea = ({input, meta: {touched, error}, ...rest}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <textarea {...input} {...rest} />
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

@reduxForm({
  form: 'invitation',
  validate
})
@connect(
  state => ({
    loaded: state.invitation.loaded,
    loading: state.invitation.loading,
    response: state.invitation.response,
    user: state.auth.user
  }),
  {sendInvitation, pushState}
)
class Invitation extends Component {
  static propTypes = {
    sendInvitation: React.PropTypes.func.isRequired,
    response: React.PropTypes.object,
    loading: React.PropTypes.boolean,
    intl: intlShape.isRequired,
    user: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    initialize: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
    pushState: PropTypes.bool.isRequired,
  };
  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  componentDidMount() {
    this.props.initialize({
      emails: ['']
    });
  }

  onSubmit = (data) => {
    this.props.sendInvitation(data)
      .then(response => {
        if (response && response.message) {
          this.props.pushState('/invitation/sent');
        }
      });
  };

  renderEmails = ({fields}) => (
    <div>
      {fields && fields.map((item, index)=> (
        <div className={styles.input} key={index}>
          <FormattedMessage
            id="Invitation.SendTo"
            defaultMessage="Send to"
          />
          <Field
            component={Input}
            name={item}
            type="text"
            style={{width: '100%', fontSize: '20px'}}
          />
        </div>
      ))}
      {fields.length < 5 && <button className={styles.btnAdd} onClick={() => fields.push()}>
        <img src={invitationAddButtonImage} alt="Add"/>
      </button>}
    </div>
  );

  render() {
    const {
      intl,
      user,
      submitting,
      invalid,
      handleSubmit
    } = this.props;

    return (
      <div id="wrap">
        <Helmet title="Commission"/>
        <div className="commission" id="contents">
          <Board>
            <div className={styles.inner}>
              <div className={styles.head}>
                <FormattedMessage
                  id="Invitation.Invitation"
                  defaultMessage="Invitation"
                />
              </div>
              <div className={styles.code}>
                <QRCode
                  size={160}
                  value={user.accountType === 'gcloud' ?
                    (`http://admin.brave.works/registration/gcloud?introducerID=${user.applicantID}&introducerName=${user.applicantName}`) :
                    (`http://mypage.brave.works/registration/brave?introducerID=${user.applicantID}&introducerName=${user.applicantName}`)}
                />
              </div>
              <FieldArray name="emails" component={this.renderEmails}/>
              <div className={styles.input}>
                <FormattedMessage
                  id="Invitation.Title"
                  defaultMessage="Title"
                />
                <Field
                  component={Input}
                  name="title"
                  type="text"
                  placeholder={
                    intl.formatMessage({id: 'Invitation.Title', defaultMessage: 'Title'})
                  }
                  style={{width: '100%', fontSize: '20px'}}/>
              </div>
              <div className={styles.input}>
                <FormattedMessage
                  id="Invitation.Message"
                  defaultMessage="Message"
                />
                <Field
                  component={Textarea}
                  name="message"
                  rows="5"
                  placeholder={
                    intl.formatMessage({id: 'Invitation.Message', defaultMessage: 'Message'})
                  }
                  style={{width: '100%', fontSize: '20px'}}/>
                <button className={styles.btnSend} disabled={submitting || invalid} onClick={handleSubmit(this.onSubmit)}>
                  <img src={invitationSendButtonImage} alt="Add"/>
                </button>
              </div>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}

export default injectIntl(Invitation);
