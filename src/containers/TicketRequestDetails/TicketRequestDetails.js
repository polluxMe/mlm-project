import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {injectIntl, intlShape, defineMessages, FormattedMessage} from 'react-intl';
import { load } from 'redux/modules/ticketRequestDetails';
import {Grid} from 'react-bootstrap';
// import { asyncConnect } from 'redux-async-connect';
import {DetailsTable} from 'components';

const messages = defineMessages({
  RequestID: {
    id: 'TicketRequestDetails.RequestID',
    defaultMessage: 'Request ID'
  },
  MemberID: {
    id: 'TicketRequestDetails.MemberID',
    defaultMessage: 'Member ID'
  },
  CreationDate: {
    id: 'TicketRequestDetails.CreationDate',
    defaultMessage: 'Creation date'
  },
  Status: {
    id: 'TicketRequestDetails.Status',
    defaultMessage: 'Status'
  },
  PurchaseAmount: {
    id: 'TicketRequestDetails.PurchaseAmount',
    defaultMessage: 'Purchase amount'
  },
  BraveAmount: {
    id: 'TicketRequestDetails.BraveAmount',
    defaultMessage: 'Brave amount'
  },
  PurchaseRate: {
    id: 'TicketRequestDetails.PurchaseRate',
    defaultMessage: 'Purchase rate'
  }
});

@connect(state => ({
  request: state.ticketRequestDetails.data
}),
  {load})
class TicketRequestDetails extends Component {
  static propTypes = {
    request: PropTypes.object,
    load: PropTypes.func,
    params: PropTypes.object,
    intl: intlShape.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {amount: 0, cashRate: 0};
  }

  componentWillMount() {
    this.props.load(this.props.params.id);
  }

  render() {
    const {intl, request} = this.props;
    let result;
    result = (
      <Grid>
        <h2>
          <FormattedMessage
            id="TicketRequestDetails.Header"
            defaultMessage="Ticket request details"
          />
        </h2>
        <DetailsTable data={request && [
          {
            label: intl.formatMessage(messages.RequestID),
            value: request._id
          },
          {
            label: intl.formatMessage(messages.MemberID),
            value: request.memberID
          },
          {
            label: intl.formatMessage(messages.CreationDate),
            value: request.createDate
          },
          {
            label: intl.formatMessage(messages.Status),
            value: request.status
          },
          {
            label: intl.formatMessage(messages.PurchaseAmount),
            value: request.ticketPurchaseAmount
          },
          {
            label: intl.formatMessage(messages.BraveAmount),
            value: request.ticketBraveAmount
          },
          {
            label: intl.formatMessage(messages.PurchaseRate),
            value: request.purchaseRate
          }
        ]}/>
      </Grid>
    );

    return result;
  }
}

export default injectIntl(TicketRequestDetails);

