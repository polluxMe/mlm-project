import React, { Component, PropTypes } from 'react';
// import config from '../../config';
import _ from 'lodash'; // eslint-disable-line
import {connect} from 'react-redux';
import {FormattedMessage, FormattedNumber} from 'react-intl';
import {Board} from 'components';
import Helmet from 'react-helmet';
import * as cashRewardCommissionActions from 'redux/modules/cashRewardCommission';
import * as mainCommissionPurchasedActions from 'redux/modules/mainCommissionPurchased';
import * as mainConfigActions from 'redux/modules/mainConfig';
import moment from 'moment';
import { LinkContainer } from 'react-router-bootstrap';

const backImg = require('../../theme/mlm/images/btn-double-back.png');

const currYear = moment().format('YYYY');
const currMonth = +moment().format('MM');
const months = [];
const years = [];
for (let ii = currYear; ii >= currYear - 3; ii -= 1) {
  years.push({key: ii, value: ii + '年'});
}
for (let ii = 1; ii <= 12; ii += 1) {
  months.push({key: ii, value: ii + '月'});
}

const Select = ({options, ...props}) =>
  <select {...props}>
    <option key="0" value="0">...</option>
    {options && options.map((item, index) => <option key={index} value={item.key}>{item.value}</option>)}
  </select>;

@connect(state => ({
  user: state.auth.user,
  commission: state.cashRewardCommission.item,
  purchased: state.mainCommissionPurchased.item,
  mainConfig: state.mainConfig.data
}),
  {
    loadCashRewardCommission: cashRewardCommissionActions.load,
    loadPurchased: mainCommissionPurchasedActions.load,
    loadMainConfig: mainConfigActions.load
  }
)
export default class Commission extends Component {
  static propTypes = {
    user: PropTypes.object,
    commission: PropTypes.object,
    dividend: PropTypes.object,
    purchased: PropTypes.object,
    mainConfig: PropTypes.object,
    loadCashRewardCommission: PropTypes.func,
    loadPurchased: PropTypes.func,
    loadMainConfig: PropTypes.func
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  state = {
    year: currYear,
    month: currMonth
  };

  componentWillMount() {
    const {loadCashRewardCommission, user, loadPurchased, loadMainConfig} = this.props;
    loadCashRewardCommission(user._id, {year: currYear, month: currMonth});
    loadPurchased(user._id, {year: currYear, month: currMonth});
    loadMainConfig();
  }

  onMonthChange = ({target: {value}}) => {
    const {loadCashRewardCommission, user, loadPurchased} = this.props;
    this.setState({month: value});
    loadCashRewardCommission(user._id, {year: this.state.year, month: value});
    loadPurchased(user._id, {year: this.state.year, month: value});
  };

  onYearChange = ({target: {value}}) => {
    const {loadCashRewardCommission, user, loadPurchased} = this.props;
    this.setState({year: value});
    loadCashRewardCommission(user._id, {year: value, month: this.state.month});
    loadPurchased(user._id, {year: value, month: this.state.month});
  };

  render() {
    const {commission, purchased, user, mainConfig} = this.props;
    const percent = mainConfig && mainConfig.cashRewardPercents && mainConfig.cashRewardPercents[(user.rank || 1) - 1];
    const createDate = commission && commission.createDate ? new Date(commission.createDate) : null;

    return (
      <div id="wrap">
        <Helmet title="Cash reward commission"/>
        <div id="contents" className="comission_detail">
          <Board>
            <div>
              <div className="main_cash">
                <h2>
                  <FormattedMessage
                    id="CashRewardCommission.CashRewardCommission"
                    defaultMessage="Cash reward commission"
                  />
                </h2>
                <div>
                  <div className="select-wrapL">
                    <Select
                      value={this.state.year}
                      options={years}
                      onChange={this.onYearChange}
                      className="year"
                    />
                  </div>
                  <div className="select-wrapR">
                    <Select
                      value={this.state.month}
                      options={months}
                      onChange={this.onMonthChange}
                      className="month"
                    />
                  </div>
                </div>
                <div style={{'clear': 'both'}}></div>
                <div>
                  <h3>
                    <FormattedMessage
                      id="CashRewardCommission.AmountOofDividenedMoney"
                      defaultMessage="Amount of dividened money"
                    />
                  </h3>
                  <p className="tac">
                    <FormattedNumber value={(commission) || 0} />
                  </p>
                  <h3>ランク／％</h3>
                  <table>
                    <tr>
                      <td>R{(user && user.rank) || 1}</td>
                      <td>{percent || 0}%</td>
                    </tr>
                  </table>
                  <h3>
                    <FormattedMessage
                      id="CashRewardCommission.SelfPurchaseAmount"
                      defaultMessage="Self purchase amount"
                    />
                  </h3>
                  <p className="tac">
                    <FormattedNumber value={(purchased && purchased.cash) || 0} />&nbsp;
                    <FormattedMessage
                      id="General.Yen"
                      defaultMessage="￥"
                    />
                  </p>
                  <h3>
                    <FormattedMessage
                      id="CashRewardCommission.AccumulatedPurchasingMoneyAmount"
                      defaultMessage="Accumulated purchasing money amount"
                    />
                  </h3>
                  <p className="tac">
                    <FormattedNumber value={purchased && purchased.sum || 0} />&nbsp;
                    <FormattedMessage
                      id="General.Yen"
                      defaultMessage="￥"
                    />
                  </p>
                  {createDate && <div>
                    <h3>
                      <FormattedMessage
                        id="MegaCommission.DayPlanningToPay"
                        defaultMessage="Day planning to pay:"
                      />
                      {createDate && createDate.getMonth() + 1}月 {createDate && createDate.getDate()}日
                    </h3>
                  </div>}
                  <div style={{padding: '10px'}}>
                    <LinkContainer to="/my_account/commissions/">
                      <img src={backImg} width="100%" />
                    </LinkContainer>
                  </div>
                </div>
              </div>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}
