import React, { Component, PropTypes } from 'react';
// import config from '../../config';
import {connect} from 'react-redux';
import {injectIntl, intlShape, FormattedMessage} from 'react-intl';
import {Board} from 'components';
import Helmet from 'react-helmet';
import {push as pushState} from 'react-router-redux';
import {Button} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

@connect(
  state => ({
    response: state.invitation.response
  }),
  {pushState}
)
class Invitation extends Component {
  static propTypes = {
    response: React.PropTypes.object,
    intl: intlShape.isRequired,
    pushState: PropTypes.bool.isRequired,
  };
  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  render() {
    const styles = require('./InvitationSent.scss');
    const {
      // intl
    } = this.props;

    return (
      <div id="wrap">
        <Helmet title="Invitation sent"/>
        <div className="commission" id="contents">
          <Board>
            <div className={styles.inner}>
              <p className="text-center">
                <FormattedMessage
                  id="InvitationSent.MailsentComplete"
                  defaultMessage="Mail sent complete"
                />
              </p>
              <p className="text-center">
                <FormattedMessage
                  id="InvitationSent.Details"
                  defaultMessage="We successfully delivered the invitation mail."
                />
              </p>
              <div className="text-center">
                <LinkContainer to="/invitation">
                  <Button
                    bsSize="large"
                    block
                  >
                    <FormattedMessage
                      id="General.OK"
                      defaultMessage="OK"
                    />
                  </Button>
                </LinkContainer>
              </div>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}

export default injectIntl(Invitation);
