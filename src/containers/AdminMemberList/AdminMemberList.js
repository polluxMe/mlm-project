import React, {Component, PropTypes} from 'react';
// import Helmet from 'react-helmet';
import {connect} from 'react-redux';
import * as memberListActions from 'redux/modules/adminMemberList';
import { isLoaded, load as loadMembers } from 'redux/modules/adminMemberList';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { asyncConnect } from 'redux-async-connect';
import {FormattedMessage} from 'react-intl';
import { push } from 'react-router-redux';
import classNames from 'classnames';
import { AdminMemberListSearchForm } from 'components';
import { Glyphicon, Button } from 'react-bootstrap';
import { Spinner } from 'components';
import { Link } from 'react-router';
import config from '../../config';

require('../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css');

@asyncConnect([{
  deferred: true,
  promise: ({store: {dispatch, getState}}) => {
    if (!isLoaded(getState())) {
      return dispatch(loadMembers());
    }
  }
}])
@connect(
  state => ({
    members: state.adminMemberList.data,
    wallets: state.adminMemberList.wallets,
    loading: state.adminMemberList.loading,
    calculating: state.adminMemberList.calculating,
    loaded: state.adminMemberList.loaded,
    totalDataSize: state.adminMemberList.totalDataSize,
    currentPage: state.adminMemberList.currentPage,
    sizePerPage: state.adminMemberList.sizePerPage,
    sorter: state.adminMemberList.sorter,
  }),
  {...memberListActions, pushState: push})
export default class AdminMemberList extends Component {
  static propTypes = {
    members: PropTypes.array,
    wallets: PropTypes.object,
    loading: PropTypes.bool,
    calculating: PropTypes.bool,
    loaded: PropTypes.bool,
    load: PropTypes.func.isRequired,
    calculate: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    totalDataSize: PropTypes.number,
    sizePerPage: PropTypes.number,
    changeSort: PropTypes.func.isRequired,
    changePage: PropTypes.func.isRequired,
    changeSizePerPage: PropTypes.func.isRequired,
    changeFilter: PropTypes.func.isRequired,
    pushState: PropTypes.func,
    changeStatus: PropTypes.func,
    changeWrittenNotice: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {searchVisible: false};
  }

  onSearch = (fields) =>
    this.onFilterChange(fields);

  onSortChange = (name, order) => {
    const sortOrder = {name: name, order: (order === 'asc') ? 1 : -1};
    this.props.changeSort(JSON.stringify(sortOrder));
  };

  onPageChange = (page, limit) => {
    this.props.changePage(page, limit);
  };

  // onSearchChange = () => {
  //
  // };

  onFilterChange = (filterObj) =>
    this.props.changeFilter(JSON.stringify(filterObj));

  onSizePerPageList = (sizePerPage) => {
    this.props.changeSizePerPage(sizePerPage);
  };

  onUploadCsv = () => {
    this.props.pushState('/members/import_csv');
  };
  render() {
    const calculateFormatter = (cell, row) => {
      if (row.status === 'temp') {
        return (<div></div>);
      }
      const calculate = this.props.calculate;
      return (
        <button type="button" className="btn btn-info" onClick={ () => calculate(cell) }>
          <Spinner loading={this.props.calculating} size={25} exclusive>
            <FormattedMessage
              id="AdminMemberList.Calculate"
              defaultMessage="Calculate"
            />
          </Spinner>
        </button>
      );
    };

    const detailsFormatter = (cell) => {
      return (
        <Link to={'/members/' + cell}>
          <FormattedMessage
            id="AdminMemberList.Details"
            defaultMessage="Details"
          />
        </Link>
      );
    };

    function dateFormatter(cell) {
      const date = new Date(cell);
      return ('0' + date.getDate()).slice(-2) + '-' +
        ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
        date.getFullYear();
    }

    function noticeFormatter(cell) {
      return cell && cell !== 'false' ? '✓' : '';
    }

    function statusFormatter(cell) {
      let status;
      switch (cell) {
        case 'temp':
          status = '仮会員';
          break;
        case 'official':
          status = '本会員';
          break;
        case 'deactivated':
          status = '無効';
          break;
        default:
          status = '';
          break;
      }
      return status;
    }

    // function numberFormatter(cell) {
    //   if (!cell) return 0;
    //   return cell;
    // }

    function braveIDFormatter(cell) {
      return cell;
    }

    const styles = require('./AdminMemberList.scss');

    const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);

    return (
      <div>
        <div className="container">
          <h2>
            <FormattedMessage
              id="MemberList.Header"
              defaultMessage="Member list"
            />
          </h2>
          <div className={classNames(styles.searchForm, {[styles.hidden]: !this.state.searchVisible })}>
            <div className={styles.wrapper}>
              <AdminMemberListSearchForm onSubmit={this.onSearch}/>
            </div>
          </div>
          <div style={{textAlign: 'center'}}>
            <button className={styles.hideButton} onClick={()=> this.setState({searchVisible: !this.state.searchVisible})}>
              <Glyphicon glyph={this.state.searchVisible ? 'arrow-up' : 'arrow-down'}/>
              &nbsp;
              {
                this.state.searchVisible ?
                  <FormattedMessage
                    id="General.HideSearch"
                    defaultMessage="Hide search"
                  /> :
                  <FormattedMessage
                    id="General.ShowSearch"
                    defaultMessage="Show search"
                  />
              }
            </button>
          </div>
          <div className="row">
            <div className="col-xs-12 col-sm-6 col-md-6 col-lg-8">
              <div className="btn-group btn-group-sm">
                <button type="button" onClick={this.onUploadCsv} className="btn btn-info react-bs-table-add-btn">
                  <Glyphicon glyph="import"/>
                  &nbsp;
                  <FormattedMessage
                    id="AdminMemberList.ImportCSV"
                    defaultMessage="Import CSV"
                  />
                </button>
                <a href={apiPath + '/api/member/csv'} className="btn btn-success react-bs-table-add-btn" download>
                  <Glyphicon glyph="export"/>
                  &nbsp;
                  <FormattedMessage
                    id="AdminMemberList.ExportCSV"
                    defaultMessage="Export CSV"
                  />
                </a>
                <a href={apiPath + '/api/member/xls'} className="btn btn-success react-bs-table-add-btn" download>
                  <Glyphicon glyph="export"/>
                  &nbsp;
                  <FormattedMessage
                    id="AdminMemberList.ExportXLS"
                    defaultMessage="Export XLS"
                  />
                </a>
                <Button
                  href="/registration/brave"
                  target="_blank"
                >
                  <Glyphicon glyph="plus"/>
                  &nbsp;
                  <FormattedMessage
                    id="AdminMemberList.AddMember"
                    defaultMessage="Add member"
                  />
                </Button>
              </div>
            </div>
          </div>
        </div>
        <BootstrapTable
          cellEdit = {{
            mode: 'click',
            blurToSave: true,
            afterSaveCell: (item, fieldName, fieldValue) => {
              item[fieldName] = fieldValue;

              if (fieldName === 'status') {
                this.props.changeStatus(item);
              }
              if (fieldName === 'writtenNotice') {
                this.props.changeWrittenNotice(item);
              }
            },
            beforeSaveCell: (item, fieldName, fieldValue) => {
              if (item[fieldName] === fieldValue) return true;
              return confirm(`Are you sure you want to change ${fieldName} to ${fieldValue}?`);
            }
          }}
          data={this.props.members || []}
          remote
          hover
          condensed
          pagination
          fetchInfo={ { dataTotalSize: this.props.totalDataSize } }
          options={ { sizePerPage: this.props.sizePerPage,
                      onPageChange: this.onPageChange,
                      sizePerPageList: [ 5, 10, 20],
                      pageStartIndex: 1,
                      page: this.props.currentPage,
                      // onSearchChange: this.onSearchChange,
                      onSortChange: this.onSortChange,
                      // onFilterChange: this.onFilterChange,
                      onSizePerPageList: this.onSizePerPageList,
                      hideSizePerPage: true,
                      noDataText: '表示されるデータがございません。'
                     }
                  }
        >
          <TableHeaderColumn dataField="username" dataSort dataAlign="center" dataFormat={braveIDFormatter} width={250} editable={false}>
            <FormattedMessage
              id="AdminMemberList.LoginID"
              defaultMessage="LOGIN ID"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="applicantID" dataSort dataAlign="center" dataFormat={braveIDFormatter} editable={false}>
            <FormattedMessage
              id="AdminMemberList.BraveID"
              defaultMessage="BRAVE ID"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="applicantName" dataSort dataAlign="center" editable={false}>
            <FormattedMessage
              id="AdminMemberList.Name"
              defaultMessage="Name"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="applicantKatakana" dataSort dataAlign="center" editable={false}>
            <FormattedMessage
              id="AdminMemberList.NameFurigana"
              defaultMessage="Name (Furigana)"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="createDate" dataSort dataAlign="center" dataFormat={dateFormatter} editable={false}>

            <FormattedMessage
              id="AdminMemberList.registeredDate"
              defaultMessage="registered date"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="isDeposited" dataSort dataFormat={noticeFormatter} dataAlign="center" editable={false}>
            <FormattedMessage
              id="AdminMemberList.depositStatus"
              defaultMessage="Deposit status"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="rank" dataSort dataAlign="center" editable={false}>
            <FormattedMessage
              id="AdminMemberList.Rank"
              defaultMessage="Rank"
            />
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="status"
            dataSort
            dataAlign="center"
            editable={{type: 'select', options: {values: ['temp', 'official', 'deactivated']}}}
            dataFormat={statusFormatter}
            width={80}
          >
            <FormattedMessage
              id="AdminMemberList.Status"
              defaultMessage="Status"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="introducerID" dataSort dataAlign="center" editable={false}>
            <FormattedMessage
              id="AdminMemberList.BraveIntroducerID"
              defaultMessage="BRAVE Introducer ID"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="_id" isKey dataFormat={ detailsFormatter } width={100}>
            <FormattedMessage
              id="AdminMemberList.Details"
              defaultMessage="Details"
            />
          </TableHeaderColumn>
          <TableHeaderColumn dataField="_id" dataFormat={ calculateFormatter } width={100}>
            <FormattedMessage
              id="AdminMemberList.Calculate"
              defaultMessage="Calculate"
            />
          </TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}
