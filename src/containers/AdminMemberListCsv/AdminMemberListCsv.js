import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {reduxForm, Field, SubmissionError} from 'redux-form';
import {Form, FormGroup, FormControl, Button, ButtonGroup} from 'react-bootstrap';
import {FormattedMessage} from 'react-intl';
import config from '../../config.js';
import { Spinner } from 'components';
import { push } from 'react-router-redux';
import { load as loadMembers } from 'redux/modules/adminMemberList';

const styles = require('./AdminMemberListCsv.scss');

const Input = ({type, placeholder, input, disabled, accept, meta: {touched, error}}) =>
  <span className="text-center">
    <span className={(error && touched ? ' has-error' : '')}>
      <FormControl type={type} {...input} placeholder={placeholder} disabled={disabled} accept={accept} className={styles.file} />
    </span>
    {error && touched && <div className="text-danger">{error}</div>}
  </span>;

@reduxForm({
  form: 'csvImportForm'
})
@connect(
  state => ({
    form: state.form.csvImportForm
  }),
  {pushState: push, loadMembers}
)

export default class AdminMemberListCsv extends Component {
  static propTypes = {
    invalid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired,
    error: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    pushState: PropTypes.func,
    loadMembers: PropTypes.func
  };

  submit = (data) => {
    const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);
    const formData = new FormData();

    formData.append('csv', data.csv[0]);

    return fetch(apiPath + '/api/member/csv', {
      method: 'POST',
      body: formData,
      mode: 'cors',
      credentials: 'include',
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);

        if (responseJson && responseJson.code && responseJson.code === 500) {
          throw new SubmissionError({_error: responseJson.msg});
        }

        return this.props.loadMembers();
      })
      .then(() => {
        this.props.pushState('/members/');
      })
      .catch((err) => {
        console.log('error', err);
        throw new SubmissionError({_error: err.errors && err.errors._error});
      });
  };

  render() {
    const { valid, error, handleSubmit, submitting, pristine } = this.props;

    return (
      <div>
        <div className="container">
          <h2>
            <FormattedMessage
              id="AdminMemberList.ImportCSV"
              defaultMessage="Import CSV"
            />
          </h2>
          <Form horizontal onSubmit={handleSubmit(this.submit)}>
            <FormGroup className="text-center">
              <Field name="csv" type="file" component={Input} accept="text/csv" />
              <p>
                <a href="/example.csv">
                  <FormattedMessage
                    id="AdminMemberListCsv.Example"
                    defaultMessage="Download example"
                  />
                </a>
              </p>
            </FormGroup>
            <FormGroup className="text-center">
              {error && <div className="text-danger">{error}</div>}
              <ButtonGroup vertical>
                <Button
                  bsSize="large"
                  disabled={!valid || pristine || submitting || error}
                  onClick={handleSubmit(this.submit)}
                >
                  <Spinner loading={submitting} size={25}>
                    <i className="fa fa-paper-plane"/>&nbsp;
                    <FormattedMessage
                      id="AdminMemberListCsv.UploadFile"
                      defaultMessage="Upload CSV"
                    />
                  </Spinner>
                </Button>
              </ButtonGroup>
            </FormGroup>
          </Form>
        </div>
      </div>
    );
  }
}
