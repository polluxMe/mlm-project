import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {bindActionCreators} from 'redux';
import * as authActions from 'redux/modules/auth';
// import { browserHistory } from 'react-router';
// import { saveUser } from 'redux/modules/auth';
import {FormattedMessage} from 'react-intl';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import {Form, FormGroup, FormControl, ControlLabel, Button} from 'react-bootstrap';
import { replace as replaceRoute } from 'react-router-redux';
import { Spinner } from 'components';
import config from '../../config.js';

const Input = ({type, placeholder, input, disabled, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <FormControl type={type} {...input} placeholder={placeholder} disabled={disabled} />
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

@reduxForm({
  form: 'forgetPassword',
  enableReinitialize: true
  // validate: LoginValidation
})
@connect(
  state => ({user: state.auth.user}),
  dispatch => bindActionCreators({...authActions, replaceRoute}, dispatch)
)
export default class ForgetPassword extends Component {
  static propTypes = {
    user: PropTypes.object,
    login: PropTypes.func,
    logout: PropTypes.func,
    saveUser: PropTypes.func,
    replaceRoute: PropTypes.func,
    location: PropTypes.object,
    initialize: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {isSent: false};
  }

  submit = (data) => {
    this.setState({isSent: false});
    return Promise.resolve()
      .then(() => {
        const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);

        return fetch(apiPath + '/api/auth/forget_password', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          credentials: 'include',
          mode: 'cors',
          body: JSON.stringify(data)
        })
          .then((response) => response.json())
          .then((response) => {
            if (response.code === 500) {
              throw new SubmissionError({_error: response.msg});
            } else {
              this.props.initialize({
                username: ''
              });
              this.setState({isSent: true});
            }
          })
          .catch((err) => {
            console.log('error', err);
            throw new SubmissionError({_error: err.errors && err.errors._error});
          });
      });
  };

  render() {
    /* eslint-disable */
    const {
      handleSubmit,
      pristine,
      error,
      submitting
    } = this.props;
    /* eslint-disable */

    const styles = require('./ForgetPassword.scss');

    return (
      <div className={styles.loginPage + ' container'}>
        <Helmet title="Forget password"/>
        <div className={'row ' + styles.mTLg}>
          <div className="col-sm-12">
            <div className="panel panel-default panel-border-color panel-border-color-warning">
              <div className="panel-heading panel-heading-divider">
                <h1>
                  <FormattedMessage
                    id="ForgetPassword.Header"
                    defaultMessage="Forget password"
                  />
                </h1>
              </div>
              <div className="panel-body">
                <p>
                  ログインIDを入力の上、送信ボタンを押して下さい。<br />
                  パスワード再設定用URLが記載されたメールを送信します。
                </p>
                <Form onSubmit={handleSubmit(this.submit)}>
                  <FormGroup
                    controlId="login"
                  >
                    <ControlLabel>ログイン ID</ControlLabel>
                    <Field name="username"
                           type="text"
                           component={Input}
                           placeholder="ログイン ID"
                    />
                    <FormControl.Feedback />
                  </FormGroup>
                  <div style={{textAlign: 'center'}}>
                    {error && <div className="text-danger">{error}</div>}
                    {this.state.isSent && <div className="text-success">ご登録頂いたメールアドレスに、メールを送信しました。</div>}
                    <Button
                      bsSize="large"
                      type="submit"
                      disabled={pristine || submitting || error}
                    >
                      <Spinner loading={submitting} size={25}>
                        <i className="fa fa-sign-in"/>{' '}
                        <FormattedMessage
                          id="ForgetPassword.SendEmail"
                          defaultMessage="Send email"
                        />
                      </Spinner>
                    </Button>
                  </div>
                </Form>
              </div>
              </div>
            </div>
          </div>
      </div>
    );
  }
}
