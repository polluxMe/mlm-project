import React, {Component, PropTypes} from 'react';
// import Helmet from 'react-helmet';
import {connect} from 'react-redux';
import * as memberDetailsActions from 'redux/modules/adminMemberDetails';
import {FormattedMessage} from 'react-intl';
import { push } from 'react-router-redux';
import {Table, Button} from 'react-bootstrap';
import {SwitchableInput, Spinner} from '../../components';
import moment from 'moment';
import _ from 'lodash'; //eslint-disable-line

@connect(
  state => ({
    member: state.adminMemberDetails.member,
    loaded: state.adminMemberDetails.loaded,
    loading: state.adminMemberDetails.loading,
    rankHistory: state.adminMemberDetails.rankHistory,
    loadedRankHistory: state.adminMemberDetails.loadedRankHistory,
    loadingRankHistory: state.adminMemberDetails.loadingRankHistory,
    savingRankHistory: state.adminMemberDetails.savingRankHistory,
  }),
  {...memberDetailsActions, pushState: push})
export default class AdminMemberList extends Component {
  static propTypes = {
    member: PropTypes.object,
    rankHistory: PropTypes.array,
    loaded: PropTypes.bool,
    loading: PropTypes.bool,
    params: PropTypes.object,
    load: PropTypes.func,
    loadRankHistory: PropTypes.func,
    saveRankHistory: PropTypes.func,
    loadedRankHistory: PropTypes.bool,
    loadingRankHistory: PropTypes.bool,
    savingRankHistory: PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.state = {notes: []};
  }

  componentWillMount() {
    this.props.load(this.props.params.id);
    this.props.loadRankHistory(this.props.params.id)
      .then(() => {
        const notes = {};
        _.each(this.props.rankHistory, item => {
          notes[item._id] = item.note;
        });

        this.setState({
          ...this.state,
          notes
        });
      });
  }

  onFieldChange = (fieldName) => {
    return (value) => {
      this.setState({notes: {...this.state.notes, [fieldName]: value}});
    };
  };

  onSave = () => {
    this.props.saveRankHistory(this.props.member._id, {notes: this.state.notes})
      .then(() => {
        this.setState({saved: true});
      });
  };

  render() {
    const {member, loaded, loading, rankHistory, loadingRankHistory, loadedRankHistory, savingRankHistory} = this.props;
    return (
        <div className="container">
          <h2>
            <FormattedMessage
              id="MemberRankHistory.Header"
              defaultMessage="Rank history: "
            />
            {member && member.applicantName}
          </h2>
          {
            (loading || loadingRankHistory) && <span><FormattedMessage
              id="General.Loading"
              defaultMessage="Loading..."
            /></span>
          }
          {
            loaded && loadedRankHistory &&
            <div>
              <Table striped bordered condensed>
                <thead>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="MemberRankHistory.Date"
                        defaultMessage="Date"
                      />
                    </th>
                    <th>
                      <FormattedMessage
                        id="MemberRankHistory.Rank"
                        defaultMessage="Rank"
                      />
                    </th>
                    <th>
                      <FormattedMessage
                        id="MemberRankHistory.Note"
                        defaultMessage="Note"
                      />
                    </th>
                  </tr>
                </thead>
                <tbody>
                {rankHistory.map(item => (
                  <tr key={item._id}>
                    <td>{moment(item.createDate).format('YYYY-MM-DD HH:mm:ss')}</td>
                    <td>{item.rank}</td>
                    <td>
                      <SwitchableInput editable value={this.state.notes[item._id] || '-'} onChange={this.onFieldChange(item._id)}/>
                    </td>
                  </tr>
                )) }
                </tbody>
              </Table>
            </div>
          }
          {loaded && loadedRankHistory && this.state.saved && (
            <p>
              <FormattedMessage
                id="EditAccountInfo.Saved"
                defaultMessage="Account info successfuly saved"
              />
            </p>)}
          {loaded && loadedRankHistory &&
            <div className="text-center">
              <Button bsStyle="success" onClick={this.onSave}>
                <Spinner loading={savingRankHistory} size={25} exclusive>
                  <FormattedMessage
                    id="General.Save"
                    defaultMessage="Save"
                  />
                </Spinner>
              </Button>
            </div>
          }
        </div>
    );
  }
}
