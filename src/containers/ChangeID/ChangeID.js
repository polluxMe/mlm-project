import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {FormattedMessage} from 'react-intl';
import { ChangeIDForm } from 'components';

@connect(
  state => ({user: state.auth.user})
)
export default class ChangeID extends Component {
  static propTypes = {
    user: PropTypes.object,
    login: PropTypes.func,
    logout: PropTypes.func
  }

  render() {
    const styles = require('./ChangeID.scss');
    return (
      <div className={styles.loginPage + ' container'}>
        <Helmet title="Login"/>
        <div className={'row ' + styles.mTLg}>
          <div className="col-sm-12">
            <div className="panel panel-default panel-border-color panel-border-color-warning">
              <div className="panel-heading panel-heading-divider">
                  <p>
                    <FormattedMessage
                      id="LoginFlow.ThisIsTheFirstTimeLoggingIn"
                      defaultMessage="This is the first time logging in."
                    />
                  </p>
                  <p>
                    <FormattedMessage
                      id="LoginFlow.PleaseChangeYourIDAndPassword"
                      defaultMessage="Please change your ID and password."
                    />
                  </p>
                </div>
                <div className="panel-body">
                  <ChangeIDForm />
                </div>
              </div>
            </div>
          </div>
      </div>
    );
  }
}
