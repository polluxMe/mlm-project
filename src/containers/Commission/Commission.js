import React, { Component, PropTypes } from 'react';
// import config from '../../config';
import _ from 'lodash'; // eslint-disable-line
import {connect} from 'react-redux';
import {FormattedMessage, FormattedNumber} from 'react-intl';
import {Board} from 'components';
import Helmet from 'react-helmet';
import * as mainCommissionActions from 'redux/modules/mainCommission';
import * as mainCommissionDividendActions from 'redux/modules/mainCommissionDividend';
import * as mainCommissionPurchasedActions from 'redux/modules/mainCommissionPurchased';
import * as mainConfigActions from 'redux/modules/mainConfig';
import moment from 'moment';
import { LinkContainer } from 'react-router-bootstrap';

const backImg = require('../../theme/mlm/images/btn-double-back.png');

const currYear = moment().format('YYYY');
const currMonth = +moment().format('MM');
const months = [];
const years = [];
for (let ii = currYear; ii >= currYear - 3; ii -= 1) {
  years.push({key: ii, value: ii + '年'});
}
for (let ii = 1; ii <= 12; ii += 1) {
  months.push({key: ii, value: ii + '月'});
}

const Select = ({options, ...props}) =>
  <select {...props}>
    <option key="0" value="0">...</option>
    {options && options.map((item, index) => <option key={index} value={item.key}>{item.value}</option>)}
  </select>;

@connect(state => ({
  user: state.auth.user,
  commission: state.mainCommission.item,
  dividend: state.mainCommissionDividend.item,
  purchased: state.mainCommissionPurchased.item,
  mainConfig: state.mainConfig.data
}),
  {
    loadMainCommission: mainCommissionActions.load,
    loadDividend: mainCommissionDividendActions.load,
    loadPurchased: mainCommissionPurchasedActions.load,
    loadMainConfig: mainConfigActions.load
  }
)
export default class Commission extends Component {
  static propTypes = {
    user: PropTypes.object,
    commission: PropTypes.object,
    dividend: PropTypes.object,
    purchased: PropTypes.object,
    mainConfig: PropTypes.object,
    loadMainCommission: PropTypes.func,
    loadDividend: PropTypes.func,
    loadPurchased: PropTypes.func,
    loadMainConfig: PropTypes.func
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  state = {
    year: currYear,
    month: currMonth
  };

  componentWillMount() {
    const {loadMainCommission, loadDividend, user, loadPurchased, loadMainConfig} = this.props;
    loadMainCommission(user._id, {year: currYear, month: currMonth});
    loadDividend(user._id, {year: currYear, month: currMonth, type: 'main'});
    loadPurchased(user._id, {year: currYear, month: currMonth});
    loadMainConfig();
  }

  onMonthChange = ({target: {value}}) => {
    const {loadMainCommission, loadDividend, user, loadPurchased} = this.props;
    this.setState({month: value});
    loadMainCommission(user._id, {year: this.state.year, month: value});
    loadDividend(user._id, {year: this.state.year, month: value, type: 'main'});
    loadPurchased(user._id, {year: this.state.year, month: value});
  };

  onYearChange = ({target: {value}}) => {
    const {loadMainCommission, loadDividend, user, loadPurchased} = this.props;
    this.setState({year: value});
    loadMainCommission(user._id, {year: value, month: this.state.month});
    loadDividend(user._id, {year: value, month: this.state.month, type: 'main'});
    loadPurchased(user._id, {year: value, month: this.state.month});
  };

  render() {
    const {commission, purchased, user, mainConfig} = this.props;
    const campaignPercent = mainConfig && mainConfig.campaignPercents && _.find(mainConfig.campaignPercents, {year: this.state.year + '', month: this.state.month + ''});
    const percent = campaignPercent && user && campaignPercent.percent && campaignPercent.percent[user.rank || 1];

    return (
      <div id="wrap">
        <Helmet title="Commission"/>
        <div id="contents" className="comission_detail">
          <Board>
            <div className="main_com">
              <div>
                <h2>
                  <FormattedMessage
                    id="Commission.MainCommission"
                    defaultMessage="Main commission"
                  />
                </h2>
                <div>
                  <div className="select-wrapL">
                    <Select
                      value={this.state.year}
                      options={years}
                      onChange={this.onYearChange}
                      className="year"
                    />
                  </div>
                  <div className="select-wrapR">
                    <Select
                      value={this.state.month}
                      options={months}
                      onChange={this.onMonthChange}
                      className="month"
                    />
                  </div>
                </div>
                <div style={{'clear': 'both'}}></div>
                <div>
                  <h3>
                    <FormattedMessage
                      id="Commission.NumberOfDividenedVS"
                      defaultMessage="Number of dividened VS"
                    />
                  </h3>
                  <p className="tac">
                    <FormattedNumber value={(commission && commission.amount) || 0} />
                    &nbsp;VS
                  </p>
                  <h3>
                    <FormattedMessage
                      id="Commission.NumberOfPayedVS"
                      defaultMessage="Number of payed VS"
                    />
                  </h3>
                  <p className="tac">
                    <FormattedNumber value={(commission && commission.amount) || 0} />
                    &nbsp;VS
                  </p>
                  <h3>
                    <FormattedMessage
                      id="Commission.Rank"
                      defaultMessage="Rank"
                    />　%
                  </h3>
                  <table>
                    <tr>
                      <td>R{(user && user.rank) || 1}</td>
                      <td>{percent || 0}%</td>
                    </tr>
                  </table>
                  <h3>
                    <FormattedMessage
                      id="Commission.SelfPurchaseAmount"
                      defaultMessage="Self purchase money amount"
                    />
                  </h3>
                  <p className="comission_detail_main_com_p">
                    <FormattedNumber value={(purchased && purchased.sum) || 0} />
                    &nbsp;
                    <FormattedMessage
                      id="General.Yen"
                      defaultMessage="￥"
                    />
                  </p>
                  <div style={{padding: '10px'}}>
                    <LinkContainer to="/my_account/commissions/">
                      <img src={backImg} width="100%" />
                    </LinkContainer>
                  </div>
                </div>
              </div>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}
