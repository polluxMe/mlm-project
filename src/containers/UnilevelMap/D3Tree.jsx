import React, {PropTypes} from 'react';
import d3 from 'd3';
import Faux from 'react-faux-dom';

const renderTree = (treeData, svgWidth, svgHeight, svgDomNode, images, applicantID) => {
  const margin = {top: 50, right: 10, bottom: 10, left: 10};
  const width = svgWidth;
  const height = svgHeight;
  const tipWidth = 100;

  let ii = 0;
  let root;

  // Cleans up the SVG on re-render
  d3.select(svgDomNode).selectAll('*').remove();

  const tree = d3.layout.tree()
    .size([height, width]);

  const diagonal = d3.svg.diagonal()
    .projection(dd => [dd.x, dd.y]);

  const svg = d3.select(svgDomNode)
    .attr('width', width + margin.right + margin.left)
    .attr('height', height + margin.top + margin.bottom)
    .append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

  // Define the div for the tooltip
  const tip = d3.select('body').append('div')
    .attr('class', 'tooltip')
    .style('opacity', 0);

  root = treeData[0];
  root.x0 = 0;
  root.y0 = 0;

  function update(source) {
    // Compute the new tree layout.
    const nodes = tree.nodes(root).reverse();
    const links = tree.links(nodes);

    // Normalize for fixed-depth.
    nodes.forEach((dd) => { dd.y = dd.depth * 80; });

    // Update the nodes…
    const node = svg.selectAll('g.node')
      .data(nodes, dd => dd.id || (dd.id = ++ii));

    // Enter any new nodes at the parent's previous position.
    const nodeEnter = node.enter().append('g')
      .attr('class', 'node')
      .attr('transform', () => 'translate(' + source.x0 + ',' + source.y0 + ')');
    // .on('click', click);

    function tipOut() {
      tip.transition()
        .duration(500)
        .style('opacity', 0);
    }

    let currTimeount;
    function tipOn(dd) {
      tip.transition()
        .duration(200)
        .style('opacity', 0.9);

      const tipHtml = `
          ${dd.nickname || ''} <br />
          ランク: ${(dd.rank || 0)} <br />
        `;

      const layerXPoint = d3.event.pageX - (tipWidth / 2);
      tip.html(tipHtml)
        .style('left', (layerXPoint) + 'px')
        .style('top', (d3.event.pageY + 15) + 'px');

      clearTimeout(currTimeount);
      currTimeount = setTimeout(() => {tipOut();}, 1000);
    }

    nodeEnter
      .append('svg:a')
      .append('image')
      .attr('xlinkHref', (dd) => dd.applicantID === applicantID ? images.gray : images.blue)
      .attr('x', -16)
      .attr('y', -16)
      .attr('width', 32)
      .attr('height', 32)
      .on('click', tipOn);
      // .on('mouseover', (dd) => {
      //   tip.transition()
      //     .duration(200)
      //     .style('opacity', 0.9);
      //
      //   const tipHtml = `
      //     applicantID: ${dd.applicantID} <br />
      //     applicantName: ${dd.applicantName} <br />
      //     rank: ${(dd.rank || 0)} <br />
      //   `;
      //
      //   tip.html(tipHtml)
      //     .style('left', (d3.event.pageX) + 'px')
      //     .style('top', (d3.event.pageY - 28) + 'px');
      // })
      // .on('mouseout', () => {
      //   tip.transition()
      //     .duration(500)
      //     .style('opacity', 0);
      // });

    nodeEnter.append('text')
      .attr('y', (dd) => dd.children || dd._children ? -30 : 30)
      .attr('dy', '.35em')
      .attr('text-anchor', 'middle')
      .text(dd => dd.rank !== undefined ? `${dd.rank}` : 0)
      .style('fill-opacity', 1);

    // Transition nodes to their new position.
    const nodeUpdate = node
    // .duration(duration)
      .attr('transform', dd => 'translate(' + dd.x + ',' + dd.y + ')');

    nodeUpdate.select('text')
      .style('fill-opacity', 1);

    // Transition exiting nodes to the parent's new position.
    const nodeExit = node.exit()
      .attr('transform', () => 'translate(' + source.x + ',' + source.y + ')')
      .remove();

    nodeExit.select('circle')
      .attr('r', 1e-6);

    nodeExit.select('text')
      .style('fill-opacity', 1e-6);

    // Update the links…
    const link = svg.selectAll('path.link')
      .data(links, dd => dd.target.id);

    // Enter any new links at the parent's previous position.
    link.enter().insert('path', 'g')
      .attr('class', 'link')
      // .style('stroke', (dd) => dd.target.level)
      .attr('d', () => {
        const oo = {x: source.x0, y: source.y0};
        return diagonal({source: oo, target: oo});
      });

    // Transition links to their new position.
    link
      .attr('d', diagonal);

    // Transition exiting nodes to the parent's new position.
    link.exit().transition()
    // .duration(duration)
      .attr('d', () => {
        const oo = {x: source.x, y: source.y};
        return diagonal({source: oo, target: oo});
      })
      .remove();

    // Stash the old positions for transition.
    nodes.forEach(dd => {
      dd.x0 = dd.x;
      dd.y0 = dd.y;
    });
  }

  update(root);
};

const D3Tree = React.createClass({
  propTypes: {
    treeData: PropTypes.array.isRequired,
    svgWidth: PropTypes.number.isRequired,
    svgHeight: PropTypes.number.isRequired,
    images: PropTypes.object.isRequired,
    applicantID: PropTypes.string.isRequired
  },

  mixins: [
    Faux.mixins.core,
    Faux.mixins.anim
  ],

  getInitialState() {
    return {
      chart: 'loading...'
    };
  },

  componentDidMount() {
    const faux = this.connectFauxDOM('svg', 'chart');
    // Render the tree usng d3 after first component mount
    renderTree(this.props.treeData, this.props.svgWidth, this.props.svgHeight, faux, this.props.images, this.props.applicantID);

    this.animateFauxDOM(800);
  },

  /* shouldComponentUpdate: function(nextProps) {
   const faux = this.connectFauxDOM('svg', 'chart');
   // Delegate rendering the tree to a d3 function on prop change
   renderTree(nextProps.treeData, this.props.svgWidth, this.props.svgHeight, faux);

   // Do not allow react to render the component on prop change
   return false;
   }*/

  render() {
    return (
      <div>
        {this.state.chart}
      </div>
    );
  }
});

export default D3Tree;
