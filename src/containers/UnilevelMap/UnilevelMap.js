import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
// import Helmet from 'react-helmet';
import D3Tree from './D3Tree';
import {bindActionCreators} from 'redux';
import * as mapActions from 'redux/modules/unilevelMap';
import dimensions from 'react-dimensions';
import {FormattedMessage} from 'react-intl';
import { Board } from 'components';
import { LinkContainer } from 'react-router-bootstrap';

const images = {
  'blue': require('../Map/img/blue_map_member.png'),
  'gray': require('../Map/img/gray_map_member.png'),
  'red': require('../Map/img/red_map_member.png'),
  'yellow': require('../Map/img/yellow_map_member.png'),
  'orange': require('../Map/img/orange_map_member.png')
};

const backImg = require('../../theme/mlm/images/btn-double-back.png');

@connect(
  state => ({
    user: state.auth.user,
    map: state.unilevelMap.data,
    loaded: state.unilevelMap.loaded,
    left: state.unilevelMap.left,
    right: state.unilevelMap.right,
    top: state.unilevelMap.top,
    bottom: state.unilevelMap.bottom
  }),
  dispatch => bindActionCreators(mapActions, dispatch)
)
class UnilevelMap extends Component {
  static propTypes = {
    user: PropTypes.object,

    map: PropTypes.array,
    loaded: PropTypes.bool.isRequired,
    load: PropTypes.func.isRequired,

    left: PropTypes.number.isRequired,
    right: PropTypes.number.isRequired,
    top: PropTypes.number.isRequired,
    bottom: PropTypes.number.isRequired,

    shiftLeft: PropTypes.func.isRequired,
    shiftRight: PropTypes.func.isRequired,
    shiftTop: PropTypes.func.isRequired,
    shiftBottom: PropTypes.func.isRequired,

    containerWidth: PropTypes.number.isRequired,
    containerHeight: PropTypes.number.isRequired,
    location: PropTypes.object.isRequired
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  componentDidMount() {
    const {user, load, location} = this.props;
    const userId = (location && location.query && location.query.id) || (user && user._id);
    load(userId);

    setTimeout(() => {
      document.querySelector('.parentBlock').scrollLeft += 100000;
      const scroll = document.querySelector('.parentBlock').scrollLeft;
      document.querySelector('.parentBlock').scrollLeft = (scroll / 2);
    }, 1000);
  }

  render() {
    require('../Map/Map.css');
    const styles = require('./UnilevelMap.scss');
    const {map, right, top, bottom, loaded, user} = this.props;

    // const containerWidth = 520;
    // const containerHeight = 815;

    const xCount = map && map[0] && map[0].maxWidth || 8;
    const yCount = map && map[0] && map[0].maxDepth || 4;

    let multiplier = xCount / yCount;
    let multiplierY = 1;
    if (multiplier < 1) {
      multiplierY = yCount / xCount;
      multiplier = 1;
    }

    const svgWidth = xCount * multiplierY * 150;
    const svgHeight = yCount * multiplier * 150;

    const stl = {
      right,
      top,
      bottom,
      width: svgWidth,
      height: svgHeight
    };

    // if (containerWidth < svgWidth) {
    //   // stl.left -= (svgWidth - containerWidth) / 2;
    // }

    // const isShiftRightButtonDisabled = (svgWidth + stl.left) < containerWidth;
    // const isShiftBottomButtonDisabled = (svgHeight + stl.top) < containerWidth;
    //
    // const isShiftLeftButtonDisabled = stl.left >= 0;
    // const isShiftTopButtonDisabled = stl.top >= 0;

    const applicantID = user && user.applicantID;

    return (
      <div id="wrap">
        <div id="contents">
          <Board>
            <div className={'parentBlock ' + styles.parent}>
              <p className="text-center">
                <LinkContainer to="/my_account/commissions/">
                  <img src={backImg} width="250" style={{'marginLeft': (svgWidth / 2) - 115}}/>
                </LinkContainer>
              </p>
              <div className="block" style={stl}>
                {loaded ? <D3Tree treeData={map} svgWidth={svgWidth} svgHeight={svgHeight} images={images} applicantID={applicantID} /> :
                  <FormattedMessage
                    id="General.Loading"
                    defaultMessage="Loading..."
                  />}
              </div>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}

module.exports = dimensions()(UnilevelMap);
