import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import { initialize } from 'redux-form';
import { GCloudRegistrationForm, FormHeader } from 'components';
import { browserHistory } from 'react-router';
import { setTempUser } from 'redux/modules/register';
import moment from 'moment';
import cookie from 'react-cookie';

const GCLOUD_FORM_COLOR = '#009688';

@connect(
  () => ({}),
  {
    initialize,
    setTempUser
  })
export default class GCloudRegistration extends Component {
  static propTypes = {
    initialize: PropTypes.func.isRequired
  }

  componentWillMount() {
    this.handleInitialize();
  }

  handleSubmit = (response) => {
    this.props.setTempUser({
      username: response.applicantName,
      id: response.applicantID,
      password: response.pwd
    });
    browserHistory.push('/registration/completion');
  }

  handleInitialize = () => {
    const currentYear = +moment().format('YYYY');
    const currentMonth = +moment().format('M');
    const currentDay = +moment().format('D');

    const introducerID = cookie.load('introducerID');

    this.props.initialize('gCloudRegistration', {
      // applicantName: 'A',
      // applicantKatakana: '会社',
      // nickname: 'A',
      introducerID,
      purchaseType: 'cash',
      profile: {
        // gender: 'male',
        // phone: '300000000',
        // mobilePhone: '22222222',
        // nationality: 'Japan',
        // postal: '11111111',
        // address: '2342342',
        yearOfBirth: currentYear,
        monthOfBirth: currentMonth,
        dayOfBirth: currentDay,
        nationality: 'Japan'
        // dateOfBirth: '09/07/2016',
      },
      // email: 'a@gmail.com',
      // introducerID: '111',
    });
  }

  render() {
    return (
      <div className="container" style={{background: 'white'}}>
        <h1>管理者用 GCLOUD登録</h1>
        <Helmet title="GCloud"/>
        <FormHeader color={GCLOUD_FORM_COLOR}/>
        <GCloudRegistrationForm {...this.props} onSubmit={this.handleSubmit}/>
      </div>
    );
  }
}

GCloudRegistration.propTypes = {
  setTempUser: React.PropTypes.func
};
