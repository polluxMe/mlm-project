import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import { Board } from 'components';
import {bindActionCreators} from 'redux';
import {loadWallet} from 'redux/modules/memberInfo';
import { Link } from 'react-router';

const brave1WayTitleImage = require('theme/mlm/images/1way-title.png');
const brave1WayTicketImage = require('theme/mlm/images/1way-ticket.png');
const brave1WayHistoryImage = require('theme/mlm/images/1way-history.png');


@connect(state => ({
  user: state.auth.user,
  wallet: state.memberInfo.wallet
}),
  dispatch => bindActionCreators({loadWallet}, dispatch)
)
export default class Brave1Way extends Component {
  static propTypes = {
    user: PropTypes.object,
    wallet: PropTypes.object,
    loadWallet: PropTypes.func,
  };

  componentWillMount() {
    this.props.loadWallet(this.props.user._id);
  }

  render() {
    return (
      <div id="wrap">
        <Helmet title="Brave 1 way"/>
        <div className="three-way" id="contents">
          <Board>
            <h2 className="mb10"><img src={brave1WayTitleImage} alt="BRAVE$ 1WAY"/></h2>
            <dl>
              <dt>保有3WAY</dt>
              <dd>{this.props.wallet ? Math.floor(this.props.wallet.commission / 2) + ' B$' : 0 + ' B$'}</dd>
            </dl>
            <div className="mb10">
              <Link to="/my_account/brave1way/tickets">
                <img src={brave1WayTicketImage} alt="チケットへ変換"/>
              </Link>
            </div>
            <div>
              <Link to="/my_account/brave1way/history">
                <img src={brave1WayHistoryImage} alt="使用履歴"/>
              </Link>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}

