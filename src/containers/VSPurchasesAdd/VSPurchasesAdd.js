import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import { initialize } from 'redux-form';
import { VSPurchasesAddForm } from 'components';
import {FormattedMessage} from 'react-intl';

@connect(
  () => ({}),
  {
    initialize
  })
export default class VSPurchasesAdd extends Component {
  static propTypes = {
    initialize: PropTypes.func.isRequired
  };

  componentWillMount() {
    this.handleInitialize();
  }

  handleInitialize = () => {
    this.props.initialize('VSConfigForm', {

    });
  };

  render() {
    return (
      <div className="container">
        <h1>
          <FormattedMessage
            id="VSPurchasesAdd.Header"
            defaultMessage="VS purchase"
          />
        </h1>
        <Helmet title="VSPurchase"/>
        <VSPurchasesAddForm {...this.props} initialValues={{purchaseType: 'cash'}} />
      </div>
    );
  }
}
