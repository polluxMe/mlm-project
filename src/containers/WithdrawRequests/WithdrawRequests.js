import React, {Component, PropTypes} from 'react';
// import Helmet from 'react-helmet';
import {connect} from 'react-redux';
import * as withdrawRequestsActions from 'redux/modules/withdrawRequests';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import {FormattedMessage, FormattedNumber} from 'react-intl';
import { WithdrawRequestsSearchForm } from 'components';
import classNames from 'classnames';
import { Glyphicon } from 'react-bootstrap';
import { push } from 'react-router-redux';
import Helmet from 'react-helmet';
import moment from 'moment';
import { Link } from 'react-router';

require('../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css');

@connect(
  state => ({
    requests: state.withdrawRequests.data,
    error: state.withdrawRequests.error,
    loading: state.withdrawRequests.loading,
    loaded: state.withdrawRequests.loaded,
    totalDataSize: state.withdrawRequests.totalDataSize,
    currentPage: state.withdrawRequests.currentPage,
    sizePerPage: state.withdrawRequests.sizePerPage,
    // search: state.vsPurchases.search,
    sorter: state.withdrawRequests.sorter,
  }),
  {...withdrawRequestsActions, pushState: push})
export default class withdrawRequests extends Component {
  static propTypes = {
    requests: PropTypes.array,
    error: PropTypes.string,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    load: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    totalDataSize: PropTypes.number,
    sizePerPage: PropTypes.number,
    changeSort: PropTypes.func.isRequired,
    changePage: PropTypes.func.isRequired,
    changeSizePerPage: PropTypes.func.isRequired,
    changeFilter: PropTypes.func.isRequired,
    pushState: PropTypes.func,
    changeStatus: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {searchVisible: false};
  }

  componentWillMount() {
    this.props.load();
  }

  onSearch = (fields) => {
    this.onFilterChange(fields);
  };

  onSortChange = (name, order) => {
    const sortOrder = {name: name, order: (order === 'asc') ? 1 : -1};
    this.props.changeSort(JSON.stringify(sortOrder));
  };

  onPageChange = (page, limit) => {
    this.props.changePage(page, limit);
  };

  // onSearchChange = () => {
  //
  // };

  onFilterChange = (filterObj) =>
    this.props.changeFilter(JSON.stringify(filterObj));

  onSizePerPageList = (sizePerPage) => {
    this.props.changeSizePerPage(sizePerPage);
  };

  render() {
    function dateFormatter(cell) {
      return moment(cell).format('DD-MM-YYYY HH:mm:ss');
    }

    function memberIDFormatter(cell, row) {
      return <Link to={'/members/' + cell}>{row.member && row.member.applicantID}</Link>;
    }

    function memberNameFormatter(cell, row) {
      return row.member && row.member.applicantName;
    }

    function numberFormatter(cell) {
      return <FormattedNumber value={cell}/>;
    }

    const styles = require('./WithdrawRequests.scss');

    return (
        <div>
          <div className="container">
            <Helmet script={[
              {'src': 'https://code.jquery.com/jquery-2.1.3.min.js', 'type': 'text/javascript'},
              {'src': 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js', 'type': 'text/javascript'},

            ]}/>
            <h2>
              <FormattedMessage
                id="WithdrawRequests.Header"
                defaultMessage="Withdrawal List"
              />
            </h2>
            <div className={classNames(styles.searchForm, {[styles.hidden]: !this.state.searchVisible })}>
              <div className={styles.wrapper}>
                <WithdrawRequestsSearchForm onSubmit={this.onSearch}/>
              </div>
            </div>
            <div style={{textAlign: 'center'}}>
              <button className={styles.hideButton} onClick={()=> this.setState({searchVisible: !this.state.searchVisible})}>
                <Glyphicon glyph={this.state.searchVisible ? 'arrow-up' : 'arrow-down'}/>
                &nbsp;
                {
                  this.state.searchVisible ?
                    <FormattedMessage
                      id="General.HideSearch"
                      defaultMessage="Hide search"
                    /> :
                    <FormattedMessage
                      id="General.ShowSearch"
                      defaultMessage="Show search"
                    />
                }
                </button>
            </div>
          </div>
          <BootstrapTable
            cellEdit = {{
              mode: 'click',
              blurToSave: true,
              afterSaveCell: (item, fieldName, fieldValue) => {
                item[fieldName] = fieldValue;
                this.props.changeStatus(item);
              },
              beforeSaveCell: (item, fieldName, fieldValue) => {
                if (item[fieldName] === fieldValue) return true;
                return confirm(`Are you sure you want to change ${fieldName} to ${fieldValue}?`);
              }
            }}
            data={this.props.requests || []}
            remote
            hover
            condensed
            pagination
            fetchInfo={ { dataTotalSize: this.props.totalDataSize } }
            options={ { sizePerPage: this.props.sizePerPage,
                        onPageChange: this.onPageChange,
                        sizePerPageList: [ 5, 10, 20],
                        pageStartIndex: 1,
                        page: this.props.currentPage,
                        // onSearchChange: this.onSearchChange,
                        onSortChange: this.onSortChange,
                        // onFilterChange: this.onFilterChange,
                        onSizePerPageList: this.onSizePerPageList,
                        hideSizePerPage: true,
                        noDataText: '表示されるデータがございません。'
                       }
                    }

          >
            <TableHeaderColumn dataField="createDate" dataSort dataFormat={dateFormatter} dataAlign="center" editable={false}>
              <FormattedMessage
                id="WithdrawRequests.CreationDate"
                defaultMessage="Creation date"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="status" dataSort dataAlign="center" editable={{type: 'select', options: {values: ['removed', 'created', 'purchased']}}}>
              <FormattedMessage
                id="WithdrawRequests.Status"
                defaultMessage="Status"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="_id" dataAlign="center" dataFormat={memberNameFormatter} editable={false}>
              <FormattedMessage
                id="WithdrawRequests.Name"
                defaultMessage="Name"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="requestAmount" dataSort dataAlign="center" dataFormat={numberFormatter} editable={false}>
              <FormattedMessage
                id="WithdrawRequests.RequestAmount"
                defaultMessage="Request amount"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="rate" dataSort dataAlign="center" editable={false}>
              <FormattedMessage
                id="WithdrawRequests.Rate"
                defaultMessage="Rate"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="amount" dataSort dataAlign="center" dataFormat={numberFormatter} editable={false}>
              <FormattedMessage
                id="WithdrawRequests.WithdrawAmount"
                defaultMessage="Withdrawal amount"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="memberID" dataAlign="center" dataFormat={memberIDFormatter} editable={false}>
              <FormattedMessage
                id="WithdrawRequests.MemberID"
                defaultMessage="Member ID"
              />
            </TableHeaderColumn>
            <TableHeaderColumn dataField="_id" dataSort isKey dataAlign="center" editable={false}>
              <FormattedMessage
                id="WithdrawRequests.WithdrawalRequestID"
                defaultMessage="Withdrawal request ID"
              />
            </TableHeaderColumn>
          </BootstrapTable>
        </div>
    );
  }
}

