import {createValidator, minValue} from 'utils/validation';

export default createValidator({
  requestAmount: minValue(1),
  amount: minValue(1)
});
