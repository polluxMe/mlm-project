import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {reduxForm, Field, SubmissionError} from 'redux-form';
import Helmet from 'react-helmet';
import { Board } from 'components';
import {Form, FormControl, Button, InputGroup, Alert} from 'react-bootstrap';
import MoneyWithdrawValidation from './MoneyWithdrawValidation';
import {bindActionCreators} from 'redux';
import {loadRate} from 'redux/modules/withdrawalRate';
import {loadWallet} from 'redux/modules/memberInfo';
import {load as loadConfig} from 'redux/modules/mainConfig';
import config from '../../config.js';
import { browserHistory } from 'react-router';
import { Spinner } from 'components';

const exchangeTitle = require('theme/mlm/images/exchange-title.png');
const exchangeBtnKeisan = require('theme/mlm/images/exchange-btn-keisan.png');
const exchangeBtnIrai = require('theme/mlm/images/exchange-btn-irai.png');

const Input = ({disabled, type, input, placeholder, readOnly, suffix, min, max, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <InputGroup>
        <FormControl disabled={disabled} type={type} {...input} placeholder={placeholder} readOnly={readOnly} min={min} max={max} />
        {suffix && <InputGroup.Addon>{suffix}</InputGroup.Addon>}
      </InputGroup>
    </div>
    {!readOnly && error && touched && <div className="text-danger">{error}</div>}
  </div>;

@reduxForm({
  form: 'moneyWithdraw',
  validate: MoneyWithdrawValidation,
  enableReinitialize: true
})
@connect(
  state => ({
    user: state.auth.user,
    form: state.form.moneyWithdraw,
    rate: state.withdrawalRate.rate,
    wallet: state.memberInfo.wallet,
    configuration: state.mainConfig.data
  }),
  dispatch => bindActionCreators({loadRate, loadWallet, loadConfig}, dispatch)
)
export default class MoneyWithdraw extends Component {
  static propTypes = {
    valid: PropTypes.bool,
    user: PropTypes.object.isRequired,
    rate: PropTypes.number,
    loadRate: PropTypes.func,
    loadWallet: PropTypes.func,
    loadConfig: PropTypes.func,
    handleSubmit: PropTypes.func,
    form: PropTypes.object,
    wallet: PropTypes.object,
    configuration: PropTypes.object,
    initialize: PropTypes.func
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {exchangeAmount: 0};
  }

  componentWillMount() {
    this.props.loadRate();
    this.props.loadWallet(this.props.user._id);
    this.props.loadConfig();
  }

  onCalculate = () => {
    const exchangeRequest = this.getExchangeRequest();
    const amount = this.calculateWithdrawalQuantity(exchangeRequest);

    console.info(exchangeRequest, exchangeRequest);
    if (exchangeRequest && amount) {
      this.props.initialize({
        ...this.props.form.values,
        amount
      });
    }
  };

  getExchangeRequest = () => this.props.form.values && this.props.form.values.requestAmount || 0;

  calculateWithdrawalQuantity = (exchangeRequest) => {
    return exchangeRequest * this.props.rate;
  };

  submit = (data) => {
    data.memberID = this.props.user._id;
    data.rate = this.props.rate;
    // data.amount = this.state.exchangeAmount;

    // console.info(data.requestAmount, Math.round(this.props.wallet.commission / 2));
    // console.info(data.requestAmount, this.props.configuration.minimumWithdrawalAmount);

    console.info('s', data);
    return new Promise((resolve, reject) => {
      const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);
      return fetch(apiPath + '/api/withdraw_request', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        mode: 'cors',
        credentials: 'include',
        body: JSON.stringify({...data})
      })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);

          if (responseJson.errors) {
            console.log('error');
            reject(new SubmissionError({_error: responseJson.message}));
          } else {
            browserHistory.push('/my_page?reload=true');
            resolve(responseJson);
          }
        })
        .catch((err) => {
          console.log('err', err);
          reject(new SubmissionError({_error: err.message || err.error || err}));
        });
    });
  };

  render() {
    const styles = require('./MoneyWithdraw.scss');

    /* eslint-disable */
    const { handleSubmit, valid, rate, wallet, configuration, error, submitting } = this.props;
    /* eslint-disable */

    return (
      <div id="wrap">
        <Helmet title="Money withdraw"/>
        <div className="exchange" id="contents">
          <Form horizontal>
            <Board>
              <h2 className="mb05"><img src={exchangeTitle} alt="両替" /></h2>
              <div className="box">
                <div className="inner">
                    <table>
                      <tbody>
                      <tr>
                        <th>保有3WAY</th>
                        <td className="tar">
                          {this.props.user.commision ? Math.floor(this.props.user.commision / 2) + ' B$' : 0 + ' B$'}
                        </td>
                      </tr>
                      <tr>
                        <th className="no-border">両替依頼</th>
                        <td className="no-border">
                          <Field name="requestAmount"
                                 type="number"
                                 readOnly={!this.props.user.commision}
                                 component={Input}
                                 placeholder="0"
                                 min={configuration && configuration.minimumWithdrawalAmount}
                                 max={this.props.user.commision && Math.floor(this.props.user.commision / 2)}
                                 suffix="B$"
                          />
                        </td>
                      </tr>
                      </tbody>
                    </table>

                    <div className="tar">
                      最低出金額：{configuration && configuration.minimumWithdrawalAmount} B$
                    </div>

                    {error && <Alert bsStyle="danger">
                      {error}
                    </Alert>}

                    <div className="rate">
                      レート1B$ = {rate}円
                    </div>

                    <div className="tac mt10 mb10">
                      <Button
                        onClick={this.onCalculate}
                        className={styles.button}
                      >
                        <img src={exchangeBtnKeisan} width="117" alt="計算" />
                      </Button>
                    </div>

                    <table>
                      <tbody>
                      <tr>
                        <th>両替金額</th>
                        <td className="tar">
                          <Field name="amount" type="number" component={Input} readOnly min={1} suffix="円" />
                        </td>
                      </tr>
                      </tbody>
                    </table>

                    <div className="tac mt10">
                      <Button
                        disabled={!valid || submitting}
                        className={styles.button}
                        onClick={handleSubmit(this.submit)}
                      >
                        <Spinner loading={submitting} size={25} exclusive>
                          <img src={exchangeBtnIrai} width="100%" alt="依頼する" />
                        </Spinner>
                      </Button>
                    </div>
                </div>
              </div>
            </Board>
          </Form>
        </div>
      </div>
    );
  }
}

