import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
// import Helmet from 'react-helmet';
import D3Tree from '../UnilevelMap/D3Tree';
import {bindActionCreators} from 'redux';
import * as mapActions from 'redux/modules/adminUnilevelMap';
import dimensions from 'react-dimensions';
import {
  Button
} from 'react-bootstrap';
import {FormattedMessage} from 'react-intl';

const images = {
  'blue': require('../Map/img/blue_map_member.png'),
  'gray': require('../Map/img/gray_map_member.png'),
  'red': require('../Map/img/red_map_member.png'),
  'yellow': require('../Map/img/yellow_map_member.png'),
  'orange': require('../Map/img/orange_map_member.png')
};

@connect(
  state => ({
    user: state.auth.user,
    map: state.adminUnilevelMap.data,
    loaded: state.adminUnilevelMap.loaded,
    left: state.adminUnilevelMap.left,
    right: state.adminUnilevelMap.right,
    top: state.adminUnilevelMap.top,
    bottom: state.adminUnilevelMap.bottom
  }),
  dispatch => bindActionCreators(mapActions, dispatch)
)
class AdminUnilevelMap extends Component {
  static propTypes = {
    user: PropTypes.object,

    map: PropTypes.array,
    loaded: PropTypes.bool.isRequired,
    load: PropTypes.func.isRequired,

    left: PropTypes.number.isRequired,
    right: PropTypes.number.isRequired,
    top: PropTypes.number.isRequired,
    bottom: PropTypes.number.isRequired,

    shiftLeft: PropTypes.func.isRequired,
    shiftRight: PropTypes.func.isRequired,
    shiftTop: PropTypes.func.isRequired,
    shiftBottom: PropTypes.func.isRequired,

    containerWidth: PropTypes.number.isRequired,
    location: PropTypes.object.isRequired
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  componentDidMount() {
    const {user, load, location} = this.props;
    const userId = (location && location.query && location.query.id) || (user && user._id);
    load(userId);
  }

  render() {
    require('../Map/Map.css');
    const {map, left, right, top, bottom, shiftLeft, shiftRight, shiftBottom, shiftTop, containerWidth, loaded, user, location} = this.props;

    const xCount = map && map[0] && map[0].maxWidth || 8;
    const yCount = map && map[0] && map[0].maxWidth || 4;
    const svgWidth = xCount * 120;
    const svgHeight = yCount * 120;

    const stl = {
      left,
      right,
      top,
      bottom,
      width: svgWidth,
      height: svgHeight
    };

    if (containerWidth < svgWidth) {
      stl.left -= (svgWidth - containerWidth) / 2;
    }

    const isShiftRightButtonDisabled = (svgWidth + stl.left) < containerWidth;
    const isShiftBottomButtonDisabled = (svgHeight + stl.top) < containerWidth;

    const isShiftLeftButtonDisabled = stl.left >= 0;
    const isShiftTopButtonDisabled = stl.top >= 0;

    const applicantID = user && user.applicantID;
    const userId = (location && location.query && location.query.id) || (user && user._id);

    return (
      <div className="parent" style={{width: containerWidth, height: containerWidth}}>
        <p className="text-center">
          <br />
          <Button bsStyle="primary" href={'/map/?id=' + userId}>
            <FormattedMessage
              id="AdminUnilevelMap.SwitchMap"
              defaultMessage="Switch Map"
            />
          </Button>
        </p>
        <div className="buttonBlock">
          <button type="button" className="btn btn-lg" onClick={shiftLeft} disabled={isShiftLeftButtonDisabled}>
            <i className="fa fa-chevron-left"/>
          </button>
          <button type="button" className="btn btn-lg" onClick={shiftTop} disabled={isShiftTopButtonDisabled}>
            <i className="fa fa-chevron-up"/>
          </button>
          <button type="button" className="btn btn-lg" onClick={shiftBottom} disabled={isShiftBottomButtonDisabled}>
            <i className="fa fa-chevron-down"/>
          </button>
          <button type="button" className="btn btn-lg" onClick={shiftRight} disabled={isShiftRightButtonDisabled}>
            <i className="fa fa-chevron-right"/>
          </button>
        </div>
        <div className="block" style={stl}>
          {loaded ? <D3Tree treeData={map} svgWidth={svgWidth} svgHeight={svgHeight} images={images} applicantID={applicantID} /> :
            <FormattedMessage
              id="General.Loading"
              defaultMessage="Loading..."
            />}
        </div>
      </div>
    );
  }
}

module.exports = dimensions()(AdminUnilevelMap);
