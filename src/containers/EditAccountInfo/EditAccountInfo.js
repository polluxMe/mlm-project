import React, { Component, PropTypes } from 'react';
// import config from '../../config';
import {connect} from 'react-redux';
import {FormattedMessage, FormattedNumber} from 'react-intl';
import {Board} from 'components';
import Helmet from 'react-helmet';
import {bindActionCreators} from 'redux';
import {loadUserInfo} from 'redux/modules/memberInfo';
import moment from 'moment';
import countries from 'utils/countries';
import { save as saveMemberData } from 'redux/modules/account';
import {Button} from 'react-bootstrap';
import {SwitchableInput} from '../../components';

const countryOptions = countries.map(item => ({label: item.name, value: item.name}));

const styles = require('./EditAccountInfo.scss');

const backButtonImage = require('theme/mlm/images/edit-account-btn-back.png');
const saveButtonImage = require('theme/mlm/images/edit-account-btn-save.png');

const currentYear = +moment().format('YYYY');
const years = [];
const months = [];
const days = [];

for (let ii = currentYear; ii > currentYear - 100; ii -= 1) {
  years.push({_id: ii, name: ii});
}

for (let ii = 1; ii <= 12; ii += 1) {
  months.push({_id: ii, name: ii});
}

for (let ii = 1; ii <= 31; ii += 1) {
  days.push({_id: ii, name: ii});
}

const DatePicker = React.createClass({
  propTypes: {
    value: PropTypes.string,
    onChange: PropTypes.func,
    onBlur: PropTypes.func
  },

  onYearChange(event) {
    this.props.onChange(moment(this.props.value).year(event.target.value)._d);
  },

  onMonthChange(event) {
    this.props.onChange(moment(this.props.value).month(event.target.value - 1)._d);
  },

  onDateChange(event) {
    this.props.onChange(moment(this.props.value).date(event.target.value)._d);
  },

  render() {
    return (
      <div>
        <select value={moment(this.props.value).year()} onChange={this.onYearChange} onBlur={this.props.onBlur}>
          {years && years.map((item, index) => <option key={index} value={item._id}>{item.name}</option>)}
          </select>
        <select value={moment(this.props.value).month() + 1} onChange={this.onMonthChange} onBlur={this.props.onBlur}>
          {months && months.map((item, index) => <option key={index} value={item._id}>{item.name}</option>)}
          </select>
        <select value={moment(this.props.value).date()} onChange={this.onDateChange} onBlur={this.props.onBlur}>
          {days && days.map((item, index) => <option key={index} value={item._id}>{item.name}</option>)}
          </select>
      </div>
    );
  }
});

const Select = ({value, onChange, options, onBlur}) =>
  <select autoFocus onChange={event => onChange(event.target.value)} value={value} onBlur={onBlur}>
    {
      options && options.map((item, index) => <option key={index} value={item.value}>{item.label}</option>)
    }
  </select>;

@connect(state => ({
  user: state.auth.user,
  saved: state.account.saved,
  saving: state.account.saving
}),
  dispatch => bindActionCreators({loadUserInfo, saveMemberData}, dispatch)
)
export default class EditAccountInfo extends Component {
  static propTypes = {
    user: PropTypes.object,
    info: PropTypes.object,
    loadUserInfo: PropTypes.func,
    saveMemberData: PropTypes.func,
    saved: PropTypes.bool,
    saving: PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.state = {user: {...this.props.user, ...this.props.user.profile, password: ''}, saved: false};
  }

  // componentWillMount() {
  //   this.props.loadUserInfo(this.props.user._id, true)
  //     .then(response => {
  //       console.log('RESPONSE', response);
  //       this.setState({user: response});
  //     });
  // }

  onFieldChange = (fieldName) => {
    return (value) => {
      this.setState({user: {...this.state.user, [fieldName]: value}});
    };
  };

  onSave = () => {
    this.props.saveMemberData({
      _id: this.props.user._id,
      applicantName: this.state.user.applicantName,
      applicantKatakana: this.state.user.applicantKatakana,
      nickname: this.state.user.nickname,
      email: this.state.user.email,
      note: this.state.user.note,
      password: this.state.user.password,
      profile: {
        nationality: this.state.user.nationality,
        dateOfBirth: this.state.user.dateOfBirth,
        address: this.state.user.address,
        phone: this.state.user.phone,
        mobilePhone: this.state.user.mobilePhone,
        gender: this.state.user.gender,
        passport: this.state.user.passport,
      }
    })
      .then(() => {
        this.setState({saved: true});
      });
  };

  render() {
    function noticeFormatter(value) {
      if (value) return '通知済み';
      return '未通知';
    }

    function birthDateFormatter(value) {
      return moment(value).format('YYYY/MM/DD');
    }

    function dateFormatter(value) {
      return moment(value).format('DD-MM-YYYY');
    }

    function genderFormatter(value) {
      if (value === 'male') return '男';
      return '女';
    }

    function statusFormatter(value) {
      let status;
      switch (value) {
        case 'temp':
          status = '仮会員';
          break;
        case 'official':
          status = '本会員';
          break;
        case 'deactivated':
          status = '無効';
          break;
        default:
          status = '';
          break;
      }
      return status;
    }

    return (
      <div id="wrap">
        <Helmet title="Edit account"/>
        <div className="commission" id="contents">
          <Board>
            <div className={styles.background}>
              <div className={styles.head}>
                <FormattedMessage
                  id="EditAccountInfo.Header"
                  defaultMessage="Member info"
                />
              </div>
              <div className={styles.inner}>
                <table className={styles.infoTable}>
                  <tbody>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.BraveID"
                            defaultMessage="Brave ID"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput value={this.state.user.applicantID}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.LoginID"
                            defaultMessage="Login ID"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput value={this.state.user.username}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.BraveIntroducer"
                            defaultMessage="IntroducerID"
                          />
                        </div>
                        <div className={styles.input}>
                          {this.state.user.accountType === 'brave' && <SwitchableInput value={this.state.user.introducerID}/>}
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.GCloudID"
                            defaultMessage="GCloudID"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput value={this.state.user.applicantID}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.GCloudIntroducerID"
                            defaultMessage="GCloud introducer ID"
                          />
                        </div>
                        <div className={styles.input}>
                          {this.state.user.accountType === 'gcloud' && <SwitchableInput value={this.state.user.introducerID}/>}
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.Name"
                            defaultMessage="Name"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput editable value={this.state.user.applicantName} onChange={this.onFieldChange('applicantName')}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.Name"
                            defaultMessage="Name"
                          />
                          <span className={styles.notice}>（フリガナ）</span>
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput editable value={this.state.user.applicantKatakana} onChange={this.onFieldChange('applicantKatakana')}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.Nickname"
                            defaultMessage="Nickname"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput editable value={this.state.user.nickname} onChange={this.onFieldChange('nickname')}/>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.National"
                            defaultMessage="National"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput editable
                                           value={this.state.user.nationality}
                                           onChange={this.onFieldChange('nationality')}
                                           inputComponent={Select}
                                           options={countryOptions}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.Sex"
                            defaultMessage="Gender"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput editable
                                           value={this.state.user.gender}
                                           onChange={this.onFieldChange('gender')}
                                           inputComponent={Select}
                                           options={[{label: '男', value: 'male'}, {label: '女', value: 'female'}]}
                                           valueFormat={genderFormatter}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.Birthday"
                            defaultMessage="Birthday"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput editable value={this.state.user.dateOfBirth} inputComponent={DatePicker} valueFormat={birthDateFormatter} onChange={this.onFieldChange('dateOfBirth')}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.Address"
                            defaultMessage="Address"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput editable value={this.state.user.address || '-'} onChange={this.onFieldChange('address')}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.MobileNumber"
                            defaultMessage="Mobile number"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput editable value={this.state.user.mobilePhone || '-'} onChange={this.onFieldChange('mobilePhone')}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.PhoneNumber"
                            defaultMessage="Phone number"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput editable value={this.state.user.phone || '-'} onChange={this.onFieldChange('phone')}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.EmailAdress"
                            defaultMessage="Email adress"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput editable value={this.state.user.email || '-'}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.RegistrationDate"
                            defaultMessage="Registration Date"
                          /><br />
                          <span className={styles.notice}>
                          （BRAVE新規申込者は、申込書ご記入日になります）
                          </span>
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput value={this.state.user.createDate} valueFormat={dateFormatter}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.Passport"
                            defaultMessage="Passport"
                          />
                          <span className={styles.notice}>
                            （パスポート、免許証）
                          </span>
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput editable value={this.state.user.passport || '-'} onChange={this.onFieldChange('passport')}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.Note"
                            defaultMessage="Note"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput editable value={this.state.user.note || '-'} onChange={this.onFieldChange('note')}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.BelongTo"
                            defaultMessage="Belong to"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput value={this.state.user.accountType} />
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.LastLogin"
                            defaultMessage="Last login"
                          />
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput value={this.state.user.lastLoginTime && moment(this.state.user.lastLoginTime).format('YYYY-MM-DD HH:mm:ss')} />
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.PurchaseAmount"
                            defaultMessage="Purchase mount （￥）"
                          />
                        </div>
                        <div className={styles.input}>
                          <FormattedNumber value={this.state.user.purchasePrice || 0} />
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.PurchaseAmountATC"
                            defaultMessage="Purchase mount （ATC）"
                          />
                        </div>
                        <div className={styles.input}>
                          <FormattedNumber value={this.state.user.purchasePriceATC || 0} />
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.PurchaseAmountBTC"
                            defaultMessage="Purchase mount （Bit）"
                          />
                        </div>
                        <div className={styles.input}>
                          <FormattedNumber value={this.state.user.purchasePriceBTC || 0} />
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.Rank"
                            defaultMessage="RANK"
                          />
                        </div>
                        <div className={styles.input}>
                          <FormattedNumber value={this.state.user.rank || 0} />
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.MemberStatus"
                            defaultMessage="Member status"
                          />
                          <span className={styles.notice}>（仮会員 / 本会員 / 無効）</span>
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput value={this.state.user.status} valueFormat={statusFormatter}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.MailNoticeStatus"
                            defaultMessage="Mail notice status"
                          />
                          <span className={styles.notice}>（通知済み / 未通知）</span>
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput value={this.state.user.emailNotice} valueFormat={noticeFormatter}/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className={styles.label}>
                          <FormattedMessage
                            id="EditAccountInfo.DocumentNoticeStatus"
                            defaultMessage="Document notice status"
                          />
                          <span className={styles.notice}>（通知済み / 未通知）</span>
                        </div>
                        <div className={styles.input}>
                          <SwitchableInput value={this.state.user.writtenNotice} valueFormat={noticeFormatter}/>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
                {this.state.saved && (
                  <h3>
                    <FormattedMessage
                    id="EditAccountInfo.Saved"
                    defaultMessage="Account info successfuly saved"
                    />
                  </h3>)}
                <div className={styles.controls}>
                  <Button
                    href="/my_account"
                    className={styles.controlsBtn}
                  >
                    <img src={backButtonImage} alt="Back" />
                  </Button>
                  <Button
                    className={styles.controlsBtn}
                    disabled="disabled"
                  >
                    <img src={saveButtonImage}/>
                  </Button>
                </div>
              </div>
            </div>
          </Board>
        </div>
      </div>
    );
  }
}
