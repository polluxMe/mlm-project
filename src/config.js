if (!global._babelPolyfill) {
  require('babel-polyfill');
}

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  },
  prod: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT || 80,
  apiHost: process.env.APIHOST || 'mypage.brave.works',
  apiPort: process.env.APIPORT || 3030,
  app: {
    title: 'Brave',
    description: 'Brave Works',
    head: {
      titleTemplate: 'Brave: %s',
      meta: [
        {name: 'description', content: 'Brave'},
        {charset: 'utf-8'},
        {property: 'og:site_name', content: 'Brave'},
        {property: 'og:image', content: ''},
        {property: 'og:locale', content: 'ja_JA'},
        {property: 'og:title', content: 'Brave'},
        {property: 'og:description', content: 'Brave'},
        {property: 'og:card', content: 'summary'},
        {property: 'og:site', content: '@eliftech'},
        {property: 'og:creator', content: '@eliftech'},
        {property: 'og:image:width', content: '200'},
        {property: 'og:image:height', content: '200'}
      ]
    }
  },
  'siteUrl': 'http://mypage.brave.works',
  'qrUrl': 'http://mypage.brave.works/img/qr.png'
}, environment);
