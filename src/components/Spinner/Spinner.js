import React from 'react';

export default class Spinner extends React.Component {
  static propTypes = {
    children: React.PropTypes.any,
    loading: React.PropTypes.bool,
    size: React.PropTypes.number,
    exclusive: React.PropTypes.bool,
    block: React.PropTypes.bool
  };

  static defaultProps = {
    size: 10
  };

  constructor(props) {
    super(props);
  }

  render() {
    const styles = require('./Spinner.scss');
    const {
      children,
      loading,
      size,
      exclusive
    } = this.props;

    const spinnerStyle = {
      width: size,
      height: size,
      position: 'absolute',
      top: `calc(50% - ${size / 2}px)`,
      left: `calc(50% - ${size / 2}px)`
    };

    const contentStyle = {
      visibility: (exclusive && loading) ? 'hidden' : 'visible'
    };

    return (
      <div className={styles.container}>
        <div style={contentStyle}>{children}</div>
        {loading && <div style={spinnerStyle} className={styles.spinner}/>}
      </div>
    );
  }
}
