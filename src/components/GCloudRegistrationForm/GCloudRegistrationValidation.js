import memoize from 'lru-memoize';
// import {createValidator, required, maxLength, requiredCash, requiredACTTransfer, requiredBitcoin} from 'utils/validation';
//
// const surveyValidation = createValidator({
//   applicantName: [required],
//   applicantKatakana: [required],
//   nickname: [required, maxLength(14)],
//   introducerID: [required],
//   nationality: [required],
//   'profile.dateOfBirth': [required],
//   'profile.gender': [required],
//   'profile.address': [required],
//   'profile.mobilePhone': [required],
//   'profile.phone': [required],
//   email: [required],
//   purchaseType: [required],
//   purchasePrice: [requiredCash],
//   purchasePriceATC: [requiredACTTransfer],
//   purchasePriceBTC: [requiredBitcoin]
// });
// export default memoize(10)(surveyValidation);

const validate = (values) => {
  const errors = {};
  if (!values.applicantName) {
    errors.applicantName = '必須';
  }
  if (!values.applicantKatakana) {
    errors.applicantKatakana = '必須';
  }
  if (!values.nickname) {
    errors.nickname = '必須';
  }
  // if (!values.introducerID) {
  //   errors.introducerID = '必須';
  // }
  if (values.profile) {
    if (!values.profile.gender) {
      errors.profile = errors.profile || {};
      errors.profile.gender = '必須';
    }
    if (!values.profile.postal1) {
      errors.profile = errors.profile || {};
      errors.profile.postal1 = '必須';
    }
    if (!values.profile.postal2) {
      errors.profile = errors.profile || {};
      errors.profile.postal2 = '必須';
    }
    if (!values.profile.prefecture) {
      errors.profile = errors.profile || {};
      errors.profile.prefecture = '必須';
    }
    if (!values.profile.municipalDistrict) {
      errors.profile = errors.profile || {};
      errors.profile.municipalDistrict = '必須';
    }
    if (!values.profile.street) {
      errors.profile = errors.profile || {};
      errors.profile.street = '必須';
    }
    if (!values.profile.address) {
      errors.profile = errors.profile || {};
      errors.profile.address = '必須';
    }
    if (!values.profile.mobilePhone && !values.profile.phone) {
      errors.profile = errors.profile || {};
      errors.profile.mobilePhone = '必須';
    }
    if (!values.profile.mobilePhone && !values.profile.phone) {
      errors.profile = errors.profile || {};
      errors.profile.phone = '必須';
    }
    if (!values.profile.nationality) {
      errors.profile = errors.profile || {};
      errors.profile.nationality = '必須';
    }
  }
  if (!values.email) {
    errors.email = '必須';
  }
  if (!values.purchaseType) {
    errors.purchaseType = '必須';
  }
  if (!values.purchasePrice && values.purchaseType === 'cash') {
    errors.purchasePrice = '必須';
  }
  if (!values.purchasePriceATC && values.purchaseType === 'ATCTransfer') {
    errors.purchasePriceATC = '必須';
  }
  if (!values.purchasePriceBTC && values.purchaseType === 'bitcoin') {
    errors.purchasePriceBTC = '必須';
  }

  return errors;
};

export default memoize(10)(validate);
