import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Form, FormGroup, FormControl, ControlLabel, Button, HelpBlock} from 'react-bootstrap';
import {FormattedMessage} from 'react-intl';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import ChangeIDValidation from './ChangeIDValidation';
import { replace as replaceRoute } from 'react-router-redux';
import {bindActionCreators} from 'redux';
import {setApplicantID} from 'redux/modules/auth';
import {loadWallet} from 'redux/modules/memberInfo';
import cookie from 'react-cookie';

import config from '../../config.js';

const Input = ({type, placeholder, input, disabled, help, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <FormControl type={type} {...input} placeholder={placeholder} disabled={disabled} />
    </div>
    {help && <HelpBlock>{help}</HelpBlock> }
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;


@reduxForm({
  form: 'changeIDForm',
  validate: ChangeIDValidation
})
@connect(
  state => ({
    form: state.form.changeIDForm,
    user: state.auth.user,
    wallet: state.memberInfo.wallet
  }),
  dispatch => bindActionCreators({setApplicantID, replaceRoute, loadWallet}, dispatch)
)
export default class ChangeIDForm extends Component {
  static propTypes = {
    tempUser: PropTypes.object,
    replaceRoute: PropTypes.func,
    setApplicantID: PropTypes.func,
    user: PropTypes.object,
    wallet: PropTypes.object,
    loadWallet: PropTypes.func
  }

  submit = (data) => {
    return Promise.resolve()
      .then(() => {
        const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);

        return fetch(apiPath + '/api/auth/registerEnd', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          credentials: 'include',
          mode: 'cors',
          body: JSON.stringify(data)
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log('responseJson', responseJson);
          if (responseJson.code && responseJson.code === 401) {
            console.info('GOING to login...');
            this.props.replaceRoute('/registration/temp_login');
          } else if (responseJson.applicantID) {
            this.props.setApplicantID(responseJson.username);

            // this.props.replaceRoute('/my_page');
            return this.props.loadWallet(this.props.user._id)
              .then((response) => {
                cookie.save('vsAmount', response.vsAmount, {path: '/', domain: '.brave.works'});
                cookie.save('username', this.props.user.username, {path: '/', domain: '.brave.works'});
                cookie.save('rank', this.props.user.rank, {path: '/', domain: '.brave.works'});

                if (window) {
                  window.location = '/my_page';
                }
              });
          } else if (responseJson.code && responseJson.code === 500) {
            throw new SubmissionError({_error: responseJson.msg});
          }
        })
        .catch((err) => {
          throw new SubmissionError({_error: (err.errors && err.errors._error) || 'Inserted data is not correct.'});
        });
      });
  };

  render() {
    /* eslint-disable */
    const {
      asyncValidating,
      dirty,
      active,
      handleSubmit,
      reset,
      invalid,
      pristine,
      valid,
      form,
      error,
      submitting
    } = this.props;
    /* eslint-disable */

    return (
      <Form onSubmit={handleSubmit(this.submit)}>
        <FormGroup
          controlId="login"
        >
          <ControlLabel>
            <FormattedMessage
              id="LoginFlow.LogInName"
              defaultMessage="Log in name."
            />
        </ControlLabel>
          <Field name="id"
                 component={Input}
                 type="text"
                 help={
                   <div>
                     <div>
                       <FormattedMessage
                         id="LoginFlow.IDHelp1"
                         defaultMessage="* Login name is a 12-digit single-byte No. alphanumeric characters,"
                       />
                     </div>
                     <div>
                       <FormattedMessage
                         id="LoginFlow.IDHelp2"
                         defaultMessage="Number (-. [Half], _ [underscore], @ [at sign], [dot])"
                       />
                     </div>
                   </div>
                 }
          />
          <FormControl.Feedback />
        </FormGroup>
        <FormGroup
          controlId="password"
        >
          <ControlLabel>
            <FormattedMessage
              id="LoginFlow.Password"
              defaultMessage="Password"
            />
          </ControlLabel>
          <Field name="password"
                 component={Input}
                 type="password"
                 help={
                   <FormattedMessage
                     id="LoginFlow.PasswordHelp"
                     defaultMessage="* Password, 8 to 16-digit alphanumeric"
                   />
                 }
          />
          <FormControl.Feedback />
        </FormGroup>
        <FormGroup
          controlId="password"
        >
          <ControlLabel>
          <FormattedMessage
            id="LoginFlow.RepeatRassword"
            defaultMessage="Repeat password"
          />
            </ControlLabel>
            <Field
              name="repeatPassword"
              component={Input}
              type="password"
            />
        </FormGroup>
        <div style={{textAlign: 'center'}}>
          {error && <div className="text-danger">{error}</div>}
          <Button
            bsSize="large"
            disabled={!valid}
            onClick={handleSubmit(this.submit)}
          >
            <FormattedMessage
              id="LoginFlow.Next"
              defaultMessage="Next"
            />
          </Button>
        </div>
      </Form>
    );
  }
}
