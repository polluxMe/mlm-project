const validate = (values) => {
  const errors = {};
  if (!values.id) {
    errors.id = '必須';
  } else if (!values.id.match(/^([a-ｚ]|@|-|_|\.|\d){12}$/gi)) {
    errors.id = 'パターンに一致しなければなりません。';
  }

  if (!values.password) {
    errors.password = '必須';
  } else if (values.password.length < 8 || values.password.length > 16) {
    errors.password = 'パスワードは8〜16文字でなければなりません。';
  } else if (!/[a-ｚ]/.test(values.password)) {
    errors.password = 'パスワードが半角英字のみになる。';
  }

  if (!values.repeatPassword) {
    errors.repeatPassword = '必須';
  } else if (values.password !== values.repeatPassword) {
    errors.repeatPassword = '入力したパスワードが一致しません。';
  }

  return errors;
};


export default validate;
