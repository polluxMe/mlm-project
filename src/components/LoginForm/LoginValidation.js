const validate = (values) => {
  const errors = {};
  if (!values.id) {
    errors.id = '必須';
  }

  if (!values.password) {
    errors.password = '必須';
  }

  return errors;
};


export default validate;
