import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Form, FormGroup, FormControl, ControlLabel, Button} from 'react-bootstrap';
import {injectIntl, FormattedMessage, intlShape} from 'react-intl';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import LoginValidation from './LoginValidation';
import { Spinner } from 'components';
import config from '../../config.js';

const Input = ({type, placeholder, input, disabled, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <FormControl type={type} {...input} placeholder={placeholder} disabled={disabled} />
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;


@reduxForm({
  form: 'tempForm',
  validate: LoginValidation
})
@connect(
  state => ({
    form: state.form.loginForm,
  }))
class LoginForm extends Component {
  static propTypes = {
    tempUser: PropTypes.object,
    onSubmit: PropTypes.func,
    intl: intlShape.isRequired,
  };

  submit = (data) => {
    return Promise.resolve()
      .then(() => {
        const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);

        data.username = data.id;
        return fetch(apiPath + '/api/auth/login', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          credentials: 'include',
          mode: 'cors',
          body: JSON.stringify(data)
        })
          .then((response) => response.json())
          .then((responseJson) => {
            if (responseJson && responseJson.info && responseJson.info.message && responseJson.info.message === 'User is deactivated') {
              throw new SubmissionError({_error: 'ユーザーが非アクティブ化されます'});
            }
            if (!responseJson.user) {
              throw new SubmissionError({_error: 'ログイン名またはパスワードが正しくありません。'});
            }
            this.props.onSubmit(responseJson);
          })
          .catch((err) => {
            console.log('error', err);
            throw new SubmissionError({_error: err.errors && err.errors._error || 'ログイン名またはパスワードが正しくありません。'});
          });
      });
  };

  render() {
    /* eslint-disable */
    const {
      handleSubmit,
      pristine,
      error,
      submitting,
      intl
    } = this.props;
    /* eslint-disable */

    return (
      <Form onSubmit={handleSubmit(this.submit)}>
        <FormGroup
          controlId="login"
        >
          <ControlLabel>ID</ControlLabel>
          <Field name="id"
                 type="text"
                 component={Input}
                 placeholder="ID"
          />
          <FormControl.Feedback />
        </FormGroup>

        <FormGroup
          controlId="password"
        >
          <ControlLabel>
            <FormattedMessage
              id="LoginFlow.Password"
              defaultMessage="Password"
            />
          </ControlLabel>
          <Field name="password"
                 type="password"
                 component={Input}
                 placeholder={
                   intl.formatMessage({id: 'LoginFlow.Password'}, {defaultMessage: 'Password'})
                 }
          />
          <FormControl.Feedback />
        </FormGroup>
        <p className="text-right">
          <a href="/registration/forget_password">
            <FormattedMessage
              id="LoginFlow.ForgetPassword"
              defaultMessage="Forget password"
            />
          </a>
        </p>
        <div style={{textAlign: 'center'}}>
          {error && <div className="text-danger">{error}</div>}
          <Button
            bsSize="large"
            type="submit"
            disabled={pristine || submitting || error}
          >
            <Spinner loading={submitting} size={25}>
              <i className="fa fa-sign-in"/>{' '}
              <FormattedMessage
                id="LoginFlow.LogInButton"
                defaultMessage="Log In"
              />
            </Spinner>
          </Button>
        </div>
      </Form>
    );
  }
}

export default injectIntl(LoginForm);