import {createValidator, required, minValue} from 'utils/validation';

const vsPurchasesValidation = createValidator({
  purchaseAmount: minValue(1),
  purchaseQuantity: minValue(1),
  purchaseType: [required]
});
export default vsPurchasesValidation;
