import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {reduxForm, Field} from 'redux-form';
import vsPurchasesAddFormValidation from './VSPurchasesAddFormValidation';
import {load as loadMembers} from 'redux/modules/addMapMember';
import {loadRate, loadCashRate} from 'redux/modules/vsRate';
import {Form, FormGroup, FormControl, ControlLabel, Button, Col, ButtonGroup, InputGroup} from 'react-bootstrap';
import Moment from 'moment';
import momentLocalizer from 'react-widgets/lib/localizers/moment';
import {FormattedMessage} from 'react-intl';
import config from '../../config.js';
import { Link } from 'react-router';

// const styles = require('./VSPurchasesAddForm.scss');

momentLocalizer(Moment);

const Input = ({type, placeholder, input, readOnly, suffix, min, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <InputGroup>
        <FormControl type={type} {...input} placeholder={placeholder} readOnly={readOnly} min={min} />
        {suffix && <InputGroup.Addon>{suffix}</InputGroup.Addon>}
      </InputGroup>
    </div>
    {!readOnly && error && touched && <div className="text-danger">{error}</div>}
  </div>;


const Radio = ({input, radioValue, children}) =>
  <label>
    <input type="radio" {...input} value={radioValue} checked={input.value === radioValue}/>&nbsp;{children}
  </label>;

@reduxForm({
  form: 'vsPurchasesAddForm',
  validate: vsPurchasesAddFormValidation,
  enableReinitialize: true
})
@connect(
  state => ({
    form: state.form.vsPurchasesAddForm,
    members: state.addMapMember.data,
    purchaseRate: state.vsRate.rate,
    cashRate: state.vsRate.cashRate,
    memberID: state.vsRate.memberID,
  }),
  dispatch => bindActionCreators({loadMembers, loadRate, loadCashRate}, dispatch)
)
export default class VSConfigAdd extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
    form: PropTypes.object.isRequired,
    purchaseRate: PropTypes.number,
    cashRate: PropTypes.number,
    loadMembers: PropTypes.func.isRequired,
    loadRate: PropTypes.func.isRequired,
    loadCashRate: PropTypes.func.isRequired,
    params: PropTypes.func.isRequired,
    initialize: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {purchaseQuantity: 0, submitted: false};
  }

  componentWillMount() {
    this.props.loadMembers();
    this.props.loadRate();
    this.props.loadCashRate();
  }

  onCalculate = () => {
    const amount = this.getPurchaseAmount();
    const quantity = this.calculatePurchaseQuantity(amount);

    if (quantity && amount) {
      this.props.initialize({
        ...this.props.form.values,
        purchaseQuantity: quantity
      });
    }
  };

  getPurchaseAmount = () => this.props.form.values && this.props.form.values.purchaseAmount || 0;

  calculatePurchaseQuantity = (amount) => {
    return Math.floor(amount / this.props.purchaseRate / this.props.cashRate);
  };

  submitPurchase = (data) => {
    data.cashRate = this.props.cashRate;
    data.purchaseRate = this.props.purchaseRate;
    // data.purchaseQuantity = this.calculatePurchaseQuantity(this.getPurchaseAmount());
    data.memberID = this.props.params.memberID;

    const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);

    console.info(data, 'data');

    return fetch(apiPath + '/api/vs_purchase', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({...data})
    })
      .then((response) => response.json())
      .then(() => {
        this.setState({submitted: true});
        this.props.initialize({});
      })
      .catch(error => {
        console.error(error);
      });
  };

  render() {
    // console.log(this.props);

    /* eslint-disable */
    // const { editStop, fields: {id, color, sprocketCount, owner}, formKey, handleSubmit, invalid,
    const { handleSubmit, valid, error } = this.props;
    /* eslint-disable */
    //   pristine, save, submitting, saveError: { [formKey]: saveError }, values } = this.props;
    // const styles = require('containers/Widgets/Widgets.scss');
    // require('theme/css/style.min.css');


    return (
      <div>
        <Form horizontal>
          <FormGroup>
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="WidgetsAdd.CurrentRate"
                  defaultMessage="Current rate:"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <FormControl.Static>
                1 VS = {Math.ceil(this.props.cashRate * 100)} ¢
              </FormControl.Static>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="WidgetsAdd.purchaseType"
                  defaultMessage="Purchase type:"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <Field name="purchaseType" component={Radio} radioValue="cash">円</Field>&nbsp;
              <Field name="purchaseType" component={Radio} radioValue="BTC">Bitcoin</Field>&nbsp;
              <Field name="purchaseType" component={Radio} radioValue="ATC">ATC</Field>
            </Col>
          </FormGroup>
          <FormGroup
            controlId="vsPurchasesAddFormPurchaseAmount"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="WidgetsAdd.PurchaseAmount"
                  defaultMessage="Purchase Amount"
                />
              </ControlLabel>
            </Col>
            <Col xs={9}>
              <Field name="purchaseAmount" type="number" component={Input} min={1} suffix="円" />
            </Col>
            <Col xs={1}>
              <Button bsSize="large" onClick={this.onCalculate}>
                <FormattedMessage
                  id="WidgetsAdd.Calculate"
                  defaultMessage="Calculate"
                />
              </Button>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="WidgetsAdd.PurchaseRate"
                  defaultMessage="Purchase rate:"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <FormControl.Static>
                1B$ = {this.props.purchaseRate} 円
              </FormControl.Static>
            </Col>
          </FormGroup>
          <FormGroup
            controlId="vsPurchasesAddFormPurchaseQuantity"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="WidgetsAdd.PlannedPurchaseQuantity"
                  defaultMessage="Planned purchase quantity"
                />
              </ControlLabel>
            </Col>
            <Col xs={9}>
              <Field name="purchaseQuantity" type="number" component={Input} readOnly min={1} suffix="VS" />
            </Col>
          </FormGroup>

          <FormGroup className="text-center">
            <Col xs={12}>
              {error && <div className="text-danger">{error}</div>}
              <ButtonGroup vertical>
                <Button
                  bsSize="large"
                  disabled={!valid}
                  onClick={handleSubmit(this.submitPurchase)}
                >
                  <FormattedMessage
                    id="WidgetsAdd.PurchaseOrder"
                    defaultMessage="Purchase order"
                  />
                </Button>
                <Link to="/vs_purchases">
                  <Button
                    bsSize="large"
                  >
                    <FormattedMessage
                      id="WidgetsAdd.PurchaseOrderWasCreated"
                      defaultMessage="Purchase order was created"
                    />
                  </Button>
                </Link>
              </ButtonGroup>
            </Col>
          </FormGroup>
        </Form>
      </div>
    );
  }
}
