import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {reduxForm, Field, SubmissionError} from 'redux-form';
import configValidation from './configValidation';
import * as withdrawalConfigActions from 'redux/modules/withdrawalConfig';
import {Form, FormGroup, FormControl, ControlLabel, Button, Col} from 'react-bootstrap';
import { DateTimePicker } from 'react-widgets';
import Moment from 'moment';
import momentLocalizer from 'react-widgets/lib/localizers/moment';
import {FormattedMessage} from 'react-intl';
import config from '../../config.js';

import { browserHistory } from 'react-router';

// import { push } from 'react-router-redux';

const styles = require('./WithdrawalConfigAdd.scss');

momentLocalizer(Moment);


const DatePicker = ({input, meta: {touched, error}}) =>
  <div>
    <div className={styles.inputContainer + (error && touched ? ' has-error' : '')}>
      <DateTimePicker time={false} value={(input && input.value) ? new Date(input.value) : new Date()} onChange={input.onChange}/>
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

const Input = ({max, type, placeholder, input, disabled, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <FormControl max={max} type={type} {...input} placeholder={placeholder} disabled={disabled} />
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;


@connect(
  () => ({}),
  dispatch => bindActionCreators(withdrawalConfigActions, dispatch)
)
@reduxForm({
  form: 'record',
  fields: ['id', 'color', 'sprocketCount', 'owner'],
  validate: configValidation
})
export default class WithdrawalConfigAdd extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    editStop: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    save: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    formKey: PropTypes.string.isRequired,
    values: PropTypes.object.isRequired,
    pushState: PropTypes.func.isRequired,
    params: PropTypes.object,
    initialize: PropTypes.func.isRequired
  };


  constructor(props) {
    super(props);
    this.state = {id: props.params && props.params.id ? props.params : null};

    if (this.props.params.id) {
      const prom = new Promise((resolve, reject) => {
        const path = '/api/withdrawal_config/' + this.props.params.id;
        const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);
        fetch(apiPath + path, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          mode: 'cors',
          credentials: 'include',
        })
          .then((response) => response.json())
          .then((responseJson) => {
            this.props.initialize(responseJson);
            resolve(responseJson);
          })
          .catch((err) => {
            reject(err);
          });
      });
      prom.then(() => {
        this.setState({...this.state, loadedData: true});
      });
    }
  }
  // onCancel = () => {
  //   this.props.pushState('/vs_config');
  // }

  submit = (data) => {
    return new Promise((resolve, reject) => {
      const startDate = new Moment(data.startDate);
      const endDate = new Moment(data.endDate);
      const dateDiff = endDate.diff(startDate);
      if (!data.withdrawalRate || !data.startDate || !data.endDate) {
        throw new SubmissionError({_error: 'All fields required.'});
      }
      if (dateDiff < 0) {
        throw new SubmissionError({_error: 'Start date must be more than end date.'});
      }

      const method = this.state.loadedData ? 'PUT' : 'POST';
      const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);

      fetch(apiPath + '/api/withdrawal_config', {
        method: method,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
      })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.errors) {
          reject(new SubmissionError({_error: responseJson.message}));
        }
        browserHistory.push('/withdrawal_config');
        resolve(responseJson);
      })
      .catch((err) => {
        reject(new SubmissionError({_error: err.message || err.error || err}));
      });
    });
  }

  render() {
    /* eslint-disable */
    // const { editStop, fields: {id, color, sprocketCount, owner}, formKey, handleSubmit, invalid,
    const { handleSubmit, valid, error } = this.props;
    /* eslint-disable */
    //   pristine, save, submitting, saveError: { [formKey]: saveError }, values } = this.props;
    // const styles = require('containers/Widgets/Widgets.scss');

    return (
      <div>
        <Form onSubmit={handleSubmit(this.submit)} horizontal>
          <FormGroup
            controlId="searchFormMemberId"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="WithdrawalConfigAdd.StartDate"
                  defaultMessage="Start date"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <Field name="startDate" component={DatePicker}/>
            </Col>
          </FormGroup>
          <FormGroup
            controlId="searchFormMemberId"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="WithdrawalConfigAdd.EndDate"
                  defaultMessage="End date"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <Field name="endDate" component={DatePicker}/>
            </Col>
          </FormGroup>
          <FormGroup
            controlId="searchFormMemberId"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="WithdrawalConfigAdd.Ammount"
                  defaultMessage="Withdrawal Rate"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <Field name="withdrawalRate"
                     component={Input}
                     type="number"
                     max="100"
                     placeholder=""
              />
            </Col>
          </FormGroup>

          {error &&
          <FormGroup
            controlId="errrors"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="WithdrawalConfigAdd.Error"
                  defaultMessage="Error"
                />
              </ControlLabel>
            </Col>
            <Col red large xs={10}>
              <div className="text-danger">{error}</div>
            </Col>
          </FormGroup>}

          <div style={{textAlign: 'center'}}>
            <Button
              bsSize="large"
              disabled={!valid}
              onClick={handleSubmit(this.submit)}
            >
              <FormattedMessage
                id="WithdrawalConfigAdd.Add"
                defaultMessage="Add"
              />
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}
