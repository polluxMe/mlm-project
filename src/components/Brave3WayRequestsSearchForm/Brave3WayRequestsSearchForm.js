import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Form, FormGroup, FormControl, ControlLabel, Button, Col} from 'react-bootstrap';
import {injectIntl, FormattedMessage} from 'react-intl';
import { reduxForm, Field } from 'redux-form';
import {Spinner} from 'components';

const styles = require('./Brave3WayRequestsSearchForm.scss');

const Input = ({type, placeholder, input, disabled, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <FormControl type={type} {...input} placeholder={placeholder} disabled={disabled} />
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

const CheckboxGroup = ({options, input: {value, onChange}}) =>
  <div className={styles.checkboxGroup}>
    {options.map((option, index) => <label key={index}>
      <input
        type="checkbox"
        checked={value === option.value}
        onClick={
          () => {return value === option.value ? onChange('') : onChange(option.value);}
        }
      />
      {' ' + option.label}
      </label>)}
  </div>;

@reduxForm({
  form: 'brave3WayRequestsSearchForm',
})
@connect(
  state => ({
    form: state.form.brave3WayRequestsSearchForm,
  })
)
class Brave3WayRequestsSearchForm extends Component {
  render() {
    /* eslint-disable */
    const {
      handleSubmit,
      valid,
      error,
      intl,
      submitting,
      onSubmit
    } = this.props;
    /* eslint-disable */

    return (
      <Form onSubmit={handleSubmit} horizontal>
        <FormGroup
          controlId="searchFormMemberId"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="Brave3WayRequestsSearchForm.MemberID"
                defaultMessage="Member ID"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="member.applicantID"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'Brave3WayRequestsSearchForm.MemberID', defaultMessage: 'Member ID'})
                   }
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormBrave3WayRequestId"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="Brave3WayRequestsSearchForm.Brave3WayRequestID"
                defaultMessage="Brave 3 way request ID"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="_id"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'Brave3WayRequestsSearchForm.Brave3WayRequestID', defaultMessage: 'Brave 3 way request ID'})
                   }
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormBrave3WayPurchaseAmount"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="Brave3WayRequestsSearchForm.Brave3WayPurchaseAmount"
                defaultMessage="Brave 3 way purchase amount"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="brave3WayPurchaseAmount"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'Brave3WayRequestsSearchForm.Brave3WayPurchaseAmount', defaultMessage: 'Brave 3 way purchase amount'})
                   }
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="Brave3WayRequestsSearchForm.Status"
                defaultMessage="Status"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="status"
                   component={CheckboxGroup}
                   options={[
                     {
                       label: '成立済み',
                       value: 'created',
                     },
                     {
                       label: '変更済み',
                       value: 'changed',
                     },
                     {
                       label: '削除済み',
                       value: 'removed',
                     }
                   ]}
            />
          </Col>
        </FormGroup>

        <div style={{textAlign: 'center'}}>
          {error && <div className="text-danger">{error}</div>}
          <Button
            bsSize="large"
            disabled={!valid}
            onClick={handleSubmit(onSubmit)}
          >
            <Spinner loading={submitting} size={25} exclusive>
              <FormattedMessage
                id="Brave3WayRequestsSearchForm.Search"
                defaultMessage="Search"
              />
            </Spinner>
          </Button>
        </div>
      </Form>
    );
  }
}

export default injectIntl(Brave3WayRequestsSearchForm);
