import React, {Component, PropTypes} from 'react';
import {reduxForm, Field, SubmissionError} from 'redux-form';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import BraveRegistrationValidation from './BraveRegistrationValidation';
import {
  Form,
  FormControl,
  Alert,
  Glyphicon
} from 'react-bootstrap';
import {Row as FlexRow, Col} from 'react-flexbox-grid/lib';
import momentLocalizer from 'react-widgets/lib/localizers/moment';
import {injectIntl, intlShape, defineMessages, FormattedMessage} from 'react-intl';
import { parseServerValidationError } from 'utils/validation';
// import Rates from 'utils/rates';
import config from '../../config.js';
import { Spinner } from 'components';
import moment from 'moment';
import countries from 'utils/countries';
import { loadRate, loadCashRate} from 'redux/modules/vsRate';

// import { asyncConnect } from 'redux-async-connect';
import {load as loadConfig } from 'redux/modules/mainConfig';

import 'react-widgets/dist/css/react-widgets.css';

momentLocalizer(moment);

const messages = defineMessages({
  NamePlaceholder: {
    id: 'BraveRegistration.NamePlaceholder',
    defaultMessage: 'Name'
  },
  Furigana: {
    id: 'BraveRegistration.Furigana',
    defaultMessage: 'Furigana'
  },
  IntoducerID: {
    id: 'BraveRegistration.IntoducerID',
    defaultMessage: 'Introducer ID'
  },
  IntoducerName: {
    id: 'GCloudRegistration.IntoducerName',
    defaultMessage: 'Introducer Name'
  },
  NicknamePlaceholder: {
    id: 'BraveRegistration.NicknamePlaceholder',
    defaultMessage: 'Nickname'
  },
  Prefecture: {
    id: 'BraveRegistration.Prefecture',
    defaultMessage: 'Prefecture'
  },
  MunicipalDistrict: {
    id: 'BraveRegistration.MunicipalDistrict',
    defaultMessage: 'Municipal District'
  },
  Street: {
    id: 'BraveRegistration.Street',
    defaultMessage: 'Street'
  },
  Address: {
    id: 'BraveRegistration.Address',
    defaultMessage: 'Address'
  },
  PhoneNumber: {
    id: 'BraveRegistration.PhoneNumber',
    defaultMessage: 'Phone number'
  },
  EmailAddress: {
    id: 'BraveRegistration.EmailAddress',
    defaultMessage: 'Email address'
  },
  Passport: {
    id: 'BraveRegistration.Passport',
    defaultMessage: 'Passport'
  }
});

// function asyncValidate(data, dispatch, {isValidEmail}) {
//   if (!data.email) {
//     return Promise.resolve({});
//   }
//   return isValidEmail(data);
// }

const styles = require('./BraveRegistrationForm.scss');

const Select = ({suffix, input, options, valueField, visualField, meta: {touched, error}}) =>
  <div>
    <div className={error && touched ? ' has-error' : ''}>
      <FormControl componentClass="select" {...input} >
        {
          options && options.map((item) => <option key={item[valueField]} value={item[valueField]}>{item[visualField]}</option>)
        }
      </FormControl>
      <span>{suffix && <span style={{paddingLeft: 5, width: 40}}>{suffix}</span>}</span>
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

const Input = ({suffix, type, placeholder, input, disabled, onBlur, meta: {touched, error}}) =>
  <div>
    <div className={styles.inputContainer + (error && touched ? ' has-error' : '')}>
      <FormControl type={type || 'text'} {...input} placeholder={placeholder} disabled={disabled} onBlur={onBlur} />
      <Text bold>{suffix && <span style={{paddingLeft: 5, width: 40}}>{suffix}</span>}</Text>
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

const Radio = ({input, radioValue, children}) =>
  <div className={styles.radioContainer}>
    <label>
      <input type="radio" {...input} checked={radioValue === input.value} value={radioValue}/>{children}
    </label>
  </div>;

const Checkbox = ({input, radioValue, children}) =>
  <div className={styles.checkboxContainer}>
    <label className={styles.bold}>
      <input type="checkbox" {...input} value={radioValue}/><span className={styles.mBottom}>{children}</span>
    </label>
  </div>;

const Container = ({children, center, top, bottom, left, right, padding, vertical}) =>
  <div style={{
    height: '100%',
    display: 'flex',
    alignItems: top && 'flex-start' || bottom && 'flex-end' || center && 'center',
    justifyContent: left && 'flex-start' || right && 'flex-end' || center && 'space-around',
    flexDirection: vertical && 'column',
    padding: padding && 10
  }}>
    {children}
  </div>;

const RankTable = ({ input: {onChange}, rankThresholds, rate = 1, cashRate = 1}) => {
  return (
  <div className={styles.rankTable}>
    <table>
      <thead>
      <tr>
        <td rowSpan={2}></td>
        <td rowSpan={2}>Rank</td>
        <td colSpan={2}>
          <FormattedMessage
            id="BraveRegistration.Ticket"
            defaultMessage="Ticket cumulative purchase amount"
          />
        </td>
        <td rowSpan={2}>
          <FormattedMessage
            id="BraveRegistration.VirtualStockAmount"
            defaultMessage="Virtual stock amount"
          />
        </td>
      </tr>
      <tr>
        <td className={styles.amountHeaderCell}>
          <FormattedMessage
            id="BraveRegistration.TheCummulativePurchaseAmount"
            defaultMessage="The cummulative purchase amount"
          />
        </td>
        <td className={styles.amountHeaderCell}>
          <span className={styles.currency}>
            <FormattedMessage
              id="BraveRegistration.Circle"
              defaultMessage="Circle"
            />
          </span>
          <FormattedMessage
            id="BraveRegistration.YenPurchaseAmount"
            defaultMessage="Yen Purchase amount"
          />

        </td>
      </tr>
      </thead>
      <tbody>
      {rankThresholds.map((rank, index) => {
        const braveAmount = rankThresholds[rankThresholds.length - index - 1];
        const yenAmount = Math.floor(braveAmount * rate);
        const vsAmount = Math.floor(braveAmount / cashRate);

        const formatNum = (nStr) => {
          const xx = (nStr + '').split('.');
          let x1 = xx[0];
          const x2 = xx.length > 1 ? '.' + xx[1] : '';
          const rgx = /(\d+)(\d{3})/;
          while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
          }
          return x1 + x2;
        };

        const FormatCustomNumber = injectIntl(({number}) => (
          <span>
            {formatNum(number)}
          </span>
        ));

        // console.log('RATE', braveAmount, cashRate, ' = ', vsAmount);
        return (<tr key={index}>
          <td>
            <input name="rank" type="radio" value={rankThresholds.length - index} onChange={onChange}/>
          </td>
          <td>{`R${rankThresholds.length - index}`}</td>
          <td>
            $&nbsp;
            <FormatCustomNumber number={braveAmount} />
          </td>
          <td>
            <FormatCustomNumber number={yenAmount} />
            &nbsp;
            <FormattedMessage
              id="BraveRegistration.Circle"
              defaultMessage="Circle"
            />
          </td>
          <td>
            ATC&nbsp;
            <FormatCustomNumber number={vsAmount} />
          </td>
        </tr>);
      })}
      </tbody>
    </table>
  </div>
  );
};

const LabelCell = ({children, ...props}) =>
  <Col {...props} className={styles.labelCell}>
    {children}
  </Col>;

const ControlCell = ({children, ...props}) =>
  <Col {...props} className={styles.controlCell}>
    {children}
  </Col>;

const NoticeCell = ({children, ...props}) =>
  <Col {...props} className={styles.noticeCell}>
    {children}
  </Col>;

const Row = ({children}) =>
  <FlexRow className={styles.row}>
    {children}
  </FlexRow>;

// const LabelText = ({children}) =>
//   <span className={styles.label}>
//     {children}
//   </span>;

const Text = ({children, white, red, green, purple, bold, large, xlarge, inline}) =>
  <div
    style={{
      color: white && 'white'
      || red && 'red'
      || green && 'green'
      || purple && 'purple',
      fontWeight: bold && 'bold',
      fontSize: large && '1.2em' || xlarge && '1.6em',
      display: inline && 'inline'
    }}
  >
    {children}
  </div>;

const currentYear = +moment().format('YYYY');
const years = [];
const months = [];
const days = [];

for (let ii = currentYear; ii > currentYear - 100; ii -= 1) {
  years.push({_id: ii, name: ii});
}

for (let ii = 1; ii <= 12; ii += 1) {
  months.push({_id: ii, name: ii});
}

for (let ii = 1; ii <= 31; ii += 1) {
  days.push({_id: ii, name: ii});
}


@reduxForm({
  form: 'braveRegistration',
  validate: BraveRegistrationValidation,
  enableReinitialize: true
})
@connect(state => ({
  form: state.form.braveRegistration,
  config: state.mainConfig.data,
  cashRate: state.vsRate.cashRate,
  rate: state.vsRate.rate
}),
  dispatch => bindActionCreators({loadConfig, loadRate, loadCashRate}, dispatch)
)
class BraveRegistrationFrom extends Component {
  static propTypes = {
    active: PropTypes.string,
    asyncValidating: PropTypes.bool.isRequired,
    dirty: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    reset: PropTypes.func,
    invalid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func,
    config: PropTypes.object,
    loadConfig: PropTypes.func,
    loadRate: PropTypes.func,
    loadCashRate: PropTypes.func,
    cashRate: PropTypes.number,
    rate: PropTypes.number,
    intl: intlShape.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {agreeFailed: false};
  }

  componentWillMount() {
    this.props.loadConfig();
    this.props.loadRate();
    this.props.loadCashRate();
  }

  submit = (data) => {
    this.setState({
      agreeFailed: !data.isAgree
    });

    console.log('DATA', data);
    if (!data.isAgree) {
      return false;
    }

    const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);

    data.profile.dateOfBirth = moment(`${data.profile.yearOfBirth}-${data.profile.monthOfBirth}-${data.profile.dayOfBirth}`)._d;

    data.accountType = 'brave';

    data.purchaseBC = this.props.config.rankThresholds[data.rank - 1];
    data.purchaseAmount = Math.floor(data.purchaseBC * this.props.rate);
    data.purchaseQuantity = Math.floor(data.purchaseBC / this.props.cashRate);

    // delete data.rank;

    data.profile.postal = data.profile.postal1 + '-' + data.profile.postal2;
    data.profile.address = data.profile.prefecture + data.profile.municipalDistrict + data.profile.street + data.address;
    data.introducerID = data.introducerID && data.introducerID.trim();

    return fetch(apiPath + '/api/auth/register', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
      mode: 'cors',
      credentials: 'include',
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.errors) {
          if (typeof responseJson.errors === 'string') {
            throw new SubmissionError({_error: responseJson.errors});
          } else if (typeof responseJson.errors === 'object') {
            console.log(parseServerValidationError(responseJson.errors));
            throw new SubmissionError(parseServerValidationError(responseJson.errors));
          }
        }
        this.props.onSubmit(responseJson);
      })
      .catch((err) => {
        if (err instanceof SubmissionError) throw err;
        else console.log('err', err);
      });
  };

  render() {
    /* eslint-disable */
    const {
      handleSubmit,
      pristine,
      form,
      error,
      submitting,
      intl,
      anyTouched,
      valid,
      submitFailed
    } = this.props;
    const {agreeFailed} = this.state;
    /* eslint-disable */

    const loadZipData = () => {
      const postal1 = form.values && form.values.profile && form.values.profile.postal1;
      const postal2 = form.values && form.values.profile && form.values.profile.postal2;
      if (!postal1 || !postal2 || postal1.length !== 3 || postal2.length !== 4) {
        return;
      }

      const apiPath = 'https://yubinbango.github.io/yubinbango-data/data/' + postal1 + '.js';

      fetch(apiPath, {
        method: 'GET'
      })
        .then((response) => {
          if (response.status !== 200) {
            return '{}';
          }

          return response.text();
        })
        .then(responseData => {
          const dd = responseData.replace('$yubin(', '').replace(');', '');

          return JSON.parse(dd);
        })
        .then(json => {
          const PREFMAP = [null, '北海道', '青森県', '岩手県', '宮城県', '秋田県', '山形県', '福島県', '茨城県', '栃木県', '群馬県', '埼玉県', '千葉県', '東京都', '神奈川県', '新潟県', '富山県', '石川県', '福井県', '山梨県', '長野県', '岐阜県', '静岡県', '愛知県', '三重県', '滋賀県', '京都府', '大阪府', '兵庫県', '奈良県', '和歌山県', '鳥取県', '島根県', '岡山県', '広島県', '山口県', '徳島県', '香川県', '愛媛県', '高知県', '福岡県', '佐賀県', '長崎県', '熊本県', '大分県', '宮崎県', '鹿児島県', '沖縄県'];

          _.each(json, item => {
            item[0] = PREFMAP[item[0]];
          });

          return json;
        })
        .then(json => {
          const item = json[postal1 + postal2];

          if (item) {
            const newValues = form.values;
            if (!form.values.profile) {
              newValues.profile = {}
            }

            newValues.profile.prefecture = item[0];
            newValues.profile.municipalDistrict = item[1];
            newValues.profile.street = item[2];

            this.props.initialize(newValues);
          }
        })
    };

    return (
      <div>
        <Form horizontal onSubmit={handleSubmit(this.submit)}>
          <div className={styles.braveForm}>
            <Row>
              <LabelCell xs={12} sm={3} md={2} >
                <Container center left padding>
                  <Text white large bold>
                    <FormattedMessage
                      id="BraveRegistration.Name"
                      defaultMessage="Register name"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12} sm={9} md={3}>
                <Container top left padding>
                  <Field name="applicantName" component={Input} normalize={string => string.replace(/\u3000/g,' ')} placeholder={
                    intl.formatMessage(messages.NamePlaceholder)
                  } />
                </Container>
              </ControlCell>
              <ControlCell xs={12} smOffset={3} sm={9} md={7} mdOffset={0}>
                <Container top left padding>
                  <Field name="applicantKatakana" component={Input} normalize={string => string.replace(/\u3000/g,' ')} placeholder={
                    intl.formatMessage(messages.Furigana)
                  } />
                </Container>
              </ControlCell>
            </Row>
            <Row>
              <LabelCell xs={12}  md={2} sm={3}>
                <Container top left padding vertical>
                  <Text white large bold>
                    <FormattedMessage
                        id="BraveRegistration.Nickname"
                        defaultMessage="Nickname*"
                    />
                  </Text>
                  <Text white bold>
                    <FormattedMessage
                        id="BraveRegistration.Within alphanumeric 14 characters"
                        defaultMessage="within aplhanumeric 14 characters*"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12}  md={10} sm={9}>
                <Container top left padding>
                  <Field name="nickname" component={Input} placeholder={
                    intl.formatMessage(messages.NicknamePlaceholder)
                  } />
                </Container>
              </ControlCell>
            </Row>


            <Row>
              <LabelCell xs={12}  md={2} sm={3}>
                <Container center left padding>
                  <Text white large bold>
                    <FormattedMessage
                      id="BraveRegistration.National"
                      defaultMessage="National"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12}  md={3} sm={9}>
                <Container top left padding>
                  <Field name="profile.nationality" component={Select} options={countries} valueField="name" visualField="name" />
                </Container>
              </ControlCell>
              <LabelCell xs={12}  md={1} sm={3}>
                <Container center left padding>
                  <Text white large bold>
                    <FormattedMessage
                      id="BraveRegistration.Sex"
                      defaultMessage="Sex"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12}  md={2} sm={3}>
                <Container center left>
                  <Field name="profile.gender" component={Radio} radioValue="male">
                    <FormattedMessage
                      id="BraveRegistration.Male"
                      defaultMessage="Male"
                    />
                  </Field>
                  <Field name="profile.gender" component={Radio} radioValue="female">
                    <FormattedMessage
                      id="BraveRegistration.Female"
                      defaultMessage="Female"
                    />
                  </Field>
                  {
                    form.syncErrors &&
                    form.syncErrors.profile &&
                    form.syncErrors.profile.gender &&
                    form.fields &&
                    form.fields.profile &&
                    form.fields.profile.gender &&
                    form.fields.profile.gender.touched &&
                    <div className="text-danger">{form.syncErrors.profile.gender}</div>}
                </Container>
              </ControlCell>
              <LabelCell xs={12}  md={1} sm={2}>
                <Container center left padding>
                  <Text white large bold>
                    <FormattedMessage
                      id="BraveRegistration.Birthday"
                      defaultMessage="Birthday"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12}  md={3} sm={4}>
                <Container center left padding>
                  <Field name="profile.yearOfBirth" component={Select} options={years} valueField="_id" visualField="name" />
                  <Field name="profile.monthOfBirth" component={Select} options={months} valueField="_id" visualField="name" />
                  <Field name="profile.dayOfBirth" component={Select} options={days} valueField="_id" visualField="name" />
                </Container>
              </ControlCell>
            </Row>

            <Row>
              <LabelCell xs={12} sm={3}  md={2}>
                <Container top left padding>
                  <Text white large bold>
                    <FormattedMessage
                      id="GCloudRegistration.PostalCode"
                      defaultMessage="Postal code"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12} sm={9} md={3}>
                <Container top left padding>
                  <Field name="profile.postal1" component={Input} type="text" placeholder="210" maxLength={3} onBlur={loadZipData} />
                </Container>
              </ControlCell>
              <ControlCell xs={12} smOffset={3} sm={9} mdOffset={0} md={7}>
                <Container top left padding>
                  <Field name="profile.postal2" component={Input} type="text" placeholder="0805" maxLength={4} onBlur={loadZipData} />
                </Container>
              </ControlCell>
            </Row>

            <Row>
              <LabelCell xs={12} sm={3} md={2}>
                <Container top left padding>
                  <Text white large bold>
                    <FormattedMessage
                      id="GCloudRegistration.Address"
                      defaultMessage="Address"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12} sm={4} md={2}>
                <Container top left padding>
                  <Field name='profile.prefecture' component={Input} type='text' placeholder={
                    intl.formatMessage(messages.Prefecture)
                  } />
                </Container>
              </ControlCell>
              <ControlCell xs={12} sm={5} md={2}>
                <Container top left padding>
                  <Field name='profile.municipalDistrict' component={Input} type='text' placeholder={
                    intl.formatMessage(messages.MunicipalDistrict)
                  } />
                </Container>
              </ControlCell>
              <ControlCell xs={12} smOffset={3} mdOffset={0} sm={4} md={2}>
                <Container top left padding>
                  <Field name='profile.street' component={Input} type='text' placeholder={
                    intl.formatMessage(messages.Street)
                  } />
                </Container>
              </ControlCell>
              <ControlCell xs={12} sm={5} md={4}>
                <Container top left padding>
                  <Field name='address' component={Input} type='text' placeholder={
                    intl.formatMessage(messages.Address)
                  } />
                </Container>
              </ControlCell>
            </Row>

            <Row>
              <LabelCell xs={12}  md={2} sm={3}>
                <Container top left padding>
                  <Text white large bold>
                    <FormattedMessage
                      id="BraveRegistration.Email"
                      defaultMessage="Email"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12}  md={10} sm={9}>
                <Container top left padding>
                  <Field name="email" component={Input} type="email" placeholder={
                    intl.formatMessage(messages.EmailAddress)
                  } />
                </Container>
              </ControlCell>
            </Row>

            <Row>
              <LabelCell xs={12} sm={3} md={2} >
                <Container center left padding>
                  <Text white large bold>
                    <FormattedMessage
                      id="BraveRegistration.MobilePhone"
                      defaultMessage="Mobile phone"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12} sm={9} md={3}>
                <Container top left padding>
                  <Field name="profile.mobilePhone" component={Input} placeholder={
                    intl.formatMessage(messages.PhoneNumber)
                  } />
                </Container>
              </ControlCell>
              <LabelCell xs={12} sm={3} md={2} >
                <Container center left padding>
                  <Text white large bold>
                    <FormattedMessage
                      id="BraveRegistration.PhoneNumber"
                      defaultMessage="Phone number"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12} sm={9} md={5}>
                <Container top left padding>
                  <Field name="profile.phone" component={Input} placeholder={
                    intl.formatMessage(messages.PhoneNumber)
                  } />
                </Container>
              </ControlCell>
            </Row>

            <Row>
              <LabelCell xs={12}  md={2} sm={3}>
                <Container top left padding>
                  <Text white large bold>
                    <FormattedMessage
                      id="BraveRegistration.Passport"
                      defaultMessage="Passport"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12}  md={10} sm={9}>
                <Container top left padding>
                  <Field name="profile.passport" component={Radio} radioValue="パスポート">パスポート</Field>
                  <Field name="profile.passport" component={Radio} radioValue="免許証">免許証</Field>
                </Container>
              </ControlCell>
            </Row>

            <Row>
              <LabelCell xs={12} sm={3} md={2}>
                <Container center left padding>
                  <Text white large bold>
                    <FormattedMessage
                      id="BraveRegistration.HowToApply"
                      defaultMessage="How to apply"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12} sm={9} md={2}>
                <Container center left>
                  <Field name="purchaseType" component={Radio} radioValue="cash">
                    <FormattedMessage
                      id="BraveRegistration.Cash"
                      defaultMessage="Cash"
                    />
                  </Field>
                </Container>
              </ControlCell>
              <ControlCell xs={12} smOffset={3} sm={9} mdOffset={0} md={8}>
                <Container center left padding>
                  <Text>
                    <FormattedMessage
                      id="BraveRegistration.PurchasingExampleIncaseOf"
                      defaultMessage="Purchasing example: Incase of 18 cent rate, may purchase 44,444 virtual stock by 1.040.000 yen (ATC) money. (With 20 cent rate, will be 40,000 virtual stock also with 1.040.000 yen (ATC)"
                    />
                  </Text>
                  {
                    form.syncErrors &&
                    form.syncErrors.purchaseType &&
                    form.fields &&
                    form.fields.purchaseType &&
                    form.fields.purchaseType.touched &&
                    <div className="text-danger">{form.syncErrors.purchaseType}</div>
                  }
                </Container>
              </ControlCell>
            </Row>

            <Row>
              <LabelCell xs={12} sm={3} md={2}>
                <Container center left padding>
                  <Text white bold large>
                    <FormattedMessage
                      id="BraveRegistration.HopeRank"
                      defaultMessage="Hope rank"
                    />
                    <Text white bold>
                      <FormattedMessage
                        id="BraveRegistration.HopeRankNote"
                        defaultMessage="Please put a ✓ to the purchase price of the rank of your choice"
                      />
                    </Text>
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12} sm={9} md={10}>
                <div style={{padding: 20, borderWidth: 2}}>
                  <div className={styles.currentRate}>
                    <FormattedMessage
                      id="GCloudRegistration.PurchaseRateBraveDollar"
                      defaultMessage="Purchase rate: 1 B$ = "
                    />
                    {`${Math.round(this.props.rate)} 円` }
                    <FormattedMessage
                      id="GCloudRegistration.PurchaseRateVirtualStock"
                      defaultMessage=" 1 Virtual Stock = "
                    />
                    {`${Math.round(this.props.cashRate * 100)} ¢`}
                  </div>
                  <Field
                    name="rank"
                    component={RankTable}
                    rankThresholds={this.props.config && this.props.config.rankThresholds || []}
                    rate={this.props.rate}
                    cashRate={this.props.cashRate}/>
                  {
                    form.syncErrors &&
                    form.syncErrors.rank &&
                    form.fields &&
                    form.fields.rank &&
                    form.fields.rank.touched &&
                    <div className="text-danger">{form.syncErrors.rank}</div>
                  }
                </div>
              </ControlCell>
            </Row>
            <Row>
              <LabelCell xs={12} sm={3} md={2} >
                <Container center left padding>
                  <Text white large bold>
                    <FormattedMessage
                      id="BraveRegistration.IntroducerName"
                      defaultMessage="Introducer name"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12} sm={9} md={10}>
                <Container top left padding>
                  <Field name="introducerName" component={Input}  normalize={string => string.replace(/\u3000/g,' ')} placeholder={
                    intl.formatMessage(messages.IntoducerName)
                  } />
                </Container>
              </ControlCell>
            </Row>
            <Row>
              <LabelCell xs={12} sm={3} md={2}>
                <Container left center padding>
                  <Text white large bold>
                    <FormattedMessage
                      id="BraveRegistration.IntroducerId"
                      defaultMessage="Introducer ID"
                    />
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12} sm={9} md={10} >
                <Container top left padding>
                  <Field name="introducerID" component={Input} placeholder={
                    intl.formatMessage(messages.IntoducerID)
                  } />
                </Container>
              </ControlCell>
            </Row>
            <Row>
              <LabelCell xs={12} sm={3} md={2}>
                <Container center left padding>
                  <Text white large bold>
                    振込先
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12} sm={9} md={10}>
                <Container padding center vertical>
                      <Text xlarge>楽天銀行 第2営業支店（普）</Text>
                      <Text xlarge>7308081 カ）アドバンス</Text>
                      <Text>*登録者と振込先名は同一にしてお願いいたします。</Text>
                </Container>
              </ControlCell>
            </Row>
            <Row>
              <LabelCell xs={12} sm={3} md={2}>
                <Container center left padding>
                  <Text white large bold>
                    お問合せ
                  </Text>
                </Container>
              </LabelCell>
              <ControlCell xs={12} sm={3} md={4}>
                <Container center padding>
                  <Text xlarge><Glyphicon style={{paddingRight: 10}} glyph="phone-alt"/>050-5532-3061</Text>
                </Container>
              </ControlCell>
              <NoticeCell xs={12} sm={3} md={2}>
                  <Container vertical center padding>
                    <Text>受付時間</Text>
                    <Text>平日 AM10：00～PM5：00</Text>
                  </Container>
              </NoticeCell>
              <ControlCell xs={12} sm={3} md={4}>
                <Container center padding>
                  <Text xlarge>FAX 03-6369-3281</Text>
                </Container>
              </ControlCell>
            </Row>
          </div>
          <Row>
            <ControlCell xs={12} sm={9} md={12}>
              <div className={'form-group text-center ' + styles.bold}>
                <div>
                  <FormattedMessage
                    id="BraveRegistration.AgreeTextOne"
                    defaultMessage="AgreeText1"
                  />
                </div>
                <div>
                  <FormattedMessage
                    id="BraveRegistration.AgreeTextTwo"
                    defaultMessage="AgreeText2"
                  />
                </div>
              </div>
              <Row>
                <ControlCell xs={12} sm={9} md={12}>
                  <Container center padding>
                    <Field name="isAgree" component={Checkbox} radioValue="isAgree">
                      <FormattedMessage
                        style={{paddingBottom: 10}}
                        id="BraveRegistration.IAgree"
                        defaultMessage="I'm agree"
                      />
                    </Field>
                  </Container>
                </ControlCell>
              </Row>
            </ControlCell>
          </Row>
          <Row>
            <Col xs={12}>
              <div className={'form-group text-center ' + styles.btnBlock }>

                  <button className="btn btn-success" disabled={pristine || submitting || error}>
                    <Spinner loading={submitting} size={25} exclusive>
                      <i className="fa fa-paper-plane"/>&nbsp;
                      <FormattedMessage
                        id="BraveRegistration.Submit"
                        defaultMessage="Submit"
                      />
                    </Spinner>
                  </button>
                <br />
                <br />
                {error && <Alert bsStyle="danger">
                  {error}
                </Alert>}
                {!error && !valid && anyTouched && submitFailed && <Alert bsStyle="danger">
                  必須項目が設定されていません。設定内容をご確認下さい。
                </Alert>}
                {agreeFailed && <Alert bsStyle="danger">
                  「同意する」にチェックがされていません。チェックをしてから登録ボタンをクリックして下さい。
                </Alert>}
                {/* <button className="btn btn-danger" type="button" onClick={reset} disabled={pristine} style={{marginLeft: 15}}>
                  <i className="fa fa-undo"/> Reset
                </button> */}
                </div>
              </Col>
            </Row>

          </Form>

      </div>
    );
  }
}

export default injectIntl(BraveRegistrationFrom);
