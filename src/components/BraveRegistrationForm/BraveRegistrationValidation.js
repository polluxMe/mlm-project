import memoize from 'lru-memoize';
// import {createValidator, required, maxLength} from 'utils/validation';
//
// const surveyValidation = createValidator({
//   name: [required],
//   katagane: [required],
//   introducerName: [required],
//   introducerKatagana: [required],
//   introducerId: [required],
//   nickname: [required, maxLength(14)],
//   nationality: [required],
//   email: [required],
//   sex: [required],
//   birthday: [required],
//   address: [required],
//   mobilePhoneNumber: [required],
//   phoneNumber: [required],
//   purchaseType: [required],
//   rank: [required]
// });
//
// export default memoize(10)(surveyValidation);

const validate = (values) => {
  const errors = {};
  if (!values.applicantName) {
    errors.applicantName = '必須';
  }
  if (!values.applicantKatakana) {
    errors.applicantKatakana = '必須';
  }
  if (!values.introducerID) {
    errors.introducerID = '必須';
  }
  if (!values.nickname) {
    errors.nickname = '必須';
  }
  if (values.profile) {
    if (!values.profile.gender) {
      errors.profile = errors.profile || {};
      errors.profile.gender = '必須';
    }
    if (!values.profile.postal1) {
      errors.profile = errors.profile || {};
      errors.profile.postal1 = '必須';
    }
    if (!values.profile.postal2) {
      errors.profile = errors.profile || {};
      errors.profile.postal12 = '必須';
    }
    if (!values.profile.prefecture) {
      errors.profile = errors.profile || {};
      errors.profile.prefecture = '必須';
    }
    if (!values.profile.municipalDistrict) {
      errors.profile = errors.profile || {};
      errors.profile.municipalDistrict = '必須';
    }
    if (!values.profile.street) {
      errors.profile = errors.profile || {};
      errors.profile.street = '必須';
    }
    if (!values.profile.address) {
      errors.profile = errors.profile || {};
      errors.profile.address = '必須';
    }
    if (!values.profile.mobilePhone && !values.profile.phone) {
      errors.profile = errors.profile || {};
      errors.profile.mobilePhone = '必須';
    }
    if (!values.profile.mobilePhone && !values.profile.phone) {
      errors.profile = errors.profile || {};
      errors.profile.phone = '必須';
    }
    if (!values.profile.nationality) {
      errors.profile = errors.profile || {};
      errors.profile.nationality = '必須';
    }
    if (!values.profile.passport) {
      errors.profile = errors.profile || {};
      errors.profile.passport = '必須';
    }
  }
  if (!values.email) {
    errors.email = '必須';
  }
  if (!values.purchaseType) {
    errors.purchaseType = '必須';
  }
  if (!values.rank) {
    errors.rank = '必須';
  }
  return errors;
};

export default memoize(10)(validate);
