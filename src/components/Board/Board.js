import React from 'react';
import classNames from 'classnames';

export default class Board extends React.Component {
  static propTypes = {
    children: React.PropTypes.any
  };
  render() {
    const {
      children
    } = this.props;
    const styles = require('./Board.scss');
    return (
      <div className={classNames(styles.board, 'bord')}>
        <div className={classNames(styles.content)}>
          {children}
        </div>
      </div>
    );
  }
}
