import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Form, FormGroup, FormControl, ControlLabel, Button, Col} from 'react-bootstrap';
import {injectIntl, FormattedMessage} from 'react-intl';
import { reduxForm, Field } from 'redux-form';
import { DateTimePicker } from 'react-widgets';
import moment from 'moment';
import momentLocalizer from 'react-widgets/lib/localizers/moment';
momentLocalizer(moment);
import {Spinner} from 'components';

const styles = require('./VSFramePurchaseHistoryListSearchForm.scss');

const Input = ({type, placeholder, input, disabled, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <FormControl type={type} {...input} placeholder={placeholder} disabled={disabled} />
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

const CheckboxGroup = ({options, input: {value, onChange}}) =>
  <div className={styles.checkboxGroup}>
    {options.map((option, index) => <label>
      <input
        type="checkbox"
        key={index}
        checked={value === option.value}
        onClick={
          () => {return value === option.value ? onChange('') : onChange(option.value);}
        }
      />
      {' ' + option.label}
      </label>)}
  </div>;

const DatePicker = ({input, meta: {touched, error}}) =>
  <div>
    <div className={styles.inputContainer + (error && touched ? ' has-error' : '')}>
      <DateTimePicker time={false} value={(input && input.value) ? new Date(input.value) : new Date()} onChange={input.onChange}/>
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;


@reduxForm({
  form: 'vSFramePurchaseHistoryListSearchForm',
})
@connect(
  state => ({
    form: state.form.vSFramePurchaseHistoryListSearchForm,
  })
)
class VSFramePurchaseHistoryListSearchForm extends Component {
  render() {
    /* eslint-disable */
    const {
      handleSubmit,
      valid,
      error,
      intl,
      submitting,
      onSubmit
    } = this.props;
    /* eslint-disable */

    return (
      <Form onSubmit={handleSubmit(onSubmit)} horizontal>
        <FormGroup
          controlId="searchFormDate"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="VSPurchasesSearchForm.Date"
                defaultMessage="Date"
              />
            </ControlLabel>
          </Col>
          <Col xs={5}>
            <Field name="startDate" component={DatePicker}/>
          </Col>
          <Col xs={5}>
            <Field name="endDate" component={DatePicker}/>
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormMemberId"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="VSPurchasesSearchForm.MemberID"
                defaultMessage="Member ID"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="profile.applicantID"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'VSPurchasesSearchForm.MemberID', defaultMessage: 'Member ID'})
                   }
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormPurchaseId"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="VSPurchasesSearchForm.PurchaseID"
                defaultMessage="Purchase ID"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="_id"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'VSPurchasesSearchForm.PurchaseID', defaultMessage: 'Purchase ID'})
                   }
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormName"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="VSPurchasesSearchForm.Name"
                defaultMessage="Name"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="member.applicantName"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'VSPurchasesSearchForm.Name', defaultMessage: 'Name'})
                   }
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormNickname"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="VSPurchasesSearchForm.Nickname"
                defaultMessage="Nickname"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="member.nickname"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'VSPurchasesSearchForm.Nickname', defaultMessage: 'Nickname'})
                   }
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormEmail"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="VSPurchasesSearchForm.Email"
                defaultMessage="Email"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="member.email"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'VSPurchasesSearchForm.Email', defaultMessage: 'Email'})
                   }
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="VSPurchasesSearchForm.PurchaseType"
                defaultMessage="Purchase Type"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="purchaseType"
                   component={CheckboxGroup}
                   options={[
                     {
                       label: '現金購入',
                       value: 'cash',
                     },
                     {
                       label: 'ATC購入',
                       value: 'ATC',
                     },
                     {
                       label: 'Bitcoin購入',
                       value: 'BTC',
                     }
                   ]}
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="VSPurchasesSearchForm.Status"
                defaultMessage="Status"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="status"
                   component={CheckboxGroup}
                   options={[
                     {
                       label: '現金買い注文',
                       value: 'cash_order'
                     },
                     {
                       label: 'ATC買い注文',
                       value: 'atc_order'
                     },
                     {
                       label: 'Bitcoin買い注文',
                       value: 'btc_order'
                     },
                     {
                       label: '現金購入',
                       value: 'cash_purchased'
                     },
                     {
                       label: 'ATC購入',
                       value: 'atc_purchased'
                     },
                     {
                       label: 'Bitcoin購入',
                       value: 'btc_purchased'
                     },
                     {
                       label: '注文キャンセル（管理）',
                       value: 'removed_order_admin'
                     },
                     {
                       label: '注文キャンセル',
                       value: 'removed_order_user'
                     },
                     {
                       label: '購入キャンセル（管理）',
                       value: 'removed_purchase_admin'
                     }
                   ]}
            />
          </Col>
        </FormGroup>

        <div style={{textAlign: 'center'}}>
          {error && <div className="text-danger">{error}</div>}
          <Button
            bsSize="large"
            disabled={!valid}
            onClick={handleSubmit(onSubmit)}
          >
            <Spinner loading={submitting} size={25} exclusive>
              <FormattedMessage
                id="VSPurchasesSearchForm.Search"
                defaultMessage="Search"
              />
            </Spinner>
          </Button>
        </div>
      </Form>
    );
  }
}

export default injectIntl(VSFramePurchaseHistoryListSearchForm);
