import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {
  Tabs,
  Tab,
  Button
} from 'react-bootstrap';

@connect(
  state => ({
    data: state.membership.data
  }))
export default class MembershipBlock extends Component {
  static propTypes = {
    data: PropTypes.object
  };

  render() {
    const {data} = this.props;

    return (
      <div>
        <div id="membersip">
          <div>
            Member {data.memberId}
          </div>

          <div>
            Rank {data.rank}
          </div>

          <div>
            <Button
              bsStyle="default">
              {"Commission"}
            </Button>
            <Button
              bsStyle="default">
              {"Basic information"}
            </Button>
          </div>

          <Tabs defaultActiveKey={2} id="uncontrolled-tab-example">
            <Tab eventKey={1} title="Tab 1">
              Owned stock
              <div>{data.ownedStock}</div>
            </Tab>
            <Tab eventKey={2} title="Tab 2">
              Brave
              <div>{data.brave}</div>
            </Tab>
            <Tab eventKey={3} title="Tab 3">
              Brave coin
              <div>{data.braveCoin}</div>
            </Tab>
            <Tab eventKey={4} title="Tab 4">
              ticket
              <div>{data.ticket}</div>
            </Tab>
          </Tabs>
        </div>

      </div>
    );
  }
}
