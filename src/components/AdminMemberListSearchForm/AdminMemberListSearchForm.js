import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Form, FormGroup, FormControl, ControlLabel, Button, Col} from 'react-bootstrap';
import {injectIntl, FormattedMessage} from 'react-intl';
import { reduxForm, Field } from 'redux-form';
import _ from 'lodash'; //eslint-disable-line
import {Spinner} from 'components';

const styles = require('./AdminMemberListSearchForm.scss');

const Input = ({type, placeholder, input, disabled, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <FormControl type={type} {...input} placeholder={placeholder} disabled={disabled} />
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

const CheckboxGroup = ({options, input: {value, onChange}}) =>
  <div className={styles.checkboxGroup}>
    {options.map((option, index) => <label>
      <input
        type="checkbox"
        key={index}
        onClick={
          () => {return _.includes(value, option.value) ? onChange(_.without(value, option.value)) : onChange([...value, option.value]);}
        }
      />
      {' ' + option.label}
      </label>)}
  </div>;

@reduxForm({
  form: 'adminMemberListSearchForm',
})
@connect(
  state => ({
    form: state.form.adminMemberListSearchForm,
  })
)
class AdminMemberListSearchForm extends Component {
  render() {
    /* eslint-disable */
    const {
      asyncValidating,
      dirty,
      active,
      handleSubmit,
      reset,
      invalid,
      pristine,
      valid,
      form,
      error,
      submitting,
      intl,
      onSubmit
    } = this.props;
    /* eslint-disable */

    return (
      <Form onSubmit={handleSubmit(onSubmit)} horizontal>
        <FormGroup
          controlId="searchFormMemberId"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="AdminMemberListSearchForm.BraveID"
                defaultMessage="BRAVE ID"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="username"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'AdminMemberListSearchForm.BraveID'}, {defaultMessage: 'BRAVE ID'})
                   }
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormPurchaseId"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="AdminMemberListSearchForm.BraveIntroducerID"
                defaultMessage="BRAVE Introducer ID"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="introducerID"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'AdminMemberListSearchForm.BraveIntroducerID'}, {defaultMessage: 'BRAVE Introducer ID'})
                   }
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormRevenue"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="AdminMemberListSearchForm.Name"
                defaultMessage="Name"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="applicantName"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'AdminMemberListSearchForm.Name'}, {defaultMessage: 'Name'})
                   }
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormPurchaseRate"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="AdminMemberListSearchForm.NameFurigana"
                defaultMessage="Name (Furigana)"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="applicantKatakana"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'AdminMemberListSearchForm.NameFurigana'}, {defaultMessage: 'Name (Furigana)'})
                   }
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormPurchaseAmount"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="AdminMemberListSearchForm.RegisteredDate"
                defaultMessage="Registered date"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="createDate"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'AdminMemberListSearchForm.RegisteredDate'}, {defaultMessage: 'Registered date'})
                   }
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormPurchaseAmount"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="AdminMemberListSearchForm.PurchaseMoneyAmount"
                defaultMessage="Purchase money amount"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="purchasePrice"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'AdminMemberListSearchForm.PurchaseMoneyAmount'}, {defaultMessage: 'Purchase money amount'})
                   }
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="AdminMemberListSearchForm.Status"
                defaultMessage="Status"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="status"
                   component={CheckboxGroup}
                   options={[
                     {
                       label: '仮会員',
                       value: 'temp',
                     },
                     {
                       label: '本会員',
                       value: 'official',
                     },
                     {
                       label: '無効',
                       value: 'deactivated',
                     }
                   ]}
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormPurchaseAmount"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="AdminMemberListSearchForm.MemberID"
                defaultMessage="Member ID"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="applicantID"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'AdminMemberListSearchForm.MemberID'}, {defaultMessage: 'Member ID'})
                   }
            />
          </Col>
        </FormGroup>
        <div style={{textAlign: 'center'}}>
          {error && <div className="text-danger">{error}</div>}
          <Button
            bsSize="large"
            disabled={!valid || submitting}
            onClick={handleSubmit(onSubmit)}
          >
            <Spinner loading={submitting} size={25} exclusive>
              <FormattedMessage
                id="VSPurchasesSearchForm.Seacrh"
                defaultMessage="Search"
              />
            </Spinner>
          </Button>
        </div>
      </Form>
    );
  }
}

export default injectIntl(AdminMemberListSearchForm);