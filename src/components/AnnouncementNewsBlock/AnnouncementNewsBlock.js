import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';

@connect(
  state => ({
    news: state.news.data
  }))
export default class EventsBlock extends Component {
  static propTypes = {
    news: PropTypes.array
  };

  render() {
    const {news} = this.props;

    return (
      <div>
        <h2>Announcement News</h2>
        {
          news.map((item) =>
            <div key={item.id}>
              <div><span>{item.date}</span></div>
              <div><a>{item.title}</a></div>
              <div>{item.text}</div>
            </div>)
        }
        <div>

        </div>
      </div>
    );
  }
}
