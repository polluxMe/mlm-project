import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {reduxForm, Field, SubmissionError} from 'redux-form';
import * as vsConfigActions from 'redux/modules/vsConfig';
import {Form, FormGroup, FormControl, ControlLabel, Button, Col, InputGroup} from 'react-bootstrap';
import { DateTimePicker } from 'react-widgets';
import Moment from 'moment';
import momentLocalizer from 'react-widgets/lib/localizers/moment';
import {injectIntl, intlShape, FormattedMessage, defineMessages} from 'react-intl';
import config from '../../config.js';

import { browserHistory } from 'react-router';

const messages = defineMessages({
  StartDateMustBeMoreThaEndDate: {
    id: 'VSConfigAdd.StartDateMustBeMoreThaEndDate',
    defaultMessage: 'Start date must be more than end date.'
  },
  RateB$cents: {
    id: 'VSConfigAdd.RateB$cents',
    defaultMessage: 'Rate in B$ cents'
  },
  AllFieldsAreRequired: {
    id: 'VSConfigAdd.AllFieldsAreRequired',
    defaultMessage: 'VSConfigAdd.AllFieldsAreRequired'
  }
});

const styles = require('./VSconfig.scss');

momentLocalizer(Moment);

const DatePicker = ({input, meta: {touched, error}}) =>
  <div>
    <div className={styles.inputContainer + (error && touched ? ' has-error' : '')}>
      <DateTimePicker time={false} value={(input && input.value) ? new Date(input.value) : new Date()} onChange={input.onChange}/>
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

const Input = ({type, placeholder, input, readOnly, suffix, max, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <InputGroup>
        <FormControl type={type} {...input} placeholder={placeholder} readOnly={readOnly} max={max} />
        {suffix && <InputGroup.Addon>{suffix}</InputGroup.Addon>}
      </InputGroup>
    </div>
    {!readOnly && error && touched && <div className="text-danger">{error}</div>}
  </div>;

@reduxForm({
  form: 'record',
  enableReinitialize: true

})
@connect(
  () => ({}),
  dispatch => bindActionCreators(vsConfigActions, dispatch)
)
class VSConfigAdd extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
    pushState: PropTypes.func.isRequired,
    params: PropTypes.object,
    initialize: PropTypes.func.isRequired,
    load: PropTypes.func.isRequired,
    intl: intlShape.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {purchaseQuantity: 0, id: props.params && props.params.id ? props.params : null};

    if (this.props.params.id) {
      const prom = new Promise((resolve, reject) => {
        const path = '/api/vs_config/' + this.props.params.id;
        const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);
        fetch(apiPath + path, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          mode: 'cors',
          credentials: 'include',
        })
          .then((response) => response.json())
          .then((responseJson) => {
            this.props.initialize(responseJson);
            resolve(responseJson);
          })
          .catch((err) => {
            console.log('err', err);
            reject(err);
          });
      });
      prom.then((res) => {
        this.setState({...this.state, loadedData: true});
        console.log('res', res);
      });
    }
  }

  // onCancel = () => {
  //   this.props.pushState('/vs_config');
  // }

  submit = (data) => {
    const {intl} = this.props;
    return new Promise((resolve, reject) => {
      const startDate = new Moment(data.startDate);
      const endDate = new Moment(data.endDate);
      const dateDiff = endDate.diff(startDate);
      if (!data.purchaseRate || !data.startDate || !data.endDate) {
        throw new SubmissionError({_error: intl.formatMessage(messages.AllFieldsAreRequired)});
      }
      if (dateDiff < 0) {
        throw new SubmissionError({_error: intl.formatMessage(messages.StartDateMustBeMoreThaEndDate)});
      }

      let method = 'POST';
      if (this.state.loadedData) {
        method = 'PUT';
      }

      const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);
      fetch(apiPath + '/api/vs_config', {
        method: method,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
        mode: 'cors',
        credentials: 'include',
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('responseJson', responseJson);
        if (responseJson.errors) {
          return reject(new SubmissionError({_error: responseJson.message}));
        }
      })
      .then(() => {
        browserHistory.push('/vs_config');
      })
      .catch((err) => {
        console.log('err', err);
        reject(new SubmissionError({_error: err.message || err.error || err}));
      });
    });
  }

  render() {
    /* eslint-disable */
    // const { editStop, fields: {id, color, sprocketCount, owner}, formKey, handleSubmit, invalid,
    const { handleSubmit, valid, error, intl } = this.props;
    /* eslint-disable */
    //   pristine, save, submitting, saveError: { [formKey]: saveError }, values } = this.props;
    // const styles = require('containers/Widgets/Widgets.scss');

    return (
      <div>
        <Form onSubmit={handleSubmit(this.submit)} horizontal>
          <FormGroup
            controlId="searchFormMemberId"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="VSConfigAdd.StartDate"
                  defaultMessage="Start date"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <Field name="startDate" component={DatePicker}/>
            </Col>
          </FormGroup>
          <FormGroup
            controlId="searchFormMemberId"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="VSConfigAdd.EndDate"
                  defaultMessage="End date"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <Field name="endDate" component={DatePicker}/>
            </Col>
          </FormGroup>
          <FormGroup
            controlId="searchFormMemberId"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="VSConfigAdd.Ammount"
                  defaultMessage="Purchase Rate"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <Field name="purchaseRate"
                     component={Input}
                     type="number"
                     max="100"
                     placeholder={intl.formatMessage(messages.RateB$cents)}
                     suffix="¢"
              />
            </Col>
          </FormGroup>

          {error &&
          <FormGroup
            controlId="errrors"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="VSConfigAdd.Error"
                  defaultMessage="Error"
                />
              </ControlLabel>
            </Col>
            <Col red large xs={10}>
              <div className="text-danger">{error}</div>
            </Col>
          </FormGroup>}

          <div style={{textAlign: 'center'}}>
            <Button
              bsSize="large"
              disabled={!valid}
              onClick={handleSubmit(this.submit)}
            >
              <FormattedMessage
                id="VSConfigAdd.Add"
                defaultMessage="Add"
              />
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}

export default injectIntl(VSConfigAdd);