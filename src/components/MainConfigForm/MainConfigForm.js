import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
// import {bindActionCreators} from 'redux';
import {reduxForm, Field, SubmissionError} from 'redux-form';
import configValidation from './configValidation';

import {Form, FormGroup, ControlLabel, FormControl, Button, Col} from 'react-bootstrap';
import {FormattedMessage} from 'react-intl';
import config from '../../config.js';

// import { browserHistory } from 'react-router';

// import { push } from 'react-router-redux';

const days = [];
for (let ii = 1; ii <= 31; ii += 1) {
  days.push({_id: ii, name: ii});
}
days.push({_id: 0, name: 'End of month'});

const Select = ({suffix, input, options, valueField, visualField, meta: {touched, error}}) =>
  <div>
    <div className={error && touched ? ' has-error' : ''}>
      <FormControl componentClass="select" {...input} >
        {
          options && options.map((item) => <option key={item[valueField]} value={item[valueField]}>{item[visualField]}</option>)
        }
      </FormControl>
      <span>{suffix && <span style={{paddingLeft: 5, width: 40}}>{suffix}</span>}</span>
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

const Input = ({max, type, placeholder, input, disabled, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <FormControl max={max} type={type} {...input} placeholder={placeholder} disabled={disabled} />
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

const RankSelect = ({placeholder, input, disabled, meta: {touched, error}}) => {
  const ranks = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  return (
    <div>
      <div className={(error && touched ? ' has-error' : '')}>
        <FormControl componentClass="select" {...input} placeholder={placeholder} disabled={disabled}>
          {ranks.map((rank, index) => {
            return (<option key={index} value={rank}>{`R${rank}`}</option>);
          })}
        </FormControl>
      </div>
    {error && touched && <div className="text-danger">{error}</div>}
    </div>
  );
};


@connect(
  () => ({})
)
@reduxForm({
  form: 'mainConfig',
  validate: configValidation
})
export default class VSConfigAdd extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    editStop: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    save: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    formKey: PropTypes.string.isRequired,
    values: PropTypes.object.isRequired,
    pushState: PropTypes.func.isRequired
  };

  submit = (data) => {
    delete data._id;
    return new Promise((resolve, reject) => {
      const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);
      fetch(apiPath + '/api/main_config', {
        method: 'PUT',
        mode: 'cors',
        credentials: 'include',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
      })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log('responseJson', responseJson);
          if (responseJson.errors) {
            reject(new SubmissionError({_error: responseJson.message}));
          }
          resolve(responseJson);
        })
        .catch((err) => {
          console.log('err', err);
          reject(new SubmissionError({_error: err.message || err.error || err}));
        });
    });
  }

  renderRankInputs(name, suffix) {
    const ranks = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    return (
      ranks.map((rank, index) =>
        <FormGroup
          key={index}
          controlId={'mainConfigForm' + name + rank}
        >
        <Col xs={1}>
          <ControlLabel>
            {`R${rank}:`}
          </ControlLabel>
        </Col>
        <Col xs={10}>
          <Field name={`${name}[${index}]`} component={Input}/>
        </Col>
        <Col xs={1}>
          <ControlLabel>
            {suffix}
          </ControlLabel>
        </Col>
      </FormGroup>)
    );
  }

  renderStageInputs(name, suffix) {
    const ranks = [1, 2, 3, 4, 5, 6, 7, 8];
    return (
      ranks.map((stage, index) =>
        <FormGroup
          key={index}
          controlId={'mainConfigForm' + name + stage}
        >
          <Col xs={1}>
            <ControlLabel>
              {stage}
              {'-'}
              <FormattedMessage
                id="MainConfig.Stage"
                defaultMessage="stage"
              />
              {':'}
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name={`${name}[${index}]`} component={Input}/>
          </Col>
          <Col xs={1}>
            <ControlLabel>
              {suffix}
            </ControlLabel>
          </Col>
        </FormGroup>)
    );
  }

  render() {
    /* eslint-disable */
    // const { editStop, fields: {id, color, sprocketCount, owner}, formKey, handleSubmit, invalid,
    const { handleSubmit, valid, error } = this.props;
    /* eslint-disable */
    //   pristine, save, submitting, saveError: { [formKey]: saveError }, values } = this.props;
    // const styles = require('containers/Widgets/Widgets.scss');

    return (
      <div>
        <Form onSubmit={handleSubmit(this.submit)} horizontal>
          <FormGroup
            controlId="mainConfigFormCommisionCycle"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="MainConfig.CycleOfTheMainCommissionCutoffDate"
                  defaultMessage="Cycle of the main commission cutoff date"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <Field name="mainCommissionCycle" component={Select} options={days} valueField="_id" visualField="name" />
            </Col>
          </FormGroup>
          <FormGroup
            controlId="mainConfigFormCashRewardsCycle"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="MainConfig.CycleOfCacheRewardsCutoffDate"
                  defaultMessage="Cycle of cache Rewards cutoff date"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <Field name="cacheRewardsCycle" component={Select} options={days} valueField="_id" visualField="name" />
            </Col>
          </FormGroup>
          <FormGroup
            controlId="mainConfigFormMatchingBonusCycle"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="MainConfig.CycleOfMatchingBonusCutoffDate"
                  defaultMessage="Cycle of matching bonus cutoff date"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <Field name="matchingBonusCycle" component={Select} options={days} valueField="_id" visualField="name" />
            </Col>
          </FormGroup>
          <FormGroup
            controlId="mainConfigFormRankThreshold"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="MainConfig.RankThresholdSaveCriterion"
                  defaultMessage="RANK threshold (sales criterion)"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              {this.renderRankInputs('rankThresholds','円')}
            </Col>
          </FormGroup>
          <FormGroup
            controlId="mainConfigFormRankThreshold"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="MainConfig.MainCommissionBonusLevelPer"
                  defaultMessage="Main commission bonus LEVEL Per%"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              {this.renderStageInputs('mainCommissionLevels','%')}
            </Col>
          </FormGroup>
          <FormGroup
            controlId="mainConfigFormRankThreshold"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="MainConfig.TheMainCommissionOfBonusLevelThreshold"
                  defaultMessage="The main commission of bonus LEVEL threshold"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              {this.renderStageInputs('mainCommissionThresholds','円')}
            </Col>
          </FormGroup>
          <FormGroup
            controlId="mainConfigFormBravePurchase"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="MainConfig.BravePurchaseRate"
                  defaultMessage="BRAVE $ purchase rate"
                />
              </ControlLabel>
            </Col>
            <Col xs={2}>
              <ControlLabel className="pull-right">
                1 BRAVE $ =
              </ControlLabel>
            </Col>
            <Col xs={7}>
              <Field name="bravePurchaseRate" component={Input}/>
            </Col>
            <Col xs={1}>
              <ControlLabel className="pull-left">円</ControlLabel>
            </Col>
          </FormGroup>
          <FormGroup
            controlId="mainConfigFormBraveSale"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="MainConfig.BraveSaleRate"
                  defaultMessage="BRAVE $ sale rate"
                />
              </ControlLabel>
            </Col>
            <Col xs={2}>
              <ControlLabel className="pull-right">
                1 BRAVE $ =
              </ControlLabel>
            </Col>
            <Col xs={7}>
              <Field name="braveSaleRate" component={Input}/>
            </Col>
            <Col xs={1}>
              <ControlLabel className="pull-left">円</ControlLabel>
            </Col>
          </FormGroup>
          <FormGroup
            controlId="mainConfigFormRankThreshold"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="MainConfig.MatchingBonusTargetRank"
                  defaultMessage="Matching bonus target RANK"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <Field name="matchingBonusTargetRank" component={RankSelect}/>
            </Col>
          </FormGroup>
          <FormGroup
            controlId="mainConfigFormMatchingBonusPercents"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="MainConfig.PercentOfEachLevelOfMatchingBonus"
                  defaultMessage="% Of each LEVEL of matching bonus"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              {this.renderStageInputs('matchingBonusPercents','%')}
            </Col>
          </FormGroup>
          <FormGroup
            controlId="mainConfigFormMatchingRewardPercents"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="MainConfig.CashRewardsOfReductionPercent"
                  defaultMessage="Cash Rewards of reduction%"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              {this.renderRankInputs('cashRewardPercents','%')}
            </Col>
          </FormGroup>
          <FormGroup
            controlId="mainConfigBinaryMapCinfirmCycle"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="MainConfig.BinaryMapConfigCycle"
                  defaultMessage="Binary map confirm cycle"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <Field name="binaryMapConfirmCycle" component={Select} options={days} valueField="_id" visualField="name" />
            </Col>
          </FormGroup>
          <FormGroup
            controlId="minimumWithdrawalAmount"
          >
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="MainConfig.MinimumWithdrawalAmount"
                  defaultMessage="Minimum withdraw amount"
                />
              </ControlLabel>
            </Col>
            <Col xs={10}>
              <Field name="minimumWithdrawalAmount" component={Input} type="number" />
            </Col>
          </FormGroup>

          <div style={{textAlign: 'center'}}>
            <Button
              bsSize="large"
              disabled={!valid}
              onClick={handleSubmit(this.submit)}
            >
              <FormattedMessage
                id="MainConfig.Save"
                defaultMessage="Save"
              />
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}
