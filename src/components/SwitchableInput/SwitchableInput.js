import React, {Component, PropTypes} from 'react';

class SwitchableInput extends Component {
  static propTypes = {
    type: PropTypes.string,
    editable: PropTypes.bool,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onChange: PropTypes.func,
    valueFormat: PropTypes.func,
    inputComponent: PropTypes.oneOfType([PropTypes.element, PropTypes.object, PropTypes.func])
  };

  static defaultProps = {
    type: 'text'
  };

  constructor(props) {
    super(props);
    this.state = {editing: false};
  }

  onLabelClick = () => {
    this.setState({editing: true});
  };

  onInputBlur = () => {
    this.setState({editing: false});
  }

  render() {
    const styles = require('./SwitchableInput.scss');

    let result = <div onClick={this.onLabelClick} className={this.props.editable && styles.switchableInput}>{this.props.valueFormat ? this.props.valueFormat(this.props.value) : this.props.value}</div>;
    if (this.props.editable && this.state.editing) {
      if (this.props.inputComponent) {
        result = React.createElement(this.props.inputComponent, {...this.props, value: this.props.value, onChange: this.props.onChange, onBlur: this.onInputBlur});
      } else {
        result = <input autoFocus value={this.props.value} onChange={(event) => this.props.onChange(event.target.value)} onBlur={this.onInputBlur} type="text"/>;
      }
    }
    return result;
  }
}

export default SwitchableInput;

