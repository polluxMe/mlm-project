import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Form, FormGroup, FormControl, ControlLabel, Button, Col} from 'react-bootstrap';
import {injectIntl, FormattedMessage} from 'react-intl';
import { reduxForm, Field } from 'redux-form';
import { DateTimePicker } from 'react-widgets';
import moment from 'moment';
import momentLocalizer from 'react-widgets/lib/localizers/moment';
import {Spinner} from 'components';

momentLocalizer(moment);

const styles = require('./MainCommissionRequestsSearchForm.scss');

const Input = ({type, placeholder, input, disabled, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <FormControl type={type} {...input} placeholder={placeholder} disabled={disabled} />
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

const CheckboxGroup = ({options, input: {value, onChange}}) =>
  <div className={styles.checkboxGroup}>
    {options.map((option, index) => <label>
      <input
        type="checkbox"
        key={index}
        checked={value === option.value}
        onClick={
          () => {return value === option.value ? onChange('') : onChange(option.value);}
        }
      />
      {' ' + option.label}
      </label>)}
  </div>;


const DatePicker = ({input, meta: {touched, error}}) =>
  <div>
    <div className={styles.inputContainer + (error && touched ? ' has-error' : '')}>
      <DateTimePicker time={false} onChange={input.onChange} />
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;

@reduxForm({
  form: 'vsPurchasesSearchForm',
})
@connect(
  state => ({
    form: state.form.vsPurchasesSearchForm,
  })
)
class MainCommissionRequestsSearchForm extends Component {
  render() {
    /* eslint-disable */
    const {
      handleSubmit,
      valid,
      error,
      intl,
      submitting,
      onSubmit
    } = this.props;
    /* eslint-disable */

    return (
      <Form onSubmit={handleSubmit(onSubmit)} horizontal>
        <FormGroup>
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="MainCommissionRequestsSearchForm.creationDate"
                defaultMessage="Creation date"
              />
            </ControlLabel>
          </Col>
          <Col xs={5}>
            <Field name="startDate" component={DatePicker} />
          </Col>
          <Col xs={5}>
            <Field name="endDate" component={DatePicker} />
          </Col>
        </FormGroup>

        <FormGroup
          controlId="searchFormMemberId"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="MainCommissionRequestsSearchForm.MemberID"
                defaultMessage="Member ID"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="member.applicantID"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'MainCommissionRequestsSearchForm.MemberID', defaultMessage: 'Member ID'})
                   }
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormCommissionRequestId"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="MainCommissionRequestsSearchForm.CommissionRequestID"
                defaultMessage="Main Commission request ID"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="_id"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'MainCommissionRequestsSearchForm.CommissionRequestID', defaultMessage: 'Main Commission request ID'})
                   }
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="searchFormCommissionAmount"
        >
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="MainCommissionRequestsSearchForm.CommissionAmount"
                defaultMessage="Commission amount"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="amount"
                   component={Input}
                   type="text"
                   placeholder={
                     intl.formatMessage({id: 'MainCommissionRequestsSearchForm.CommissionAmount', defaultMessage: 'Commission amount'})
                   }
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col xs={2}>
            <ControlLabel>
              <FormattedMessage
                id="MainCommissionRequestsSearchForm.Status"
                defaultMessage="Status"
              />
            </ControlLabel>
          </Col>
          <Col xs={10}>
            <Field name="status"
                   component={CheckboxGroup}
                   options={[
                     {
                       label: '成立済み',
                       value: 'created',
                     },
                     {
                       label: '変更済み',
                       value: 'changed',
                     },
                     {
                       label: '削除済み',
                       value: 'removed',
                     }
                   ]}
            />
          </Col>
        </FormGroup>

        <div style={{textAlign: 'center'}}>
          {error && <div className="text-danger">{error}</div>}
          <Button
            bsSize="large"
            disabled={!valid}
            onClick={handleSubmit(onSubmit)}
          >
            <Spinner loading={submitting} size={25} exclusive>
              <FormattedMessage
                id="MainCommissionRequestsSearchForm.Seacrh"
                defaultMessage="Search"
              />
            </Spinner>
          </Button>
        </div>
      </Form>
    );
  }
}

export default injectIntl(MainCommissionRequestsSearchForm);


