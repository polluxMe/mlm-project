import React, {PropTypes} from 'react';
import {Table} from 'react-bootstrap';

export default class DetailsTable extends React.Component {
  static propTypes = {
    data: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
      })
    )
  };

  constructor(props) {
    super(props);
  }

  render() {
    // const styles = require('./DetailsTable.scss');

    return (
      <Table>
        <tbody>
          {this.props.data && this.props.data.map((item, index) =>
            <tr key={index}>
              <td>{item.label}</td>
              <td>{item.value}</td>
            </tr>
          )}
        </tbody>
      </Table>
    );
  }
}
