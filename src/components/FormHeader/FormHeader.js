import React from 'react';
import {FormattedMessage} from 'react-intl';
import {Row, Col} from 'react-flexbox-grid/lib';

export default class FormHeader extends React.Component {
  static propTypes = {
    color: React.PropTypes.string
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { color = 'black' } = this.props;
    return (
      <div style={{borderTop: '2px solid', borderColor: color, padding: 5}}>
        <Row>
          <Col xs={12} >
            <div style={{backgroundColor: color, color: 'white', fontWeight: 'bold', textAlign: 'center', padding: 4, fontSize: 20, marginBottom: 5}}>
              <FormattedMessage
                id="BraveRegistration.NoteWhenRegistering"
                defaultMessage="Note when registering"
              />
            </div>
            <div style={{color: color, fontSize: 15}}>
              <FormattedMessage
                id="BraveRegistration.InPreOpenTimeCanChange"
                defaultMessage="In pre-open time, can change types of schedule and rate, and other changes. Please confirm the newest information to introducer or confirm at seminar."
              />
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
