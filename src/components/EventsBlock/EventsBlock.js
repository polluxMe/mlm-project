import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {FormattedMessage} from 'react-intl';

@connect(
  state => ({
    events: state.events.data
  }))
export default class EventsBlock extends Component {
  static propTypes = {
    events: PropTypes.array
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const {events} = this.props;

    return (
      <div>
        <h2>
          <FormattedMessage
            id="EventsBlock.title"
            defaultMessage="Event seminar information"
          />
        </h2>
        {
          events.map((event) =>
            <div key={event.id}>
              <h3>{event.title}</h3>
              <div>{event.text}</div>
            </div>)
        }
      </div>
    );
  }
}
