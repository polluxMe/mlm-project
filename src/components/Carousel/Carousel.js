import React from 'react';
import Slider from 'react-slick';

// require('../../../node_modules/slick-carousel/slick/slick.css');
// require('../../../node_modules/slick-carousel/slick/slick-theme.css');

const Carousel = React.createClass({
  render() {
    const settings = {
      vertical: true,
      arrows: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      speed: 500
    };
    return (
        <Slider {...settings}>
          <div><img src="http://placekitten.com/g/400/200" /></div>
          <div><img src="http://placekitten.com/g/400/200" /></div>
          <div><img src="http://placekitten.com/g/400/200" /></div>
        </Slider>
    );
  }
});

export default Carousel;
