import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
// import {reduxForm, Field, SubmissionError} from 'redux-form';
// import * as vsConfigActions from 'redux/modules/vsConfigFrame';
import {loadFrames, loadFrame, save, load as loadFramesList, update} from 'redux/modules/vsConfigFrame';
import {Form, FormGroup, FormControl, ControlLabel, Button, Col} from 'react-bootstrap';
import Moment from 'moment';
import momentLocalizer from 'react-widgets/lib/localizers/moment';
import {FormattedMessage} from 'react-intl';
import {Spinner} from 'components';

import _ from 'lodash'; // eslint-disable-line

// import config from '../../config.js';

// import { browserHistory } from 'react-router';

import { push as pushRoute} from 'react-router-redux';

// const MIN_END_RATE = 35;
const MAX_END_RATE = 50;

momentLocalizer(Moment);

const Select = ({options, ...props}) =>
  <div>
    <div>
      <FormControl {...props} componentClass="select">
        <option key="0" value="0">...</option>
        {options && options.map((item, index) => <option key={index} value={item}>{item}</option>)}
      </FormControl>
    </div>
  </div>;

@connect(
  state => ({
    frames: state.vsConfigFrame.frames,
    frame: state.vsConfigFrame.frame,
    loadedData: state.vsConfigFrame.loadedData,
    saving: state.vsConfigFrame.saving,
  }),
  {loadFrames, loadFrame, save, update, pushRoute, loadFramesList}
)
export default class VSConfigFrameAdd extends Component {
  static propTypes = {
    save: PropTypes.func.isRequired,
    update: PropTypes.func.isRequired,
    pushRoute: PropTypes.func.isRequired,
    loadFrames: PropTypes.func.isRequired,
    loadFrame: PropTypes.func.isRequired,
    loadFramesList: PropTypes.func.isRequired,
    frames: PropTypes.object,
    frame: PropTypes.object,
    params: PropTypes.object,
    saving: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.state = {startRate: null, endRate: null, frames: []};
  }

  componentWillMount() {
    if (this.isEdit()) {
      this.props.loadFrame(this.props.params.id)
        .then((frameConfig) => this.setState({
          startRate: frameConfig.startRate,
          endRate: frameConfig.endRate,
          purchaseAmount: frameConfig.purchaseAmmount,
          frames: _.map(frameConfig.frames, (frame) => _.pick(frame, ['purchaseFrame', 'status']))
        }));
    }
    this.props.loadFrames();
  }

  onGenerateFrames = () => {
    const frames = [];
    for (let rate = this.state.startRate; rate <= this.state.endRate; rate++ ) {
      frames.push({purchaseFrame: this.state.purchaseAmount, status: 'created'});
    }
    this.setState({frames});
  };

  onStartRateChange = (event) => {
    this.setState({startRate: event.target.value, frames: []});
  };

  onEndRateChange = (event) => {
    this.setState({endRate: event.target.value, frames: []});
  };

  onAmountChange = (event) => {
    this.setState({purchaseAmount: event.target.value, frames: []});
  };

  onFrameAmountChange = (event, frameIndex) => {
    const newFrames = [...this.state.frames];
    newFrames[frameIndex].purchaseFrame = event.target.value;
    this.setState({frames: newFrames});
  };

  onDeleteFrame = () => {
    this.setState({
      frames: this.state.frames.slice(0, -1),
      endRate: this.state.endRate - 1
    });
  };

  onAddFrame = () => {
    this.setState({
      frames: [...this.state.frames, {purchaseFrame: this.state.purchaseAmount, status: 'created'}],
      endRate: this.state.endRate + 1
    });
    console.log(this.state);
  };

  onSubmit = () => {
    if (this.isEdit()) {
      this.props.update({
        _id: this.props.frame._id,
        startRate: this.state.startRate,
        endRate: this.state.endRate,
        purchaseAmmount: this.state.purchaseAmount,
        frameAmounts: this.state.frames.map((frame) => frame.purchaseFrame),
      })
        .then(() => this.props.pushRoute('/vs_config_frame'));
    } else {
      this.props.save({
        startRate: this.state.startRate,
        endRate: this.state.endRate,
        purchaseAmmount: this.state.purchaseAmount,
        frameAmounts: this.state.frames.map((frame) => frame.purchaseFrame),
      })
        .then(() => this.props.pushRoute('/vs_config_frame'));
    }
  };

  isEdit = () => {
    return this.props.params && this.props.params.id;
  };

  renderFrames = () => {
    return (this.state.frames.map((frame, index) =>
        <FormGroup controlId="framesBlock" key={index}>
          <Col xs={2}>{index + +this.state.startRate} B$c</Col>
          <Col xs={8}>
            <FormControl key={index}
                         type="number"
                         value={frame.purchaseFrame}
                         onChange={(event) => {this.onFrameAmountChange(event, index);}}
                         disabled={frame.status !== 'created'}
            />
          </Col>
          <Col xs={2}>
            {index === this.state.frames.length - 1 &&
            // index + +this.state.startRate > MIN_END_RATE &&
            frame.status === 'created' &&
            this.isEdit() &&
            <Button bsSize="large" bsStyle="warning" onClick={this.onDeleteFrame}>
              <FormattedMessage
                id="VSConfigFrameAdd.Delete"
                defaultMessage="Delete"
              />
            </Button>}
          </Col>
        </FormGroup>
    ));
  };

  renderAddButton = () => {
    const nextRate = this.state.frames.length + +this.state.startRate;
    if (nextRate <= MAX_END_RATE) {
      return (
        <FormGroup>
          <Col xs={2}>{this.state.frames.length + +this.state.startRate} B$c</Col>
          <Col xs={8}>
            <Button bsSize="large" bsStyle="success" onClick={this.onAddFrame}>
              <FormattedMessage
                id="VSConfigFrameAdd.AddFrame"
                defaultMessage="Add frame"
              />
            </Button>
          </Col>
        </FormGroup>
      );
    }
  };

  // submit = (data) => {
  //   return new Promise((resolve, reject) => {
  //     if (!this.state.loadedData && (!data.purchaseAmmount || !data.startRate || !data.endRate)) {
  //       throw new SubmissionError({_error: 'All fields required.'});
  //     }
  //     const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);
  //     let method = 'POST';
  //     if (this.state.loadedData) {
  //       method = 'PUT';
  //     }
  //     fetch(apiPath + '/api/vs_frame_config', {
  //       method: method,
  //       headers: {
  //         'Accept': 'application/json',
  //         'Content-Type': 'application/json',
  //       },
  //       body: JSON.stringify(data)
  //     })
  //     .then((response) => response.json())
  //     .then((responseJson) => {
  //       console.log('responseJson', responseJson);
  //       if (responseJson.errors) {
  //         return reject(new SubmissionError({_error: responseJson.message}));
  //       }
  //       browserHistory.push('/vs_config_frame');
  //       resolve(responseJson);
  //     })
  //     .catch((err) => {
  //       console.log('err', err);
  //       reject(new SubmissionError({_error: err.message || err.error || err}));
  //     });
  //   });
  // }

  render() {
    // const styles = require('containers/Widgets/Widgets.scss');

    return (
      <div>
        <Form horizontal>
          <FormGroup>
            <Col xs={2}>
              <ControlLabel>
                <FormattedMessage
                  id="VSConfigFrameAdd.Rates"
                  defaultMessage="Rate:"
                />
              </ControlLabel>
            </Col>
            <Col xs={3}>
              <Select value={this.state.startRate }
                      disabled={this.isEdit()}
                      options={this.props.frames.from}
                      onChange={this.onStartRateChange}/>
            </Col>
            <Col xs={3}>
              <Select value={this.state.endRate}
                      disabled={this.isEdit()}
                      options={this.props.frames.to}
                      onChange={this.onEndRateChange}/>
            </Col>
            <Col xs={1}>
              <ControlLabel>
                <FormattedMessage
                  id="VSConfigFrameAdd.FramesBlock"
                  defaultMessage="Frames"
                />
              </ControlLabel>
            </Col>
            <Col xs={2}>
              <FormControl value={this.state.purchaseAmount}
                     type="number"
                     disabled={this.isEdit()}
                     placeholder="VS"
                     onChange={this.onAmountChange}/>
            </Col>
            <Col xs={1}>
              <Button onClick={this.onGenerateFrames}
                      disabled={this.isEdit() ||
                      !this.state.purchaseAmount ||
                      !this.state.startRate ||
                      !this.state.endRate}
                      bsSize="large">
                <FormattedMessage
                  id="VSConfigFrameAdd.Add"
                  defaultMessage="Add"
                />
              </Button>
            </Col>
          </FormGroup>

          <FormGroup
            controlId="frames"
          >
            <Col red large xsOffset={2} xs={10}>
              {this.renderFrames()}
              {this.isEdit() && this.renderAddButton()}
            </Col>
          </FormGroup>

          <div style={{textAlign: 'center'}}>
            <Button
              bsSize="large"
              disabled={!this.state.frames.length}
              onClick={this.onSubmit}
            >
              <Spinner loading={this.props.saving} exclusive size={20}>
                <FormattedMessage
                  id="VSConfigFrameAdd.Add"
                  defaultMessage="Add"
                />
              </Spinner>
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}
