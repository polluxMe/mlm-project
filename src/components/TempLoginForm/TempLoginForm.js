import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Form, FormGroup, FormControl, ControlLabel, Button} from 'react-bootstrap';
import {FormattedMessage} from 'react-intl';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import TempLoginValidation from './TempLoginValidation';
import { Spinner } from 'components';

import config from '../../config.js';

const Input = ({type, placeholder, input, disabled, meta: {touched, error}}) =>
  <div>
    <div className={(error && touched ? ' has-error' : '')}>
      <FormControl type={type} {...input} placeholder={placeholder} disabled={disabled} />
    </div>
    {error && touched && <div className="text-danger">{error}</div>}
  </div>;


@reduxForm({
  form: 'tempLoginForm',
  validate: TempLoginValidation
})
@connect(
  state => ({
    form: state.form.tempLoginForm,
    tempUser: state.register.tempUser
  }))
export default class TempLoginForm extends Component {
  static propTypes = {
    tempUser: PropTypes.object,
  }

  render() {
    /* eslint-disable */
    const {
      asyncValidating,
      dirty,
      active,
      handleSubmit,
      reset,
      invalid,
      pristine,
      valid,
      form,
      error,
      submitting
    } = this.props;
    /* eslint-disable */

    const submit = (data) => {
      return Promise.resolve()
        .then(() => {
          const apiPath = 'http://' + config.apiHost + ':' + (config.apiPort || 3030);

          data.username = data.id;
          return fetch(apiPath + '/api/auth/login', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            credentials: 'include',
            mode: 'cors',
            body: JSON.stringify(data)
          })
          .then((response) => response.json())
          .then((responseJson) => {
            if(!responseJson.user) {
              throw new SubmissionError({_error:'ログイン名またはパスワードが正しくありません。'});
            }
            this.props.onSubmit(responseJson);
          })
          .catch((err) => {
            console.log('error', err);
            throw new SubmissionError({_error:'ログイン名またはパスワードが正しくありません。'});
          });
        })
    };

    return (
      <Form onSubmit={handleSubmit(submit)}>
        <FormGroup
          controlId="login"
        >
          <ControlLabel>ID</ControlLabel>
          <Field name="id"
                 component={Input}
                 type="text"
                 placeholder="ID"
          />
          <FormControl.Feedback />
        </FormGroup>

        <FormGroup
          controlId="password"
        >
          <ControlLabel>
            <FormattedMessage
              id="LoginFlow.TemporaryPassword"
              defaultMessage="Temporary password"
            />
          </ControlLabel>
          <Field name="password"
                 component={Input}
                 type="password"
                 placeholder="Password"
          />
          <FormControl.Feedback />
        </FormGroup>
        <div style={{textAlign: 'center'}}>
          {error && <div className="text-danger">{error}</div>}
          <Button
            bsSize="large"
            type="submit"
            disabled={!valid || submitting || error}>
            <Spinner loading={submitting} size={25} exclusive>
              <i className="fa fa-sign-in"/>{' '}
              <FormattedMessage
                id="TempLogin.LogIn"
                defaultMessage="Log in"
              />
            </Spinner>
          </Button>
        </div>
      </Form>
    );
  }
}
