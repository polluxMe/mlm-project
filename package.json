{
  "name": "react-redux-universal-hot-example",
  "description": "Example of an isomorphic (universal) webapp using react redux and hot reloading",
  "author": "Erik Rasmussen <rasmussenerik@gmail.com> (http://github.com/erikras)",
  "license": "MIT",
  "version": "0.9.0",
  "repository": {
    "type": "git",
    "url": "https://github.com/erikras/react-redux-universal-hot-example"
  },
  "homepage": "https://github.com/erikras/react-redux-universal-hot-example",
  "keywords": [
    "react",
    "isomorphic",
    "universal",
    "webpack",
    "express",
    "hot reloading",
    "react-hot-reloader",
    "redux",
    "starter",
    "boilerplate",
    "babel"
  ],
  "main": "bin/server.js",
  "scripts": {
    "start": "concurrent --kill-others \"npm run start-prod\" \"npm run start-prod-api\"",
    "start-prod": "better-npm-run start-prod",
    "start-prod-api": "better-npm-run start-prod-api",
    "build": "better-npm-run build",
    "postinstall": "npm run build",
    "lint": "eslint -c .eslintrc src",
    "start-dev": "better-npm-run start-dev",
    "start-dev-api": "better-npm-run start-dev-api",
    "watch-client": "better-npm-run watch-client",
    "dev": "concurrent --kill-others \"npm run watch-client\" \"npm run start-dev\" \"npm run start-dev-api\"",
    "test": "karma start",
    "test-node": "./node_modules/mocha/bin/mocha $(find api -name '*-test.js') --compilers js:babel-core/register",
    "test-node-watch": "./node_modules/mocha/bin/mocha $(find api -name '*-test.js') --compilers js:babel-core/register --watch",
    "build:langs": "./node_modules/.bin/babel-node scripts/translate.js"
  },
  "betterScripts": {
    "start-prod": {
      "command": "node ./bin/server.js",
      "env": {
        "NODE_PATH": "./src",
        "NODE_ENV": "production",
        "PORT": 80,
        "APIPORT": 3030
      }
    },
    "start-prod-api": {
      "command": "node ./bin/api.js",
      "env": {
        "NODE_PATH": "./api",
        "NODE_ENV": "production",
        "APIPORT": 3030
      }
    },
    "start-dev": {
      "command": "node ./bin/server.js",
      "env": {
        "NODE_PATH": "./src",
        "NODE_ENV": "development",
        "PORT": 3000,
        "APIPORT": 3030,
        "LOCALE": "en-US"
      }
    },
    "start-dev-api": {
      "command": "node ./bin/api.js",
      "env": {
        "NODE_PATH": "./api",
        "NODE_ENV": "development",
        "APIPORT": 3030
      }
    },
    "watch-client": {
      "command": "node webpack/webpack-dev-server.js",
      "env": {
        "UV_THREADPOOL_SIZE": 100,
        "NODE_PATH": "./src",
        "PORT": 3000,
        "APIPORT": 3030
      }
    },
    "build": {
      "command": "webpack --verbose --colors --display-error-details --config webpack/prod.config.js",
      "env": {
        "NODE_ENV": "production"
      }
    }
  },
  "dependencies": {
    "async": "^2.0.1",
    "autoprefixer-loader": "^3.1.0",
    "babel-cli": "^6.18.0",
    "babel-core": "^6.5.2",
    "babel-eslint": "^5.0.0-beta6",
    "babel-loader": "^6.2.1",
    "babel-plugin-add-module-exports": "^0.1.2",
    "babel-plugin-react-intl": "^2.2.0",
    "babel-plugin-react-transform": "^2.0.2",
    "babel-plugin-transform-decorators-legacy": "^1.3.4",
    "babel-plugin-transform-react-display-name": "^6.3.13",
    "babel-plugin-transform-runtime": "^6.3.13",
    "babel-plugin-typecheck": "^3.9.0",
    "babel-polyfill": "^6.3.14",
    "babel-preset-es2015": "^6.3.13",
    "babel-preset-react": "^6.3.13",
    "babel-preset-stage-0": "^6.3.13",
    "babel-register": "^6.3.13",
    "babel-runtime": "^6.3.19",
    "basic-auth": "^1.0.4",
    "bcryptjs": "^2.3.0",
    "better-npm-run": "0.0.8",
    "body-parser": "^1.14.1",
    "bootstrap": "^3.3.7",
    "bootstrap-sass": "^3.3.5",
    "bootstrap-sass-loader": "^1.0.9",
    "chai": "^3.3.0",
    "classnames": "^2.2.5",
    "clean-webpack-plugin": "^0.1.13",
    "commander": "^2.9.0",
    "compression": "^1.6.0",
    "concurrently": "^0.1.1",
    "connect-ensure-login": "^0.1.1",
    "connect-mongo": "^1.3.2",
    "cookie-parser": "^1.4.3",
    "cors": "^2.8.1",
    "css-loader": "^0.23.1",
    "cssmin": "^0.4.3",
    "csvtojson": "^1.0.2",
    "d3": "^3.4.11",
    "deasync": "^0.1.7",
    "eslint": "1.10.3",
    "eslint-config-airbnb": "0.1.0",
    "eslint-loader": "^1.0.0",
    "eslint-plugin-import": "^0.8.0",
    "eslint-plugin-react": "^3.5.0",
    "express": "^4.13.3",
    "express-ping": "^1.2.0",
    "express-session": "^1.14.1",
    "extract-text-webpack-plugin": "^0.9.1",
    "feathers": "^2.0.2",
    "feathers-authentication": "^0.7.10",
    "feathers-hooks": "^1.5.7",
    "file-loader": "^0.8.5",
    "flexboxgrid": "^6.3.1",
    "font-awesome": "^4.4.0",
    "font-awesome-webpack": "0.0.4",
    "glob": "^7.0.6",
    "handlebars": "^4.0.5",
    "hoist-non-react-statics": "^1.0.3",
    "html-minifier": "^3.0.2",
    "http-proxy": "^1.12.0",
    "intl": "^1.2.5",
    "intl-locales-supported": "^1.0.0",
    "intl-messageformat-parser": "^1.2.0",
    "invariant": "^2.2.0",
    "json-loader": "^0.5.4",
    "json2csv": "^3.7.1",
    "json2xls": "^0.1.2",
    "juice": "^3.0.0",
    "less": "^2.5.3",
    "less-loader": "^2.2.1",
    "lodash": "^4.15.0",
    "lru-memoize": "^1.0.0",
    "map-props": "^1.0.0",
    "moment": "^2.14.1",
    "mongoose": "^4.6.0",
    "mongoose-materialized": "^0.1.8",
    "multer": "^1.2.0",
    "multireducer": "^2.0.0",
    "node-sass": "^3.4.2",
    "nodemailer": "^2.6.0",
    "passport": "^0.3.2",
    "passport-local": "^1.0.0",
    "passport-local-mongoose": "^4.0.0",
    "piping": "^1.0.0-rc.4",
    "pretty-error": "^1.2.0",
    "qrcode.react": "^0.6.1",
    "react": "^0.14.2",
    "react-a11y": "^0.2.6",
    "react-addons-test-utils": "^0.14.0",
    "react-bootstrap": "^0.30.3",
    "react-bootstrap-table": "^2.5.2",
    "react-cookie": "^0.4.8",
    "react-data-grid": "^1.0.40",
    "react-dimensions": "^1.3.0",
    "react-dom": "^0.14.1",
    "react-faux-dom": "^3.0.0",
    "react-flexbox-grid": "^0.10.2",
    "react-helmet": "^2.2.0",
    "react-inline-css": "^2.0.0",
    "react-intl": "^2.1.5",
    "react-loader": "^2.4.0",
    "react-loading-spinner": "^1.0.12",
    "react-location": "^0.2.2",
    "react-redux": "^4.0.0",
    "react-router": "2.0.0",
    "react-router-bootstrap": "^0.20.1",
    "react-router-redux": "^4.0.0",
    "react-sidebar": "^2.2.1",
    "react-slick": "^0.13.3",
    "react-transform-catch-errors": "^1.0.0",
    "react-transform-hmr": "^1.0.1",
    "react-widgets": "^3.4.4",
    "redbox-react": "^1.1.1",
    "redux": "^3.0.4",
    "redux-async-connect": "^1.0.0-rc2",
    "redux-form": "6.0.2",
    "sass-loader": "^3.1.2",
    "scroll-behavior": "^0.3.2",
    "serialize-javascript": "^1.1.2",
    "serve-favicon": "^2.3.0",
    "sinon": "^1.17.2",
    "slick-carousel": "^1.6.0",
    "socket.io": "^1.3.7",
    "socket.io-client": "^1.3.7",
    "strip-loader": "^0.1.2",
    "style-loader": "^0.13.0",
    "superagent": "^1.4.0",
    "timekeeper": "0.0.5",
    "url-loader": "^0.5.7",
    "warning": "^2.1.0",
    "webpack": "^1.13.3",
    "webpack-combine-loaders": "2.0.0",
    "webpack-isomorphic-tools": "2.6.3",
    "whatwg-fetch": "^1.0.0",
    "winston": "^2.2.0"
  },
  "devDependencies": {
    "karma": "^0.13.22",
    "karma-cli": "^0.1.1",
    "karma-coverage": "^1.1.1",
    "karma-mocha": "^0.2.0",
    "karma-mocha-reporter": "^1.1.1",
    "karma-phantomjs-launcher": "^0.2.1",
    "karma-sourcemap-loader": "^0.3.5",
    "karma-webpack": "^1.7.0",
    "mocha": "^2.3.3",
    "redux-devtools": "^3.0.0-beta-3",
    "redux-devtools-dock-monitor": "^1.0.0-beta-3",
    "redux-devtools-log-monitor": "^1.0.0-beta-3",
    "webpack": "^1.12.9",
    "webpack-dev-middleware": "^1.4.0",
    "webpack-hot-middleware": "^2.5.0"
  },
  "engines": {
    "node": "5.6.0"
  }
}
