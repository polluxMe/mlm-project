#!/usr/bin/env bash

aws deploy create-deployment \
  --application-name "MLM-app" \
  --s3-location "bucket=brave-mlm-prod,key=deployments/$1,bundleType=tgz" \
  --deployment-group-name "$2" \
  --deployment-config-name "CodeDeployDefault.OneAtATime" --region "ap-northeast-1"