#!/usr/bin/env bash

cd /home/ubuntu/mlm-app

APP_ENV=$DEPLOYMENT_GROUP_NAME

if [ $DEPLOYMENT_GROUP_NAME == "production" ] || [ $DEPLOYMENT_GROUP_NAME == "prodBlue" ] ; then
    APP_ENV="prod"
fi

# launch application
APP_ENV=$APP_ENV NODE_ENV=$APP_ENV pm2 start ./node_modules/.bin/better-npm-run --name "mlm-app-api" -- start-prod-api
sleep 5
APP_ENV=$APP_ENV NODE_ENV=$APP_ENV pm2 start ./node_modules/.bin/better-npm-run -f --name "mlm-app" -- start-prod
sleep 10