#!/usr/bin/env bash
HASH=`cat /home/ubuntu/mlm-app/.commit-hash`
function notifySlack {
    status=$1
    emoji=$2
    webHookUrl="https://hooks.slack.com/services/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"

    slackChannel="mlm-ci"

    if [ "$status" == "success" ] ; then
        color="#2FA44F"
    else
        color="#D50200"
    fi

    curl -X POST --data-urlencode 'payload={"channel": "#'$slackChannel'", "username": "CodeDeploy MLM-app '$DEPLOYMENT_GROUP_NAME'", "attachments": [ { "color": "'$color'", "text": "<https://eu-west-1.console.aws.amazon.com/codedeploy/home?region=eu-west-1#/deployments/'$DEPLOYMENT_ID'|Deployment> '$status' on the instance. Commit: <https://bitbucket.org/polluxMe/mlm-project/commits/'$HASH'|'${HASH:0:7}'>" } ], "icon_emoji": "'$emoji'"}' $webHookUrl
}

function sendUrlToSlack {
    service=$1
    port=$2
    webHookUrl="https://hooks.slack.com/services/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"
    ec2Url=$(curl http://169.254.169.254/latest/meta-data/public-hostname)
    slackChannel="mlm-ci"
    curl -X POST --data-urlencode 'payload={"channel": "#'$slackChannel'", "username": "CodeDeploy MLM-app '$DEPLOYMENT_GROUP_NAME'", "attachments": [ { "text": "Service ['$service'] launched succesfully. URL: '$ec2Url':'$port'" } ] }' $webHookUrl
}
sleep 30
resultApi=$(curl -s -o /dev/null -w "%{http_code}" http://localhost:3030/ping)
resultWeb=$(curl -s -o /dev/null -w "%{http_code}" http://localhost:80/)

if [[ "$resultApi" == "200" ]]; then
    sendUrlToSlack 'mlm-app-api', '3030'
    if [[ "$resultWeb" == "200" ]]; then
      sendUrlToSlack 'mlm-app', '8080'
      notifySlack 'success' ':yay:'
      exit 0
    else
      notifySlack 'failure' ':interrobang:'
      exit 1
    fi
else
    notifySlack 'failure' ':interrobang:'
    exit 1
fi
