#!/usr/bin/env bash

if [ -d /home/ubuntu/mlm-app/ ]; then

  cd /home/ubuntu/mlm-app
  rm -rf node_modules
  pm2 kill
  # clear caches
  rm -Rf /tmp/cache/

fi
