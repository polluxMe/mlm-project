'use strict';

var program = require('commander');
var config = require('../src/config');
var deasync = require('deasync');
var mongoose = require('mongoose');
var async = require('async');
var mainConfig = require('./modules/admin/models/mainConfig');
var members = require('./modules/member/models/member');
var _ = require('lodash');
var mail = require('./services/mail');
var moment = require('moment');
program
  .version('0.0.1')
;

var connectToMongo = function(next) {
  console.log('Connecting to mongodb...');

  console.log('config.mongodb', config.mongodb);
  mongoose.connection.on('error', function (err) {
    console.log(err);
  });
  mongoose.set('debug', false);

  mongoose.connect(config.mongodb, next);
};

var mainCommission = deasync(function (domainName, next) {
  connectToMongo(function () {
    // write your logic buddy

  });
});

var cashReward = deasync(function (domainName, next) {
  connectToMongo(function () {
    // write your logic buddy
  });
});

var matchingBonus = deasync(function (domainName, next) {
  connectToMongo(function () {
    // write your logic buddy
  });
});

var calculate = deasync(function (domainName, next) {

  connectToMongo(function (err, data) {
    // write your logic buddy
    const Member = mongoose.model('Member', members.schema);
    var MainConfig = mongoose.model('MainConfig', mainConfig.schema);

    function memberCalculate(offset, config, next) {
      Member.find({status: 'official'}, function (err, data) {
        if(err) {
          console.log('calculate err', err);
          return next(err);
        }

        if(!data || data && !data.length) {
          console.log('End calculation!');
          return next();
        }

        // let members = [];
        const currentDate = new Date();
        const currentDay = currentDate.getDate();

        function compute(members, index, next) {
          let member = members[index];

          console.log('member', member);

          if(!member) {
            console.log('members', members);
          }

          async.auto({
            'member': function (next) {
              Member.findById(member._id, next);
            },
            'commission': ['member', function (data, next) {
              // if(currentDay !== config.mainCommissionCycle) {
              //   return next();
              // }
              Member.mainCommission(data.member, next);
            }],
            'matchingBonus': ['member', 'commission', function (data, next) {
              // if(currentDay !== config.cacheRewardsCycle) {
              //   return next();
              // }
              Member.matchingBonus(data.member, next);
            }],
            'cashReward': ['member', 'commission', function (data, next) {
              // if(currentDay !== config.matchingBonusCycle) {
              //   return next();
              // }
              Member.cashReward(data.member, next);
            }]
          }, function (err, data) {
            if(err) {
              return next(err);
            }
            console.log('data.member', data.member.email);

            if(index+1 !== members.length) {
              index++;
              compute(members, index, next)
            } else {
              console.log('offset', offset);
              let nextOffset = offset + 10;
              memberCalculate(nextOffset, config, next);
              // next(null, true);
            }
          });
        }
        compute(data, 0, next);

        // Promise.all(members)
        //   .then(result => {
        //     if(!result.length) {
        //       return next();
        //     }
        //     console.log('offset', offset);
        //     let nextOffset = offset + 10;
        //     memberCalculate(nextOffset, config, next);
        //   })
        //   .catch(err => {
        //     console.log('errr cal', err);
        //     next(err);
        //   });
      }).limit(10).skip(offset);
    }

    async.auto({
      'config': function (next) {
        MainConfig.findOne({}, function (err, data) {
          if(err) {
            return next(err);
          }

          const cycleDates = ['mainCommissionCycle', 'cacheRewardsCycle', 'matchingBonusCycle'];
          cycleDates.forEach(param => {
            let day = data[param];
            if(day !== 0) {
              return;
            }
            let endDayOfMonth = moment().endOf('month').date();
            data[param] = endDayOfMonth;
          });
          next(null, data);
        });
      },
      'calculations': ['config', function (data, next) {
        let cycleDates = [data.config.mainCommissionCycle, data.config.cacheRewardsCycle, data.config.matchingBonusCycle];
        console.log('cycleDates', cycleDates);
        let currentDate = new Date();
        // if(cycleDates.indexOf(currentDate.getDate()) === -1) {
        //   console.log('No one day match. Maybe tomorrow...');
        //   return next();
        // }

        memberCalculate(0, data.config, next);
      }]
    }, next);
  });
});

var notifyImportedMembers = deasync(function (next) {
  const Member = mongoose.model('Member', members.schema);

  const app = {config: config};
  _.partial(mail.init, app);

  async.series([
    _.partial(async.parallel, [
      _.partial(mail.init, app)
    ]),

  ], function (err) {
    if (err) { console.log(err); }
  });

  connectToMongo(function () {
    Member.find({}, (err, data) => {
      if (err) {
        return next(err);
      }

      let i = 0;

      var timerId = setInterval(() => {
        if  (i+1 === data.length) {
          clearInterval(timerId);
          return next();
        }

        const member = data[i];
        // let mailSend = member.email;
        let mailSend = ['yuriy.kosakivsky@eliftech.com',
          'huy@topsquare.vn',
          'huync01@gmail.com',
          'huy_nguyencong@yahoo.com',
          'huy@newwave.vn',
          'huy@infobanca.jp',
          'huy@mikadogames.net'];

        if (member) {
          const siteUrl = config.siteUrl;
          const qr = config.qrUrl;
          mail.sendTemplate('invitation', mailSend, {id: member.username, password: member.tempPassword, name: member.applicantName, siteUrl: siteUrl, qr: qr}, (err) => {
            if (err) {
              console.log(err);
              return next(err);
            }
            console.log('member mail', member.email);
            next();
            //member.isImported = false;
            // member.save((err) => {
            //   if (err) return next(err);
            // })
          });

          i++;
        }
      }, 5000);
    });
  });
});

program
  .command('commission')
  .description('Calculate main commission')
  .action(function (domainName, opts) {
    mainCommission(domainName);
  });

program
  .command('cashReward')
  .description('Calculate cash reward')
  .action(function (domainName, opts) {
    cashReward(domainName);
  });


program
  .command('matchingBonus')
  .description('Calculate matching bonus')
  .action(function (domainName, opts) {
    matchingBonus(domainName);
  });

program
  .command('calculate')
  .description('Calculate all')
  .action(function (domainName, opts) {
    calculate(domainName);
  });

program
  .command('notify-imported-members')
  .description('Send all imported members email notification')
  .action(function () {

    notifyImportedMembers();
  });

program
  .command('*')
  .action(function(commandName){
    console.log('Invalid command', commandName);
  });

program.parse(process.argv);

if (!program.args.length) {
  program.help();
}

