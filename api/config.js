if (!global._babelPolyfill) {
  require('babel-polyfill');
}

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  },
  prod: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT || 80,
  apiHost: process.env.APIHOST || 'mypage.brave.works',
  apiPort: process.env.APIPORT || 3030,
  app: {
    title: 'Brave',
    description: 'Brave Works',
    head: {
      titleTemplate: 'Brave: %s',
      meta: [
        {name: 'description', content: 'Brave'},
        {charset: 'utf-8'},
        {property: 'og:site_name', content: 'Brave'},
        {property: 'og:image', content: ''},
        {property: 'og:locale', content: 'ja_JA'},
        {property: 'og:title', content: 'Brave'},
        {property: 'og:description', content: 'Brave'},
        {property: 'og:card', content: 'summary'},
        {property: 'og:site', content: '@eliftech'},
        {property: 'og:creator', content: '@eliftech'},
        {property: 'og:image:width', content: '200'},
        {property: 'og:image:height', content: '200'}
      ]
    }
  },
  mongodb: 'mongodb://ip-172-31-26-91.ap-northeast-1.compute.internal:27017/mlm',
  'mail': {
    'SESUser': 'ses-smtp-user.20161020-174852',
    'noReply': 'BRAVE <no-reply@brave.works>',
    'transport': {
      'service': 'brave.works',
      'host': 'email-smtp.eu-west-1.amazonaws.com',
      'port': 465,
      'secure': true,
      'auth': {
        'user': 'AKIAJHKAW3OO7S7GMEDQ',
        'pass': 'AtJSM3w3IwkGUknd75m0KYoCJJltroePV2bSq0Ei9dI8'
      },
    },
    'imagesPath': 'http://mypage.brave.works/static/mail'
  },
  'siteUrl': 'http://mypage.brave.works',
  'qrUrl': 'http://mypage.brave.works/img/qr.png'
}, environment);
