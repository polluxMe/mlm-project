#!/bin/bash

echo "Start dumping"
DATE=$(date +"%Y%m%d")
echo "Creating dump fo date - "$DATE
PATH_DIR=/home/ubuntu/dumps
# clear old
i=0
max_dumps=30
num_to_remove=0
for d in $PATH_DIR/*/ ; do
   i=$((i=i+1))
   if [ $i -gt $max_dumps ]; then
    num_to_remove=$((num_to_remove=num_to_remove+1))
   fi
done

index_removed=0
if [ $num_to_remove -gt 0 ]; then
for d in $PATH_DIR/*/ ; do
   if [ $index_removed -lt $num_to_remove ]; then
     echo "Removing deprecated folders: $d"
     rm -rf $d
   fi
   index_removed=$((index_removed=index_removed+1))
done
fi
# create dump
PATH_OUT=$PATH_DIR/dump_$DATE
mongodump -h localhost -p 27017 -d mlm -o $PATH_OUT
echo "Stop dumping"