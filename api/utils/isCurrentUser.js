export default function (req, res, user, next) {
  const memberID = user._id && user._id.toString();

  if (!req.user || (!req.user.isAdmin && (!memberID || memberID !== req.user._id.toString()))) {
    return res.status(403).send('You cannot change information about another user');
  }

  next();
}