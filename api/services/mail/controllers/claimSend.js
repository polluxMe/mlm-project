'use strict';

var async = require('async');

exports.controller = function (app, sendOpts, model, next) {
  sendOpts.subject = 'Release claim';


  async.auto({
    claim: function (next) {
      app.models.claims.findById(model._id, next);
    }
  }, function (err, data) {
    if (err) { return next(err); }
    model.claim = data.claim;
    model.text = data.claim.isRelease ? 'release claim' : 'takedown';
    next(null, model);
  });
};
