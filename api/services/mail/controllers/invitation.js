'use strict';

exports.controller = function (app, sendOpts, model, next) {
  console.log('sendOpts', sendOpts);
  sendOpts.subject = '- BRAVE - 登録完了のお知らせ';
  next(null, model);
};
