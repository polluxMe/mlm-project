'use strict';

exports.controller = function (app, sendOpts, model, next) {
  sendOpts.subject = 'New asset uploaded';
  next(null, model);
};
