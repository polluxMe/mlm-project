if (!global._babelPolyfill) {
  require('babel-polyfill');
}

import express from 'express';
import session from 'express-session';
import * as health from 'express-ping';
import bodyParser from 'body-parser';
// import config from '../src/config';
import * as actions from './actions/index';
// import {mapUrl} from 'utils/url.js';
import PrettyError from 'pretty-error';
import http from 'http';
// import SocketIo from 'socket.io';
import async from 'async';
import cors from 'cors';
import passport from 'passport';
import LocalStrategy from 'passport-local';
import cookieParser from 'cookie-parser';

import _ from 'lodash';
import mongoose from 'mongoose';
import logger from './logger';
import json2xls from 'json2xls';

import config from './config';
const Strategy = LocalStrategy.Strategy;
const pretty = new PrettyError();
const app = express();
app.config = config;
app.logger = logger;

const Member = require('./modules/member/models/member');
const server = new http.Server(app);

// const io = new SocketIo(server);
// io.path('/ws');

app.use(function(req, res, next) {
  var hostname = req.hostname.replace('http://', '');
  // res.header("Access-Control-Allow-Origin", "*");
  // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  var allowedOrigins = ['http://'+hostname+':3000',
    'http://'+hostname+':8080',
    'http://'+hostname+':80',
    'http://'+hostname+':3030',
    'http://'+hostname,
    'http://brave.works',
    'http://brave.works/',
    'http://'+hostname+'/',
    'https://brave.works',
    'https://brave.works/',
    'http://mypage.brave.works',
    'https://mypage.brave.works',
    'http://dev.brave.works',
    'http://dev.brave.works:8080',
    'http://admin.brave.works',
    'https://admin.brave.works',
  ];

  var origin = req.headers.origin;
  // if(!origin) return next('Origin empty');

  if(allowedOrigins.indexOf(origin) > -1){
    res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, X-AUTHENTICATION, X-IP, Content-Type, Accept');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  next();
});

// var corsOptionsDelegate = function(req, callback){
//   var corsOptions = { credentials: true };
//   corsOptions.exposedHeaders = ['x-total-count'];
//   callback(null, corsOptions);
// };
// app.use(cors(corsOptionsDelegate));

app.use(express.static('../static'));

// passport.use(new Strategy(Member.authenticate()));

// passport.use('local-login', new Strategy({
//     // by default, local strategy uses username and password, we will override with email
//     usernameField : 'username',
//     passwordField : 'password',
//     passReqToCallback : true // allows us to pass back the entire request to the callback
//   },
//   function(req, username, password, done) { // callback with email and password from our form
//     console.log('username', username, password);
//     // find a user whose email is the same as the forms email
//     // we are checking to see if the user trying to login already exists
//     Member.findOne({ 'username' :  username }, function(err, user) {
//       // if there are any errors, return the error before anything else
//       if (err)
//         return done(err);
//
//       // if no user is found, return the message
//       if (!user)
//         return done('No user found.', false); // req.flash is the way to set flashdata using connect-flash
//
//       // if the user is found but the password is wrong
//       if (!user.validPassword(password))
//         return done('Oops! Wrong password.', false); // create the loginMessage and save it to session as flashdata
//
//       // all is well, return successful user
//       return done(null, user);
//     });
//
//   }));


passport.use('local-signup', new Strategy({
  usernameField: 'username',
  passwordField: 'password',
  passReqToCallback: true
}, Member.signup.bind(Member)));

passport.use('local-login', new Strategy({
  usernameField: 'username',
  passwordField: 'password',
  passReqToCallback: true
}, Member.login.bind(Member)));

// passport.serializeUser(Member.serializeUser());
// passport.deserializeUser(Member.deserializeUser());

passport.serializeUser(function(user, cb) {
  console.log('user ser', user);
  cb(null, user._id);
});

passport.deserializeUser(function(id, cb) {
  console.log('id', id);
  Member.findById(id, function (err, user) {
    if (err) { return cb(err); }
    cb(null, user);
  });
});



var sessionOpts = {
  saveUninitialized: true,
  resave: true,
  //store: new MongoStoreInc({ mongooseConnection: mongoose.connection }),
  secret: 'nyan cat',
  cookie : { maxAge : 2419200000},
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser('nyan cat'));
app.use(session(sessionOpts));

app.use(passport.initialize());
app.use(passport.session());
app.use(json2xls.middleware);

app.options('*', function(req, res) {
  res.status(200).end();
});

// START INIT MODULES
app.models = {};
import UsersModule from './modules/member/index';
import AdminModule from './modules/admin/index';
app.modules = {
  users: new UsersModule(app),
  admin: new AdminModule(app),
  each: function(callFunc) {
    _.forEach(app.modules, function(obj, name) {
      if (typeof obj == 'object') {
        callFunc(obj);
      }
    });
  }
};
app.modules.each(function(moduleObj) {
  moduleObj.initModels();
});
app.modules.each(function(moduleObj) {
  moduleObj.initRoutes();
});


app.services = {
  mail: require('./services/mail'),
};

// END

app.use(health.ping());


var startDate = Date.now();
app.logger.info('Starting', config.apiHost, 'configuration ...');

async.series([
  _.partial(async.parallel, [
    _.partial(app.services.mail.init, app)
  ]),
  function (next) {
    app.logger.info('Connecting to mongodb...');
    mongoose.connect(config.mongodb, next);

    mongoose.connection.on('error', function (err) {
      app.logger.error(err);
    });
    mongoose.set('debug', false);
  },
  function (next) {
    app.logger.info('Connected to mongodb successfully');
    next();
  },
  function (next) {
    app.logger.info('Looking for admin user...');
    Member.findOne({isAdmin: true, username: '__mast3r__'}, (err, data) => {
      if (err) next(err);

      if (!data) {
        const member = {
          status: 'official',
          applicantID: '__mast3r__',
          applicantName: '__master__',
          username: '__mast3r__',
          email: 'yuriy.kosakivsky@eliftech.com',
          earnings: 0,
          commision: 0,
          purchaseType: 'cash',
          isChangeId: true,
          isAdmin: true
        };

        const tempPwd = '__Q3zvr8__';

        const admin = new Member(member);

        admin.password = admin.generateHash(tempPwd);
        admin.save((err, data) => {
          if(err) next(err);
          if (data) {
            app.logger.info('Created admin user');
          }
          next();
        });
      }
      else {
        app.logger.info('Admin user is already exist');
        next();
      }
    });
  },
  // function (next) {
  //   require('./migrations').migrateToActual(app, next);
  // },
  function (next) {
    app.logger.info('Http server starting at', config.apiPort, '...');
    next();
  },
  function (next) {
    // app.modules.each(function(moduleObj) {
    //   if (moduleObj.initServer) {
    //     moduleObj.initServer();
    //   }
    // });
    next();
  }

], function (err) {
  if (err) { app.logger.error(err); }
  app.logger.info('Configuration successfully loaded in', Date.now() - startDate, 'ms');
});


const bufferSize = 100;
const messageBuffer = new Array(bufferSize);
let messageIndex = 0;

if (config.apiPort) {
  const runnable = app.listen(config.apiPort, (err) => {
    if (err) {
      app.logger.error(err);
    }
    app.logger.info('----\n==> 🌎  API is running on port %s', config.apiPort);
    app.logger.info('==> 💻  Send requests to http://%s:%s', config.apiHost, config.apiPort);
  });

} else {
  app.logger.error('==>     ERROR: No PORT environment variable has been specified');
}
