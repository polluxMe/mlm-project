export default function (req, res, next) {
  if (!req.user || !req.user.isAdmin) {
    return res.status(403).send('You do not have enough rights');
  }

  next();
}