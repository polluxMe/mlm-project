// ROUTES
import routesVSConfig from './routes/vsConfig.js';
import routesVSFrameConfig from './routes/vsFrameConfig.js';
import routesMainConfig from './routes/mainConfig.js';
import withdrawalConfig from './routes/withdrawalConfig.js';

// MODELS
import * as vsConfigModel from './models/vsConfig.js';
import * as vsFrameConfigModel from './models/vsFrameConfig.js';
import * as mainConfigModel from './models/mainConfig.js';
import * as withdrawalConfigModel from './models/withdrawalConfig.js';

function AdminModule(app) {
  this.app = app;

  this.initModels = function () {
    this.app.models.vsConfig = vsConfigModel;
    this.app.models.vsFrameConfig = vsFrameConfigModel;
    this.app.models.mainConfig = mainConfigModel;
    this.app.models.withdrawalConfig = withdrawalConfigModel;
  };

  this.initRoutes = function () {
    this.app.use('/api/vs_config', routesVSConfig);
    this.app.use('/api/vs_frame_config', routesVSFrameConfig);
    this.app.use('/api/main_config', routesMainConfig);
    this.app.use('/api/withdrawal_config', withdrawalConfig);
  };
}

export default AdminModule;