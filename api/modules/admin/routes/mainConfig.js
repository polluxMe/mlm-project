import express from 'express';
import mongoose from 'mongoose';
import async from 'async';
import requireLogin from '../../../middlewares/requireLogin';
import requireAdmin from '../../../middlewares/requireAdmin';

var routesMainConfig = express.Router();

routesMainConfig
  .get(requireLogin)
  .post(requireLogin)
  .put(requireLogin)
  .delete(requireLogin);

routesMainConfig.get('/', function (req, res, next) {
  var Model = mongoose.model('MainConfig', req.app.models.mainConfig.schema);

  async.auto({
    'item': function (next) {
      Model.find({}, next);
    },
    'newItem': ['item', function (data, next) {
      if (!data.item[0]) {

        var config = new Model();
        config.save(next)
      }
      else next(null);
    }]
  }, function (err, data) {
    if(err) {
      return res.status(404).send(err);
    }
    res.json(data.item[0] || data.newItem[0]);
  });

});

routesMainConfig
  .put('/', requireAdmin)
  .put('/', function (req, res, next) {
    var Model = mongoose.model('MainConfig', req.app.models.mainConfig.schema);

    async.auto({
      'item': function (next) {
        Model.findOne({}, next);
      },
      'newItem': ['item', function (data, next) {
        if (!data.item) {
          var config = new Model();
          config.save(next)
        }
        else next(null);
      }],
      'updated': ['newItem', function (data, next) {
        Model.update({},req.body, next);
      }]
    }, function (err, data) {
      if(err) {
        return res.status(500).send(err);
      }
      return res.json(data.updated);
    });
  });

export default routesMainConfig;
