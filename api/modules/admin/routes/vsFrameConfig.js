import express from 'express';
import mongoose from 'mongoose';
import async from 'async';
import _ from 'lodash';
import moment from 'moment';
import requireLogin from '../../../middlewares/requireLogin';
import requireAdmin from '../../../middlewares/requireAdmin';

var routesVSFrameConfig = express.Router();

routesVSFrameConfig
  .get(requireLogin)
  .post(requireLogin)
  .put(requireLogin)
  .delete(requireLogin);

routesVSFrameConfig
  .get(requireAdmin)
  .post(requireAdmin)
  .put(requireAdmin)
  .delete(requireAdmin);

routesVSFrameConfig.get('/', function (req, res, next) {
  var Model = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);
  // var limit = Number(req.query.limit),
    // page = req.query.page,
    // skip = (((page-1) * limit)),
    // search = req.query.search,
    // sort = req.query.sort;
    // filter = req.query.filter;

  let queryParams = {};
  // if(search) {
  //   queryParams = {$text: {$search: search}};
  // }
  // if(filter && filter !== '{}') {
  //   const filterObj = JSON.parse(filter);
  //   _.each(filterObj, (value, index) => {
  //     queryParams[index] = new RegExp(value);
  //   });
  // }
  async.auto({
    'count': function (next) {
      Model.count(queryParams, next);
    },
    'pureItems': ['count', function (data, next) {
      if (data.count === -1) {
        return next(null, []);
      }
      // if(search) {
      //   skip = 0;
      // }
      // if(sort) {
      //   let sortOptions = JSON.parse(sort);
      //   sort = {};
      //   sort[sortOptions.name] = sortOptions.order;
      // }
      Model.find(queryParams, next).lean(true).sort({level: 1});/*.skip(skip).limit(limit).sort(sort)*/;
    }],
    'items': ['pureItems', (data, next) => {
      let items = data.pureItems;
      if (!items) {
        return next(null, []);
      }

      _.each(items, (value, key) => {
        value.hasPrevLevel = items[key - 1] && items[key-1].status === 'created';
        value.hasNextLevel = items[key + 1] && items[key+1].status === 'created';

        value.prevItemId = items[key - 1] && items[key - 1]._id;
        value.nextItemId = items[key + 1] && items[key + 1]._id;
      });

      next(null, items);
    }]
  }, function (err, data) {
    if(err) {
      return res.status(404).send(err);
    }

    res.json(data);
  });

});

routesVSFrameConfig.get('/:id/frame', function (req, res, next) {
  var Model = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);
  async.auto({
    item: function (next) {
      Model.findOne({_id: req.params.id}, next);
    },
    'pack': ['item', function (data, next) {
      if(!data.item) {
        return next('Not found item with ID:'+req.params.id);
      }
      let dataItem = data.item;
      // dataItem.purchaseAmmount = data.item.frames[0].purchaseFrame;
      dataItem.startDate = data.item.frames[0].startDate;
      dataItem.endDate = data.item.frames[0].endDate;
      next(null, dataItem);
    }],
  }, function (err, data) {
    if(err) {
      return next(err);
    }
    res.json(data.pack);
  });
});

routesVSFrameConfig.get('/:id/frameItem', function (req, res, next) {
  var Model = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);
  async.auto({
    item: function (next) {
      Model.findOne({'frames._id': req.params.id}, next);
    },
    frame: ['item', function (data, next) {
      let frame = data.item.frames.filter(function (item) {
        return item._id.toString() == req.params.id;
      });
      next(null, (frame ? frame[0] : null));
    }]
  }, function (err, data) {
    if(err) {
      return next(err);
    }
    res.json(data.frame);
  });
});

routesVSFrameConfig.post('/:id/frameEnd', function (req, res, next) {
  var Model = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);
  async.auto({
    item: function (next) {
      Model.findOne({'frames._id': req.params.id}, next);
    },
    nextItem: function (next) {
      Model.findOne({'status': 'created'}, next).sort({'level': -1});
    },
    frame: ['item', 'nextItem', function (data, next) {
      let frameIndex = _.findIndex(data.item.frames, function (item) {
        return item._id.toString() == req.params.id;
      });
      if(frameIndex === -1) {
        return next('The frame with ID:' + req.params.id + 'didn\'t found.');
      }
      data.item.frames[frameIndex].status = 'finished';
      data.item.frames[frameIndex].remainQuantity = 0;
      data.item.frames[frameIndex].purchaseFrame = data.item.frames[frameIndex].purchaseQuantity;
      data.item.frames[frameIndex].endDate = Date.now();
      let nextFrameIndex = frameIndex + 1;
      if(!data.item.frames[nextFrameIndex]) {
       if(!data.nextItem) {
         return next('次の枠が設定されていない為、終了できません。');
       }
       data.item.status = 'finished';
       data.item.save(function (err) {
         if(err) {
           return next(err);
         }
         data.nextItem.status = 'active';
         data.nextItem.save(next);
       });
      } else {
        data.item.frames[nextFrameIndex].status = 'selling';
        data.item.frames[nextFrameIndex].startDate = Date.now();
        data.item.save(next);
      }
    }]
  }, function (err, data) {
    if(err) {
      return res.status(500).send(err.message || err);
    }
    res.json(data.frame);
  });
});

routesVSFrameConfig.get('/frame', function (req, res, next) {
  var Model = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);
  let currentDate = moment().toDate();
  async.auto({
    'current': function(next) {
      Model.findOne({startDate: {
        $lte: currentDate
      },
      endDate: {
        $gt: currentDate
      }}, next);
    },
    'previous': ['current', function (data, next) {
      if(data.current) {
        return next();
      }
      Model.findOne({endDate: {$lt: currentDate}}, next);
    }]
  }, function (err, data) {
    if(err) {
      return next('err', err);
    }
    let rate = data.current ? data.current.purchaseRate : data.previous.purchaseRate || 0;
    res.json({rate: rate});
  });
});

routesVSFrameConfig.post('/', function (req, res, next) {
  var Model = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);
  async.auto({
    'lastInOrder': function (next) {
      Model.findOne({status: {$ne: 'finished'}}, next).sort({level: -1});
    },
    'purchase': ['lastInOrder', (data, next) => {
      // if(data.record) {
      //   return next({errors: true, message: '記録は日付に既に存在しています '+moment(data.record.startDate).format('DD-MM-YYYY')+" - "+moment(data.record.endDate).format('DD-MM-YYYY')});
      // }
      let level = 0;
      if(data.lastInOrder) {
        level = data.lastInOrder.level + 1;
      }

      let params = req.body;

      params.frames = [];
      params.frameAmounts.forEach((amount, index) => {
        params.frames.push({
          frameID: mongoose.Types.ObjectId(),
          purchaseRate: index + +params.startRate,
          purchaseFrame: amount,
          purchaseQuantity: 0,
          remainQuantity: amount,
        })
      });

      params.level = level;
      params.status = (level === 0) ? 'active' : 'created';
      var purchase = new Model(params);
      var error = purchase.validateSync();
      if(error) {
        return next(error);
      }

      Model.create(purchase, next);
    }]
  }, function (err, data) {
    if(err) {
      return res.json(err);
    }

    res.json(data.purchase);
  });

});

routesVSFrameConfig.put('/:id/change_order', function (req, res, next) {
  var Model = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);

  async.auto({
    'record1': function (next) {
      Model.findById(req.params.id, next);
    },
    'record2': function (next) {
      Model.findById(req.body.id, next);
    },
    'updateRecord2': ['record1', 'record2', function (data, next) {
      const record1 = data.record1;
      const record2 = data.record2;
      if(!record1) {
        return next('このIDを持つ記録が見つかりませんでした：' + req.params.id);
      }

      if(!record2) {
        return next('このIDを持つ記録が見つかりませんでした：' + req.body.id);
      }

      if (record1.status !== 'created' || record2.status !== 'created') {
        return next('タッチしたフレームの順序を変更することができません');
      }

      const record1Level = record1.level;
      const record2Level = record2.level;

      record1.level = record2Level;
      record2.level = record1Level;

      record1.save((err, doc) => {
        if (err) next(err);
        record2.save(next);
      });
    }]
  }, function (err, data) {
    if(err) {
      return res.status(500).send(err);
    }

    res.json(data.updateRecord2);
  });
});

routesVSFrameConfig.put('/', function (req, res, next) {
  var Model = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);

  async.auto({
    'record': function (next) {
      Model.findOne({_id: req.body._id}, next);
    },
    'update': ['record', function (data, next) {
      if(!data.record) {
        return next('このIDを持つ記録が見つかりませんでした：' + req.body._id);
      }

      const record = data.record;
      const params = req.body;

      if(!params.frameAmounts || !params.frameAmounts.length) {
        return next('全てのフレームを削除することはできません');
      }
      if (record.frames.length > params.frameAmounts.length) {
        if (record.frames[params.frameAmounts.length].status !== 'created') {
          return next('作成したフレームしか削除できません');
        }
        record.frames = record.frames.slice(0, params.frameAmounts.length);
      }

      params.frameAmounts.forEach((amount, index) => {
        if (record.frames[index] && record.frames[index].status !== 'created') return;
        if (record.frames[index]) {
          record.frames[index].purchaseFrame = amount;
          record.frames[index].remainQuantity = amount;
        } else {
          record.frames.push({
            frameID: mongoose.Types.ObjectId(),
            purchaseRate: index + +params.startRate,
            purchaseFrame: amount,
            purchaseQuantity: 0,
            remainQuantity: amount,
          })
        }
      });

      record.endRate = record.startRate + record.frames.length - 1;

      data.record.save(next);
    }]
  }, function (err, data) {
    if(err) {
      return res.status(500).send(err);
    }

    return res.json(data.update);
  });
});

routesVSFrameConfig.put('/:_id/update', function (req, res, next) {
  var Model = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);
  async.auto({
    item: function (next) {
      Model.findOne({'frames._id': req.params._id}, next);
    },
    frame: ['item', function (data, next) {
      let frameIndex = _.findIndex(data.item.frames, function (item) {
        return item._id.toString() == req.params._id;
      });
      if (frameIndex === -1) {
        return next('The frame with ID:' + req.params._id + 'didn\'t found.');
      }

      data.item.frames[frameIndex].purchaseFrame = req.body.frameAmount;
      data.item.frames[frameIndex].purchaseQuantity = req.body.framePurchased;
      data.item.frames[frameIndex].remainQuantity = req.body.frameRemain;

      data.item.save(next);
    }]
  }, function (err, data) {
    if(err) {
      return next(err);
    }

    res.json(data.frame);
  });
});

routesVSFrameConfig.get('/frames', function (req, res, next) {
  var frames = {from: [], to: []}, start = 15, startEnd = 35, endFrame = 50;
  for(var i=0; i<20; i++) {
    frames.from.push(start);
    if(startEnd <= endFrame) {
      frames.to.push(startEnd);
    }
    start++;
    startEnd++;
  }
  return res.json(frames);
});

routesVSFrameConfig.get('/activeRate', function (req, res, next) {
  var Model = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);
  Model.findActiveFrame(function (err, data) {
    if(err) {
      return next(err);
    }
    res.json({rate: data.purchaseRate / 100});
  });
});

routesVSFrameConfig.delete('/:_id', function (req, res, next) {
  var Model = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);
  async.auto({
    'delete': function (next) {
      Model.remove({_id: req.params._id}, next);
    },
    'list': ['delete', function (data, next) {
      Model.find({}, next);
    }]
  },
  function (err, data) {
    if(err) {
      return next(err);
    }
    res.json({items: data.list});
  });
});

routesVSFrameConfig.delete('/:_id/lastFrame', function (req, res, next) {
  var Model = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);
  async.auto({
      'record': function (next) {
        Model.findOne({_id: req.params._id}, next);
      },
      'update': ['record', function (data, next) {
        const record = data.record;
        if (record.frames.length === 1) return ('最後のフレームが削除できません');
        if (_.last(record.frames).status !== 'created') return next('作成したフレームしか削除できません');
        record.frames.pop();
        record.endRate--;
        record.save(next);
      }]
    },
    function (err, data) {
      if(err) {
        return next(err);
      }
      res.status(200).end();
    });
});

export default routesVSFrameConfig;
