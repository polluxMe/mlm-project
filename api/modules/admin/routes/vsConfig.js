import express from 'express';
import mongoose from 'mongoose';
import async from 'async';
import _ from 'lodash';
import moment from 'moment';
import requireLogin from '../../../middlewares/requireLogin';
import requireAdmin from '../../../middlewares/requireAdmin';

var routesVSConfig = express.Router();

routesVSConfig
  .get(requireLogin)
  .post(requireLogin)
  .put(requireLogin)
  .delete(requireLogin);

routesVSConfig
  .get('/', requireAdmin)
  .get('/', function (req, res, next) {
    var Model = mongoose.model('VsConfig', req.app.models.vsConfig.schema);
    console.log('params', req.body, req.params, req.query);
    var limit = Number(req.query.limit),
      page = req.query.page,
      skip = (((page-1) * limit)),
      search = req.query.search,
      sort = req.query.sort,
      filter = req.query.filter;

    let queryParams = {};
    if(search) {
      queryParams = {$text: {$search: search}};
    }
    if(filter && filter !== '{}') {
      const filterObj = JSON.parse(filter);
      _.each(filterObj, (value, index) => {
        queryParams[index] = new RegExp(value);
      });
    }
    async.auto({
      'count': function (next) {
        Model.count(queryParams, next);
      },
      'items': ['count', function (data, next) {
        if (data.count === -1) {
          return next(null, []);
        }
        if(search) {
          skip = 0;
        }
        if(sort) {
          let sortOptions = JSON.parse(sort);
          sort = {};
          sort[sortOptions.name] = sortOptions.order;
        }
        Model.find(queryParams, next).skip(skip).limit(limit).sort(sort);
      }]
    }, function (err, data) {
      if(err) {
        return res.status(404).send(err);
      }

      if (data.count !== -1) {
        res.set('x-total-count', data.count);
      }
      res.json(data);
    });

  });

routesVSConfig.get('/rate', function (req, res, next) {
  var Model = mongoose.model('VsConfig', req.app.models.vsConfig.schema);
  async.auto({
    'rate': function(next) {
      Model.getRate(next);
    }
  }, function (err, data) {
    if(err) {
      return next('err', err);
    }
    const rate = data.rate.rate;
    res.json({rate: rate});
  });
});

routesVSConfig
  .post('/', requireAdmin)
  .post('/', function (req, res, next) {
    var Model = mongoose.model('VsConfig', req.app.models.vsConfig.schema);
    let startDate = moment(req.body.startDate);
    let endDate = moment(req.body.endDate);
    async.auto({
      'collision': (next) => {
        Model.findOne({
        $or: [
          {
            startDate: {
              $lt: startDate.toDate()
            },
            endDate: {
              $gt: endDate.toDate(),
            }
          },
          {
            startDate: {
              $gte: startDate.toDate(),
              $lt: endDate.toDate()
            }
          },
          {
            endDate: {
              $lte: endDate.toDate(),
              $gt: startDate.toDate(),
            }
          }
        ]
        }, next);
      },
      'config': ['collision', (data, next) => {
        if(data.collision) {
          return next({errors: true, message: '記録は日付に既に存在しています '+moment(data.collision.startDate).format('DD-MM-YYYY')+" - "+moment(data.collision.endDate).format('DD-MM-YYYY')});
        }

        let params = req.body;
        params.startDate = startDate;
        params.endDate = endDate;
        var config = new Model(params);
        var error = config.validateSync();
        if(error) {
          return next(error);
        }

        config.save(next);
      }]
    }, function (err, data) {
      if(err) {
        return res.json(err);
      }

      res.json(data.config);
    });

  });

routesVSConfig.put('/', function (req, res, next) {
  var Model = mongoose.model('VsConfig', req.app.models.vsConfig.schema);
  var id = req.body._id;
  delete req.body._id;
  let startDate = moment(req.body.startDate);
  let endDate = moment(req.body.endDate);
  async.auto({
    'collision': (next) => {
      Model.findOne({
        _id: {
          $ne: id
        },
        $or: [
          {
            startDate: {
              $lt: startDate.toDate()
            },
            endDate: {
              $gt: endDate.toDate(),
            }
          },
          {
            startDate: {
              $gte: startDate.toDate(),
              $lt: endDate.toDate()
            }
          },
          {
            endDate: {
              $lte: endDate.toDate(),
              $gt: startDate.toDate(),
            }
          }
        ]
      }, next);
    },
    'config': ['collision', (data, next) => {
      if(data.collision) {
        return next({errors: true, message: '記録は日付に既に存在しています '+moment(data.collision.startDate).format('DD-MM-YYYY')+" - "+moment(data.collision.endDate).format('DD-MM-YYYY')});
      }

      let params = req.body;
      params.startDate = startDate;
      params.endDate = endDate;
      var config = new Model(params);
      var error = config.validateSync();
      if(error) {
        return next(error);
      }
      Model.update({_id: id}, req.body, next);
    }]
    }, function (err, data) {
      if(err) {
        return res.json(err);
      }

      res.json(data.config);
    });
});

routesVSConfig.delete('/:_id', function (req, res, next) {
  var Model = mongoose.model('VsConfig', req.app.models.vsConfig.schema);
  async.auto({
      'delete': function (next) {
        Model.remove({_id: req.params._id}, next);
      },
      'list': ['delete', function (data, next) {
        Model.find({}, next);
      }]
    },
    function (err, data) {
      if(err) {
        return next(err);
      }
      res.json({items: data.list});
    });
});

routesVSConfig.get('/:id', function (req, res, next) {
  var Model = mongoose.model('VsConfig', req.app.models.vsConfig.schema);
  Model.findOne({_id: req.params.id},  function (err, data) {
    if(err) {
      return next(err);
    }
    res.json(data);
  });
});

export default routesVSConfig;
