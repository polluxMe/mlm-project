import express from 'express';
import mongoose from 'mongoose';
import async from 'async';
import _ from 'lodash';
import moment from 'moment';
import requireLogin from '../../../middlewares/requireLogin';
import requireAdmin from '../../../middlewares/requireAdmin';

var routesWithdrawalConfig = express.Router();

routesWithdrawalConfig
  .get(requireLogin)
  .post(requireLogin)
  .put(requireLogin)
  .delete(requireLogin);

routesWithdrawalConfig
  .get(requireAdmin)
  .post(requireAdmin)
  .put(requireAdmin)
  .delete(requireAdmin);

routesWithdrawalConfig.get('/', function (req, res, next) {
  var Model = mongoose.model('WithdrawalConfig', req.app.models.withdrawalConfig.schema);
  var limit = Number(req.query.limit),
    page = req.query.page,
    skip = (((page-1) * limit)),
    search = req.query.search,
    sort = req.query.sort,
    filter = req.query.filter;

  let queryParams = {};
  if(search) {
    queryParams = {$text: {$search: search}};
  }
  if(filter && filter !== '{}') {
    const filterObj = JSON.parse(filter);
    _.each(filterObj, (value, index) => {
      queryParams[index] = new RegExp(value);
    });
  }
  async.auto({
    'count': function (next) {
      Model.count(queryParams, next);
    },
    'items': ['count', function (data, next) {
      if (data.count === -1) {
        return next(null, []);
      }
      if(search) {
        skip = 0;
      }
      if(sort) {
        let sortOptions = JSON.parse(sort);
        sort = {};
        sort[sortOptions.name] = sortOptions.order;
      }
      Model.find(queryParams, next).skip(skip).limit(limit).sort(sort);
    }]
  }, function (err, data) {
    if(err) {
      return res.status(404).send(err);
    }

    if (data.count !== -1) {
      res.set('x-total-count', data.count);
    }
    res.json(data);
  });

});

routesWithdrawalConfig.get('/rate', function (req, res, next) {
  var Model = mongoose.model('WithdrawalConfig', req.app.models.withdrawalConfig.schema);
  let currentDate = moment().toDate();
  async.auto({
    'current': function(next) {
      Model.findOne({startDate: {
        $lte: currentDate
      },
      endDate: {
        $gt: currentDate
      }}, next);
    },
    'previous': ['current', function (data, next) {
      if(data.current) {
        return next();
      }
      Model.findOne({endDate: {$lt: currentDate}}, next);
    }]
  }, function (err, data) {
    if(err) {
      return next('err', err);
    }
    let rate = data.current ? data.current.withdrawalRate : data.previous ? data.previous.withdrawalRate : 0;
    res.json({rate: rate});
  });
});

routesWithdrawalConfig.post('/', function (req, res, next) {
  var Model = mongoose.model('WithdrawalConfig', req.app.models.withdrawalConfig.schema);
  let startDate = moment(req.body.startDate).startOf('day');
  let endDate = moment(req.body.endDate).startOf('day');
  async.auto({
    'record': (next) => {
      Model.findOne({
      $or: [
        {
          startDate: {
            $gte: startDate.toDate(),
            $lt: endDate.toDate()
          },
          endDate: {
            $lte: endDate.toDate(),
            $gt: startDate.toDate(),
          }
        },
        {
          startDate: {
            $gte: startDate.toDate()
          },
          endDate: {
            $gte: endDate.toDate()
          }
        },
        {
          startDate: {
            $lte: startDate.toDate()
          },
          endDate: {
            $lte: endDate.toDate()
          }
        },
        {
          startDate: {
            $lte: startDate.toDate()
          },
          endDate: {
            $gte: endDate.toDate()
          }
        }
      ]
      }, next);
    },
    'withdrawal': ['record', (data, next) => {
      // if(data.record) {
      //   return next({errors: true, message: '記録は日付に既に存在しています '+moment(data.record.startDate).format('DD-MM-YYYY')+" - "+moment(data.record.endDate).format('DD-MM-YYYY')});
      // }

      let params = req.body;
      params.startDate = startDate;
      params.endDate = endDate;
      var withdrawal = new Model(params);
      var error = withdrawal.validateSync();
      if(error) {
        return next(error);
      }

      withdrawal.save(next);
    }]
  }, function (err, data) {
    if(err) {
      return res.json(err);
    }

    res.json(data.withdrawal[0]);
  });

});

routesWithdrawalConfig.put('/', function (req, res, next) {
  var Model = mongoose.model('WithdrawalConfig', req.app.models.withdrawalConfig.schema);
  var id = req.body._id;
  delete req.body._id;
  let startDate = moment(req.body.startDate);
  let endDate = moment(req.body.endDate);
  async.auto({
    'collision': (next) => {
      Model.findOne({
        _id: {
          $ne: id
        },
        $or: [
          {
            startDate: {
              $lt: startDate.toDate()
            },
            endDate: {
              $gt: endDate.toDate(),
            }
          },
          {
            startDate: {
              $gte: startDate.toDate(),
              $lt: endDate.toDate()
            }
          },
          {
            endDate: {
              $lte: endDate.toDate(),
              $gt: startDate.toDate(),
            }
          }
        ]
      }, next);
    },
    'config': ['collision', (data, next) => {
      if(data.collision) {
        return next({errors: true, message: '記録は日付に既に存在しています '+moment(data.collision.startDate).format('DD-MM-YYYY')+" - "+moment(data.collision.endDate).format('DD-MM-YYYY')});
      }

      let params = req.body;
      params.startDate = startDate;
      params.endDate = endDate;
      var config = new Model(params);
      var error = config.validateSync();
      if(error) {
        return next(error);
      }
      Model.update({_id: id}, req.body, next);
    }]
  }, function (err, data) {
    if(err) {
      return res.json(err);
    }

    res.json(data.config);
  });
});

routesWithdrawalConfig.delete('/:_id', function (req, res, next) {
  var Model = mongoose.model('WithdrawalConfig', req.app.models.withdrawalConfig.schema);
  async.auto({
      'delete': function (next) {
        Model.remove({_id: req.params._id}, next);
      },
      'list': ['delete', function (data, next) {
        Model.find({}, next);
      }]
    },
    function (err, data) {
      if(err) {
        return next(err);
      }
      res.json({items: data.list});
    });
});

routesWithdrawalConfig.get('/:id', function (req, res, next) {
  var Model = mongoose.model('WithdrawalConfig', req.app.models.withdrawalConfig.schema);
  Model.findOne({_id: req.params.id},  function (err, data) {
    if(err) {
      return next(err);
    }
    res.json(data);
  });
});

export default routesWithdrawalConfig;
