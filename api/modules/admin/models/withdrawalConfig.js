import  mongoose from 'mongoose';

var withdrawalConfigSchema = new mongoose.Schema({
  createDate: {type: Date, required: true, default: Date.now},
  startDate: {type: Date, required: true, default: Date.now},
  endDate: {type: Date, required: true, default: Date.now},
  status: {type: String, required: true, default: 'placed', enum: ['placed', 'changed', 'removed', 'error']},
  withdrawalRate: {type: Number, required: true, default: 0}
}, {
  strict: true,
  safe: true,
  collection: 'withdrawalConfigs'
});

withdrawalConfigSchema.index({createDate: 1, masterID: 1}, {unique: true});

export default mongoose.model('WithdrawalConfig', withdrawalConfigSchema);