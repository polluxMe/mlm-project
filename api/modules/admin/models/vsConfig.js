import  mongoose from 'mongoose';
import moment from 'moment';
import async from 'async';

var vsConfigSchema = new mongoose.Schema({
  createDate: {type: Date, required: true, default: Date.now},
  startDate: {type: Date, required: true, default: Date.now},
  endDate: {type: Date, required: true, default: Date.now},
  status: {type: String, required: true, default: 'placed', enum: ['placed', 'changed', 'removed', 'error']},
  purchaseRate: {type: Number, required: true, default: 0}
}, {
  strict: true,
  safe: true,
  collection: 'vsConfigs'
});

vsConfigSchema.index({createDate: 1, masterID: 1}, {unique: true});

vsConfigSchema.statics.getCurrentActive = function (next) {
  let currentDate = moment().toDate();
  this.findOne({
    startDate: {
      $lte: currentDate
    },
    endDate: {
      $gt: currentDate
    }
  }, function (err, data) {
    if(err) {
      return next(err);
    }
    if(!data) {
      return next('Active frame not found!');
    }
    next(null, data);
  });
};

vsConfigSchema.statics.getRate = function (next) {
  let currentDate = moment().toDate();
  const self = this;
  async.auto({
    'current': function(next) {
      self.findOne({startDate: {
        $lte: currentDate
      },
        endDate: {
          $gt: currentDate
        }}, next);
    },
    'previous': ['current', function (data, next) {
      if(data.current) {
        return next();
      }
      self.findOne({endDate: {$lt: currentDate}}, next);
    }]
  }, function (err, data) {
    if(err) {
      return next('err', err);
    }
    let rate = data.current ? data.current.purchaseRate : data.previous.purchaseRate || 0;

    return next(null, {rate: rate})
  });
};

export default mongoose.model('VsConfig', vsConfigSchema);