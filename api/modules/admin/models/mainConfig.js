import  mongoose from 'mongoose';

function endOfTheMonth() {
  var now = new Date();
  return new Date(now.getYear(), now.getMonth()+1,0).getTime();
}

var mainConfigSchema = new mongoose.Schema({
  mainCommissionCycle: {type: Number, required: true, default: 1},
  cacheRewardsCycle: {type: Number, required: true, default: 1},
  matchingBonusCycle: {type: Number, required: true, default: 1},
  rankThresholds: {type: Array, default: [100, 1000, 2000, 4000, 8000, 16000, 32000, 64000, 128000, 256000]},
  mainCommissionLevels: {type: Array, default: [  1.6, 1.6, 1.6, 1.6, 0.8, 0.4, 0.2, 0.2]},
  mainCommissionThresholds: {type: Array, default: [10000, 20000, 40000, 80000, 160000, 320000, 640000, 1280000]},
  bravePurchaseRate: {type: Number, default: 130},
  braveSaleRate: {type: Number, default: 110},
  matchingBonusTargetRank: {type: Number, default: 4},
  matchingBonusPercents: {type: Array, default: [50, 30, 20, 20, 20, 20, 20, 20]},
  cashRewardPercents: {type: Array, default: [10, 10, 10, 10, 13, 15, 17, 18, 19, 20]},
  binaryMapConfirmCycle: {type: Date, default: endOfTheMonth},
  createDate: {type: Number, required: true, default: 1},
  updateDate: {type: Number, required: true, default: 1},
  minimumWithdrawalAmount: {type: Number, required: true, default: 500},
  log: {type: String},
}, {
  strict: true,
  safe: true,
  collection: 'mainConfigs'
});

export default mongoose.model('MainConfig', mainConfigSchema);