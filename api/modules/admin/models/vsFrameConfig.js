import mongoose from 'mongoose';
import async from 'async';
import Transaction from '../../member/models/vsTransactionPurchase';

var vsFrameConfigSchema = new mongoose.Schema({
  createDate: {type: Date, required: true, default: Date.now},
  level: {type: Number, required: true, default: 0},
  startRate: {type: Number, required: true, default: 0},
  endRate: {type: Number, required: true, default: 0},
  status: {type: String, required: true, default: 'created', enum: ['created', 'active', 'finished']},
  startDate: {type: Date},
  endDate: {type: Date},
  purchaseAmmount: {type: Number, required: true, default: 0},
  frames: [
    {
      frameID: {type: mongoose.Schema.Types.ObjectId, required: true},
      purchaseRate: {type: Number, required: true, default: 0},
      purchaseFrame: {type: Number, required: true, default: 0},
      purchaseQuantity: {type: Number, required: true, default: 0},
      remainQuantity: {type: Number, required: true, default: 0},
      revenue: {type: Number, required: true, default: 0},
      startDate: {type: Date},
      endDate: {type: Date},
      status: {type: String, required: true, default: 'created', enum: ['created', 'selling', 'finished', 'extra_finished']}
    }
  ],
}, {
  strict: true,
  safe: true,
  collection: 'vsFrameConfigs'
});

vsFrameConfigSchema.index({createDate: 1}, {unique: true});

vsFrameConfigSchema.statics.getCurrentActive = function (next) {
  this.findOne({status: 'active'}, function (err, data) {
    if(err) {
     return next(err);
    }
    if(!data) {
      return next('Active frame not found!');
    }
    next(null, data);
  });
};

vsFrameConfigSchema.statics.findActiveFrame = function (next) {
  this.getCurrentActive(function (err, data) {
    if(err) {
      return next(err);
    }
    let activeFrameIndex = data.frames.findIndex((frame) => {return frame.status === 'selling'});
    if(activeFrameIndex === -1) {
      activeFrameIndex = 0;
    }
    next(null, data.frames[activeFrameIndex]);
  });
};

vsFrameConfigSchema.statics.purchase = function (purchase, next) {
  const self = this;
  async.auto({
    'active': function (next) {
      self.getCurrentActive(next);
    },
    'nextActive': function (next) {
      self.findOne({status: 'created'}, next);
    },
    'currentActiveIndex': ['active', function (data, next) {
      if(!data.active) {
        return next('Active frame not found!');
      }
      if(!data.active.frames || !data.active.frames.length) {
        return next('Active frame has no nested frames!');
      }

      let activeFrameIndex = data.active.frames.findIndex((frame) => {return frame.status === 'selling'});
      if(activeFrameIndex === -1) {
        activeFrameIndex = data.active.frames.findIndex((frame) => {return frame.status === 'finished'});
        if(activeFrameIndex === -1) {
          activeFrameIndex = 0;
        } else {
          activeFrameIndex++;
        }
      }
      next(null, activeFrameIndex);
    }],
    'purchase': ['active', 'nextActive', 'currentActiveIndex', function (data, next) {
      let activeFrameIndex = data.currentActiveIndex;

      if(data.active.frames[activeFrameIndex].purchaseQuantity === data.active.frames[activeFrameIndex].remainQuantity) {
        data.active.frames[activeFrameIndex].purchaseQuantity = 0;
      }

      let purchaseDiff = data.active.frames[activeFrameIndex].remainQuantity - purchase.purchaseQuantityCreated;

      if(purchaseDiff <= 0) {
        let lastItemFrame = (activeFrameIndex+1) === data.active.frames.length;
        if(!lastItemFrame) {
          data.active.frames[activeFrameIndex].status = 'finished';
          if(!data.active.frames[activeFrameIndex].endDate) {
            data.active.frames[activeFrameIndex].endDate = Date.now();
          }

          data.active.frames[activeFrameIndex].purchaseQuantity += purchase.purchaseQuantityCreated;
          data.active.frames[activeFrameIndex].purchaseFrame = data.active.frames[activeFrameIndex].purchaseQuantity;
          data.active.frames[activeFrameIndex].remainQuantity = 0;

          console.log('data.active.frames[activeFrameIndex]', data.active.frames[activeFrameIndex]);

          activeFrameIndex++;
        } else {
          let amountAhead = Math.abs(purchaseDiff);
          data.active.frames[activeFrameIndex].purchaseFrame += amountAhead;
          data.active.frames[activeFrameIndex].purchaseQuantity = data.active.frames[activeFrameIndex].purchaseFrame;
          data.active.frames[activeFrameIndex].remainQuantity = 0;
        }
      } else {
        data.active.frames[activeFrameIndex].purchaseQuantity += purchase.purchaseQuantityCreated;
        data.active.frames[activeFrameIndex].remainQuantity -= purchase.purchaseQuantityCreated;
      }

      data.active.frames[activeFrameIndex].status = 'selling';

      // if(purchaseDiff <= 0) {
      //   return next('You can\'t buy VS by ' + data.active.frames[0].purchaseRate + ' $c rate. Only ' + data.active.frames[activeFrameIndex].remainQuantity + ' VS left with this rate.');
      // }

      if(!data.active.frames[activeFrameIndex].startDate) {
        data.active.frames[activeFrameIndex].startDate = Date.now();
      }

      data.active.save(next);
    }]
  }, function (err, data) {
    if(err) {
      return next(err);
    }
    next(null, data);
  });
};

vsFrameConfigSchema.statics.retrieve = function (purchase, next) {
  const self = this;
  const orderStatuses = ['btc_order', 'atc_order', 'cash_order'];
  const purchasedStatuses = ['removed_order_admin', 'removed_order_user', 'removed_purchase_admin'];

  var TransactionModel = mongoose.model('VsTransactionPurchase', Transaction.schema);
  console.log('retrieve purchase', purchase);
  async.auto({
    'removeStatus': function (next) {
      TransactionModel.findOne({masterID: purchase.masterID, status: {
        $in: purchasedStatuses
      }}, next).sort({createDate: -1});
    },
    'active': function (next) {
      self.getCurrentActive(next);
    },
    'shake': ['removeStatus', 'active', function (data, next) {
      if(data.removeStatus && purchasedStatuses.indexOf(data.removeStatus.status) !== -1) {
        console.log('we cant decrease removed order!');
        return next();
      }
      if (!data.active) {
        return next('Active frame not found!');
      }
      let frameId = purchase.frameID;
      let activeFrameIndex = data.active.frames.findIndex((frame) => {return frame._id.toString() == frameId});

      if (activeFrameIndex === -1) {
        // no need to return frames - the main block was sold
        return next();
      }

      data.active.frames[activeFrameIndex].purchaseQuantity -= purchase.purchaseQuantityCreated;
      data.active.frames[activeFrameIndex].remainQuantity += purchase.purchaseQuantityCreated;

      data.active.save(next);
    }]
  }, function (err, data) {
    if(err) {
      return next(err);
    }
    next(null, data);
  });
};


export default mongoose.model('VsFrameConfig', vsFrameConfigSchema);