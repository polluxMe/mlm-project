import mongoose from 'mongoose';

var memberSchema = new mongoose.Schema({
  createDate: {type: Date, required: true, default: Date.now},
  token: {type: String, required: true},
  memberID: {type: mongoose.Schema.Types.ObjectId, required: true}
}, {
  strict: true,
  safe: true,
  collection: 'resetPasswordTokens'
});

export default mongoose.model('ResetPasswordToken', memberSchema);
