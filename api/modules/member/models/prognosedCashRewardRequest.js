import  mongoose from 'mongoose';

var prognosedCashRewardRequestSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  baseUserID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  reportYear: {type: Number, required: true},
  reportMonth: {type: Number, required: true},
  updateDate: {type: Date, required: true, default: Date.now},
  amount: {type: Number, required: true, default: 0},
  log: {type: String},
}, {
  strict: true,
  safe: true,
  collection: 'prognosedCashRewardRequests'
});

prognosedCashRewardRequestSchema.index({createDate: 1, memberID: 1, baseUserID: 1}, {unique: true});

export default mongoose.model('PrognosedCashRewardRequest', prognosedCashRewardRequestSchema);
