import  mongoose from 'mongoose';
import passportLocalMongoose from 'passport-local-mongoose';
import materializedPlugin from 'mongoose-materialized';
import bcrypt from 'bcryptjs';
import masterPurchase from './vsMasterPurchase';
import memberRankTransaction from './memberRankTransaction';
import async from 'async';
import mainConfig from '../../admin/models/mainConfig';
import vsConfig from '../../admin/models/vsConfig';
import matchingBonus from './masterMatchingBonusRequest';
import mainCommission from './masterCommissionRequest';
import cashReward from './masterCashRewardRequest';
import matchingBonusTr from './transactionMatchingBonusRequest';
import mainCommissionTr from './transactionCommissionRequest';
import cashRewardTr from './transactionCashRewardRequest';
import cashRewardPrognose from './prognosedCashRewardRequest';
import wallet from './wallet';
import _ from 'lodash';
import moment from 'moment';
import logger from '../../../logger';

var memberSchema = new mongoose.Schema({
  accountType: {type: String, enum: ['brave', 'gcloud']},
  status: {type: String, default: 'temp', enum: ['temp', 'official', 'deactivated']},
  importID: {type: String},
  applicantID: {type: String},
  applicantName: {type: String, required: true},
  applicantKatakana: {type: String},
  introducerID: {type: String},
  introducerName: {type: String},
  gCloudIntroducerID: {type: mongoose.Schema.Types.ObjectId},
  gCloudIntroducerName: {type: String},
  nickname: {type: String},
  email: {type: String/*, required: true*/},
  createDate: {type: Date, required: true, default: Date.now},
  modifyDate: {type: Date},
  activityDate: {type: Date},
  loginDate: {type: Date},
  pwd: {type: String},
  password: {type: String},
  tempPassword: {type: String},
  salt: {type: String},
  activationDate: Date,
  passwordResetDate: {type: Date},
  earnings: {type: Number, required: true, default: 0},
  commision: {type: Number, required: true, default: 0},
  profile: {
    country: {type: String},
    dateOfBirth: {type: Date},
    age: Number,
    gender: {type: String},
    prefecture: {type: String},
    address: {type: String},
    phone: {type: String},
    mobilePhone: {type: String},
    passport: {type: String},
    nationality: {type: String},
    postal: {type: String}
  },
  purchaseType: {type: String, required: true, enum: ['cash', 'ATC', 'BTC']},
  purchasePrice: {type: Number},
  purchasePriceATC: {type: Number},
  purchasePriceBTC: {type: Number},
  bravePurchased: {type: Number},
  rank: {type: Number},
  gCloudRank: {type: String, enum: ['EXE', '2EXE', '3EXE', '4EXE', 'REXE', 'SP']},
  roles: [
    {
      _id: mongoose.Schema.Types.ObjectId,
      title: {type: String, required: true}
    }
  ],
  isLeft: Boolean,
  isRight: Boolean,
  isChangeId: {type: Boolean, required: true, default: false},
  emailNotice: {type: Boolean, default: false},
  writtenNotice: {type: Boolean, default: false},
  isDeposited: {type: Boolean, default: false},
  isAdmin: {type: Boolean, default: false},
  bankInformation: {type: String},
  lastLoginTime: {type: Date},
  isImported: {type: Boolean, default: false},
  note: {type: String}
}, {
  strict: true,
  safe: true,
  collection: 'members'
});

memberSchema.plugin(passportLocalMongoose);
memberSchema.plugin(materializedPlugin);

memberSchema.index({applicantID: 1}, {unique: true});
memberSchema.index( { "$**": "text" } );

memberSchema.post('save', (member, next) => {
  var Wallet = mongoose.model('Wallet', wallet.schema);
  const MemberRankTransaction = mongoose.model('MemberRankTransaction', memberRankTransaction.schema);
  async.auto({
    'wallet': function (next) {
      Wallet.findOne({memberID: member._id}, next);
    },
    'createWallet': ['wallet', (data, next) => {
      if (data.wallet) {
        return next();
      }
      var wallet = new Wallet({
        memberID: member._id
      });
      wallet.save(next);
    }],
    'lastRankChangeTransaction': (next) => {
      MemberRankTransaction
        .findOne({memberID: member._id}, next)
        .sort({createDate: -1});
    },
    'rankChangeHistory': ['lastRankChangeTransaction', (data, next) => {
      const lastTransaction = data.lastRankChangeTransaction;

      if (!member.rank || lastTransaction && lastTransaction.rank === member.rank) {
        return next();
      }

      const transaction = MemberRankTransaction({
        memberID: member._id,
        rank: member.rank
      });

      transaction.save(next);
    }]
  }, function (err, data) {
    if(err) {
      return next(err);
    }

    next(null, data);
  });
});

memberSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

memberSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

memberSchema.statics.login = function(req, username, password, done) {
  this.findOne({
    'username': username
  }, function(err, user) {
    if (err) return done(err);

    if (!user) {
      return done(null, false, {
        message: 'User not found'
      });
    }

    if (!user.validPassword(password)) {
      return done(null, false, {
        message: 'Wrong password'
      });
    }

    if (user.status === 'deactivated') {
      return done(null, false, {
        message: 'User is deactivated'
      });
    }

    user.lastLoginTime = new Date();

    user.save(() => {
      return done(null, user, {
        message: 'Successfully logged in'
      });
    });
  });
}

memberSchema.statics.signup = function(req, username, password, done) {
  let UserModel = this;

  this.findOne({
    'username': username
  }, function(err, user) {
    if (err) return done(err);

    if (user) {
      return done(null, false, {
        message: 'This email is already in use'
      });
    }

    let newUser = new UserModel();
    newUser.username = username;
    newUser.password = newUser.generateHash(password);

    newUser.save(function(err, product) {
      if (err) return done(err);

      done(null, product, {
        message: 'Successfully signed up'
      });
    });
  });
}


memberSchema.statics.computeRank = function (member, next) {
  var Purchase = mongoose.model('VsMasterPurchase', masterPurchase.schema);
  var MainConfig = mongoose.model('MainConfig', mainConfig.schema);
  var self = this;
  async.auto({
    'introducer': function (next) {
      if(!member.introducerID) {
        return next();
      }
      self.findOne({applicantID: member.introducerID}, next);
    },
    'records': ['introducer', function (data, next) {
      let ids = [];
      ids.push(member._id);
      Purchase.find({memberID: {$in: ids}}, {_id: true}, next);
    }],
    'purchases': ['records', function (data, next) {
      if(!data.records) {
        // No records for this member.
        return next();
      }
      const promises = [];
      data.records.forEach((record) => {
        promises.push(new Promise((resolve, reject) => {
          Purchase.lastTransaction(record, function (err, transaction) {
            if(err) {
              return reject(err);
            }
           resolve(transaction);
          });
        }));
      });

      Promise
        .all(promises)
        .catch(err => {
          return next(err);
        })
        .then((transactions) => {
          return next(null, transactions);
        });
    }],
    'config': function (next) {
      MainConfig.findOne({}, next);
    },
    'rank': ['purchases', 'config', function (data, next) {
      if(!data.purchases) {
        // No purchases found.
        return next();
      }

      const purchasedStatuses = ['cash_purchased', 'atc_purchased', 'btc_purchased', 'purchased'];

      let purchases = data.purchases;
      if(!member.isImported) {
        purchases = data.purchases.filter((purchase) => {
          return purchasedStatuses.indexOf(purchase.status) !== -1
        });
        if (!purchases.length) {
          return next();
        }
      }

      let calculatedSum = 0;
      let amount = 0;
      purchases.forEach((purchase) => {
        amount = purchase.purchaseBC;
        calculatedSum += amount;
      });
      const rankDesign = data.config.rankThresholds.filter((rankAmount) => {return calculatedSum >= rankAmount});
      if(!rankDesign.length) {
        return next();
      }
      const rank = rankDesign.length;
      next(null, rank);
    }],
    'setRank': ['rank', function (data, next) {
      if(!data.rank) {
        return next();
      }

      const gCloudRanks = {
        5: 'EXE',
        6: '2EXE',
        7: '3EXE',
        8: '4EXE',
        9: 'REXE',
        10: 'SP'
      };


      const isImportDecide = !member.isImported || (member.isImported && (data.rank > member.rank));

      member.rank = isImportDecide ? data.rank : member.rank;
      if(data.rank > 4 && isImportDecide) {
        member.gCloudRank = gCloudRanks[data.rank];
      }

      if(member.accountType === 'gcloud' && member.rank < 4 && isImportDecide) {
        member.rank = 4;
      }

      member.save(next);
    }]
  }, next);
}

memberSchema.statics.mainCommission = function(member, next, onlyCalculate) {
  let queryParams = {fields: {
    applicantID: true,
    _id:true,
    path: true,
    children: true,
    isLeft: true,
    isRight: true,
    parentId: true,
    email: true,
    applicantName: true,
    commision: true,
    bravePurchased: true
  }};
  var Wallet = mongoose.model('Wallet', wallet.schema);
  var MainConfig = mongoose.model('MainConfig', mainConfig.schema);
  var MainCommission = mongoose.model('MasterCommissionRequest', mainCommission.schema);
  var MainCommissionTransaction = mongoose.model('TransactionCommissionRequest', mainCommissionTr.schema);

  const calculateTree = (model, next) => {
    let branchTotal = {total: 0, maxItem: 0};
    model.getChildren({limit:1000, fields: queryParams.fields}, function (err, data) {
      data = data.concat(model);
      if(err) {
        return next(err);
      }

      data.forEach((leaf) => {
        if(leaf.bravePurchased && leaf.bravePurchased > 0) {
          branchTotal.total += leaf.bravePurchased;
          branchTotal.maxItem = (leaf.bravePurchased > branchTotal.maxItem) ? leaf.bravePurchased : branchTotal.maxItem;
        }
      });
      return next(null, branchTotal);
    });
  };

  async.auto({
    'siblings': function (next) {
      member.getChildren({limit:10,
        condition: { parentId: member._id},
        fields: queryParams.fields
      }, next);
    },
    'wallet': function (next) {
      Wallet.findOne({memberID: member._id}, next);
    },
    'self': ['wallet', function (data, next) {
      if(!data.wallet) {
        return next();
      }

      let purchaseAmmount = data.wallet.cashPurchased + data.wallet.btcPurchased + data.wallet.atcPurchased;
      next(null, purchaseAmmount);
    }],
    'leftBranch': ['siblings', function (data, next) {
      let leftBranch = data.siblings.filter((leaf) => {return leaf.isLeft});
      if(!leftBranch.length) {
        return next();
      }
      let leftLeaf = leftBranch[0];
      calculateTree(leftLeaf, next);
    }],
    'rightBranch': ['siblings', function (data, next) {
      let rightBranch = data.siblings.filter((leaf) => {return leaf.isRight});
      if(!rightBranch.length) {
        return next();
      }
      let rightLeaf = rightBranch[0];
      calculateTree(rightLeaf, next);
    }],
    'config': function (next) {
      MainConfig.findOne({}, next);
    },
    'currentRate': function (next) {
      var ModelRate = mongoose.model('VsConfig', vsConfig.schema);
      ModelRate.getCurrentActive(next);
    },
    'commission': ['config', 'self', 'leftBranch', 'rightBranch', 'wallet', 'currentRate', function (data, next) {
      if(!data.self) {
        return next();
      }

      const currentRate = data.currentRate ? data.currentRate.purchaseRate : 0;
      // calculate the B$ if didnt have
      const braveAmount = member.bravePurchased || Math.floor(data.self / currentRate);
      // const braveLeftMaxAmount = (data.leftBranch !== undefined) ? data.leftBranch.maxItem : 0;
      // const braveRightMaxAmount = (data.rightBranch !== undefined) ? data.rightBranch.maxItem : 0;
      const braveLeftAmount = data.leftBranch && data.leftBranch.total || 0;
      const braveRightAmount = data.rightBranch && data.rightBranch.total || 0;


      const thresholds = data.config.mainCommissionThresholds;
      const commissionsLevels = data.config.mainCommissionLevels;
      let levelsReachedSelf = thresholds.filter((threshold) => {return threshold < braveAmount});
      let levelsReachedLeft = thresholds.filter((threshold) => {return threshold < braveLeftAmount});
      let levelsReachedRight = thresholds.filter((threshold) => {return threshold < braveRightAmount});
      let maxReachedLevel = Math.max(levelsReachedSelf.length, levelsReachedLeft.length, levelsReachedRight.length);
      let reachableLevelLeft = 0;
      let reachableLevelRight = 0;
      // we didnt reach any commission
      if(!maxReachedLevel) {
        return next();
      }
      // maybe miss

      let mainSummary = 0;
      let mainStepCommission = 0;
      let mainCommission = 0;
      let reachableLevelSelf = 0;

      for(let i=0;i<maxReachedLevel;i++) {
        reachableLevelLeft =  (thresholds[i] < braveLeftAmount) ? thresholds[i] : braveLeftAmount;
        reachableLevelRight = (thresholds[i] < braveRightAmount) ? thresholds[i] : braveRightAmount;
        reachableLevelSelf = (thresholds[i] < braveAmount) ?  braveAmount : 0;
        mainSummary = (reachableLevelSelf + reachableLevelLeft + reachableLevelRight - thresholds[i]);
        mainStepCommission = mainSummary * (commissionsLevels[i] / 100);
        mainCommission += Math.floor(mainStepCommission);
        console.log('reachableLevelSelf', braveAmount, thresholds[i], mainSummary, reachableLevelLeft, reachableLevelRight, reachableLevelSelf, mainStepCommission);
      }

      if(onlyCalculate) {
        return next(null, mainCommission);
      }

      let date = new Date();
      async.auto({
        'commission': function (cb) {
          MainCommission.findOne({
            memberID: member._id,
            reportYear: date.getFullYear(),
            reportMonth: date.getMonth() + 1
          }, cb);
        },
        'save': ['commission', function (data, cb) {
          let commission = data.commission;

          const saveMemberData = function (err, saveData) {
            if(err) {
              return cb(err);
            }

            if(saveData && saveData.masterID && saveData.status === 'changed') {
              return cb();
            }
            member.commision += mainCommission;
            member.save(cb);
          };

          console.log('MAIN COMMISSION', mainCommission);

          if(!commission) {
            commission = new MainCommission({
              memberID: member._id,
              reportYear: date.getFullYear(),
              reportMonth: date.getMonth() + 1,
              amount: mainCommission
            });
            commission.save(saveMemberData);
          } else {
            let transaction = new MainCommissionTransaction({
              memberID: member._id,
              masterID: data.commission._id,
              reportYear: date.getFullYear(),
              reportMonth: date.getMonth() + 1,
              status: 'changed',
              amount: mainCommission
            });
            transaction.save(saveMemberData);
          }
        }]
      }, next);
    }]
  }, next);
};


memberSchema.statics.getMatchingMembers = function (member, next, usePhaseIndex) {
  const stepPercents = {
      0: 20,
      1: 10,
      2: 5,
      3: 5,
      4: 5,
      5: 5,
      6: 5,
      7: 5
    },
    self = this;
  var dataToGet = {_id: true, applicantID: true, commision: true, rank: true},
    MainCommission = mongoose.model('MasterCommissionRequest', mainCommission.schema);


  let date = new Date();

  const getMemberCommision = (member) => {
    return new Promise((resolve, reject) => {
      MainCommission.findOne(
        {
          memberID: member._id,
          reportYear: date.getFullYear(),
          reportMonth: date.getMonth() + 1
        }, {amount: true}, (err, data) => {
          if(err) {
            return reject(err);
          }

          member.commision = data ? data.amount : 0;
          resolve(member);
        });
    });
  };

  const makePhaseStep = (members, children, step, stepLimit, cb) => {
    if(stepLimit !== null && step >= stepLimit || step > 7) {
      return cb(children);
    }

    self.find({introducerID: {$in: members}}, dataToGet, (err, directChilds) => {
      if(err) {
        return next(err);
      }

      if(!directChilds || !directChilds.length) {
        return cb(children);
      }

      // setup phases count
      // stepLimit = directChilds.length; toDo Check if this ok

      // get childs recursively
      const directPromises = directChilds
        .map(child => getMemberCommision(child));

      if(!directPromises.length) {
        return cb(children);
      }

      Promise.all(directPromises).then(commissions => {
        const stepIndex = usePhaseIndex ? step : stepPercents[step];
        const childsAlreadySetted = children[stepIndex] || [];
        children[stepIndex] = childsAlreadySetted.concat(commissions.filter(child => child.rank >= 4));
        step++;

        makePhaseStep(directChilds.map(direct => direct.applicantID), children, step, stepLimit, cb);
      }, err => next(err));
    });
  };

  makePhaseStep([member.applicantID], {}, 0, null, (response) => {
    next(null, response);
  });
};

memberSchema.statics.matchingBonus = function(member, next) {
  var self = this;
  var dataToGet = {_id: true, applicantID: true, commision: true, rank: true};
  var MainConfig = mongoose.model('MainConfig', mainConfig.schema);

  if(member.rank < 5) {
    return next();
  }

  async.auto({
    '1wave': function (next) {
      self.find({introducerID: member.applicantID}, dataToGet, next);
    },
    'config': function (next) {
      MainConfig.findOne({}, next);
    },
    '2wave': ['1wave', 'config', function (data, next) {
      if(!data['1wave']) {
        return next();
      }
      let deepChilds = [];
      let childsMoreFourRank = [];
      data['1wave'].forEach((member) => {
        if(member.rank >= data.config.matchingBonusTargetRank) {
          childsMoreFourRank.push(member);
          return;
        }
        deepChilds.push(member);
      });
      if(deepChilds.length) {
        deepChilds.forEach((childRecord, index) => {
          self.find({introducerID: childRecord.applicantID}, dataToGet, function (err, resp) {
            if(err) {
              return next(err);
            }
            let checkFoundedChilds = 0;
            resp.forEach((child) => {
              if(child.rank >= data.config.matchingBonusTargetRank) {
                checkFoundedChilds++;
              }
            });
            // if more or exact than 2 members was in the child invitees - add him to the list
            if(checkFoundedChilds >= 2) {
              childsMoreFourRank.push(childRecord);
            }

            if(deepChilds.length === (index+1)) {
              return next(null, childsMoreFourRank);
            }
          });
        });
      } else {
        next(null, childsMoreFourRank);
      }
    }],
    'calculate': ['2wave', 'config', function (data, next) {
      if(!data['2wave']) {
        return next();
      }
      const percents = data.config.matchingBonusPercents.map((percent, index) => {
        let percentage = {};
        percentage[(index+1)] = percent;
        return percentage;
      });
      // {1:50, 2:30, 3:20, 4:20, 5:20, 6:20, 7:20, 8:20};
      let totalSum = 0;
      let calculatedBonus = 0;
      data['2wave'].forEach((member, index) => {
        let calcIndex = index+1;
        calculatedBonus = member.comission * (percents[calcIndex] / 100);
        totalSum += calculatedBonus;
      });
      if(!totalSum) {
        return next();
      }
      var MatchingBonus = mongoose.model('MasterMatchingBonusRequest', matchingBonus.schema);
      var MatchingBonusTransaction = mongoose.model('TransactionMatchingBonusRequest', matchingBonusTr.schema);

      const date = new Date();

      async.auto({
        'bonus': function (cb) {
          MatchingBonus.findOne({
            memberID: member._id,
            reportYear: date.getFullYear(),
            reportMonth: date.getMonth() + 1
          }, cb);
        },
        'save': ['bonus', function (data, cb) {
          console.log('MATCHING BONUS', totalSum);
          const saveMemberData = function (err, saveData) {
            if(err) {
              return cb(err);
            }

            if(saveData && saveData.masterID && saveData.status === 'changed') {
              return cb();
            }
            member.commision += totalSum;
            member.save(cb);
          };

          if(!data.bonus) {
            let matchingBonusReward = new MatchingBonus({
              memberID: member._id,
              amount: totalSum,
              reportYear: date.getFullYear(),
              reportMonth: date.getMonth() + 1
            });
            matchingBonusReward.save(saveMemberData);
          } else {
            data.bonus.update({$set: {
              amount: totalSum,
              status: 'changed'
            }}, saveMemberData);
          }
        }]
      }, next);
    }]
  }, next);
}

memberSchema.statics.cashReward = function(member, next, prognose) {
  var self = this;
  var CashReward = mongoose.model('MasterCashRewardRequest', cashReward.schema);
  var CashRewardPrognose = mongoose.model('PrognosedCashRewardRequest', cashRewardPrognose.schema);

  var MainConfig = mongoose.model('MainConfig', mainConfig.schema);
  var Wallet = mongoose.model('Wallet', wallet.schema);
  const date = new Date();

  function cashRewardProcess(member, baseUserID, rewardAmount, next) {
    async.auto({
      'cashBonus': function (cb) {
        if(!prognose) {
          return cb();
        }

        CashRewardPrognose.findOne({
          memberID: member._id,
          baseUserID: baseUserID,
          reportYear: date.getFullYear(),
          reportMonth: date.getMonth() + 1
        }, cb);
      },
      'save': ['cashBonus', function (data, cb) {
        let rewardRound = Math.floor(rewardAmount);
        console.log('CASH REWARD', rewardRound);
        if(!rewardRound) {
          return cb();
        }

        if(prognose) {
          if(!data.cashBonus) {
            let reward = new CashReward({
              memberID: member._id,
              amount: rewardRound,
              baseUserID: baseUserID,
              reportYear: date.getFullYear(),
              reportMonth: date.getMonth() + 1
            });
            reward.save(cb);
          } else {
            data.cashBonus.update({
              $set: {amount: rewardRound}
            }, cb);
          }
        } else {
          let reward = new CashReward({
            memberID: member._id,
            amount: rewardRound,
            baseUserID: baseUserID,
            reportYear: date.getFullYear(),
            reportMonth: date.getMonth() + 1
          });
          reward.save(cb);
        }
      }]
    }, next);
  }

  function getParent(member, amount, rewardSum, config, currentPercent, maxRank, next) {
    if(!member.introducerID || currentPercent >= 20) {
      return next(null, rewardSum);
    }

    const cashRewardPercents = config.cashRewardPercents;
    const rankMap = config.rankThresholds.map(bc => {
      return bc * 130;
      // return bc;
    });

    async.auto({
      'member': function (next) {
        self.findOne({
          applicantID: member.introducerID
        }, next);
      },
      'introducer': ['member', function (data, next) {
        if(!data.member || !data.member.introducerID) {
          return next();
        }
        self.findOne({
          applicantID: data.member.introducerID
        }, next);
      }],
      'childsAccumulated': ['member', function (data, next) {
        if(!data.member) {
          return next();
        }
        // console.log('data.member.applicantID', data.member.applicantID);
        self.find({introducerID: data.member.applicantID}, function(err, childs) {
          if(err) {
            return next(err);
          }

          if(!childs || !childs.length) {
            return next(null, 0);
          }
          let accumulationSum = 0;
          let childAmount = 0;
          childs.forEach(child => {
            childAmount = (child.purchasePrice + child.purchasePriceATC) / 2;
            accumulationSum += Math.floor(childAmount);
          });
          next(null, accumulationSum);
        });
      }],
      'memberRank': ['member', 'introducer', 'childsAccumulated', function (data, next) {
        if(!data.member) {
          return next();
        }
        // const introducerAmount = data.introducer ? (data.introducer.purchasePrice / 2) : 0;
        // const memberAmount = data.member.purchasePrice + Math.floor(introducerAmount);
        const memberAmount = Math.floor(data.member.purchasePrice + data.member.purchasePriceATC + data.childsAccumulated);
        const introducerRank = rankMap.filter(amount => {
          return memberAmount >= amount;
        }).length;
        next(null, introducerRank);
      }],
      'reward': ['memberRank', 'member', function (data, next) {
        if(!data.member) {
          return next(null, rewardSum);
        }

        let totalAmount = amount;
        // if(currentPercent === 0) {
        //   const introducerAmount = data.member.purchasePrice / 2;
        //   totalAmount += Math.floor(introducerAmount);
        // }
        console.log('data.memberRank > maxRank', data.memberRank, maxRank);
        if(data.memberRank > maxRank) {

          maxRank = data.memberRank > maxRank ? data.memberRank : maxRank;

          const introPercent = parseInt(cashRewardPercents[(data.memberRank-1)]);
          // const memberPercent = parseInt(cashRewardPercents[(member.rank-1)]);
          let percents = introPercent;
          if(currentPercent > 0) {
            percents = introPercent - currentPercent;
          }
          const realPercent = (percents / 100);
          console.log('currentPercent', currentPercent, realPercent);
          let rewardCurrentSum = totalAmount * realPercent;
          rewardSum += rewardCurrentSum;
          currentPercent += percents;

          cashRewardProcess(data.member, member._id, rewardCurrentSum, function (err) {
            if(err) {
              return next(err);
            }

            getParent(data.member, totalAmount, rewardSum, config, currentPercent, maxRank, next);
          });
        } else {
          getParent(data.member, totalAmount, rewardSum, config, currentPercent, maxRank, next);
        }
      }]
    }, next);
  }

  async.auto({
    'config': function (next) {
      MainConfig.findOne({}, next);
    },
    'wallet': function (next) {
      Wallet.findOne({memberID: member._id}, next);
    },
    'reward': ['config', 'wallet', function (data, next) {
      if(!data.wallet || !data.wallet.cashPurchased || data.wallet.cashPurchased < 1000) {
        // nothing to reward
        return next();
      }

      getParent(member, data.wallet.cashPurchased, 0, data.config, 0, 0, next);
    }]
  }, next);
};

memberSchema.statics.getCommissions = function (member, next) {
  const self = this;
  const CashRewardTransaction = mongoose.model('TransactionCashRewardRequest', cashRewardTr.schema);
  // const MatchingBonusTransaction = mongoose.model('TransactionMatchingBonusRequest', matchingBonusTr.schema);
  const MainCommissionTransaction = mongoose.model('TransactionCommissionRequest', mainCommissionTr.schema);
  // const Purchase = mongoose.model('VsMasterPurchase', masterPurchase.schema);

  if (!member) {
    return next();
  }

  async.auto({
    'member': function (next) {
      self.findById(member._id, next);
    },
    // 'siblings': ['member', (data, next) => {
    //   data.member.getChildren({limit:10,
    //     condition: { parentId: data.member._id}
    //   }, next);
    // }],
    // 'left': ['siblings', function (data, next) {
    //   let leftBranch = data.siblings.filter((leaf) => {return leaf.isLeft});
    //   if(!leftBranch.length) {
    //     return next();
    //   }
    //   let leftLeaf = leftBranch[0];
    //   leftLeaf.getChildren({limit:1000}, function (err, childs) {
    //     childs = childs.concat(leftLeaf);
    //     if (err) {
    //       return next(err);
    //     }
    //     let commissions = childs.map((child) => {
    //       return child.commision;
    //     });
    //
    //     next(null, {count: childs.length, sum: _.sum(commissions)});
    //   });
    // }],
    // 'right': ['siblings', function (data, next) {
    //   let rightBranch = data.siblings.filter((leaf) => {return leaf.isRight});
    //   if(!rightBranch.length) {
    //     return next();
    //   }
    //   let rightLeaf = rightBranch[0];
    //   rightLeaf.getChildren({limit:1000}, function (err, childs) {
    //     childs = childs.concat(rightLeaf);
    //     if (err) {
    //       return next(err);
    //     }
    //
    //     let commissions = childs.map((child) => {
    //       return child.commision;
    //     });
    //     next(null, {count: childs.length, sum: _.sum(commissions)});
    //   });
    // }],
    'commission': ['member', (data, next) => {
      const date = moment();

      MainCommissionTransaction.findOne({
        reportMonth: date.format('M'),
        reportYear: date.format('YYYY'),
        memberID: data.member._id,
        status: {$ne: 'removed'}
      }, {amount: true}, (err, data) => {
        next(err, data && data.amount);
      });
    }],
    'cashReward': ['member', (data, next) => {
      const findDate = moment();
      // const startDay = findDate.startOf('month').add(1, 'day').toISOString();
      // const endDay = findDate.endOf('month').toISOString();
      CashRewardTransaction.find({
        memberID: data.member._id,
        // createDate: {$gte: startDay, $lte: endDay},
        status: {$ne: 'removed'},
        reportMonth: findDate.format('M'),
        reportYear: findDate.format('YYYY'),
      }, function (err, data) {
        if(err) {
          return next(err);
        }
        let rewardSum = 0;
        data.forEach((reward) => {
          rewardSum += parseInt(reward.amount);
        });
        next(null, rewardSum);
      });
    }]
  }, function (err, data) {
    if(err) {
      return next(err);
    }
    let commissions = _.pick(data, 'commission', 'cashReward');
    commissions.commission = commissions.commission || 0;
    commissions.cashReward = commissions.cashReward || 0;
    next(null, commissions);
  });
};

memberSchema.statics.export = function(next, limit = null, offset = null) {
  const self = this;

  async.auto({
    'members': (next) => {
      self.find(
        {applicantID: {$nin: ['__master__']}},
        {
          path: false,
          password: false,
          parentId: false,
          isImported: false,
          isAdmin: false,
          isChangeId: false,
          roles: false,
          commision: false,
          earnings: false,
          __v: false,
          _w: false,
          hash: false
        },
        (err, data) => {
          const response = _.map(data, item => {
            const clonedObj = _.clone(item._doc);
            const member = _.assign(clonedObj, clonedObj.profile);
            delete member.profile;

            return member;
          });

          next(err, response);
        }
      ).limit(limit).skip(offset);
    },
    'membersWithCommissions': ['members', (data, next) => {
      const members = data.members;

      logger.info('MEMBER_MODEL:', 'START get commission');
      // const lastMember = members[members.length-1];
      const lastMembersIndex = members.length;
      let indexProcesed = 0;

      _.each(members, (member) => {
        self.getCommissions(member, (err, data) => {
          _.assign(member, data);
          indexProcesed++;
          if (indexProcesed === lastMembersIndex) {
            logger.info('MEMBER_MODEL:', 'processed records', indexProcesed);
            next(null, members);
          }
        });
      });
    }]
  }, (err, data) => {
    logger.info('MEMBER_MODEL:', 'END get commission');
    if (err) return err;
    next(null, data.membersWithCommissions);
  })
};
// // generating a hash
// memberSchema.methods.generateHash = function(password) {
//   return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
// };
//
// // checking if password is valid
// memberSchema.methods.validPassword = function(password) {
//   return bcrypt.compareSync(password, this.password);
// };

export default mongoose.model('Member', memberSchema);
