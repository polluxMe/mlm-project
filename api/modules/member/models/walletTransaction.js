import  mongoose from 'mongoose';

var walletTransactionSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  masterID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  vsAmount: {type: Number, required: true, default: 0},
  cashPurchased: {type: Number, required: true, default: 0},
  btcPurchased: {type: Number, required: true, default: 0},
  atcPurchased: {type: Number, required: true, default: 0},
  lastRate: {type: Number, required: true, default: 0},
  totalCommission: {type: Number, required: true, default: 0},
  commission: {type: Number, required: true, default: 0},
  log: {type: String}
}, {
  strict: true,
  safe: true,
  collection: 'walletTransactions'
});

walletTransactionSchema.index({masterID: 1, createDate: 1}, {unique: true});

export default mongoose.model('WalletTransaction', walletTransactionSchema);