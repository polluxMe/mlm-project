import  mongoose from 'mongoose';
import Transaction from './transactionRankCommissionRequest';
import devidendModel  from './mainCommissionDevidendRequest';
import {auto} from 'async';
import moment from 'moment';
import MemberModel from './member';

var masterRankCommissionRequestSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  dateFrom: {type: Date, required: true, default: Date.now},
  dateTo: {type: Date, required: true, default: Date.now},
  reportYear: {type: Number, required: true},
  reportMonth: {type: Number, required: true},
  amount: {type: Number, required: true, default: 0},
  daysToGo: {type: Number, required: true, default: 0},
}, {
  strict: true,
  safe: true,
  collection: 'masterRankCommissionRequests'
});

masterRankCommissionRequestSchema.index({memberID: 1, reportYear: 1, reportMonth: 1}, {unique: true});

masterRankCommissionRequestSchema.post('save', (commission, next) => {
  if(!commission) {
    return next('Main commision request insert error.');
  }

  var TransactionModel = mongoose.model('TransactionRankCommissionRequest', Transaction.schema);
  console.log('commission', commission);
  var transaction = new TransactionModel({
    masterID: commission._id,
    reportYear: commission.reportYear,
    reportMonth: commission.reportMonth,
    dateFrom: commission.dateFrom,
    dateTo: commission.dateTo,
    memberID: commission.memberID,
    amount: commission.amount,
    daysToGo: commission.daysToGo,
  });

  var error = transaction.validateSync();

  if(error) {
    return next(error);
  }

  TransactionModel.create(transaction, function (err, transactionRecord) {
    if(err) {
      return next(err);
    }
    next(null, transactionRecord);
  });
});

masterRankCommissionRequestSchema.statics.devidend = function (commission, next) {
  const Devidend = mongoose.model('MainCommissionDevidendRequest', devidendModel.schema);
  const Member = mongoose.model('Member', MemberModel.schema);
  const todayDate = moment();
  auto({
    'devidendRecord': next => {
      const devidend = new Devidend({
        memberID: commission.memberID,
        year: todayDate.year(),
        month: todayDate.month()+1,
        day: todayDate.date(),
        totalAmount: commission.amount,
        amount: Math.floor(commission.amount / commission.daysToGo),
        type: 'rank'
      });
      Devidend.create(devidend, next);
    },
    'member': next => {
      Member.findOne({_id: new mongoose.mongo.ObjectId(commission.memberID)}, next);
    },
    'updateMember': ['devidendRecord', 'member', (data, next) => {
      if(!data.member) {
        return next('Member not found!');
      }
      console.log('Member', commission.memberID, ' data ', data.devidendRecord.amount);
      data.member.commision += data.devidendRecord.amount;
      data.member.save(next);
    }]
  }, next);
};

export default mongoose.model('MasterRankCommissionRequest', masterRankCommissionRequestSchema);