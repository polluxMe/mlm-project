import  mongoose from 'mongoose';

var transactionWithdrawRequestSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  masterID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  updateDate: {type: Date, required: true, default: Date.now},
  status: {type: String, required: true, default: 'created', enum: ['created', 'changed', 'removed', 'error', 'not_formed', 'purchased']},
  requestAmount: {type: Number, required: true, default: 0},
  rate: {type: Number, required: true, default: 0},
  amount: {type: Number, required: true, default: 0},
  log: {type: String},
}, {
  strict: true,
  safe: true,
  collection: 'transactionWithdrawRequests'
});

transactionWithdrawRequestSchema.index({createDate: 1, masterID: 1, status: 1}, {unique: true});

export default mongoose.model('TransactionWithdrawRequest', transactionWithdrawRequestSchema);
