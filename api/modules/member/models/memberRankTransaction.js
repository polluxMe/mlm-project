import  mongoose from 'mongoose';

var memberRankTransactionSchema = new mongoose.Schema({
  memberID: {type: mongoose.Schema.Types.ObjectId, required: true},
  rank: {type: Number, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  note: {type: String}
}, {
  strict: true,
  safe: true,
  collection: 'memberRankTransactions'
});

export default mongoose.model('MemberRankTransaction', memberRankTransactionSchema);