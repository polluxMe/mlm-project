import  mongoose from 'mongoose';

var vsMasterFramePurchaseSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  ticketPurchaseAmount: {type: String, required: true},
  purchaseType: {type: String, required: true, enum: ['cash', 'ATC', 'BTC']},
  purchaseRate: {type: Number, required: true, default: 0},
  ticketCashAmount: {type: Number, required: true, default: 0},
  ticketVSAmount: {type: Number, required: true, default: 0},
  revenue: {type: Number, required: true, default: 0},
}, {
  strict: true,
  safe: true,
  collection: 'vsMasterFramePurchases'
});

vsMasterFramePurchaseSchema.index({createDate: 1}, {unique: true});

export default mongoose.model('VsMasterFramePurchase', vsMasterFramePurchaseSchema);