import  mongoose from 'mongoose';
import Transaction from './transactionMatchingBonusRequest';


var masterMatchingBonusRequestSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  reportYear: {type: Number, required: true},
  reportMonth: {type: Number, required: true},
  amount: {type: Number, required: true},
}, {
  strict: true,
  safe: true,
  collection: 'masterMatchingBonusRequests'
});

masterMatchingBonusRequestSchema.index({memberID: 1, reportYear: 1, reportMonth: 1}, {unique: true});

masterMatchingBonusRequestSchema.post('save', (matchingBonus, next) => {
  if(matchingBonus.frameID) {
    return next();
  }
  if(!matchingBonus) {
    return next('Matching bonus request insert error.');
  }

  var TransactionModel = mongoose.model('TransactionMatchingBonusRequest', Transaction.schema);

  var transaction = new TransactionModel({
    masterID: matchingBonus._id,
    memberID: matchingBonus.memberID,
    reportYear: matchingBonus.reportYear,
    reportMonth: matchingBonus.reportMonth,
    amount: matchingBonus.amount,
  });

  var error = transaction.validateSync();

  if(error) {
    return next(error);
  }

  TransactionModel.create(transaction, function (err, transactionRecord) {
    if(err) {
      return next(err);
    }
    next(null, transactionRecord);
  });
});

export default mongoose.model('MasterMatchingBonusRequest', masterMatchingBonusRequestSchema);