import  mongoose from 'mongoose';

var vsTransactionTicketSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  masterID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  updateDate: {type: Date, required: true, default: Date.now},
  status: {type: String, required: true, default: 'placed', enum: ['placed', 'changed', 'removed', 'error']},
  purchaseRate: {type: Number, required: true, default: 0},
  ticketBraveAmount: {type: Number, required: true, default: 0},
  ticketPurchaseAmount: {type: String, required: true}
}, {
  strict: true,
  safe: true,
  collection: 'vsTransactionTickets'
});

vsTransactionTicketSchema.index({createDate: 1, masterID: 1, status: 1}, {unique: true});

export default mongoose.model('VsTransactionTicket', vsTransactionTicketSchema);