import  mongoose from 'mongoose';

var mainCommissionDevidendRequestSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  year: {type: Number, required: true},
  month: {type: Number, required: true},
  day: {type: Number, required: true},
  totalAmount: {type: Number, required: true, default: 0},
  amount: {type: Number, required: true, default: 0},
  type: {type: String, required: true, default:'main', enum: ['main', 'rank', 'mega']},
}, {
  strict: true,
  safe: true,
  collection: 'mainCommissionDevidendRequests'
});

mainCommissionDevidendRequestSchema.index({memberID: 1, day: 1, month: 1, year: 1}, {unique: true});

export default mongoose.model('MainCommissionDevidendRequest', mainCommissionDevidendRequestSchema);
