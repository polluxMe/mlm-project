import  mongoose from 'mongoose';
import Transaction from './vsTransactionTicket';

var vsMasterTicketSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  ticketPurchaseAmount: {type: String, required: true},
  purchaseType: {type: String, required: true, enum: ['cash', 'ATC', 'BTC']},
  purchaseRate: {type: Number, required: true, default: 0},
  ticketBraveAmount: {type: Number, required: true, default: 0}
}, {
  strict: true,
  safe: true,
  collection: 'vsMasterTickets'
});

vsMasterTicketSchema.post('save', (ticket, next) => {
  if(!ticket) {
    return next('Ticket insert error.');
  }

  const TransactionModel = mongoose.model('VsTransactionTicket', Transaction.schema);

  ticket.createDate = ticket.createDate || Date.now();

  var transaction = new TransactionModel({
    createDate: ticket.createDate,
    purchaseType: ticket.purchaseType,
    purchaseRate: ticket.purchaseRate,
    memberID: ticket.memberID,
    masterID: ticket._id,
    ticketPurchaseAmount: ticket.ticketPurchaseAmount,
    ticketBraveAmount: ticket.ticketBraveAmount
  });

  var error = transaction.validateSync();
  if(error) {
    return next(error);
  }

  transaction.save(function (err, transactionData) {
    if (err) {
      return next(err);
    }

    next(null, transactionData);
  });
});

export default mongoose.model('VsMasterTicket', vsMasterTicketSchema);