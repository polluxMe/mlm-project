import  mongoose from 'mongoose';

var transactionCashRewardRequestSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  masterID: {type: String, required: true},
  baseUserID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  reportYear: {type: Number, required: true},
  reportMonth: {type: Number, required: true},
  updateDate: {type: Date, required: true, default: Date.now},
  status: {type: String, required: true, default: 'created', enum: ['created', 'changed', 'removed', 'error', 'not_formed', 'purchased']},
  amount: {type: Number, required: true, default: 0},
  log: {type: String},
}, {
  strict: true,
  safe: true,
  collection: 'transactionCashRewardRequests'
});

transactionCashRewardRequestSchema.index({createDate: 1, masterID: 1, status: 1}, {unique: true});

export default mongoose.model('TransactionCashRewardRequest', transactionCashRewardRequestSchema);
