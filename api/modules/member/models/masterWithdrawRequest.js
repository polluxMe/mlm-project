import  mongoose from 'mongoose';
import Transaction from './transactionWithdrawRequest';


var masterWithdrawRequestSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  requestAmount: {type: Number, required: true, default: 0},
  amount: {type: Number, required: true},
  rate: {type: Number, required: true, default: 0},
}, {
  strict: true,
  safe: true,
  collection: 'masterWithdrawRequests'
});

masterWithdrawRequestSchema.index({createDate: 1}, {unique: true});

masterWithdrawRequestSchema.post('save', (withdraw, next) => {
  if(!withdraw) {
    return next('Withdraw request insert error.');
  }

  var TransactionModel = mongoose.model('TransactionWithdrawRequest', Transaction.schema);

  var transaction = new TransactionModel({
    masterID: withdraw._id,
    memberID: withdraw.memberID,
    requestAmount: withdraw.requestAmount,
    rate: withdraw.rate,
    amount: withdraw.amount,
  });

  var error = transaction.validateSync();

  if(error) {
    return next(error);
  }

  TransactionModel.create(transaction, function (err, transactionRecord) {
    if(err) {
      return next(err);
    }
    next(null, transactionRecord);
  });
});

export default mongoose.model('MasterWithdrawRequest', masterWithdrawRequestSchema);