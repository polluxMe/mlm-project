import  mongoose from 'mongoose';


var walletSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  vsAmount: {type: Number, required: true, default: 0},
  cashPurchased: {type: Number, required: true, default: 0},
  btcPurchased: {type: Number, required: true, default: 0},
  atcPurchased: {type: Number, required: true, default: 0},
  lastRate: {type: Number, required: true, default: 0},
  totalCommission: {type: Number, required: true, default: 0},
  commission: {type: Number, required: true, default: 0},
  bravePurchased: {type: Number, required: true, default: 0},
}, {
  strict: true,
  safe: true,
  collection: 'wallets'
});

walletSchema.index({memberID: 1}, {unique: true});

export default mongoose.model('Wallet', walletSchema);
