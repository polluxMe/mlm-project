import  mongoose from 'mongoose';
import Transaction from './vsTransactionBrave3Way';

var vsMasterBrave3WaySchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  brave3WayPurchaseAmount: {type: String, required: true}
}, {
  strict: true,
  safe: true,
  collection: 'vsMasterBrave3Ways'
});

vsMasterBrave3WaySchema.post('save', (brave3Way, next) => {
  if(!brave3Way) {
    return next('brave3Way insert error.');
  }

  const TransactionModel = mongoose.model('VsTransactionBrave3Way', Transaction.schema);

  brave3Way.createDate = brave3Way.createDate || Date.now();

  var transaction = new TransactionModel({
    createDate: brave3Way.createDate,
    memberID: brave3Way.memberID,
    masterID: brave3Way._id,
    brave3WayPurchaseAmount: brave3Way.brave3WayPurchaseAmount
  });

  var error = transaction.validateSync();
  if(error) {
    return next(error);
  }

  transaction.save(function (err, transactionData) {
    if (err) {
      return next(err);
    }

    next(null, transactionData);
  });
});

export default mongoose.model('VsMasterBrave3Way', vsMasterBrave3WaySchema);