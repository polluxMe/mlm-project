import  mongoose from 'mongoose';
import Transaction from './vsTransactionBrave2Way';

var vsMasterBrave2WaySchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  brave2WayPurchaseAmount: {type: String, required: true}
}, {
  strict: true,
  safe: true,
  collection: 'vsMasterBrave2Ways'
});

vsMasterBrave2WaySchema.post('save', (brave2Way, next) => {
  if(!brave2Way) {
    return next('brave2Way insert error.');
  }

  const TransactionModel = mongoose.model('VsTransactionBrave2Way', Transaction.schema);

  brave2Way.createDate = brave2Way.createDate || Date.now();

  var transaction = new TransactionModel({
    createDate: brave2Way.createDate,
    memberID: brave2Way.memberID,
    masterID: brave2Way._id,
    brave2WayPurchaseAmount: brave2Way.brave2WayPurchaseAmount
  });

  var error = transaction.validateSync();
  if(error) {
    return next(error);
  }

  transaction.save(function (err, transactionData) {
    if (err) {
      return next(err);
    }

    next(null, transactionData);
  });
});

export default mongoose.model('VsMasterBrave2Way', vsMasterBrave2WaySchema);