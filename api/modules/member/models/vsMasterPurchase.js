import  mongoose from 'mongoose';
import Transaction from './vsTransactionPurchase';
import FrameModel from '../../admin/models/vsFrameConfig';
import vsConfig from '../../admin/models/vsConfig';
import wallet from './wallet';
import member from './member';
import async from 'async';

var vsMasterPurchaseSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  approveDate: {type: Date},
  createOrderDate: {type: Date},
  purchaseAmount: {type: Number, required: true},
  purchaseType: {type: String, required: true, enum: ['cash', 'ATC', 'BTC']},
  cashRate: {type: Number, required: true, default: 0},
  purchaseRate: {type: Number, required: true, default: 0},
  purchaseRateCreated: {type: Number, required: true, default: 0},
  purchaseQuantityCreated: {type: Number, required: true, default: 0},
  purchaseQuantity: {type: Number, required: true, default: 0},
  cashRateCreated: {type: Number, required: true, default: 0},
  revenue: {type: Number, required: true, default: 0},
  frameBlockID: {type: mongoose.Schema.Types.ObjectId},
  frameID: {type: mongoose.Schema.Types.ObjectId},
  purchaseBC: {type: Number, required: true, default: 0},
  purchaseBCCreated: {type: Number, required: true, default: 0},
  isImported: {type: Number, default: false}
}, {
  strict: true,
  safe: true,
  collection: 'vsMasterPurchases'
});

// vsMasterPurchaseSchema.index({createDate: 1}, {unique: true}); it's silly key, csv uploading is not working because a bunch of users with they purchases can be created in a second

  vsMasterPurchaseSchema.post('save', (purchase, next) => {
    if(purchase.frameID) {
      return next();
    }
    if(!purchase) {
      return next('Purchase insert error.');
    }

    var TransactionModel = mongoose.model('VsTransactionPurchase', Transaction.schema);
    const orderStatuses = {'BTC': 'btc_order', 'ATC': 'atc_order', 'cash': 'cash_order'};

    const self = this;

    const createDate = purchase.createDate ? purchase.createDate : Date.now();
    const createOrderDate = purchase.createOrderDate ? purchase.createOrderDate : Date.now();

    var transaction = new TransactionModel({
      masterID: purchase._id,
      memberID: purchase.memberID,
      purchaseAmount: purchase.purchaseAmount,
      purchaseType: purchase.purchaseType,
      purchaseRateCreated: purchase.purchaseRate,
      purchaseQuantityCreated: purchase.purchaseQuantity,
      cashRateCreated: purchase.cashRate,
      revenue: purchase.revenue,
      createDate: createDate,
      createOrderDate: createOrderDate,
      status: orderStatuses[purchase.purchaseType],
      approveDate: purchase.approveDate,
      isImported: purchase.isImported
    });

    var error = transaction.validateSync();
    if(error) {
      return next(error);
    }

    transaction.save(function (err, transactionData) {
      if(err) {
        return next(err);
      }

      const purchasedStatuses = ['btc_order', 'atc_order', 'cash_order'];
      if (purchasedStatuses.indexOf(transactionData.status) === -1 || transactionData.frameID) {
        return next();
      }

      const getActiveFrameID = (data) => {
        let activeFrameIndex = data.frame.active.frames.findIndex((frame) => {return frame.status === 'selling'});
        if(activeFrameIndex === -1) {
          activeFrameIndex = 0;
        }
        return data.frame.active.frames[activeFrameIndex]._id;
      }

      //creation of frame transactionData and save its data back to transaction
      const PurchaseFrameModel = mongoose.model('VsFrameConfig', FrameModel.schema);
      const WalletModel = mongoose.model('Wallet', wallet.schema);
      const Member = mongoose.model('Member', member.schema);

      async.auto({
        'member': (next) => {
          Member.findOne({_id: transactionData.memberID}, next);
        },
        'wallet': function (next) {
          WalletModel.findOne({memberID: transactionData.memberID}, next);
        },
        'frame': function (next) {
          if (transactionData.approveDate) {
            return next();
          }

          PurchaseFrameModel.purchase(transactionData, next);
        },
        'currentRate': function (next) {
          var ModelRate = mongoose.model('VsConfig', vsConfig.schema);
          ModelRate.getCurrentActive(next);
        },
        'bcAmount': ['currentRate', 'member', function (data, next) {
          let currentRate = (data.currentRate) ? data.currentRate.purchaseRate : 0;
          currentRate = data.member.isImported ? 130 : currentRate;
          const bCAmount = Math.floor(transactionData.purchaseAmount / currentRate);
          next(null, bCAmount);
        }],
        'transaction': ['frame', 'bcAmount', 'member', function (data, next) {
          transactionData.purchaseBCCreated = data.bcAmount;
          if (data.member.isImported) {
            transactionData.purchaseBC = data.bcAmount;
          }
          if(data.frame) {
            transactionData.frameBlockID = data.frame.active._id;
            transactionData.frameID = data.frame.active.frames[data.frame.currentActiveIndex]._id;
          }
          transactionData.save(next);
        }],
        'master': ['frame', 'transaction', 'bcAmount', function (data, next) {
            if (data.frame) {
              purchase.update({$set: {
                purchaseBC: data.bcAmount,
                frameBlockID: data.frame.active._id,
                frameID: data.frame.active.frames[data.frame.currentActiveIndex]._id
              }}, next);
            }
            else {
              purchase.update({$set: {
                purchaseBC: data.bcAmount
              }}, next);
            }
        }]
      }, function (err, data) {
        if(err) {
          const removeRecords = (err, data) => {
            const recordsToRemove = ['transaction'];
            recordsToRemove.forEach(record => {
              let removedTransaction = data[record];
              if (removedTransaction && removedTransaction[0]) {
                removedTransaction[0].remove();
              }
            });
            return next(err);
          }
          console.log('Error on purchasing VS', err);
          if(data.frame && data.frame.purchase) {
            // trying to rollback error transaction
            PurchaseFrameModel.retrieve(transactionData, function (errPurchase) {
              let errPass = errPurchase || err;
              removeRecords(errPass, data);
            });
          } else {
            removeRecords(err, data);
          }
        } else {
          next(null, data);
        }
      });
    });
});

vsMasterPurchaseSchema.statics.retrieve = function (purchase, next) {
  const PurchaseFrameModel = mongoose.model('VsFrameConfig', FrameModel.schema);
  const WalletModel = mongoose.model('Wallet', wallet.schema);
  const purchasedStatuses = ['cash_purchased', 'atc_purchased', 'btc_purchased', 'purchased'];
  const removedStatuses = ['removed_order_admin', 'removed_order_user', 'removed_purchase_admin'];
  const MemberModel = mongoose.model('Member', member.schema);

  // if(purchasedStatuses.indexOf(purchase.status) !== -1) {
  //   return next();
  // }

  const self = this;
  async.auto({
    'masterRecord': function (next) {
      self.findById(purchase.masterID, next);
    },
    'frame': ['masterRecord', function (data, next) {
      if(!data.masterRecord) {
        return next('Master record was not found.');
      }
      PurchaseFrameModel.retrieve(purchase, next);
    }],
    'wallet': ['masterRecord', 'frame', function (data, next) {
      if(!data.masterRecord) {
        return next('Master record was not found.');
      }
      WalletModel.findOne({memberID: data.masterRecord.memberID}, next);
    }],
    'isPurchased': function (next) {
      const isPurchased = purchasedStatuses.indexOf(purchase.status) !== -1;
      next(null, isPurchased);
    },
    'shakeWallet': ['wallet', 'masterRecord', 'isPurchased', function (data, next) {
      if(!data.masterRecord) {
        return next('Master record was not found.');
      }
      if(!data.wallet || !data.isPurchased) {
        return next();
      }

      const masterPurchase = purchase;
      const cashPurchased = masterPurchase.purchaseType === 'cash' ? masterPurchase.purchaseAmount : 0;
      const btcPurchased = masterPurchase.purchaseType === 'BTC' ? masterPurchase.purchaseAmount : 0;
      const atcPurchased = masterPurchase.purchaseType === 'ATC' ? masterPurchase.purchaseAmount : 0;
      data.wallet.vsAmount -= masterPurchase.purchaseQuantity;
      data.wallet.cashPurchased -= cashPurchased;
      data.wallet.btcPurchased -= btcPurchased;
      data.wallet.atcPurchased -= atcPurchased;
      if(masterPurchase.purchaseBC && data.wallet.bravePurchased) {
        data.wallet.bravePurchased -= masterPurchase.purchaseBC;
      }

      // here we didnt add check for negative numbers, because the ammounts must match in adding and deleting
      data.wallet.save(next);
    }],
    'member': ['masterRecord', function (data, next) {
      MemberModel.findOne({_id: data.masterRecord.memberID}, next);
    }],
    'memberPurchases': ['member', 'masterRecord', 'isPurchased', function (data, next) {
      if(!data.isPurchased) {
        return next();
      }
      const transaction = purchase;
      const cashPurchased = transaction.purchaseType === 'cash' ? transaction.purchaseAmount : 0;
      const btcPurchased = transaction.purchaseType === 'BTC' ? transaction.purchaseAmount : 0;
      const atcPurchased = transaction.purchaseType === 'ATC' ? transaction.purchaseAmount : 0;

      if(data.member.purchasePrice) {
        data.member.purchasePrice -= cashPurchased;
      }
      if(data.member.purchasePriceBTC) {
        data.member.purchasePriceBTC -= btcPurchased;
      }
      if(data.member.purchasePriceATC) {
        data.member.purchasePriceATC -= atcPurchased;
      }
      if(transaction.purchaseBC && data.member.bravePurchased) {
        data.member.bravePurchased -= transaction.purchaseBC;
      }

      data.member.save(next);
    }]
  }, next);
}

vsMasterPurchaseSchema.statics.lastTransaction = function (record, next) {
  var TransactionModel = mongoose.model('VsTransactionPurchase', Transaction.schema);
  TransactionModel.findOne({
    masterID: record._id
  }, next).sort({createDate: -1});
}

export default mongoose.model('VsMasterPurchase', vsMasterPurchaseSchema);