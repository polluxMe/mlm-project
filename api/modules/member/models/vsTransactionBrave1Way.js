import  mongoose from 'mongoose';

var vsTransactionBrave1WaySchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  masterID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  updateDate: {type: Date, required: true, default: Date.now},
  status: {type: String, required: true, default: 'placed', enum: ['placed', 'changed', 'removed', 'error']},
  brave1WayPurchaseAmount: {type: String, required: true}
}, {
  strict: true,
  safe: true,
  collection: 'vsTransactionBrave1Ways'
});

vsTransactionBrave1WaySchema.index({createDate: 1, masterID: 1, status: 1}, {unique: true});

export default mongoose.model('VsTransactionBrave1Way', vsTransactionBrave1WaySchema);