import  mongoose from 'mongoose';

var vsTransactionBrave2WaySchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  masterID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  updateDate: {type: Date, required: true, default: Date.now},
  status: {type: String, required: true, default: 'placed', enum: ['placed', 'changed', 'removed', 'error']},
  brave2WayPurchaseAmount: {type: String, required: true}
}, {
  strict: true,
  safe: true,
  collection: 'vsTransactionBrave2Ways'
});

vsTransactionBrave2WaySchema.index({createDate: 1, masterID: 1, status: 1}, {unique: true});

export default mongoose.model('VsTransactionBrave2Way', vsTransactionBrave2WaySchema);