import  mongoose from 'mongoose';
import Transaction from './transactionCashRewardRequest';


var masterCashRewardRequestSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  baseUserID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  reportYear: {type: Number, required: true},
  reportMonth: {type: Number, required: true},
  amount: {type: Number, required: true, default: 0},
}, {
  strict: true,
  safe: true,
  collection: 'masterCashRewardRequests'
});

masterCashRewardRequestSchema.index({createDate: 1}, {unique: true});

masterCashRewardRequestSchema.post('save', (cashReward, next) => {
  if(!cashReward) {
    return next('Cash reward request insert error.');
  }

  var TransactionModel = mongoose.model('TransactionCashRewardRequest', Transaction.schema);

  var transaction = new TransactionModel({
    masterID: cashReward._id,
    memberID: cashReward.memberID,
    baseUserID: cashReward.baseUserID,
    amount: cashReward.amount,
    reportYear: cashReward.reportYear,
    reportMonth: cashReward.reportMonth,
  });

  var error = transaction.validateSync();

  if(error) {
    return next(error);
  }

  TransactionModel.create(transaction, function (err, transactionRecord) {
    if(err) {
      return next(err);
    }
    next(null, transactionRecord);
  });
});

export default mongoose.model('MasterCashRewardRequest', masterCashRewardRequestSchema);