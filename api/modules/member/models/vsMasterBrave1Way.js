import  mongoose from 'mongoose';
import Transaction from './vsTransactionBrave1Way';

var vsMasterBrave1WaySchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  brave1WayPurchaseAmount: {type: String, required: true}
}, {
  strict: true,
  safe: true,
  collection: 'vsMasterBrave1Ways'
});

vsMasterBrave1WaySchema.post('save', (brave1Way, next) => {
  if(!brave1Way) {
    return next('brave1Way insert error.');
  }

  const TransactionModel = mongoose.model('VsTransactionBrave1Way', Transaction.schema);

  brave1Way.createDate = brave1Way.createDate || Date.now();

  var transaction = new TransactionModel({
    createDate: brave1Way.createDate,
    memberID: brave1Way.memberID,
    masterID: brave1Way._id,
    brave1WayPurchaseAmount: brave1Way.brave1WayPurchaseAmount
  });

  var error = transaction.validateSync();
  if(error) {
    return next(error);
  }

  transaction.save(function (err, transactionData) {
    if (err) {
      return next(err);
    }

    next(null, transactionData);
  });
});

export default mongoose.model('VsMasterBrave1Way', vsMasterBrave1WaySchema);