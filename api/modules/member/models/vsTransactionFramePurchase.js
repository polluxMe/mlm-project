import  mongoose from 'mongoose';

var vsTransactionFramePurchaseSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  masterID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  updateDate: {type: Date, required: true, default: Date.now},
  status: {type: String, required: true, default: 'placed', enum: ['placed', 'changed', 'removed', 'error']},
  purchaseRate: {type: Number, required: true, default: 0},
  purchaseFrame: {type: Number, required: true, default: 0},
  remainFrame: {type: Number, required: true, default: 0},
  revenue: {type: Number, required: true, default: 0},
  log: {type: String},
}, {
  strict: true,
  safe: true,
  collection: 'vsTransactionFramePurchases'
});

vsTransactionFramePurchaseSchema.index({createDate: 1, masterID: 1, status: 1}, {unique: true});

export default mongoose.model('VsTransactionFramePurchase', vsTransactionFramePurchaseSchema);