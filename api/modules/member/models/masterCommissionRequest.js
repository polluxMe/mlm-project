import  mongoose from 'mongoose';
import Transaction from './transactionCommissionRequest';

var masterCommissionRequestSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  reportYear: {type: Number, required: true},
  reportMonth: {type: Number, required: true},
  amount: {type: Number, required: true, default: 0},
}, {
  strict: true,
  safe: true,
  collection: 'masterCommissionRequests'
});

masterCommissionRequestSchema.index({memberID: 1, reportYear: 1, reportMonth: 1}, {unique: true});

masterCommissionRequestSchema.post('save', (commission, next) => {
  if(!commission) {
    return next('Main commision request insert error.');
  }

  var TransactionModel = mongoose.model('TransactionCommissionRequest', Transaction.schema);

  var transaction = new TransactionModel({
    masterID: commission._id,
    reportYear: commission.reportYear,
    reportMonth: commission.reportMonth,
    memberID: commission.memberID,
    amount: commission.amount,
  });

  var error = transaction.validateSync();

  if(error) {
    return next(error);
  }

  TransactionModel.create(transaction, function (err, transactionRecord) {
    if(err) {
      return next(err);
    }
    next(null, transactionRecord);
  });
});

export default mongoose.model('MasterCommissionRequest', masterCommissionRequestSchema);