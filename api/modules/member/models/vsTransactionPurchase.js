import  mongoose from 'mongoose';
import FrameModel from '../../admin/models/vsFrameConfig';
import wallet from './wallet';
import member from './member';
import async from 'async';

var vsTransactionPurchaseSchema = new mongoose.Schema({
  memberID: {type: String, required: true},
  masterID: {type: String, required: true},
  createDate: {type: Date, required: true, default: Date.now},
  createOrderDate: {type: Date},
  approveDate: {type: Date},
  updateDate: {type: Date, required: true, default: Date.now},
  status: {type: String, required: true, default: 'created', enum: ['created', 'btc_order', 'atc_order', 'cash_order', 'cash_purchased', 'atc_purchased', 'btc_purchased', 'purchased', 'changed', 'removed_order_admin', 'removed_order_user', 'removed_purchase_admin', 'error', 'not_formed']},
  revenue: {type: Number, required: true, default: 0},
  log: {type: String},
  purchaseAmount: {type: Number, required: true},
  purchaseType: {type: String, required: true, enum: ['cash', 'ATC', 'BTC']},
  purchaseRate: {type: Number, required: true, default: 0},
  purchaseRateCreated: {type: Number, required: true, default: 0},
  purchaseQuantityCreated: {type: Number, required: true, default: 0},
  purchaseQuantity: {type: Number, required: true, default: 0},
  cashRateCreated: {type: Number, required: true, default: 0},
  cashRate: {type: Number, required: true, default: 0},
  frameBlockID: {type: mongoose.Schema.Types.ObjectId},
  frameID: {type: mongoose.Schema.Types.ObjectId},
  purchaseBC: {type: Number, required: true, default: 0},
  purchaseBCCreated: {type: Number, required: true, default: 0},
  isImported: {type: Boolean, default: false}
}, {
  strict: true,
  safe: true,
  collection: 'vsTransactionPurchases'
});

vsTransactionPurchaseSchema.post('save', (purchase, next) => {
  const statuses = ['cash_purchased', 'atc_purchased', 'btc_purchased'];
  if(statuses.indexOf(purchase.status) === -1 || purchase.approveDate) {
    return next();
  }

  const PurchaseFrameModel = mongoose.model('VsFrameConfig', FrameModel.schema);
  const WalletModel = mongoose.model('Wallet', wallet.schema);
  const MemberModel = mongoose.model('Member', member.schema);

  async.auto({
    'frame': function (next) {
      PurchaseFrameModel.getCurrentActive(next);
    },
    'activeFrame': ['frame', function (data, next) {
      let activeFrameIndex = data.frame.frames.findIndex((frame) => {return frame.status === 'selling'});
      if(activeFrameIndex === -1) {
        activeFrameIndex = 0;
      }
      next(null, activeFrameIndex);
    }],
    'approve': ['frame', 'activeFrame', function (data, next) {
      const activeFrame = data.frame.frames[data.activeFrame];
      const activeFrameRate = (activeFrame.purchaseRate >= 1) ? (activeFrame.purchaseRate / 100) : activeFrame.purchaseRate;
      purchase.cashRate = (purchase.cashRate && purchase.cashRate !== activeFrameRate) ? purchase.cashRate : activeFrameRate;
      purchase.purchaseQuantity = Math.floor(purchase.purchaseAmount / purchase.cashRate / purchase.purchaseRate);
      purchase.approveDate = Date.now();
      purchase.save(next);
    }],
    'wallet': function (next) {
      WalletModel.findOne({memberID: purchase.memberID}, next);
    },
    'updateWallet': ['wallet', 'approve', function (data, next) {
      if(!data.wallet) {
        return next();
      }

      const transaction = data.approve[0];
      const cashPurchased = transaction.purchaseType === 'cash' ? transaction.purchaseAmount : 0;
      const btcPurchased = transaction.purchaseType === 'BTC' ? transaction.purchaseAmount : 0;
      const atcPurchased = transaction.purchaseType === 'ATC' ? transaction.purchaseAmount : 0;
      data.wallet.vsAmount += transaction.purchaseQuantity;
      data.wallet.cashPurchased += cashPurchased;
      data.wallet.btcPurchased += btcPurchased;
      data.wallet.atcPurchased += atcPurchased;
      data.wallet.lastRate = transaction.cashRate;
      if(!data.wallet.bravePurchased) {
        data.wallet.bravePurchased = 0;
      }
      data.wallet.bravePurchased += Math.floor(transaction.purchaseAmount / transaction.purchaseRate);
      data.wallet.save(next);
    }],
    'member': function (next) {
      MemberModel.findOne({_id: purchase.memberID}, next);
    },
    'memberPurchases': ['member', 'approve', function (data, next) {
      const transaction = data.approve[0];
      const cashPurchased = transaction.purchaseType === 'cash' ? transaction.purchaseAmount : 0;
      const btcPurchased = transaction.purchaseType === 'BTC' ? transaction.purchaseAmount : 0;
      const atcPurchased = transaction.purchaseType === 'ATC' ? transaction.purchaseAmount : 0;

      data.member.purchasePrice += cashPurchased;
      data.member.purchasePriceBTC += btcPurchased;
      data.member.purchasePriceATC += atcPurchased;
      if(!data.member.bravePurchased) {
        data.member.bravePurchased = 0;
      }
      data.member.bravePurchased += Math.floor(transaction.purchaseAmount / transaction.purchaseRate);
      data.member.save(next);
    }],
    'calculateRank': ['member', 'wallet', function (data, next) {
      MemberModel.computeRank(data.member, next);
    }],
  }, next);
});

vsTransactionPurchaseSchema.index({createDate: 1, masterID: 1, status: 1}, {unique: true});

export default mongoose.model('VsTransactionPurchase', vsTransactionPurchaseSchema);
