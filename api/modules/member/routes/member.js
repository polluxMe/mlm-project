import express from 'express';
import mongoose from 'mongoose';
import async from 'async';
import _ from 'lodash';
import {Converter} from 'csvtojson';
import json2csv from 'json2csv';
import moment from 'moment';
import multer from 'multer';
import requireLogin from '../../../middlewares/requireLogin';
import requireAdmin from '../../../middlewares/requireAdmin';
import isCurrentUser from '../../../utils/isCurrentUser';

var routesMember = express.Router();

routesMember
  .get(requireLogin)
  .post(requireLogin)
  .put(requireLogin)
  .delete(requireLogin);

const upload = multer({inMemory: true});

routesMember.get('/list', function (req, res, next) {
  var Member = mongoose.model('Member', req.app.models.members.schema);
  async.auto({
    'members': function (next) {
      Member.find({},{parentId: true, introducerID: true, applicantName: true, email: true, purchasePrice: true, profile: true, commision: true}, next);
    }
  }, function (err, data) {
    if(err) {
      return res.json(err);
    }

    res.json(data.members);
  });
});

routesMember
  .get('/csv', requireAdmin)
  .get('/csv', function (req, res) {
  var Member = mongoose.model('Member', req.app.models.members.schema);
  async.auto({
    'members': (next) => {
      Member.export((err, data) => {
        next(err, data);
      });
    },
    'csv': ['members', (data, next) => {
      const members = data.members;

      if (!members || !members[0]) {
        return next();
      }

      const fields = _.keys(members[0]);

      json2csv({ data: members, fields }, next);
    }]
  }, function (err, data) {
    if(err) {
      return res.json(err);
    }

    res.set({'Content-Disposition':'attachment; filename="members.csv"'});
    res.send(data.csv);
  });
});

routesMember
  .get('/xls', requireAdmin)
  .get('/xls', function (req, res) {
  var Member = mongoose.model('Member', req.app.models.members.schema);
  async.auto({
    'members': (next) => {
      Member.export((err, data) => {
        next(err, data);
      });
    }
  }, function (err, data) {
    if(err) {
      return res.json(err);
    }

    res.xls('members.xls', data.members);
  });
});


routesMember
  .get('/', requireAdmin)
  .get('/', function (req, res) {
    var Model = mongoose.model('Member', req.app.models.members.schema);
    req.app.logger.info('MEMBER', '/', 'req.body', req.body);
    req.app.logger.info('MEMBER', '/', 'req.params', req.params);
    req.app.logger.info('MEMBER', '/', 'req.query', req.query);
    var limit = Number(req.query.limit),
      page = req.query.page,
      skip = ((page-1) * limit),
      search = req.query.search,
      sort = req.query.sort,
      filter = req.query.filter;

  const WalletModel = mongoose.model('Wallet', req.app.models.wallets.schema);
  let queryParams = {};
  if(search) {
    queryParams = {$text: {$search: search}};
  }
  if(filter && filter !== '{}') {
    const filterObj = JSON.parse(filter);
    _.each(filterObj, (value, key) => {
      if(_.isArray(value)) {
        if (value.length) {
          queryParams[key] = {$in: value};
        }
      }
      else {
        queryParams[key] = value;
      }
    });
    req.app.logger.info('MEMBER', '/', 'queryParams', queryParams);
  }
  async.auto({
    'count': function (next) {
      Model.count(queryParams, next);
    },
    'items': ['count', function (data, next) {
      if (data.count === -1) {
        return next(null, []);
      }
      if(search) {
        skip = 0;
      }
      if(sort) {
        let sortOptions = JSON.parse(sort);
        sort = {};
        sort[sortOptions.name] = sortOptions.order;
      }
      Model.find(queryParams, next).skip(skip).limit(limit).sort(sort);
    }],
    'wallets': ['items', function (data, next) {
      if(!data.items) {
        return next(null, []);
      }

      const promises = [];
      const wallets = {};

      _.each(data.items, item => {
        promises.push(new Promise((resolve, reject) => {
          WalletModel.findOne({memberID: item._id}, function (err, wallet) {
            if(err) {
              return reject(err);
            }
            resolve(wallet);
          });
        }));
      });

      Promise
        .all(promises)
        .catch(err => {
          return next(err);
        })
        .then(items => {
          _.each(items, item => {
            wallets[item.memberID] = item;
          });

          next(null, wallets);
        });
    }]
  }, function (err, data) {
    if(err) {
      return res.status(404).send(err);
    }
    if (data.count !== -1) {
      res.set('x-total-count', data.count);
    }
    res.json(data);
  });
});

routesMember.get('/item/:id', function (req, res, next) {
  var Member = mongoose.model('Member', req.app.models.members.schema);
  async.auto({
    'item': function (next) {
      Member.findById(req.params.id, next);
    },
    'isCurrentUser': ['item', (data, next) => {
      isCurrentUser(req, res, data.item, next);
    }]
  }, function (err, data) {
    if(err) {
      return res.json(err);
    }

    res.json(data.item);
  });
});

routesMember
  .get(requireAdmin)
  .get('/item/:id/rank_history', function (req, res, next) {
    var Member = mongoose.model('Member', req.app.models.members.schema);
    const MemberRankTransaction = mongoose.model('MemberRankTransaction', req.app.models.memberRankTransaction.schema);
    async.auto({
      'member': function (next) {
        Member.findById(req.params.id, next);
      },
      'items': ['member', (data, next) => {
        MemberRankTransaction.find({memberID: data.member._id}, next);
      }]
    }, function (err, data) {
      if(err) {
        return res.json(err);
      }

      res.json(data.items);
    });
  });

routesMember.get('/:_id', function (req, res, next) {
  var Member = mongoose.model('Member', req.app.models.members.schema);

  async.auto({
    'parent': (next) => {
      Member.findById(req.params._id, next);
    },
    'isCurrentUser': ['parent', (data, next) => {
      isCurrentUser(req, res, data.parent, next);
    }],
    'usedMembers': ['parent', 'isCurrentUser', (data, next) => {
      if(!data.parent) {
        return next('ユーザーが見つかりませんでした！');
      }

      data.parent.getChildren({limit: 200}, next);
    }],
    'members': ['parent', (data, next) => {
      Member.find({}, {applicantName: true, applicantID: true, introducerID: true}, next);
    }],
    'tree': ['members', 'parent', (data, next) => {
      let member = {
        _id: data.parent._id.toString(),
        applicantID: data.parent.applicantID,
        introducerID: data.parent.introducerID,
        applicantName: data.parent.applicantName
      };

      const members = _.map(data.members, item => {
        return {
          _id: item._id.toString(),
          applicantID: item.applicantID,
          applicantName: item.applicantName,
          introducerID: item.introducerID
        }
      });

      const unflatten = function( array, parent ){
        let tree = [];

        const children = _.filter( array, function(child){ return child.introducerID && child.introducerID === parent.applicantID; });

        if( !_.isEmpty( children )  ){
          if( parent.applicantID === member.applicantID ){
            tree = children;
          } else {
            parent.children = children;
          }
          _.each( children, ( child ) => { unflatten( array, child ) } );
        }

        return tree;
      };

      next(null, unflatten( members, member ));
    }],
    'plain': ['tree', 'usedMembers', (data, next) => {
      let plain = [];
      const tree = _.clone(data.tree);
      const usedMembersId = data.usedMembers && _.map(data.usedMembers, item => item._id.toString());

      const flatten = (item, arr) => {
        const children = _.clone(item.children);

        delete item.children;

        arr.push(item);

        if (children) {
          _.each(children, (item) => flatten(item, arr));
        }
      };

      _.each(tree, (item) => flatten(item, plain));

      plain = _.filter(plain, item => ~_.indexOf(usedMembersId, item._id.toString()) === 0);

      next(null, plain);
    }]
  }, function (err, data) {
    if(err) {
      return res.json(err);
    }

    res.json(data.plain);
  });
});

routesMember
  .put('/:_id/status', requireAdmin)
  .put('/:_id/status', function (req, res, next) {
    var Member = mongoose.model('Member', req.app.models.members.schema);
    const tempPwd = Math.random().toString(36).slice(-8);

    async.auto({
      'member': (next) => {
        Member.findById(req.params._id, next)
      },
      'updatedUser': ['member', function (data, next) {
        if(!data.member) {
          return next('このIDを持つ会員記録が見つかりませんでした' + req.params._id);
        }

        const member = data.member;
        member.status = req.body.status;

        if (!data.member.isChangeId && !data.member.emailNotice) {
          member.password = tempPwd;
        }

        member.save(next);
      }],
      'mail': ['member', (data, next) => {
        const member = data.member;

        if (!data.member.isChangeId && !data.member.emailNotice) {
          const siteUrl = req.app.config.siteUrl;
          req.app.services.mail.sendTemplate('invitation', member.email, {id: member.applicantID, password: tempPwd, name: member.applicantName, siteUrl: siteUrl}, next);
        }
        else {
          next(null, null);
        }
      }],
      'secondlyUpdated': ['updatedUser', (data, next) => {
        const member = data.updatedUser[0];

        if(member.emailNotice) {
          next(null, data.updatedUser);
        }

        member.emailNotice = true;

        member.save(next);
      }]
    }, function (err, data) {
      if(err) {
        return res.json(err);
      }

      res.json(data.secondlyUpdated[0]);
    });
  });

routesMember
  .put('/:_id/writtenNotice', requireAdmin)
  .put('/:_id/writtenNotice', function (req, res, next) {
    var Member = mongoose.model('Member', req.app.models.members.schema);

    async.auto({
      'member': (next) => {
        Member.findById(req.params._id, next)
      },
      'updatedUser': ['member', function (data, next) {
        if(!data.member) {
          return next('このIDを持つ会員記録が見つかりませんでした' + req.params._id);
        }

        const member = data.member;
        member.writtenNotice = req.body.writtenNotice;

        member.save(next);
      }]
    }, function (err, data) {
      if(err) {
        return res.json(err);
      }

      res.json(data.updatedUser[0]);
    });
  });

routesMember.put('/', function (req, res, next) {
  var Member = mongoose.model('Member', req.app.models.members.schema);
  async.auto({
    'member': (next) => {
      Member.findById(req.body._id, next)
    },
    'isCurrentUser': ['member', (data, next) => {
      isCurrentUser(req, res, data.member, next);
    }],
    'updatedUser': ['member', 'isCurrentUser', function (data, next) {
      if(!data.member) {
        return next('このIDを持つ会員記録が見つかりませんでした' + req.body._id);
      }

      const member = data.member;

      member.applicantName = req.body.applicantName;
      member.applicantKatakana = req.body.applicantKatakana;
      member.nickname = req.body.nickname;
      member.email = req.body.email;
      member.profile.nationality = req.body.profile.nationality;
      member.profile.gender = req.body.profile.gender;
      member.profile.dateOfBirth = req.body.profile.dateOfBirth;
      member.profile.address = req.body.profile.address;
      member.profile.mobilePhone = req.body.profile.mobilePhone;
      member.profile.phone = req.body.profile.phone;
      member.profile.passport = req.body.profile.passport;
      member.bankInformation = req.body.bankInformation;
      member.note = req.body.note;

      if (req.body.password) {
        member.password = member.generateHash(req.body.password);
      }

      if (req.body.rank) {
        member.rank = +req.body.rank;

        const gCloudRanks = {
          5: 'EXE',
          6: '2EXE',
          7: '3EXE',
          8: '4EXE',
          9: 'REXE',
          10: 'SP'
        };

        if(member.gCloudRank) {
          member.gCloudRank = undefined;
        }
        const gCloudRank = gCloudRanks[member.rank];

        if(gCloudRank) {
          member.gCloudRank = gCloudRank;
        }
        member.isImported = true;
      }

      if (req.body.note) {
        member.profile.note = req.body.note;
      }

      member.save(next);
    }]
  }, function (err, data) {
    if(err) {
      return res.json(err);
    }

    res.json(data.updatedUser[0]);
  });
});

routesMember
  .put('/item/:id/rank_history', requireAdmin)
  .put('/item/:id/rank_history', function (req, res, next) {
    var Member = mongoose.model('Member', req.app.models.members.schema);
    const MemberRankTransaction = mongoose.model('MemberRankTransaction', req.app.models.memberRankTransaction.schema);
    async.auto({
      'member': function (next) {
        Member.findById(req.params.id, next);
      },
      'items': ['member', (data, next) => {
        MemberRankTransaction.find({memberID: data.member._id}, next);
      }],
      'updatedItems': ['items', (data, next) => {
        const newItems = req.body.notes;
        const items = data.items;
        const promises = [];

        _.each(items, item => {
          if (item._id.toString() !== item.note) {
            item.note = newItems[item._id.toString()];

            promises.push(new Promise((resolve, reject) => {
              item.save(function (err, response) {
                if(err) {
                  return reject(err);
                }
                resolve(response);
              });
            }));
          }
        });

        Promise
          .all(promises)
          .catch(err => {
            return next(err);
          })
          .then(items => {
            next(null, items);
          });
      }]
    }, function (err, data) {
      if(err) {
        return res.json(err);
      }

      res.json(data.updatedItems);
    });
  });

routesMember.get('/:_id/tree', function (req, res, next) {
  var Member = mongoose.model('Member', req.app.models.members.schema);
  async.auto({
    'parent': function (next) {
      Member.findById(req.params._id, next);
    },
    'isCurrentUser': ['parent', (data, next) => {
      isCurrentUser(req, res, data.parent, next);
    }],
    'tree': ['parent', 'isCurrentUser', (data, next) => {
      if(!data.parent) {
        return next('ユーザーが見つかりませんでした！');
      }
      data.parent.getArrayTree({limit: 200, fields: {
        applicantID: true,
        introducerID: true,
        _id:true,
        path: true,
        children: true,
        isLeft: true,
        isRight: true,
        parentId: true,
        email: true,
        applicantName: true,
        commision: true
      }}, next);
    }],
    // 'wallets': ['members', function (data, next) {
    //   const wallets = data.members.map((record) => {
    //     Wallet.findOne({memberID: record._id}, function (err, data) {
    //       if(err) {
    //         return next(err);
    //       }
    //       record.wallet = data;
    //       console.log('data', data);
    //       return record;
    //     })
    //   });
    //   next(null, wallets);
    // }]
  }, function (err, data) {
    if(err) {
      return res.json(err);
    }

    res.json(data.tree);
  });
});

routesMember.get('/:_id/unilevelMap', function (req, res, next) {
  var Member = mongoose.model('Member', req.app.models.members.schema);
  async.auto({
    'parent': function (next) {
      Member.findById(req.params._id, next);
    },
    'isCurrentUser': ['parent', (data, next) => {
      isCurrentUser(req, res, data.parent, next);
    }],
    'members': ['parent', 'isCurrentUser', (data, next) => {
      if(!data.parent) {
        return next('ユーザーが見つかりませんでした！');
      }

      Member.find({}, {applicantID: true, introducerID: true, applicantName: true, rank: true, nickname: true}, next);
    }],
    'tree': ['members', 'parent', (data, next) => {
      let member = {
        applicantID: data.parent.applicantID,
        introducerID: data.parent.introducerID,
        applicantName: data.parent.applicantName,
        rank: data.parent.rank,
        nickname: data.parent.nickname,
        level: 1
      };

      const members = _.map(data.members, item => {
        return {
          applicantID: item.applicantID,
          introducerID: item.introducerID,
          applicantName: item.applicantName,
          nickname: item.nickname,
          rank: item.rank
        }
      });

      let depth = 1;
      const unflatten = function( array, parent ){
        let tree = [];

        const children = _.filter( array, function(child){ return child.introducerID == parent.applicantID; });

        _.each(children, item => {
          item.level = parent.level+1;
        });

        if( !_.isEmpty( children )  ){
          depth = parent.level > depth ? parent.level : depth;

          if( parent.applicantID == member.applicantID ){
            tree = children;
          } else {
            parent.children = children;
            parent.width = parent.children.length;
          }
          _.each( children, ( child ) => { unflatten( array, child ) } );
        }

        return tree;
      };

      member.children = unflatten( members, member );
      member.levels = depth;

      next(null, [member]);
    }],
    'plain': ['tree', ({ tree }, next) => {
      let plainBuff = [];
      const treeToPlain = (node) => {
        if(node && node.children && node.children.length){
          _.each(node.children, (el) => {
            el.introducerID = node.applicantID || null;

            if(el){
              plainBuff.push(el);

              if(el)
                treeToPlain(el);
            }
          });
        }
      };

      treeToPlain(tree[0]);

      plainBuff = _.map(plainBuff, item => _.pickBy(item, (value, key) => key !== 'children'));

      next(null, plainBuff);
    }],
    'width': ['plain', ({ plain }, next) => {
      const groupedObjects = _.groupBy(plain, 'level');
      const sums = [];
      _.each(groupedObjects, (items, key) => {
        sums[key] = items.length;
      });
      next(null, _.max(sums));
    }]
  }, function (err, { tree, width }) {
    if(err) {
      return res.json(err);
    }

    tree[0].width = width;

    res.json(tree);
  });
});

routesMember.post('/:_id/ancestor', function (req, res, next) {
  var Member = mongoose.model('Member', req.app.models.members.schema);

  req.app.logger.info('MEMBER', '/:_id/ancestor', 'req.params', req.params);
  req.app.logger.info('MEMBER', '/:_id/ancestor', 'req.body', req.body);

  async.auto({
    'parent': function (next) {
      Member.findOne({'_id': req.params._id}, (err, data) => {
        req.app.logger.info('MEMBER', '/:_id/ancestor', 'data.parent', data && data._doc);
        next(err, data);
      });
    },
    'isCurrentUser': ['parent', (data, next) => {
      isCurrentUser(req, res, data.parent, next);
    }],
    'child': ['isCurrentUser', function(data, next) {
      Member.findOne({'_id': req.body._id}, (err, data) => {
        req.app.logger.info('MEMBER', '/:_id/ancestor', 'data.set', data && data._doc);
        next(err, data);
      });
    }],
    'set': ['child', 'parent', function (data, next) {
      // data.child.save({ancestors: ancestors, parent: data.ancestor._id, isLeft: req.body.isLeft || false, isRight: req.body.isRight || false}, next);
      data.parent.appendChild(data.child, function(err, child){
        data.child.parentId = data.parent._id;
        data.child.isLeft = req.body.isLeft || (!req.body.isRight ? true : false);
        data.child.isRight = req.body.isRight || false;

        data.child.save((err, data) => {
          req.app.logger.info('MEMBER', '/:_id/ancestor', 'data.set', data && data._doc);
          next(err, data);
        });
      });
    }]
  }, function (err, data) {
    if(err) {
      req.app.logger.info('MEMBER', '/:_id/ancestor', 'error', err);
      return res.json(err);
    }

    req.app.logger.info('MEMBER', '/:_id/ancestor', 'end');

    res.json(data.set);
  });
});

routesMember.delete('/:_id/remove', function (req, res) {
  var Member = mongoose.model('Member', req.app.models.members.schema);

  req.app.logger.info('MEMBER', '/:_id/remove', 'req.params', req.params);
  req.app.logger.info('MEMBER', '/:_id/remove', 'req.body', req.body);

  async.auto({
    'parent': (next) => {
      Member.findById(req.params._id, (err, data) => {
        req.app.logger.info('MEMBER', '/:_id/remove', 'data.parent', data && data._doc);
        next(err, data);
      })
    },
    'isCurrentUser': ['parent', (data, next) => {
      isCurrentUser(req, res, data.parent, next);
    }],
    'children': ['parent', 'isCurrentUser', (data, next) => {
      data.parent.getChildren({limit: 200}, (err, data) => {
        req.app.logger.info('MEMBER', '/:_id/remove', 'data.children', data && data._doc);
        next(err, data);
      });
    }],
    'members': ['children', function (data, next) {
      let ids = [];
      if (data.parent) {
        ids = _.map(data.children, item => item._id);
      }
      ids.push(req.params._id);

      Member.update({_id: {$in: ids}}, {parentId: null, path: ""}, (err, data) => {
        req.app.logger.info('MEMBER', '/:_id/remove', 'data.members', data && data._doc);
        next(err, data);
      });
    }]
  }, function (err, data) {
    if(err) {
      req.app.logger.info('MEMBER', '/:_id/remove', 'error', err);
      return res.json(err);
    }

    req.app.logger.info('MEMBER', '/:_id/remove', 'end');

    res.json(data);
  });
});

routesMember.post('/invitation', function (req, res, next) {
  const siteUrl = req.app.config.siteUrl + '/?introducerID=' + (req.user && req.user.applicantID) + '&introducerName=' + (req.user && req.user.applicantName);
  req.app.services.mail.sendTemplate('memberInvitation', req.body.to, {title: req.body.title, message: req.body.message, username: req.user && req.user.email, siteUrl: siteUrl, id: req.user.applicantID}, function (err) {
    if (err) return next(err);
    res.json({message: '招待状は正常に送信できました。'});
  });
});

const type = upload.single('csv');
routesMember
  .post('/csv', requireAdmin)
  .post('/csv', type, function (req, res, next) {
    const Member = mongoose.model('Member', req.app.models.members.schema);
    const WalletModel = mongoose.model('Wallet', req.app.models.wallets.schema);
    const Purchase = mongoose.model('VsMasterPurchase', req.app.models.vsMasterPurchases.schema);
    const TransactionModel = mongoose.model('VsTransactionPurchase', req.app.models.vsTransactionPurchases.schema);

    const converter = new Converter({ignoreEmpty: true});

    const csvString = req.file.buffer.toString();

    async.auto({
      'json': (next) => {
        req.app.logger.info('MEMBER', '/csv (POST)', 'json start');
        converter.fromString(csvString, function(err,result){
          req.app.logger.info('MEMBER', '/csv (POST)', 'json end');
          return next(err, result);
        });
      },
      'convertedData': ['json', (data, next) => {
        req.app.logger.info('MEMBER', '/csv (POST)', 'convertedData start');
        let convertedData = [];

        _.each(data.json, (item) => {
          let purchaseType = 'cash';
          const dateOfBirth = !!item['Birthday'] && moment.utc(item['Birthday'], "MM/DD/YYYY") && new Date(moment.utc(item['Birthday'], "MM/DD/YYYY"));
          const memberCreateDate = !!item['Apply date'] && moment.utc(item['Apply date'], "MM/DD/YYYY") && new Date(moment.utc(item['Apply date'], "MM/DD/YYYY"));
          const purchaseCreateDate = !!item['Payment date'] && moment.utc(item['Payment date'], "MM/DD/YYYY") && new Date(moment.utc(item['Payment date'], "MM/DD/YYYY"));

          if(item['Purchase money amount (ATC)']) {
            purchaseType = 'ATC';
          }

          if(item['Purchase money amount (Bit)']) {
            purchaseType = 'BTC';
          }

          let purchaseMoneyAmount = item['Purchase money amount (Yen)'] + '';
          purchaseMoneyAmount = purchaseMoneyAmount.replace(/,/g, '');
          purchaseMoneyAmount = +purchaseMoneyAmount;
          purchaseMoneyAmount = Number.isNaN(purchaseMoneyAmount) ? 0 : purchaseMoneyAmount;

          let purchaseMoneyAmountATC = item['Purchase money amount (ATC)'] + '';
          purchaseMoneyAmountATC = purchaseMoneyAmountATC.replace(/,/g, '');
          purchaseMoneyAmountATC = +purchaseMoneyAmountATC;
          purchaseMoneyAmountATC = Number.isNaN(purchaseMoneyAmountATC) ? 0 : purchaseMoneyAmountATC;

          let purchaseMoneyAmountBit = item['Purchase money amount (Bit)'] + '';
          purchaseMoneyAmountBit = purchaseMoneyAmountBit.replace(/,/g, '');
          purchaseMoneyAmountBit = +purchaseMoneyAmountBit;
          purchaseMoneyAmountBit = Number.isNaN(purchaseMoneyAmountBit) ? 0 : purchaseMoneyAmountBit;

        // let applicantID = item['G-CLOUD ID'] || Math.random().toString(36).replace('.', '').slice(-16).toUpperCase();
        let applicantID = item['G-CLOUD ID'] || item['ID'];
        const tempLogin = Math.random().toString(36).replace('.', '').slice(-16).toUpperCase();

        let gCloudRank = (item['G-CLOUD Rank'] && item['G-CLOUD Rank'].trim().replace('ロイヤルEXE', 'REXE').replace('なし', ''));
        const gCloudRanks = {
          'EXE': 5,
          '2EXE': 6,
          '3EXE': 7,
          '4EXE': 8,
          'REXE': 9,
          'SP': 10
        };
        let rank = gCloudRanks[gCloudRank] || 0;

          const tempPassword = Math.random().toString(36).slice(-8);

        const convertedItem = {
          accountType: !!item['G-CLOUD ID'] ? 'gcloud' : 'brave',
          applicantID: applicantID,
          username: tempLogin,
          applicantName: item['Name'],
          applicantKatakana: item['Furigana'],
          introducerID: item['Introducer ID'],
          introducerName: item['Introducer Name'],
          nickname: item['Nickname'],
          email: item['Email'],
          earnings: 0,
          commision: 0,
          profile: {
            gender: item['Sex'] === '女性' ? 'female' : 'male',
            address: item['Address'],
            phone: item['Phone'],
            mobilePhone: item['Mobile'],
            nationality: item['National'],
            postal: item['Postal code'],
            passport: item['Identification'],
          },
          purchaseType: purchaseType,
          purchasePrice: purchaseMoneyAmount,
          purchasePriceATC: purchaseMoneyAmountATC,
          purchasePriceBTC: purchaseMoneyAmountBit,
          bravePurchased: 0,
          rate: +item['Purchase rate'],
          rank,
          tempPassword,
          importID: item['ID'],
          isImported: true
        };

          if (gCloudRank) {
            convertedItem.gCloudRank = gCloudRank;
          }

          if (dateOfBirth && dateOfBirth instanceof Date && isFinite(dateOfBirth) && !Number.isNaN( dateOfBirth.getTime())) { // there is some magic
            convertedItem.profile.dateOfBirth = dateOfBirth;
          }

          if (memberCreateDate && memberCreateDate instanceof Date && isFinite(memberCreateDate) && !Number.isNaN( memberCreateDate.getTime())) { // there is some magic
            convertedItem.memberCreateDate = memberCreateDate;
          }

          if (purchaseCreateDate && purchaseCreateDate instanceof Date && isFinite(purchaseCreateDate) && !Number.isNaN( purchaseCreateDate.getTime())) { // there is some magic
            convertedItem.purchaseCreateDate = purchaseCreateDate;
          }

          const duplicatedRecord = _.find(convertedData, {applicantID: convertedItem.applicantID});
          const hasChildren = _.find(convertedData, {introducerID: convertedItem.applicantID});
          if (duplicatedRecord && !hasChildren) {
            convertedItem.applicantID = Math.random().toString(36).replace('.', '').slice(-16).toUpperCase();
          }

          convertedData.push(convertedItem);

        });

        req.app.logger.info('MEMBER', '/csv (POST)', 'convertedData end');

        return next(null, convertedData);
      }],
      'convertedDataWithPurchases': ['convertedData', (data, next) => {
        req.app.logger.info('MEMBER', '/csv (POST)', 'convertedDataWithPurchases start');
        const purchaseRate = 130;

      const groupedData = _.groupBy(data.convertedData, 'importID');
      const convertedDataWithPurchases = _.map(groupedData, item => {
        const newItem = _.first(item);
        newItem.purchases = _.map(item, groupedItem => {
          const purchaseAmount = groupedItem.purchaseType === 'cash' ? groupedItem.purchasePrice : (
            groupedItem.purchaseType === 'ATC' ? groupedItem.purchasePriceATC : (
              groupedItem.purchaseType === 'BTC' ? groupedItem.purchasePriceBTC : 0
            )
          );
          const purchaseQuantity = purchaseAmount && groupedItem.rate && purchaseRate ? Math.floor(purchaseAmount / (groupedItem.rate / 100) / purchaseRate) : 0;
          return {
            purchaseAmount: purchaseAmount,
            purchaseType: groupedItem.purchaseType,
            purchaseRate: purchaseRate,
            cashRate: groupedItem.rate / 100,
            purchaseQuantity:  purchaseQuantity,
            createDate: groupedItem.purchaseCreateDate,
            approveDate: groupedItem.purchaseCreateDate
          }
        });

          newItem.purchases = _.reject(newItem.purchases, item => !item.purchaseQuantity || !item.createDate);

          return newItem;
        });
        req.app.logger.info('MEMBER', '/csv (POST)', 'convertedDataWithPurchases end');

      return next(null, convertedDataWithPurchases);
    }],
    'validateFile': ['convertedDataWithPurchases', (data, next) => {
      const members = data.convertedDataWithPurchases;

      const chunksNumber = Math.ceil(members.length / 600);
      if (members.length && members.length > 600) {
        return next({code: 500, msg: 'ファイルの行数を少なくとも600に減少して、または ' + chunksNumber + ' チャンクにファイルを分割し'});
      }

      next();
    }],
    'existMembers': ['convertedDataWithPurchases', 'validateFile', (data, next) => {
      req.app.logger.info('MEMBER', '/csv (POST)', 'existMembers start');
      const members = data.convertedDataWithPurchases;
      const applicantIDs = _.map(members, item => item.applicantID);

      Member.find({
        'applicantID':{$in: applicantIDs}
      }, (err, doc) => {
        if (err) {
          return next(err);
        }

        req.app.logger.info('MEMBER', '/csv (POST)', 'existMembers end');

        return next(null, doc);
      });
    }],
    'members': ['convertedDataWithPurchases', 'existMembers', (data, next) => {
      req.app.logger.info('MEMBER', '/csv (POST)', 'members start');
      const convertedDataWithPurchases = data.convertedDataWithPurchases;
      const existMembers = data.existMembers;
      const members = [];

      _.each(convertedDataWithPurchases, item => {
        const record = _.find(existMembers, existItem => existItem.applicantID == item.applicantID);

        if (record) {
          return;
        }

        const memberRegister = new Member(item);

        memberRegister.purchasePrice = 0;
        memberRegister.purchasePriceBTC = 0;
        memberRegister.purchasePriceATC = 0;

        memberRegister.password = memberRegister.generateHash(item.tempPassword);

        if (item.memberCreateDate) {
          memberRegister.createDate = item.memberCreateDate;
        }

        members.push(memberRegister);
      });

      Member.create(members, (err, data) => {
        req.app.logger.info('MEMBER', '/csv (POST)', 'members end');
        let result = existMembers;
        if (data) {
          result = _.concat(result, data);
        }
        next(err, result);
      });
    }],
    'wallets': ['members', (data, next) => {
      req.app.logger.info('MEMBER', '/csv (POST)', 'wallets start');
      const members = data.members;
      const membersIds = _.map(members, item => item._id.toString());

      WalletModel.find({memberID: {$in: membersIds}}, (err, data) => {
        req.app.logger.info('MEMBER', '/csv (POST)', 'wallets end');
        next(err, data);
      });
    }],
    'purchases': ['convertedDataWithPurchases', 'members', (data, next) => {
      req.app.logger.info('MEMBER', '/csv (POST)', 'purchases start');
      const members = data.members;
      let convertedDataWithPurchases = {};

      data.convertedDataWithPurchases.forEach(dataConvert => {
        convertedDataWithPurchases[dataConvert.applicantID] = dataConvert;
      });

      const promises = [];

      _.each(members, (value, key) => {
        if (!convertedDataWithPurchases[value.applicantID] || !convertedDataWithPurchases[value.applicantID].purchases) {
          return;
        }

        const purchases = convertedDataWithPurchases[value.applicantID].purchases;

        _.each(purchases, purchaseItem => {
          purchaseItem.memberID = value._id.toString();
          purchaseItem.isImported = true;
          const purchase = new Purchase(purchaseItem);

          var error = purchase.validateSync();

          if(error) {
            return next(error);
          }

          promises.push(new Promise((resolve, reject) => {
            Purchase.create(purchase, function (err, response) {
              if(err) {
                return reject(err);
              }
              resolve(response);
            });
          }));
        });
      });


      Promise
        .all(promises)
        .catch(err => {
          return next(err);
        })
        .then(purchases => {
          req.app.logger.info('MEMBER', '/csv (POST)', 'purchases end');
          next(null, purchases);
        });
    }],
    'purchaseOrdersTransactions': ['purchases', (data, next) => {
      req.app.logger.info('MEMBER', '/csv (POST)', 'purchaseOrdersTransactions start');
      const purchases = data.purchases;
      const purchasesIds = _.map(purchases, item => item._id.toString());

      TransactionModel.find({masterID: {$in: purchasesIds}}, (err, data) => {
        req.app.logger.info('MEMBER', '/csv (POST)', 'purchaseOrdersTransactions end');
        next(err, data);
      });
    }],
    'purchasePurchasedTransactions': ['purchaseOrdersTransactions', (data, next) => {
      req.app.logger.info('MEMBER', '/csv (POST)', 'purchasePurchasedTransactions start');
      const orderTransactions = data.purchaseOrdersTransactions;

      const purchasedStatuses = {'cash': 'cash_purchased', 'ATC': 'atc_purchased', 'BTC': 'btc_purchased'};

      const purchasedTransactions = _.map(orderTransactions, transaction => {
        const newTransaction = new TransactionModel(_.omit(Object.assign({}, transaction._doc), '_id'));
        newTransaction.status = purchasedStatuses[transaction.purchaseType];
        newTransaction.cashRate = newTransaction.cashRateCreated;
        newTransaction.purchaseQuantity = newTransaction.purchaseQuantityCreated;
        newTransaction.createDate = new Date(moment(newTransaction.createDate).add(1, 'second'));
        newTransaction.createOrderDate = new Date(moment(newTransaction.createDate).add(1, 'second'));
        newTransaction.approveDate = new Date(moment(newTransaction.createDate).add(1, 'second'));
        newTransaction.isImported = true;

        return newTransaction;
      });

      TransactionModel.create(purchasedTransactions, (err, data) => {
        req.app.logger.info('MEMBER', '/csv (POST)', 'purchasePurchasedTransactions end');
        next(err, data);
      });
    }],
    'updatedWallets': ['purchasePurchasedTransactions', 'wallets', (data, next) => {
      req.app.logger.info('MEMBER', '/csv (POST)', 'updatedWallets start');
      const transactions = data.purchasePurchasedTransactions;
      const wallets = data.wallets;

      const memberTransactions = _.groupBy(transactions, 'memberID');

      _.each(memberTransactions, (value, key) => {
        const wallet = _.find(wallets, {memberID: key});

        if (!wallet) {
          return;
        }

        wallet.cashPurchased += _.sumBy(value, item => item.purchaseType === 'cash' ? item.purchaseAmount : 0);
        wallet.btcPurchased += _.sumBy(value, item => item.purchaseType === 'BTC' ? item.purchaseAmount : 0);
        wallet.atcPurchased += _.sumBy(value, item => item.purchaseType === 'ATC' ? item.purchaseAmount : 0);
        wallet.vsAmount += _.sumBy(value, 'purchaseQuantity');
        wallet.lastRate += _.findLast(value).cashRate;
        wallet.bravePurchased += _.sumBy(value, item =>
          Math.floor(item.purchaseAmount / item.purchaseRateCreated)
        );
      });

      const promises = [];
      _.each(wallets, item => {
        promises.push(new Promise((resolve, reject) => {
          item.save((err, wallet) => {
            if(err) {
              return reject(err);
            }
            resolve(wallet);
          });
        }));
      });

      Promise
        .all(promises)
        .catch(err => {
          return next(err);
        })
        .then((updatedWallets) => {
          req.app.logger.info('MEMBER', '/csv (POST)', 'updatedWallets end');
          return next(null, updatedWallets);
        });
    }],
    'updatedMembers': ['purchasePurchasedTransactions', 'members', (data, next) => {
      req.app.logger.info('MEMBER', '/csv (POST)', 'updatedMembers start');
      const transactions = data.purchasePurchasedTransactions;
      const members = data.members;

      const memberTransactions = _.groupBy(transactions, 'memberID');

      _.each(memberTransactions, (value, key) => {
        const member = _.find(members, {_id: mongoose.Types.ObjectId(key)});
        if (!member) {
          return next();
        }

        member.purchasePrice += _.sumBy(value, item => item.purchaseType === 'cash' ? item.purchaseAmount : 0);
        member.purchasePriceBTC += _.sumBy(value, item => item.purchaseType === 'BTC' ? item.purchaseAmount : 0);
        member.purchasePriceATC += _.sumBy(value, item => item.purchaseType === 'ATC' ? item.purchaseAmount : 0);
        member.bravePurchased += _.sumBy(value, item =>
          Math.floor(item.purchaseAmount / item.purchaseRateCreated)
        );
        member.status = 'official';
      });

      const promises = [];
      _.each(members, item => {
        promises.push(new Promise((resolve, reject) => {
          item.save((err, member) => {
            if(err) {
              return reject(err);
            }
            resolve(member);
          });
        }));
      });

      Promise
        .all(promises)
        .catch(err => {
          return next(err);
        })
        .then((updatedMembers) => {
          req.app.logger.info('MEMBER', '/csv (POST)', 'updatedMembers end');
          return next(null, members);
        });
    }],
    'computeRanks': ['updatedMembers', (data, next) => {
      req.app.logger.info('MEMBER', '/csv (POST)', 'computeRanks start');
      const members = data.updatedMembers;
      const promises = [];
      _.each(members, member => {
        promises.push(new Promise((resolve, reject) => {
          Member.computeRank(member, (err) => {
            if(err) {
              return reject(err);
            }
            resolve();
          });
        }));
      });

      Promise
        .all(promises)
        .catch(err => {
          return next(err);
        })
        .then(() => {
          req.app.logger.info('MEMBER', '/csv (POST)', 'computeRanks end');
          next();
        });
    }]
  }, function (err, data) {
    if(err) {
      console.log(err);
      return res.status(err.code || 500).send(err);
    }

    res.json(data.updatedMembers);
  });
});

routesMember.get('/:_id/wallet', function (req, res, next) {
  const Member = mongoose.model('Member', req.app.models.members.schema);
  const Wallet = mongoose.model('Wallet', req.app.models.wallets.schema);

  async.auto({
    'member': function (next) {
      Member.findById(req.params._id, next);
    },
    'isCurrentUser': ['member', (data, next) => {
      isCurrentUser(req, res, data.member, next);
    }],
    'wallet': ['member', 'isCurrentUser', function (data, next) {
      if(!data.member) {
        return next('Member is not found.');
      }
      Wallet.findOne({memberID: data.member._id}, next);
    }]
  }, function (err, data) {
    if(err) {
      return next(err);
    }
    res.json(data.wallet);
  });
});

routesMember
  .post('/:_id/calculate', requireAdmin)
  .post('/:_id/calculate', function (req, res, next) {
    const Member = mongoose.model('Member', req.app.models.members.schema);
    async.auto({
      'member': function (next) {
        Member.findById(req.params._id, next);
      },
      'commission': ['member', function (data, next) {
        Member.mainCommission(data.member, next);
      }],
      'matchingBonus': ['member', 'commission', function (data, next) {
        Member.matchingBonus(data.member, next);
      }],
      'cashReward': ['member', 'commission', function (data, next) {
        Member.cashReward(data.member, next);
      }]
    }, function (err, data) {
      if(err) {
        return next(err);
      }
      res.json(data);
    });
  });

routesMember.get('/:_id/commission', function (req, res, next) {
  const Member = mongoose.model('Member', req.app.models.members.schema);
  var CashRewardTransaction = mongoose.model('TransactionCashRewardRequest', req.app.models.transactionCashRewardRequest.schema);
  var MatchingBonusTransaction = mongoose.model('TransactionMatchingBonusRequest', req.app.models.transactionMatchingBonusRequest.schema);
  var MainCommissionTransaction = mongoose.model('TransactionCommissionRequest', req.app.models.transactionCommissionRequest.schema);
  var TransactionModel = mongoose.model('VsTransactionPurchase', req.app.models.vsTransactionPurchases.schema);
  var Purchase = mongoose.model('VsMasterPurchase', req.app.models.vsMasterPurchases.schema);

  async.auto({
    'member': function (next) {
      Member.findById(req.params._id, next);
    },
    'isCurrentUser': ['member', (data, next) => {
      isCurrentUser(req, res, data.member, next);
    }],
    'siblings': ['member', 'isCurrentUser', function (data, next) {
      data.member.getChildren({limit:10,
        condition: { parentId: data.member._id}
      }, next);
    }],
    'left': ['siblings', function (data, next) {
      let leftBranch = data.siblings.filter((leaf) => {return leaf.isLeft});
      if(!leftBranch.length) {
        return next();
      }
      let leftLeaf = leftBranch[0];
      leftLeaf.getChildren({limit:1000}, function (err, childs) {
        childs = childs.concat(leftLeaf);
          if (err) {
            return next(err);
          }
          let commissions = childs.map((child) => {
            return child.commision;
          });

          next(null, {count: childs.length, sum: _.sum(commissions)});
        });
    }],
    'right': ['siblings', function (data, next) {
      let rightBranch = data.siblings.filter((leaf) => {return leaf.isRight});
      if(!rightBranch.length) {
        return next();
      }
      let rightLeaf = rightBranch[0];
      rightLeaf.getChildren({limit:1000}, function (err, childs) {
        childs = childs.concat(rightLeaf);
        if (err) {
          return next(err);
        }

        let commissions = childs.map((child) => {
          return child.commision;
        });
        next(null, {count: childs.length, sum: _.sum(commissions)});
      });
    }],
    'commission': ['member', function (data, next) {
      if(!req.query.isGuess) {
        MainCommissionTransaction.findOne({
          reportMonth: req.query.paramMonth,
          reportYear: req.query.paramYear,
          memberID: data.member._id,
          status: {$ne: 'removed'}
        }, next);
      } else {
        Member.mainCommission(data.member, function (err, response) {
          if(err) {
            return next(err);
          }
          next(null, response.commission);
        }, true);
      }
    }],
    'cashReward': ['member', function (data, next) {
      if(!req.query.isGuess) {
        const findDate = (req.query.paramYear && req.query.paramMonth) ? moment(req.query.paramYear + '-' + req.query.paramMonth) : moment();
        const startDay = findDate.startOf('month').add(1, 'day').toISOString();
        const endDay = findDate.endOf('month').toISOString();
        CashRewardTransaction.find({
          memberID: data.member._id,
          createDate: {$gte: startDay, $lte: endDay},
          status: {$ne: 'removed'}
          // reportMonth: req.query.paramMonth,
          // reportYear: req.query.paramYear,
        }, function (err, data) {
          if(err) {
            return next(err);
          }
          let rewardSum = 0;
          data.forEach((reward) => {
            rewardSum += parseInt(reward.amount);
          });
          next(null, rewardSum);
        });
      } else {
        var CashRewardPrognose = mongoose.model('PrognosedCashRewardRequest', req.app.models.prognosedCashRewardRequestModel.schema);
        CashRewardPrognose.find({
          memberID: data.member._id,
          reportMonth: req.query.paramMonth,
          reportYear: req.query.paramYear
        }, function (err, response) {
          if(err) {
            return next(err);
          }
          let rewardSum = 0;
          response.forEach((reward) => {
            rewardSum += parseInt(reward.amount);
          });
          next(null, rewardSum);
        }, true);
      }
    }],
    'purchases': ['member', function (data, next) {
      const findDate = (req.query.paramYear && req.query.paramMonth) ? moment(req.query.paramYear + '-' + req.query.paramMonth) : moment();
      const startDay = findDate.startOf('month').add(1, 'day').toISOString();
      const endDay = findDate.endOf('month').toISOString();
      console.log(startDay, endDay);
      Purchase.find({
        memberID: data.member._id,
        createDate: {$gte: startDay, $lte: endDay}
      }, {_id: true}, next);
    }],
    'selfPurchase': ['member', 'purchases', function (data, next) {
      let promises = [];
      if(!data.purchases || !data.purchases.length) {
        return next();
      }

      data.purchases.map(purchase => purchase._id).forEach((purachaseID) => {
        promises.push(new Promise((resolve, reject) => {
          TransactionModel.findOne({masterID: purachaseID}, function (err, response) {
            if(err) {
              return reject(err);
            }
            resolve(response);
          }).sort({createDate: -1});
        }));
        Promise
          .all(promises)
          .catch(err => {
            return next(err);
          })
          .then(transactions => {
            const statuses = ['cash_purchased', 'atc_purchased', 'btc_purchased', 'purchased'];
            const transactionsPurchased = transactions.filter(transaction => {
              return statuses.indexOf(transaction.status) !== -1;
            });
            let purchasedSum = 0;
            transactions.forEach(transaction => {
              purchasedSum += transaction.purchaseAmount;
            });
            next(null, purchasedSum);
          });
      });
    }],
    'higherRank': ['siblings', function (data, next) {
      if(!data.siblings || !data.siblings.length) {
        return next();
      }
      const higherPeople = data.siblings.filter(child => {
        return child.rank >= 4;
      });
      next(null, higherPeople.length);
    }]
  }, function (err, data) {
    if(err) {
      return next(err);
    }
    let response = data;
    if(!response.left) {
      response.left = {count: 0, sum: 0};
    }
    if(!response.right) {
      response.right = {count: 0, sum: 0};
    }
    res.json(response);
  });
});

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

  // if user is authenticated in the session, carry on
  console.log('auth', req.isAuthenticated(), req.user, req.session);
  if (req.isAuthenticated())
    return next();

  // if they aren't redirect them to the home page
  return res.status(401).send('セッションの有効期限が切れました。もう一度ログインしてください');
}

export default routesMember;
