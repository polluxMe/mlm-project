import express from 'express';
import mongoose from 'mongoose';
import async from 'async';
import _ from 'lodash';
import requireLogin from '../../../middlewares/requireLogin';
import requireAdmin from '../../../middlewares/requireAdmin';

var withdrawRequest = express.Router();

withdrawRequest
  .get(requireLogin)
  .post(requireLogin)
  .put(requireLogin)
  .delete(requireLogin);

withdrawRequest
  .get(requireAdmin)
  .post(requireAdmin)
  .put(requireAdmin)
  .delete(requireAdmin);

withdrawRequest.get('/', function (req, res, next) {
  var Model = mongoose.model('MasterWithdrawRequest', req.app.models.masterWithdrawRequest.schema);
  var TransactionModel = mongoose.model('TransactionWithdrawRequest', req.app.models.transactionWithdrawRequest.schema);
  const Member = mongoose.model('Member', req.app.models.members.schema);
  var limit = Number(req.query.limit),
    page = req.query.page,
    skip = (((page-1) * limit)),
    search = req.query.search,
    sort = req.query.sort,
    frameId = req.query.frameId,
    filter = req.query.filter;
  let queryParams = {};
  if(search) {
    queryParams = {$text: {$search: search}};
  }

  if(filter && filter !== '{}') {
    const filterObj = JSON.parse(filter);
    _.each(filterObj, (value, index) => {
      queryParams[index] = value;
    });
  }

  let memberQueryParams = {};
  if (queryParams.member) {
    if (queryParams.member.applicantID)
      memberQueryParams.applicantID = new RegExp(queryParams.member.applicantID, 'i');
    delete queryParams.member;
  }

  if(frameId) {
    queryParams.frameID = frameId;
  }

  var transactionsQueryParams = {
    status: {
      $nin: ['error', 'not-formed']
    }
  };

  if(queryParams['status']) {
    transactionsQueryParams.status = queryParams['status'];
    delete queryParams['status'];
  }
  var membersIds = [];
  async.auto({
    'members': (next) => {
      const Member = mongoose.model('Member', req.app.models.members.schema);

      Member.find(memberQueryParams, {_id: true, applicantID: true, applicantName: true}, (err, doc) => {
        if (err) next(err);

        membersIds = _.map(doc, item => item._id);
        queryParams.memberID = {$in: membersIds};

        doc = _.map(doc, member => {
          member._id = member._id.toString();
          return member;
        });
        next(null, doc);
      }).lean(true);
    },
    'count': ['members', (data, next) => {
      Model.count(queryParams, next);
    }],
    'masterRecords': ['count', function (data, next) {
      if (data.count === -1) {
        return next(null, []);
      }
      if(search) {
        skip = 0;
      }
      if(sort) {
        let sortOptions = JSON.parse(sort);
        sort = {};
        sort[sortOptions.name] = sortOptions.order;
      }
      Model.find(queryParams, next).skip(skip).limit(limit).sort(sort);
    }],
    'items': ['masterRecords', (data, next) => {
      if (data.masterRecords.length === 0) return next(null, []);
      const ids = _.map(data.masterRecords, item => item._id);

      let transactions = [];
      let i = ids.length;
      _.each(ids, id => {

        transactionsQueryParams.masterID = id;

        TransactionModel.findOne(transactionsQueryParams, (err, item) => {
          if(item) {
            item.member = _.find(data.members, {_id: item.memberID.toString()});
            transactions.push(item);
          }

          i--;
          if(!i)
            next(null, transactions);

        } ).sort({createDate: -1}).lean(true);
      })
    }]
  }, function (err, data) {
    if(err) {
      return res.status(404).send(err);
    }

    if (data.count !== -1) {
      res.set('x-total-count', data.count);
    }
    res.json(data);
  });

});

withdrawRequest.get('/:memberId', (req, res, next) => {
  var Model = mongoose.model('MasterWithdrawRequest', req.app.models.masterWithdrawRequest.schema);
  var TransactionModel = mongoose.model('TransactionWithdrawRequest', req.app.models.transactionWithdrawRequest.schema);

  async.auto({
    'masterRecords': function (next) {
      Model.find({
        memberID: req.params.memberId
      }, next)
    },
    'transactions': ['masterRecords', (data, next) => {
      const ids = _.map(data.masterRecords, item => item._id);

      let transactions = [];
      let i = ids.length;
      _.each(ids, id => {
        TransactionModel.findOne({
          masterID: id,
          status: {$ne: 'removed'}
        }, (err, item) => {
          if(item) {
            transactions.push(item);
          }

          i--;
          if(!i)
            next(null, transactions);

        } ).sort({createDate: -1});
      })
    }]
  }, function (err, data) {
    if (err) {
      return res.status(404).send(err);
    }

    res.json(data.transactions);
  });
});

withdrawRequest.put('/:transactionId/status', function (req, res, next) {
  var Model = mongoose.model('MasterWithdrawRequest', req.app.models.masterWithdrawRequest.schema);
  var TransactionModel = mongoose.model('TransactionWithdrawRequest', req.app.models.transactionWithdrawRequest.schema);
  const Wallet = mongoose.model('Wallet', req.app.models.wallets.schema);
  const Member = mongoose.model('Member', req.app.models.members.schema);
  async.auto({
    'transactionRecord': (next) => {
      TransactionModel.findById(req.params.transactionId, next);
    },
    'masterRecord': ['transactionRecord', (data, next) => {
      if(!data.transactionRecord) {
        return next('このIDを持つ取引記録が見つかりませんでした:' + req.params.transactionId);
      }

      Model.findById(data.transactionRecord.masterID, next)
    }],
    'transaction': ['masterRecord', 'transactionRecord', function (data, next) {
      if(!data.masterRecord) {
        return next('No master record found with this ID:' + data.transactionRecord.masterID);
      }

      const transactionRecord = data.transactionRecord;

      if (transactionRecord.status !== 'created') {
        return next('取引の状態は既に変更されました。再度変更することはできません');
      }

      var transaction = data.transactionRecord;
      transaction.status = req.body.status;
      transaction.save(next);
    }],
    'wallet': ['masterRecord', (data, next) => {
      const masterRecord = data.masterRecord;

      Wallet.findOne({memberID: masterRecord.memberID}, next);
    }],
    'member': ['masterRecord', (data, next) => {
      const masterRecord = data.masterRecord;

      Member.findById(masterRecord.memberID, next);
    }],
    'purchase': ['transactionRecord', 'masterRecord', 'wallet', (data, next) => {
      if (req.body.status !== 'purchased') {
        next();
      }

      const masterRecord = data.masterRecord;
      const wallet = data.wallet;

      wallet.commission -= masterRecord.requestAmount;
      wallet.cashPurchased = (+wallet.cashPurchased) + (+masterRecord.amount);

      wallet.save(next);
    }],
    'user': ['masterRecord', 'member', (data, next) => {
      if (req.body.status !== 'purchased') {
        next();
      }

      const masterRecord = data.masterRecord;
      const member = data.member;

      member.purchasePrice += masterRecord.amount;

      member.save(next);
    }],
  }, function (err, data) {
    if(err) {
      return res.json(err);
    }

    res.json(data.transactionRecord);
  });

});

withdrawRequest.post('/', function (req, res, next) {
  var Model = mongoose.model('MasterWithdrawRequest', req.app.models.masterWithdrawRequest.schema);
  const Wallet = mongoose.model('Wallet', req.app.models.wallets.schema);
  const MemberModel = mongoose.model('Member', req.app.models.members.schema);
  const Config = mongoose.model('MainConfig', req.app.models.mainConfig.schema);
  async.auto({
    'member': (next) => {
      MemberModel.findOne({_id: req.body.memberID}, next);
    },
    'configuration': (next) => {
      Config.find({}, next);
    },
    'items': ['member', 'configuration', (data, next) => {
      const member = data.member;
      const configuration = data.configuration[0];
      var items = new Model(req.body);
      var error = items.validateSync();
      if(error) {
        return next(error);
      }

      if (items.requestAmount < configuration.minimumWithdrawalAmount) {
        return next({errors: true, message: 'You cannot exchange less than ' + configuration.minimumWithdrawalAmount + ' B$'});
      }

      if (items.requestAmount > Math.floor(member.commision / 2)) {
        return next({errors: true, message: '金額の半分以上を一回で交換できません'});
      }

      items.save(next);
    }]
  }, function (err, data) {
    if(err) {
      return res.json(err);
    }

    res.json(data.items[0]);
  });

});

// withdrawRequest.put('/', function (req, res, next) {
//   var Model = mongoose.model('MasterWithdrawRequest', req.app.models.masterWithdrawRequest.schema);
//
//   async.auto({
//     'masterRecord': function (next) {
//       Model.findOne({memberID: req.body.masterID}, next);
//     },
//     'transaction': ['masterRecord', function (data, next) {
//       if(!data.masterRecord) {
//         return next('No master record found with this ID:' + req.body.masterID);
//       }
//
//       var transaction = new Model(req.body);
//       var error = transaction.validateSync();
//       if(error) {
//         return next(error);
//       }
//
//       transaction.save(next);
//     }]
//   }, function (err, data) {
//     if(err) {
//       var transactionErr = new Model(req.body);
//       transactionErr.status = 'error';
//       transactionErr.log = err;
//       transactionErr.save();
//       return res.json(err);
//     }
//
//     return res.json(data.transaction);
//   });
// });

export default withdrawRequest;
