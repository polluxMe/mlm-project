import express from 'express';
import mongoose from 'mongoose';
import async from 'async';
import _ from 'lodash';
import requireLogin from '../../../middlewares/requireLogin';
import requireAdmin from '../../../middlewares/requireAdmin';
import isCurrentUser from '../../../utils/isCurrentUser';

var cashRewardRequest = express.Router();

cashRewardRequest
  .get(requireLogin)
  .post(requireLogin)
  .put(requireLogin)
  .delete(requireLogin);

cashRewardRequest
  .get(requireAdmin)
  .get('/', function (req, res, next) {
    var Model = mongoose.model('MasterCashRewardRequest', req.app.models.masterCashRewardRequest.schema);
    var TransactionModel = mongoose.model('TransactionCashRewardRequest', req.app.models.transactionCashRewardRequest.schema);
    const Member = mongoose.model('Member', req.app.models.members.schema);
    var limit = Number(req.query.limit),
      page = req.query.page,
      skip = (((page-1) * limit)),
      search = req.query.search,
      sort = req.query.sort,
      frameId = req.query.frameId,
      filter = req.query.filter;
    let queryParams = {};
    if(search) {
      queryParams = {$text: {$search: search}};
    }

    if(filter && filter !== '{}') {
      const filterObj = JSON.parse(filter);
      _.each(filterObj, (value, index) => {
        queryParams[index] = value;
      });
    }

    let memberQueryParams = {};
    if (queryParams.member) {
      if (queryParams.member.applicantID)
        memberQueryParams.applicantID = new RegExp(queryParams.member.applicantID, 'i');
      delete queryParams.member;
    }

    if(frameId) {
      queryParams.frameID = frameId;
    }

    var transactionsQueryParams = {
      status: {
        $nin: ['error', 'not-formed']
      }
    };

    if(queryParams['status']) {
      transactionsQueryParams.status = queryParams['status'];
      delete queryParams['status'];
    }
    var membersIds = [];
    async.auto({
      'members': (next) => {
        const Member = mongoose.model('Member', req.app.models.members.schema);

        Member.find(memberQueryParams, {_id: true, applicantID: true, applicantName: true}, (err, doc) => {
          if (err) next(err);

          membersIds = _.map(doc, item => item._id);
          queryParams.memberID = {$in: membersIds};

          doc = _.map(doc, member => {
            member._id = member._id.toString();
            return member;
          });
          next(null, doc);
        }).lean(true);
      },
      'count': ['members', (data, next) => {
        Model.count(queryParams, next);
      }],
      'masterRecords': ['count', function (data, next) {
        if (data.count === -1) {
          return next(null, []);
        }
        if(search) {
          skip = 0;
        }
        if(sort) {
          let sortOptions = JSON.parse(sort);
          sort = {};
          sort[sortOptions.name] = sortOptions.order;
        }
        Model.find(queryParams, next).skip(skip).limit(limit).sort(sort);
      }],
      'items': ['masterRecords', (data, next) => {
        if (data.masterRecords.length === 0) return next(null, []);
        const ids = _.map(data.masterRecords, item => item._id);

        let transactions = [];
        let i = ids.length;
        _.each(ids, id => {

          transactionsQueryParams.masterID = id;

          TransactionModel.findOne(transactionsQueryParams, (err, item) => {
            if(item) {
              item.member = _.find(data.members, {_id: item.memberID.toString()});
              transactions.push(item);
            }

            i--;
            if(!i)
              next(null, transactions);

          } ).sort({createDate: -1}).lean(true);
        })
      }]
    }, function (err, data) {
      if(err) {
        return res.status(404).send(err);
      }

      if (data.count !== -1) {
        res.set('x-total-count', data.count);
      }
      res.json(_.omit(data, 'members'));
    });

  });

cashRewardRequest.get('/:memberId', (req, res, next) => {
  const Member = mongoose.model('Member', req.app.models.members.schema);
  const Model = mongoose.model('MasterCashRewardRequest', req.app.models.masterCashRewardRequest.schema);
  const TransactionModel = mongoose.model('TransactionCashRewardRequest', req.app.models.transactionCashRewardRequest.schema);
  const {month, year} = req.query;
  const {memberId} = req.params;

  console.log('month', month, year);
  async.auto({
    'member': (next) => {
      Member.findById(memberId, next);
    },
    'isCurrentUser': ['member', ({member}, next) => {
      isCurrentUser(req, res, member, next);
    }],
    // 'masterRecord': ['isCurrentUser', (data, next) => {
    //   Model.findOne({
    //     memberID: memberId,
    //     reportMonth: month,
    //     reportYear: year
    //   }, next)
    // }],
    'transaction': [(next) => {
      TransactionModel.find({
        memberID: memberId,
        reportMonth: month,
        reportYear: year,
        status: {$ne: 'removed'}
      }, (err, items) => {
        if(err) {
          return next(err);
        }

        if (!items || !items.length) {
          return next(null, 0);
        }

        const amount = items.map(item => item.amount).reduce((prev, next) => prev + next);

        next(null, amount);
      } ).sort({createDate: -1});
    }]
  },  (err, {transaction}) => {
    if (err) {
      return res.status(404).send(err);
    }

    res.json(transaction);
  });
});

cashRewardRequest.get('/:masterId/master', (req, res, next) => {
  var TransactionModel = mongoose.model('TransactionCashRewardRequest', req.app.models.transactionCashRewardRequest.schema);

  TransactionModel.findOne({masterID: req.params.masterId}, (err, transaction) => {
    if (err) {
      return res.status(404).send(err);
    }
    res.json(transaction);
  }).sort({createDate: -1})
});

cashRewardRequest
  .put(requireAdmin)
  .put('/:transactionId/status', function (req, res, next) {
    var Model = mongoose.model('MasterCashRewardRequest', req.app.models.masterCashRewardRequest.schema);
    var TransactionModel = mongoose.model('TransactionCashRewardRequest', req.app.models.transactionCashRewardRequest.schema);
    const Wallet = mongoose.model('Wallet', req.app.models.wallets.schema);
    const Member = mongoose.model('Member', req.app.models.members.schema);
    async.auto({
      'transactionRecord': (next) => {
        TransactionModel.findById(req.params.transactionId, next);
      },
      'masterRecord': ['transactionRecord', (data, next) => {
        if(!data.transactionRecord) {
          return next('このIDを持つ取引記録が見つかりませんでした:' + req.params.transactionId);
        }

        Model.findById(data.transactionRecord.masterID, next)
      }],
      'transaction': ['masterRecord', 'transactionRecord', function (data, next) {
        if(!data.masterRecord) {
          return next('No master record found with this ID:' + data.transactionRecord.masterID);
        }

        const transactionRecord = data.transactionRecord;
        if (transactionRecord.status !== 'created') {
          return next('取引の状態は既に変更されました。再度変更することはできません');
        }

        var transaction = data.transactionRecord;
        var date = new Date(transaction.createDate);
        transaction.status = req.body.status;
        transaction.reportMonth = transaction.reportMonth || (date.getMonth() + 1);
        transaction.reportYear = transaction.reportYear || date.getFullYear();
        transaction.baseUserID = transaction.baseUserID || 'none';

        transaction.save(next);
      }],
      'wallet': ['masterRecord', (data, next) => {
        const masterRecord = data.masterRecord;

        Wallet.findOne({memberID: masterRecord.memberID}, next);
      }],
      'member': ['masterRecord', (data, next) => {
        const masterRecord = data.masterRecord;

        Member.findById(masterRecord.memberID, next);
      }]
    }, function (err, data) {
      if(err) {
        return res.json(err);
      }

      res.json(data.transactionRecord);
    });

  });

cashRewardRequest
  .post(requireAdmin)
  .post('/', function (req, res, next) {
    var Model = mongoose.model('MasterCashRewardRequest', req.app.models.masterCashRewardRequest.schema);
    async.auto({
      'items': (next) => {
        var items = new Model(req.body);
        var error = items.validateSync();
        if(error) {
          return next(error);
        }

        items.save(next);
      }
    }, function (err, data) {
      if(err) {
        return res.json(err);
      }

      res.json(data.items[0]);
    });

  });

cashRewardRequest
  .put(requireAdmin)
  .put('/', function (req, res, next) {
    var Model = mongoose.model('MasterCashRewardRequest', req.app.models.masterCashRewardRequest.schema);

    async.auto({
      'masterRecord': function (next) {
        Model.findOne({memberID: req.body.masterID}, next);
      },
      'transaction': ['masterRecord', function (data, next) {
        if(!data.masterRecord) {
          return next('No master record found with this ID:' + req.body.masterID);
        }

        var transaction = new Model(req.body);
        var date = new Date(transaction.createDate);
        transaction.reportMonth = transaction.reportMonth || (date.getMonth() + 1);
        transaction.reportYear = transaction.reportYear || date.getFullYear();
        transaction.baseUserID = transaction.baseUserID || 'none';
        var error = transaction.validateSync();
        if(error) {
          return next(error);
        }

        transaction.save(next);
      }]
    }, function (err, data) {
      if(err) {
        var transactionErr = new Model(req.body);
        transactionErr.status = 'error';
        transactionErr.log = err;
        transactionErr.save();
        return res.json(err);
      }

      return res.json(data.transaction);
    });
  });

export default cashRewardRequest;
