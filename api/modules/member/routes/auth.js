import express from 'express';
import mongoose from 'mongoose';
import async from 'async';
import passport from 'passport';
import moment from 'moment';
import _ from 'lodash';
// import { hooks as authHooks } from 'feathers-authentication';


var routesAuth = express.Router();

routesAuth.get('/', function (req, res, next) {
  res.json({ok: true});
});

routesAuth.post('/login', function(req, res, next) {
  passport.authenticate('local-login', function(err, user, info) {
    req.app.logger.info('AUTH', '/login POST', 'local-login', user, info);
    if (err) return next(err);

    if (!user) {
      return res.status(200).json({
        user: null,
        info: info
      });
    }

    req.logIn(user, function(err) {
      if (err) {
        return next(err);
      }
    });

    return res.status(200).json({
      user: user,
      info: info
    })
  })(req, res, next);
  // req.session.user = req.user;
  // res.json({ok: true,  user : req.user });
});

routesAuth.get('/login', function(req, res) {
  res.json({ user : req.user });
});

routesAuth.get('/logout', function(req, res) {
  // req.session.user = null;
  req.logout();
  res.json({ok: true});
});

routesAuth.post('/register', function (req, res) {
  var Member = mongoose.model('Member', req.app.models.members.schema),
    tempPwd;

  const currentDate = moment().toDate();

  req.app.logger.info('AUTH', '/register', 'req.body', req.body);

  async.auto({
    'introducer': function (next) {
      var introducerID = req.body.introducerID || req.body.gCloudIntroducerID;
      if(!introducerID) {
        return next();
      }
      Member.findOne({applicantID: introducerID}, function (err, data) {
        req.app.logger.info('AUTH', '/register', 'data.introducer', data && data._doc);
        if (!introducerID) return next(null, null);
        if(err || !data) {
          return next(err || introducerID + "の紹介者は本会員ではありませんので、使用できません。");
          // return next(null, null);
        }
        if(data.status !== 'official') {
          return next(introducerID + "の紹介者は本会員ではありませんので、使用できません。");
        }
        return next(null, data);
      });
    },
    'member': ['introducer', (data, next) => {
      let body = Object.assign({}, req.body);
      body.purchasePrice = 0;
      body.purchasePriceATC = 0;
      body.purchasePriceBTC = 0;

      var memberRegister = new Member(body);
      tempPwd = Math.random().toString(36).slice(-8);
      memberRegister.username = memberRegister.applicantID = Math.random().toString(36).replace('.', '').slice(-16).toUpperCase();
      req.body.username = memberRegister.username;
      memberRegister.tempPassword = req.body.password = tempPwd;


      if(data.introducer) {
        memberRegister.introducerID = data.introducer.applicantID;
      }

      // var error = memberRegister.validateSync();
      // if(error) {
      //   return next(error);
      // }
      memberRegister.password = memberRegister.generateHash(tempPwd);
      memberRegister.save((err, data) => {
        req.app.logger.info('AUTH', '/register', 'data.member', data && data._doc);

        next(err, data);
      });
    }],
    'cashRate': (next) => {
      const Model = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);

      Model.findActiveFrame(function (err, data) {
        req.app.logger.info('AUTH', '/register', 'data.cashRate', data && data._doc);
        if(err) {
          req.app.logger.info('AUTH', '/register', 'cashRateError', err);
          return next(err);
        }
        next(null, {rate: data.purchaseRate / 100});
      });
    },
    'currentPurchaseRate': function(next) {
      var Model = mongoose.model('VsConfig', req.app.models.vsConfig.schema);

      Model.findOne({startDate: {
        $lte: currentDate
      },
        endDate: {
          $gt: currentDate
        }}, (err, data) => {
          req.app.logger.info('AUTH', '/register', 'data.currentPurchaseRate', data && data._doc);
          next(err, data);
      });
    },
    'previousPurchaseRate': ['currentPurchaseRate', function (data, next) {
      var Model = mongoose.model('VsConfig', req.app.models.vsConfig.schema);

      if(data.currentPurchaseRate) {
        return next();
      }
      Model.findOne({endDate: {$lt: currentDate}}, (err, data) => {
        req.app.logger.info('AUTH', '/register', 'data.previousPurchaseRate', data && data._doc);
      });
    }],
    'purchaseRate': ['currentPurchaseRate', 'previousPurchaseRate', (data, next) => {
      let rate = data.currentPurchaseRate ? data.currentPurchaseRate.purchaseRate : data.previousPurchaseRate.purchaseRate || 0;
      req.app.logger.info('AUTH', '/register', 'data.purchaseRate', rate);
      next(null, {rate: rate});
    }],
    'purchase': ['member', 'cashRate', 'purchaseRate', function (data, next) {
      if(!data.member || (data.member && data.member.code && data.member.code !== 200)) {
        return next({errors: data.member.errmsg || 'Member insert error.'});
      }
      let member = data.member;

      var Purchase = mongoose.model('VsMasterPurchase', req.app.models.vsMasterPurchases.schema);
      var purchase = new Purchase(req.body);
      purchase.memberID = member._id;
      purchase.cashRate = data.cashRate.rate;
      purchase.purchaseRate = data.purchaseRate.rate;
      if(req.body.accountType === 'gcloud') {
        purchase.purchaseQuantity = Math.floor(purchase.purchaseAmount / purchase.purchaseRate / purchase.cashRate);
      } else {
        purchase.purchaseAmount = req.body.purchaseAmount;
        purchase.purchaseQuantity = req.body.purchaseQuantity;
        purchase.purchaseBC = req.body.purchaseBC;
      }

      var error = purchase.validateSync();
      if(error) {
        req.app.logger.info('AUTH', '/register', 'purchaseError', error);
        return next(error);
      }

      purchase.save((err, data) => {
        req.app.logger.info('AUTH', '/register', 'data.purchase', data && data._doc);
        next(err, data);
      });
    }],
    'ticket': ['member', 'purchase', 'purchaseRate', function (data, next) {
      if(req.body.accountType !== 'brave') {
        return next();
      }
      if(!data.member || !data.purchase) {
        return next('Error when creating purchase.');
      }
      let member = data.member;

      var Ticket = mongoose.model('vsMasterTicket', req.app.models.vsMasterTickets.schema);
      var ticket = new Ticket(req.body);

      ticket.ticketPurchaseAmount = data.purchase.purchaseAmount;
      ticket.purchaseRate = data.purchaseRate.rate;
      ticket.ticketBraveAmount = data.purchase.purchaseAmount / ticket.purchaseRate;
      ticket.memberID = member._id;
      var error = ticket.validateSync();
      if(error) {
        return next(error);
      }

      ticket.save((err, data) => {
        req.app.logger.info('AUTH', '/register', 'data.ticket', data && data._doc);
        next(err, data);
      });
    }],
    'mail': ['purchase', 'member', 'ticket', function (data, next) {
      if (!data.member || !data.purchase) {
        return next('Error when creating purchase.');
      }
      const siteUrl = req.app.config.siteUrl;
      const qr = req.app.config.qrUrl;
      const member = data.member;
      req.app.services.mail.sendTemplate('invitation', member.email, {id: member.applicantID, password: tempPwd, name: member.applicantName, siteUrl: siteUrl, qr: qr}, next);
    }],
    'transactionMail': ['purchase', 'member', 'mail', (data, next) => {
      if (!data.purchase) next();

      const purchase = _.clone(data.purchase);
      purchase.createDateMoment = moment(purchase.createDate).format('YYYY/MM/DD HH:mm');
      purchase.cashRate = purchase.cashRate * 100;
      purchase.member = data.member;
      purchase.purchaseType = purchase.purchaseType === 'cash' ? '円' : purchase.purchaseType;
      purchase.siteUrl = req.app.config.siteUrl;

      if (data.member && data.member.email) {
        return req.app.services.mail.sendTemplate('createPurchase', data.member.email, purchase, next, ' - BRAVE -  バーチャルストックのご注文ありがとうございます。');
      }

      return next();
    }],
  }, function (err, data) {
    if(err) {
      req.app.logger.error('AUTH', '/register', 'error', err);
      if(data.member) {
        data.member.remove();
      }
      if(data.purchase) {
        data.purchase.remove();
      }
      if(data.ticket) {
        data.ticket.remove();
      }
      return res.status(500).json({errors: err.toString()});
    }

    req.app.logger.error('AUTH', '/register', 'end');

    data.member.pwd = tempPwd;
    res.json(data.member);
  });

});

routesAuth.post('/registerEnd', function (req, res, next) {
  var Member = mongoose.model('Member', req.app.models.members.schema);

  req.app.logger.error('AUTH', '/registerEnd', 'req.body', req.body);

  async.auto({
    'member': (next) => {
      if(!req.user) {
        return next({code: 401, msg: 'セッションの有効期限が切れました。空白ページから資格を設定してみますか？'});
      }
      Member.findById(req.user._id, (err, data) => {
        req.app.logger.error('AUTH', '/registerEnd', 'data.member', data && data._doc);
        next(err, data);
      });
    },
    'similarUser': (next) => {
      Member.findOne({username: req.body.id}, (err, data) => {
        req.app.logger.error('AUTH', '/registerEnd', 'data.similarUser', data && data._doc);
        next(err, data);
      });
    },
    'save': ['member', 'similarUser', function(data, next) {
      if(req.body.password !== req.body.repeatPassword) {
        return next({code: 500, msg: 'パスワードが一致しません'});
      }

      if(data.similarUser) {
        return next({code: 500, msg: '入力したIDは既に使われています。'});
      }

      // data.member.applicantID = req.body.id;
      data.member.username = req.body.id;
      data.member.password = data.member.generateHash(req.body.password);
      data.member.tempPassword = '';
      data.member.isChangeId = true;
      data.member.save((err, data) => {
        req.app.logger.error('AUTH', '/registerEnd', 'data.save', data && data._doc);
        next(err, data);
      });
    }]
  }, function (err, data) {
    if(err) {
      req.app.logger.error('AUTH', '/registerEnd', 'error', err);
      return res.status(err.code || 500).send(err);
    }

    req.app.logger.error('AUTH', '/registerEnd', 'end');

    res.json(data.save);
  });
});

routesAuth.post('/forget_password', (req, res) => {
  const Member = mongoose.model('Member', req.app.models.members.schema);
  const Token  = mongoose.model('ResetPasswordToken', req.app.models.resetPasswordToken.schema);

  async.auto({
    'member': (next) => {
      Member.findOne({username: req.body.username}, next);
    },
    'token': ['member', (data, next) => {
      if (!data.member) {
        return next({code: 500, msg: 'Member with this ID (' + req.body.username +') was not found'});
      }

      const member = data.member;
      const t = Math.random().toString(36).slice(-8);
      const token = new Token({
        memberID: member._id,
        token: t
      });

      token.save(next);
    }],
    'mail': ['member', 'token', (data, next) => {
      const member = data.member;
      const applicantName = data.member.applicantName;
      const token = data.token[0];
      const siteUrl = req.app.config.siteUrl;
      req.app.services.mail.sendTemplate('forgetPassword', member.email, {token: token.token, siteUrl, applicantName}, next, '- BRAVE - ログインパスワード再設定の確認');
    }]
  }, function (err, data) {
    if(err) {
      return res.status(err.code || 500).send(err);
    }

    res.json(data);
  });
});

routesAuth.post('/change_password', (req, res) => {
  const Member = mongoose.model('Member', req.app.models.members.schema);
  const Token  = mongoose.model('ResetPasswordToken', req.app.models.resetPasswordToken.schema);

  const yesterday = moment().subtract(1, 'day')._d;
  async.auto({
    'token': (next) => {
      Token.findOne({token: req.body.token, createDate: {$gte: yesterday}}, next).sort({creationDate: -1});
    },
    'member': ['token', (data, next) => {
      const token = data.token;

      if (!token) {
        return next({code: 500, msg: 'token is not found or expired'})
      }

      Member.findById(token.memberID, next);
    }],
    'updatedMember': ['member', (data, next) => {
      const member = data.member;

      member.password = member.generateHash(req.body.password);

      member.save(next);
    }],
    'removeToken': ['token', (data, next) => {
      const token = data.token;

      token.remove(next);
    }]
  }, function (err, data) {
    if(err) {
      return res.status(err.code || 500).send(err);
    }

    res.json(data);
  });
});


export default routesAuth;
