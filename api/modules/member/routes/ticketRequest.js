import express from 'express';
import mongoose from 'mongoose';
import async from 'async';
import _ from 'lodash';
import requireLogin from '../../../middlewares/requireLogin';
import requireAdmin from '../../../middlewares/requireAdmin';

var ticketRequest = express.Router();

ticketRequest
  .get(requireLogin)
  .post(requireLogin)
  .put(requireLogin)
  .delete(requireLogin);

ticketRequest
  .get(requireAdmin)
  .post(requireAdmin)
  .put(requireAdmin)
  .delete(requireAdmin);

ticketRequest.get('/', function (req, res) {
  const Model = mongoose.model('VsMasterTicket', req.app.models.vsMasterTickets.schema);
  const TransactionModel = mongoose.model('VsTransactionTicket', req.app.models.vsTransactionTickets.schema);
  const Member = mongoose.model('Member', req.app.models.members.schema);
  var limit = Number(req.query.limit),
    page = req.query.page,
    skip = (((page-1) * limit)),
    search = req.query.search,
    sort = req.query.sort,
    frameId = req.query.frameId,
    filter = req.query.filter;
  let queryParams = {};
  if(search) {
    queryParams = {$text: {$search: search}};
  }

  if(filter && filter !== '{}') {
    const filterObj = JSON.parse(filter);
    _.each(filterObj, (value, index) => {
      queryParams[index] = value;
    });
  }

  let memberQueryParams = {};
  if (queryParams.member) {
    if (queryParams.member.applicantID)
      memberQueryParams.applicantID = new RegExp(queryParams.member.applicantID, 'i');
    delete queryParams.member;
  }

  if(frameId) {
    queryParams.frameID = frameId;
  }

  var transactionsQueryParams = {

  };

  if(queryParams['status']) {
    transactionsQueryParams.status = queryParams['status'];
    delete queryParams['status'];
  }
  var membersIds = [];
  async.auto({
    'members': (next) => {
      const Member = mongoose.model('Member', req.app.models.members.schema);

      Member.find(memberQueryParams, {_id: true, applicantID: true, applicantName: true}, (err, doc) => {
        if (err) next(err);

        membersIds = _.map(doc, item => item._id);
        queryParams.memberID = {$in: membersIds};

        doc = _.map(doc, member => {
          member._id = member._id.toString();
          return member;
        });
        next(null, doc);
      }).lean(true);
    },
    'count': ['members', (data, next) => {
      Model.count(queryParams, next);
    }],
    'masterRecords': ['count', function (data, next) {
      if (data.count === -1) {
        return next(null, []);
      }
      if(search) {
        skip = 0;
      }
      if(sort) {
        let sortOptions = JSON.parse(sort);
        sort = {};
        sort[sortOptions.name] = sortOptions.order;
      }
      Model.find(queryParams, next).skip(skip).limit(limit).sort(sort);
    }],
    'items': ['masterRecords', 'members', (data, next) => {
      if (data.masterRecords.length === 0) return next(null, []);
      const ids = _.map(data.masterRecords, item => item._id);

      const promises = [];

      _.each(ids, id => {
        promises.push(new Promise((resolve, reject) => {
          TransactionModel.findOne(_.assign(_.clone(transactionsQueryParams), {masterID: id}), function (err, response) {
            if (err) {
              return reject(err);
            }
            response.member = _.find(data.members, {_id: response.memberID.toString()});
            resolve(response);
          }).sort({createDate: -1}).lean(true);
        }));
      });

      Promise
        .all(promises)
        .catch(err => {
          return next(err);
        })
        .then(transactions => {
          next(null, transactions);
        });
    }]
  }, function (err, data) {
    if(err) {
      return res.status(404).send(err);
    }

    if (data.count !== -1) {
      res.set('x-total-count', data.count);
    }
    res.json(data);
  });

});

ticketRequest.put('/:transactionId/status', function (req, res, next) {
  const Model = mongoose.model('VsMasterTicket', req.app.models.vsMasterTickets.schema);
  const TransactionModel = mongoose.model('VsTransactionTicket', req.app.models.vsTransactionTickets.schema);
  const Wallet = mongoose.model('Wallet', req.app.models.wallets.schema);
  const Member = mongoose.model('Member', req.app.models.members.schema);
  async.auto({
    'transactionRecord': (next) => {
      TransactionModel.findById(req.params.transactionId, next);
    },
    'masterRecord': ['transactionRecord', (data, next) => {
      if(!data.transactionRecord) {
        return next('このIDを持つ取引記録が見つかりませんでした:' + req.params.transactionId);
      }

      Model.findById(data.transactionRecord.masterID, next)
    }],
    'transaction': ['masterRecord', 'transactionRecord', function (data, next) {
      if(!data.masterRecord) {
        return next('No master record found with this ID:' + data.transactionRecord.masterID);
      }

      const transactionRecord = data.transactionRecord;

      if (transactionRecord.status !== 'placed') {
        return next('取引の状態は既に変更されました。再度変更することはできません');
      }

      var transaction = data.transactionRecord;
      transaction.status = req.body.status;
      transaction.save(next);
    }]
  }, function (err, data) {
    if(err) {
      return res.json(err);
    }

    res.json(data.transactionRecord);
  });

});

ticketRequest.get('/:masterId/master', (req, res) => {
  const TransactionModel = mongoose.model('VsTransactionTicket', req.app.models.vsTransactionTickets.schema);

  TransactionModel.findOne({masterID: req.params.masterId}, (err, transaction) => {
    if (err) {
      return res.status(404).send(err);
    }
    res.json(transaction);
  }).sort({createDate: -1})
});


export default ticketRequest;
