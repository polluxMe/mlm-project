import express from 'express';
import mongoose from 'mongoose';
import async from 'async';
import _ from 'lodash';
import requireLogin from '../../../middlewares/requireLogin';
import requireAdmin from '../../../middlewares/requireAdmin';
import isCurrentUser from '../../../utils/isCurrentUser';

var routesMatchingBonusRequest = express.Router();

routesMatchingBonusRequest
  .get(requireLogin)
  .post(requireLogin)
  .put(requireLogin)
  .delete(requireLogin);

routesMatchingBonusRequest
  .get(requireAdmin)
  .get('/', function (req, res, next) {
    var Model = mongoose.model('MasterMatchingBonusRequest', req.app.models.masterMatchingBonusRequest.schema);
    var TransactionModel = mongoose.model('TransactionMatchingBonusRequest', req.app.models.transactionMatchingBonusRequest.schema);
    const Member = mongoose.model('Member', req.app.models.members.schema);
    var limit = Number(req.query.limit),
      page = req.query.page,
      skip = (((page-1) * limit)),
      search = req.query.search,
      sort = req.query.sort,
      frameId = req.query.frameId,
      filter = req.query.filter;
    let queryParams = {};
    if(search) {
      queryParams = {$text: {$search: search}};
    }

    if(filter && filter !== '{}') {
      const filterObj = JSON.parse(filter);
      _.each(filterObj, (value, index) => {
        queryParams[index] = value;
      });
    }

    let memberQueryParams = {};
    if (queryParams.member) {
      if (queryParams.member.applicantID)
        memberQueryParams.applicantID = new RegExp(queryParams.member.applicantID, 'i');
      delete queryParams.member;
    }

    if(frameId) {
      queryParams.frameID = frameId;
    }

    var transactionsQueryParams = {
      status: {
        $nin: ['error', 'not-formed']
      }
    };

    if(queryParams['status']) {
      transactionsQueryParams.status = queryParams['status'];
      delete queryParams['status'];
    }
    var membersIds = [];
    async.auto({
      'members': (next) => {
        const Member = mongoose.model('Member', req.app.models.members.schema);

        Member.find(memberQueryParams, {_id: true, applicantID: true, applicantName: true}, (err, doc) => {
          if (err) next(err);

          membersIds = _.map(doc, item => item._id);
          queryParams.memberID = {$in: membersIds};

          doc = _.map(doc, member => {
            member._id = member._id.toString();
            return member;
          });
          next(null, doc);
        }).lean(true);
      },
      'count': ['members', (data, next) => {
        Model.count(queryParams, next);
      }],
      'masterRecords': ['count', function (data, next) {
        if (data.count === -1) {
          return next(null, []);
        }
        if(search) {
          skip = 0;
        }
        if(sort) {
          let sortOptions = JSON.parse(sort);
          sort = {};
          sort[sortOptions.name] = sortOptions.order;
        }
        Model.find(queryParams, next).skip(skip).limit(limit).sort(sort);
      }],
      'items': ['masterRecords', 'members', (data, next) => {
        if (data.masterRecords.length === 0) return next(null, []);
        const ids = _.map(data.masterRecords, item => item._id);

        let transactions = [];
        let i = ids.length;
        _.each(ids, id => {

          transactionsQueryParams.masterID = id;

          TransactionModel.findOne(transactionsQueryParams, (err, item) => {
            if(item) {
              item.member = _.find(data.members, {_id: item.memberID.toString()});
              transactions.push(item);
            }

            i--;
            if(!i)
              next(null, transactions);

          } ).sort({createDate: -1}).lean(true);
        })
      }]
    }, function (err, data) {
      if(err) {
        return res.status(404).send(err);
      }

      if (data.count !== -1) {
        res.set('x-total-count', data.count);
      }
      res.json(data);
    });

  });

routesMatchingBonusRequest.get('/:memberId', (req, res, next) => {
  const Model = mongoose.model('MasterMatchingBonusRequest', req.app.models.masterMatchingBonusRequest.schema);
  const TransactionModel = mongoose.model('TransactionMatchingBonusRequest', req.app.models.transactionMatchingBonusRequest.schema);
  const Member = mongoose.model('Member', req.app.models.members.schema);
  const month = req.query.month && +req.query.month;
  const year = req.query.year && +req.query.year;
  const {memberId} = req.params;

  console.log('YEAR', month, year);

  async.auto({
    'member': (next) => {
      Member.findById(memberId, next);
    },
    'isCurrentUser': ['member', ({member}, next) => {
      isCurrentUser(req, res, member, next);
    }],
    'masterRecord': ['isCurrentUser', (data, next) => {
      Model.findOne({
        memberID: memberId,
        reportMonth: month,
        reportYear: year
      }, next).lean(true)
    }],
    'exactChildren': ['member', ({member}, next) => {
      Member.find({introducerID: member.applicantID}, (err, children) => {
        if(err) {
          return next(err);
        }
        next(null, children.length);
      });
    }],
    'transaction': ['masterRecord', ({masterRecord}, next) => {
      // if(!masterRecord) {
      //   return next();
      // }
      TransactionModel.findOne({
        memberID: memberId,
        reportMonth: month,
        reportYear: year,
        status: {$ne: 'removed'}
      }, (err, item) => {
        if(err) {
          return next(err);
        }
        next(null, item);
      } ).sort({createDate: -1}).lean(true);
    }],
    'children': ['transaction', 'member', ({member, transaction}, next) => {
      let children = {
        0: 0,
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
        7: 0
      };
      // if(!transaction) {
      //   return next(null, children);
      // }

      Member.getMatchingMembers(member, (err, childrens) => {
        if(err) {
          return next(err);
        }

        if(!childrens) {
          return next(null, children);
        }

        _.each(childrens, (childs, phase) => {
          children[phase] = childs.length;
        });

        return next(null, children);
      }, true);
    }]
  }, function (err, {transaction, children, exactChildren}) {
    if (err) {
      return res.status(404).send(err);
    }

    res.json({...transaction, children, exactChildren});
  });
});

routesMatchingBonusRequest
  .get(requireAdmin)
  .get('/:masterId/master', (req, res, next) => {
    var TransactionModel = mongoose.model('TransactionMatchingBonusRequest', req.app.models.transactionMatchingBonusRequest.schema);

    TransactionModel.findOne({masterID: req.params.masterId}, (err, transaction) => {
      if (err) {
        return res.status(404).send(err);
      }
      res.json(transaction);
    }).sort({createDate: -1})
  });


routesMatchingBonusRequest
  .put(requireAdmin)
  .put('/:transactionId/status', function (req, res, next) {
    var Model = mongoose.model('MasterMatchingBonusRequest', req.app.models.masterMatchingBonusRequest.schema);
    var TransactionModel = mongoose.model('TransactionMatchingBonusRequest', req.app.models.transactionMatchingBonusRequest.schema);
    const Wallet = mongoose.model('Wallet', req.app.models.wallets.schema);
    const Member = mongoose.model('Member', req.app.models.members.schema);

    async.auto({
      'transactionRecord': (next) => {
        TransactionModel.findById(req.params.transactionId, next);
      },
      'masterRecord': ['transactionRecord', (data, next) => {
        if(!data.transactionRecord) {
          return next('このIDを持つ取引記録が見つかりませんでした:' + req.params.transactionId);
        }

        Model.findById(data.transactionRecord.masterID, next)
      }],
      'transaction': ['masterRecord', 'transactionRecord', function (data, next) {
        if(!data.masterRecord) {
          return next('No master record found with this ID:' + data.transactionRecord.masterID);
        }

        const transactionRecord = data.transactionRecord;

        if (transactionRecord.status !== 'created') {
          return next('取引の状態は既に変更されました。再度変更することはできません');
        }

        var transaction = data.transactionRecord;
        transaction.status = req.body.status;
        transaction.save(next);
      }],
      'wallet': ['masterRecord', (data, next) => {
        const masterRecord = data.masterRecord;

        Wallet.findOne({memberID: masterRecord.memberID}, next);
      }],
      'member': ['masterRecord', (data, next) => {
        const masterRecord = data.masterRecord;

        Member.findById(masterRecord.memberID, next);
      }],
      'purchase': ['transactionRecord', 'masterRecord', 'wallet', (data, next) => {
        if (req.body.status !== 'purchased') {
          next();
        }

        const masterRecord = data.masterRecord;
        const wallet = data.wallet;

        wallet.commission += masterRecord.amount; // todo check it here

        wallet.save(next);
      }],
      // 'user': ['masterRecord', 'member', (data, next) => {
      //   if (req.body.status !== 'purchased') {
      //     next();
      //   }
      //
      //   const masterRecord = data.masterRecord;
      //   const member = data.member;
      //
      //   // member.purchasePrice += masterRecord.amount; // todo check it here
      //
      //   member.save(next);
      // }],
    }, function (err, data) {
      if(err) {
        return res.json(err);
      }

      res.json(data.transactionRecord);
    });

  });

routesMatchingBonusRequest
  .post(requireAdmin)
  .post('/', function (req, res, next) {
    var Model = mongoose.model('MasterMatchingBonusRequest', req.app.models.masterMatchingBonusRequest.schema);
    async.auto({
      'items': (next) => {
        var items = new Model(req.body);
        var error = items.validateSync();
        if(error) {
          return next(error);
        }

        items.save(next);
      }
    }, function (err, data) {
      if(err) {
        return res.json(err);
      }

      res.json(data.items[0]);
    });

  });

routesMatchingBonusRequest
  .put(requireAdmin)
  .put('/', function (req, res, next) {
    var Model = mongoose.model('MasterMatchingBonusRequest', req.app.models.masterMatchingBonusRequest.schema);

    async.auto({
      'masterRecord': function (next) {
        Model.findOne({memberID: req.body.masterID}, next);
      },
      'transaction': ['masterRecord', function (data, next) {
        if(!data.masterRecord) {
          return next('No master record found with this ID:' + req.body.masterID);
        }

        var transaction = new Model(req.body);
        var error = transaction.validateSync();
        if(error) {
          return next(error);
        }

        transaction.save(next);
      }]
    }, function (err, data) {
      if(err) {
        var transactionErr = new Model(req.body);
        transactionErr.status = 'error';
        transactionErr.log = err;
        transactionErr.save();
        return res.json(err);
      }

      return res.json(data.transaction);
    });
  });

export default routesMatchingBonusRequest;
