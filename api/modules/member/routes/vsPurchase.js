import express from 'express';
import mongoose from 'mongoose';
import async from 'async';
import _ from 'lodash';
import moment from 'moment';
import requireLogin from '../../../middlewares/requireLogin';
import requireAdmin from '../../../middlewares/requireAdmin';
import isCurrentUser from '../../../utils/isCurrentUser';

var routesVSPurchase = express.Router();

routesVSPurchase
  .get(requireLogin)
  .post(requireLogin)
  .put(requireLogin)
  .delete(requireLogin);

routesVSPurchase
  .get('/', requireAdmin)
  .get('/', function (req, res, next) {
    var Model = mongoose.model('VsMasterPurchase', req.app.models.vsMasterPurchases.schema);
    var TransactionModel = mongoose.model('VsTransactionPurchase', req.app.models.vsTransactionPurchases.schema);

    var limit = Number(req.query.limit),
      page = req.query.page,
      skip = (((page-1) * limit)),
      search = req.query.search,
      sort = req.query.sort || '{"name": "createDate", "order": -1}',
      frameId = req.query.frameId,
      filter = req.query.filter;
    let queryParams = {};
    if(search) {
      queryParams = {$text: {$search: search}};
    }

    if(filter && filter !== '{}') {
      const filterObj = JSON.parse(filter);
      _.each(filterObj, (value, index) => {
        if(_.isArray(value)) {
          if (value.length) {
            queryParams[index] = {$in: value};
          }
        }
        else if (_.isObject(value)) {
          queryParams[index] = value;
        }
        else if (_.indexOf(index, ['startDate', 'endDate', 'purchaseAmount', 'purchaseRate'])) {
          queryParams[index] = value;
        }
        else {
          queryParams[index] = new RegExp(value, 'i');
        }
      });
    }

    if (queryParams.startDate || queryParams.endDate) {
      queryParams.createDate = {};
    }

    if (queryParams.startDate) {
      const startDate = moment(queryParams.startDate).subtract(1, 'day').add(1, 'second');

      _.assign(queryParams.createDate, {
        $gte: startDate.toDate(),
      });
    }

    if (queryParams.endDate) {
      const endDate = moment(queryParams.endDate);

      _.assign(queryParams.createDate, {
        $lte: endDate.toDate(),
      });
    }

    delete queryParams.startDate;
    delete queryParams.endDate;

    let memberQueryParams = {};
    if (queryParams.member) {
      if (queryParams.member.applicantID)
        memberQueryParams.applicantID = new RegExp(queryParams.member.applicantID, 'i');
      if (queryParams.member.applicantName)
        memberQueryParams.applicantName = new RegExp(queryParams.member.applicantName, 'i');
      if (queryParams.member.nickname)
        memberQueryParams.nickname = new RegExp(queryParams.member.nickname, 'i');
      if (queryParams.member.email)
        memberQueryParams.email = new RegExp(queryParams.member.email, 'i');
      delete queryParams.member;
    }

    if(frameId) {
      queryParams.frameID = frameId;
    }

    var transactionsQueryParams = {
      status: {
        $nin: ['error', 'not-formed']
      }
    };

    if(queryParams['status']) {
      transactionsQueryParams.status = queryParams['status'];
      delete queryParams['status'];
    }

    req.app.logger.info('PURCHASES', '/', 'memberQueryParams', memberQueryParams);
    req.app.logger.info('PURCHASES', '/', 'transactionsQueryParams', transactionsQueryParams);

    let sortQuery = {};
    if(sort) {
      let sortOptions = JSON.parse(sort);
      sortQuery[sortOptions.name] = sortOptions.order;
    }

    var membersIds = [];
    async.auto({
      'members': (next) => {
        const Member = mongoose.model('Member', req.app.models.members.schema);

        Member.find(memberQueryParams, {_id: true, applicantID: true}, next).lean(true);
      },
      'count': ['members', (data, next) => {
        if (!_.isEmpty(memberQueryParams)) {
          membersIds = _.map(data.members, item => item._id);
          queryParams.memberID = {$in: membersIds};
        }
        req.app.logger.info('PURCHASES', '/', 'queryParams', queryParams);
        Model.count(queryParams, next);
      }],
      'masterRecords': ['count', function (data, next) {
        if (data.count === -1) {
          return next(null, []);
        }
        if(search) {
          skip = 0;
        }
        Model.find(queryParams, next).sort(sortQuery).skip(skip).limit(limit);
      }],
      'items': ['masterRecords', 'members', (data, next) => {
        if (data.masterRecords.length === 0) return next(null, []);
        const ids = _.map(data.masterRecords, item => item._id);
        const members = _.map(data.members, item => {
          item._id = item._id.toString();
          return item;
        });

        const promises = [];

        _.each(ids, id => {
          promises.push(new Promise((resolve, reject) => {
            const queryParams = _.assign(_.clone(transactionsQueryParams), {masterID: id});
            TransactionModel.findOne(queryParams, (err, item) => {
              if(item) {
                item.memberID = item.memberID.toString();
                item.member = _.find(members, {_id: item.memberID});
              }

              if(err) {
                return reject(err);
              }
              resolve(item);

            } ).sort({createDate: -1}).lean(true);
          }));
        });

        Promise
          .all(promises)
          .catch(err => {
            return next(err);
          })
          .then(items => {
            items = _.filter(items, item => !!item);

            if (sortQuery) {
              items = _.orderBy(items, Object.keys(sortQuery), Object.values(sortQuery).map(order => (order === -1) ? 'desc' : 'asc'));
            }
            next(null, items);
          });
      }]
    }, function (err, data) {
      if(err) {
        return res.status(404).send(err);
      }

      if (data.count !== -1) {
        res.set('x-total-count', data.count);
      }
      res.json(data);
    });

  });

routesVSPurchase
  .get('/master/:masterId', requireAdmin)
  .get('/master/:masterId', (req, res, next) => {
    var TransactionModel = mongoose.model('VsTransactionPurchase', req.app.models.vsTransactionPurchases.schema);

    TransactionModel.findOne({
      masterID: req.params.masterId,
      "status": {$in: ["cash_order","atc_order","btc_order"]}
    }, (err, transaction) => {
      if (err) {
        return res.status(404).send(err);
      }
      res.json(transaction);
    }).sort({createDate: -1})
  });

routesVSPurchase.get('/:memberId', (req, res, next) => {
  var Model = mongoose.model('VsMasterPurchase', req.app.models.vsMasterPurchases.schema);
  var TransactionModel = mongoose.model('VsTransactionPurchase', req.app.models.vsTransactionPurchases.schema);
  var Member = mongoose.model('Member', req.app.models.members.schema);

  async.auto({
    'member': (next) => {
      Member.findById(req.params.memberId, next);
    },
    'isCurrentUser': ['member', ({member}, next) => {
      isCurrentUser(req, res, member, next);
    }],
    'masterRecords': ['isCurrentUser', (data, next) => {
      Model.find({
        memberID: req.params.memberId
      }, next).sort({createDate: -1});
    }],
    'transactions': ['masterRecords', ({masterRecords}, next) => {
      const ids = _.map(masterRecords, item => item._id);

      const promises = [];
      _.each(ids, id => {
        promises.push(new Promise((resolve, reject) => {
          TransactionModel.findOne({
            masterID: id,
            // status: {$nin: ['removed', 'removed_order_admin', 'removed_order_user', 'removed_purchase_admin']}
          }, (err, item) => {
            if (err) reject(err);
            resolve(item);

          } ).sort({createDate: -1});
        }));
      });

      Promise
        .all(promises)
        .catch(err => next(err))
        .then(items => next(null, items));
    }]
  }, (err, {transactions}) => {
    if (err) {
      return res.status(404).send(err);
    }

    res.json(transactions);
  });
});

routesVSPurchase.get('/:memberId/sum', (req, res, next) => {
  const Model = mongoose.model('VsMasterPurchase', req.app.models.vsMasterPurchases.schema);
  const TransactionModel = mongoose.model('VsTransactionPurchase', req.app.models.vsTransactionPurchases.schema);
  const Member = mongoose.model('Member', req.app.models.members.schema);
  const month = req.query.month && +req.query.month;
  const year = req.query.year && +req.query.year;
  const {memberId} = req.params;
  const date = moment().year(year).month(month-1).endOf('month')._d;
  const dateFrom = moment().year(year).month(month-1).startOf('month')._d;
  console.log('---date', date, dateFrom);

  const purchasesQuery = {
    memberID: memberId,
    approveDate: {$lte: date, $gte: dateFrom},
    status: {$in: ['cash_purchased', 'atc_purchased', 'btc_purchased', 'purchased']}
  };

  async.auto({
    'member': (next) => {
      Member.findById(memberId, next);
    },
    'isCurrentUser': ['member', ({member}, next) => {
      isCurrentUser(req, res, member, next);
    }],
    'masterRecords': ['isCurrentUser', (data, next) => {
      Model.find(purchasesQuery, next).sort({createDate: -1});
    }],
    'childrenRecords': ['member', ({ member }, next) => {
      Member.find({introducerID: member.applicantID}, (err, children) => {
        if(err) {
          return next(err);
        }

        if(!children.length) {
          return next(null, 0);
        }

        const query = _.clone(purchasesQuery);
        const transactionPromises = children.map(child => {
          query.memberID = child._id;

          console.log('RECORDS', query);

          return new Promise((resolve, reject) => {
            TransactionModel.find(query, (err, records) => {
              if(err) {
                return reject(err);
              }

              if(!records.length) {
                return resolve(0);
              }

              const recordsAmount = records.map(record => record.purchaseAmount).reduce((prev, curr) => prev + curr);
              return resolve(recordsAmount);
            });
          });
        });

        Promise.all(transactionPromises)
          .then(items => {
            const accumulatedChildsCash = items.reduce((prev, curr) => prev + curr);
            next(null, (accumulatedChildsCash / 2));
          })
          .catch(next);
      });
    }],
    'cash': ['isCurrentUser', (data, next) => {
      TransactionModel.find(purchasesQuery, {purchaseAmount: true}, (err, data) => {
        if(err) {
          return next(err);
        }

        if(!data.length) {
          return next(null, 0);
        }

        const accumulatedCash = data.map(record => record.purchaseAmount).reduce((prev, curr) => prev + curr);
        next(null, accumulatedCash);
      }).sort({createDate: -1});
    }],
    'sum': ['cash', 'childrenRecords', ({cash, childrenRecords}, next) => {
      // const ids = _.map(masterRecords, item => item._id);

      next(null, cash + childrenRecords);
      // const promises = [];
      // _.each(ids, id => {
      //   promises.push(new Promise((resolve, reject) => {

        // }));
      // });
      // Promise
      //   .all(promises)
      //   .catch(err => next(err))
      //   .then(items => {
      //     // const sum =  _.sumBy(items, item => item.purchaseAmount);
      //     next(null, cashRecords + childrenRecords);
      //   });
    }],
    // 'cash': ['cashRecords', ({cashRecords}, next) => {
    //   const ids = _.map(cashRecords, item => item._id);
    //
    //   const promises = [];
    //   _.each(ids, id => {
    //     promises.push(new Promise((resolve, reject) => {
    //       TransactionModel.findOne({
    //         masterID: id,
    //         status: {$nin: ['removed', 'removed_order_admin', 'removed_order_user', 'removed_purchase_admin']}
    //       }, {purchaseAmount: true}, (err, item) => {
    //         if (err) reject(err);
    //         resolve(item);
    //
    //       }).sort({createDate: -1});
    //     }));
    //   });
    //   Promise
    //     .all(promises)
    //     .catch(err => next(err))
    //     .then(items => {
    //       const sum =  _.sumBy(items, item => item.purchaseAmount);
    //       next(null, sum);
    //     });
    // }]
  }, (err, {sum, cash}) => {
    if (err) {
      return res.status(404).send(err);
    }

    res.json({sum, cash});
  });
});

routesVSPurchase.post('/:transactionId/cancel', function (req, res, next) {
  var TransactionModel = mongoose.model('VsTransactionPurchase', req.app.models.vsTransactionPurchases.schema);
  var Model = mongoose.model('VsMasterPurchase', req.app.models.vsMasterPurchases.schema);
  var Member = mongoose.model('Member', req.app.models.members.schema);
  const purchasedStatuses = ['cash_purchased', 'atc_purchased', 'btc_purchased', 'purchased'];

  async.auto({
    'transactionRecord': (next) => {
      TransactionModel.findOne({_id: req.params.transactionId}, function (err, data) {
        if(err) {
          return next(err);
        }
        if(data) {
          return next(null, data);
        }

        // check maybe we seek by master
        Model.findOne({_id: req.params.transactionId}, function (err, data) {
          if(err) {
            return next(err);
          }
          if(!data) {
            return next('このIDを持つ取引記録及びマスター記録が見つかりませんでした。:' + req.params.transactionId);
          }

          TransactionModel.findOne({masterID: data._id}, next).sort({createDate: -1});
        });
      });
    },
    'member': ['transactionRecord', (data, next) => {
      Member.findById(data.transactionRecord.memberID, next);
    }],
    'isCurrentUser': ['member', (data, next) => {
      isCurrentUser(req, res, data.member, next);
    }],
    'removedStatus': ['transactionRecord', 'isCurrentUser', function (data, next) {
      if(!data.transactionRecord) {
        return next('このIDを持つ取引記録が見つかりませんでした:' + req.params.transactionId);
      }

      if(!req.user) {
        return next('You have no permission to do that!');
      }
      let status = 'removed_order_user';
      let userId = req.user._id.toString();
      let isPurchased = purchasedStatuses.indexOf(data.transactionRecord.status) !== -1;

      if(userId != data.transactionRecord.memberID) {
        if(!req.user.isAdmin) {
          return next('You have no permission to do that!');
        }
        status = isPurchased ? 'removed_purchase_admin' : 'removed_order_admin';
      }
      // user cant remove own purchased transaction
      if((userId == data.transactionRecord.memberID) && isPurchased) {
        return next('You have no permission to do that!');
      }

      next(null, status);
    }],
    'transaction': ['transactionRecord', 'removedStatus', function (data, next) {
      if(!data.transactionRecord) {
        return next('このIDを持つ取引記録が見つかりませんでした:' + req.params.transactionId);
      }

      Model.retrieve(data.transactionRecord, function (err) {
        if(err) {
          return next(err);
        }

        var transaction = new TransactionModel(_.omit(Object.assign({}, data.transactionRecord._doc), '_id', 'createDate'));
        transaction.status = data.removedStatus;
        transaction.purchaseBC = transaction.purchaseBC || 0;
        transaction.save(next);
      });
    }],
    // 'member': ['transaction', (data, next) => {
    //   const transaction = data.transaction[0];
    //
    //   Member.findOne({_id: transaction.memberID}, next);
    // }],
    // 'mail': ['transaction', 'member', (data, next) => {
    //   if (!data.transaction) next();
    //
    //   const transaction = _.clone(data.transaction[0]);
    //   transaction.createOrderDateMoment = moment(transaction.createOrderDate).format('YYYY/MM/DD HH:mm');
    //   transaction.approveDateMoment = moment(transaction.approveDate).format('YYYY/MM/DD HH:mm');
    //   transaction.cashRate = transaction.cashRate * 100;
    //   transaction.cashRateCreated = transaction.cashRateCreated * 100;
    //   transaction.member = data.member;
    //   transaction.purchaseType = transaction.purchaseType === 'cash' ? '円' : transaction.purchaseType;
    //   transaction.siteUrl = req.app.config.siteUrl;
    //
    //   req.app.services.mail.sendTemplate('approvePurchase', data.member.email, transaction, next, 'BRAVE - ご入金ありがとうございます。');
    // }],
  }, function (err, data) {
    if(err) {
      return res.status(500).send(err);
    }

    res.json(data.transaction);
  });

});

  routesVSPurchase.post('/', function (req, res, next) {
    var Model = mongoose.model('VsMasterPurchase', req.app.models.vsMasterPurchases.schema);
    var Member = mongoose.model('Member', req.app.models.members.schema);
    req.app.logger.info('PURCHASES', '/ (POST)', 'req.body', req.body);
    async.auto({
      'member': function (next) {
        Member.findOne({_id: req.body.memberID}, (err, data) => {
          req.app.logger.info('PURCHASES', '/ (POST)', 'data.member', data && data._doc);

          next(err, data);
        });
      },
      'isCurrentUser': ['member', (data, next) => {
        isCurrentUser(req, res, data.member, next);
      }],
      'purchase': ['isCurrentUser', (data, next) => {
        var purchase = new Model(req.body);
        var error = purchase.validateSync();
        if(error) {
          return next(error);
        }

        purchase.save((err, data) => {
          req.app.logger.info('PURCHASES', '/ (POST)', 'data.purchase', data && data._doc);
          next(err, data);
        });
      }],
      'mail': ['purchase', 'member', (data, next) => {
        if (!data.purchase) next();

        const purchase = _.clone(data.purchase);
        purchase.createDateMoment = moment(purchase.createDate).format('YYYY/MM/DD HH:mm');
        purchase.cashRate = purchase.cashRate * 100;
        purchase.member = data.member;
        purchase.purchaseType = purchase.purchaseType === 'cash' ? '円' : purchase.purchaseType;
        purchase.siteUrl = req.app.config.siteUrl;

        if (data.member && data.member.email) {
          return req.app.services.mail.sendTemplate('createPurchase', data.member.email, purchase, next, ' - BRAVE -  バーチャルストックのご注文ありがとうございます。');
        }

        return next();
      }],
    }, function (err, data) {
      if(err) {
        req.app.logger.info('PURCHASES', '/ (POST)', 'error', err);

        return res.json(err);
      }

      req.app.logger.info('PURCHASES', '/ (POST)', 'end');

      res.json(data.purchase);
    });

  });

  routesVSPurchase
    .put('/', requireAdmin)
    .put('/', function (req, res, next) {
      var Model = mongoose.model('VsTransactionPurchase', req.app.models.vsTransactionPurchases.schema);
      var Master = mongoose.model('VsMasterPurchase', req.app.models.vsMasterPurchases.schema);
      var Member = mongoose.model('Member', req.app.models.members.schema);
      const purchasedStatuses = ['cash_purchased', 'atc_purchased', 'btc_purchased', 'purchased'];
      const removedStatuses = ['removed_order_admin', 'removed_order_user', 'removed_purchase_admin'];
      const PurchaseFrameModel = mongoose.model('VsFrameConfig', req.app.models.vsFrameConfig.schema);

      req.app.logger.info('PURCHASES', '/ (PUT)', 'req.body', req.body);

      async.auto({
        'masterRecord': function (next) {
          Master.findById(req.body.masterID, (err, data) => {
            req.app.logger.info('PURCHASES', '/ (PUT)', 'data.masterRecord', data && data._doc);
            next(err, data);
          });
        },
        'member': function (next) {
          Member.findOne({_id: req.body.memberID}, (err, data) => {
            req.app.logger.info('PURCHASES', '/ (PUT)', 'data.member', data && data._doc);
            next(err, data);
          });
        },
        'checkIsChangeRank': ['member', function (data, next) {
          const user = data.member;
          if(!user) {
            return next('No user found with this ID:' + req.body.masterID);
          }
          if(user.accountType !== 'gcloud') {
            return next();
          }
          if(!user.rank) {
            return next('注文処理する前に、会員にランクを設定する必要があります。');
          }

          next();
        }],
        'transaction': ['masterRecord', 'checkIsChangeRank', function (data, next) {
          if(!data.masterRecord) {
            return next('No master record found with this ID:' + req.body.masterID);
          }

          let reqParams = req.body;
          var transaction = new Model(_.omit(reqParams, '_id', 'createDate'));
          var error = transaction.validateSync();
          if(error) {
            req.app.logger.info('PURCHASES', '/ (PUT)', 'data.transaction error', error);
            return next(error);
          }

          transaction.save(function (err, data) {
            req.app.logger.info('PURCHASES', '/ (PUT)', 'data.transaction', data && data._doc);

            if(err) {
              return next(err);
            }

            if(removedStatuses.indexOf(transaction.status) !== -1) {
              return PurchaseFrameModel.retrieve(transaction, next);
            }

            next(null, data);

          });
        }],
        'mail': ['transaction', 'member', (data, next) => {
          if (!data.transaction) return next();

          const transaction = _.clone(data.transaction);
          transaction.createOrderDateMoment = moment(transaction.createOrderDate).format('YYYY/MM/DD HH:mm');
          transaction.approveDateMoment = moment(transaction.approveDate).format('YYYY/MM/DD HH:mm');
          transaction.cashRate = transaction.cashRate * 100;
          transaction.cashRateCreated = transaction.cashRateCreated * 100;
          transaction.member = data.member;
          transaction.purchaseType = transaction.purchaseType === 'cash' ? '円' : transaction.purchaseType;
          transaction.siteUrl = req.app.config.siteUrl;

          const status = transaction.status;

          if (data.member && data.member.email && status === 'cash_purchased' || status === 'atc_purchased' || status === 'btc_purchased') {
            return req.app.services.mail.sendTemplate('approvePurchase', data.member.email, transaction, next, ' - BRAVE - ご入金ありがとうございます。');
          }

          return next();
        }],
        'calculateRank': ['member', 'checkIsChangeRank', function (data, next) {
          Member.computeRank(data.member, next);
        }],
        'official': ['member', 'checkIsChangeRank', function (data, next) {
          if (purchasedStatuses.indexOf(req.body.status) !== -1 && data.member.status !== 'official') {
            data.member.status = 'official';
            data.member.isDeposited = true;
            data.member.save(next);
          } else {
            next();
          }
        }]
      }, function (err, data) {
        if(err) {
          req.app.logger.info('PURCHASES', '/ (PUT)', 'error', err);
          var transactionErr = new Model(_.omit(req.body, '_id'));
          transactionErr.status = 'error';
          transactionErr.log = err;
          transactionErr.save();
          return res.status(500).send(err);
        }

        req.app.logger.info('PURCHASES', '/ (PUT)', 'end');

        return res.json(data.transaction);
      });
    });

  export default routesVSPurchase;
