import express from 'express';
import mongoose from 'mongoose';
import async from 'async';
import _ from 'lodash';
const ObjectId = require('mongoose').Types.ObjectId;
import requireLogin from '../../../middlewares/requireLogin';
import requireAdmin from '../../../middlewares/requireAdmin';
import moment from 'moment';
import isCurrentUser from '../../../utils/isCurrentUser';

var routes = express.Router();

routes
  .get(requireLogin)
  .post(requireLogin)
  .put(requireLogin)
  .delete(requireLogin);

routes
  .get(requireAdmin)
  .get('/', function (req, res, next) {
    const Model = mongoose.model('MainCommissionDevidendRequest', req.app.models.mainComissionDevidendRequest.schema);
    const Member = mongoose.model('Member', req.app.models.members.schema);
    var limit = Number(req.query.limit),
      page = req.query.page,
      skip = (((page-1) * limit)),
      search = req.query.search,
      filter = req.query.filter,
      sort = req.query.sort;
    let queryParams = {};
    if(search) {
      queryParams = {$text: {$search: search}};
    }

    if(filter && filter !== '{}') {
      const filterObj = JSON.parse(filter);
      _.each(filterObj, (value, index) => {
        if (index === '_id') {
          queryParams._id = new ObjectId(value);
        } else {
          queryParams[index] = value;
        }
      });
    }

    if(queryParams['startDate'] || queryParams['endDate']) {
      queryParams.createDate = {};
    }

    if(queryParams['startDate']) {
      const startDate = moment(queryParams['startDate']).startOf('day')._d;
      _.assign(queryParams.createDate, {$gt: startDate});
      delete queryParams['startDate'];
    }
    if(queryParams['endDate']) {
      const endDate = moment(queryParams['endDate']).endOf('day')._d;
      _.assign(queryParams.createDate, {$lt: endDate});
      delete queryParams['endDate'];
    }

    let memberQueryParams = {};
    if (queryParams.member) {
      if (queryParams.member.applicantID)
        memberQueryParams.applicantID = new RegExp(queryParams.member.applicantID, 'i');
      if (queryParams.member.email)
        memberQueryParams.email = new RegExp(queryParams.member.email, 'i');
      delete queryParams.member;
    }

    let membersIds = [];
    async.auto({
      'members': next => {
        Member.find(memberQueryParams, {_id: true, applicantID: true, applicantName: true}, (err, data) => {
          if (err) return next(err);
          membersIds = _.map(data, item => item._id);
          queryParams.memberID = {$in: membersIds};
          next(null, data);
        }).lean(true);
      },
      'count': ['members', (data, next) => {
        Model.count(queryParams, next);
      }],
      'requests': ['count',  ({count}, next) => {
        if (count === -1) {
          return next(null, []);
        }
        if(search) {
          skip = 0;
        }
        if(sort) {
          let sortOptions = JSON.parse(sort);
          sort = {};
          sort[sortOptions.name] = sortOptions.order;
        }
        Model.find(queryParams, next).skip(skip).limit(limit).sort(sort).lean(true);
      }],
      'items': ['requests', 'members', ({members, requests}, next) => {
        const items = _.map(requests, request => {
          request.member = _.find(members, member => request.memberID && (member._id.toString() === request.memberID.toString()));
          return request;
        });
        next(null, items);
      }]
    }, (err, {count, items}) => {
      if(err) {
        return res.status(404).send(err);
      }

      if (count !== -1) {
        res.set('x-total-count', count);
      }
      res.json({items, count});
    });

  });

routes.get('/:memberId', (req, res, next) => {
  const Member = mongoose.model('Member', req.app.models.members.schema);
  const Model = mongoose.model('MainCommissionDevidendRequest', req.app.models.mainComissionDevidendRequest.schema);
  const type = req.query.type;
  const month = req.query.month && +req.query.month;
  const year = req.query.year && +req.query.year;
  const {memberId} = req.params;

  async.auto({
    'member': (next) => {
      Member.findById(memberId, next);
    },
    'isCurrentUser': ['member', ({member}, next) => {
      isCurrentUser(req, res, member, next);
    }],
    'masterRecord': ['isCurrentUser', (data, next) => {
      Model
        .aggregate([
          {
            $match:  {
              memberID: memberId,
              month,
              year,
              type
            }
          },
          {
            $group: {
              _id : "$memberID",
              amount: { $sum: "$amount" }
            }
          }
        ])
        .allowDiskUse(true)
        .exec(next)
    }],
  },  (err, {masterRecord}) => {
    if (err) {
      return res.status(404).send(err);
    }

    res.json(masterRecord);
  });
});

export default routes;
