// ROUTES
import routesAuth from './routes/auth.js';
import routesVsPurchase from './routes/vsPurchase.js';
import routesMember from './routes/member.js';
import routesMatchingBonusRequest from './routes/matchingBonusRequest.js';
import routesCommissionRequest from './routes/commissionRequest.js';
import routesRankCommissionRequest from './routes/rankCommissionRequest.js';
import routesCashRewardRequest from './routes/cashRewardRequest.js';
import routesWithdrawRequest from './routes/withdrawRequest.js';
import routesTicketRequest from './routes/ticketRequest.js';
import routesBrave1WayRequest from './routes/brave1WayRequest.js';
import routesBrave2WayRequest from './routes/brave2WayRequest.js';
import routesBrave3WayRequest from './routes/brave3WayRequest.js';
import mainCommissionDevidendRequest from './routes/mainCommissionDevidendRequest.js';

// MODELS
import * as membersModel from './models/member.js';
import * as walletsModel from './models/wallet.js';
import * as vsMasterPurchaseModel from './models/vsMasterPurchase.js';
import * as vsMasterTicketModel from './models/vsMasterTicket.js';
import * as vsTransactionPurchaseModel from './models/vsTransactionPurchase.js';
import * as vsTransactionTicketModel from './models/vsTransactionTicket.js';
import * as masterMatchingBonusRequestModel from './models/masterMatchingBonusRequest.js';
import * as transactionMatchingBonusRequestModel from './models/transactionMatchingBonusRequest.js';
import * as masterCommissionRequestModel from './models/masterCommissionRequest.js';
import * as transactionCommissionRequestModel from './models/transactionCommissionRequest.js';
import * as masterRankCommissionRequestModel from './models/masterRankCommissionRequest.js';
import * as transactionRankCommissionRequestModel from './models/transactionRankCommissionRequest.js';
import * as masterCashRewardRequestModel from './models/masterCashRewardRequest.js';
import * as transactionCashRewardRequestModel from './models/transactionCashRewardRequest.js';
import * as prognosedCashRewardRequestModel from './models/prognosedCashRewardRequest.js';
import * as masterWithdrawRequestModel from './models/masterWithdrawRequest.js';
import * as transactionWithdrawRequestModel from './models/transactionWithdrawRequest.js';
import * as vsMasterBrave1WayModel from './models/vsMasterBrave1Way.js';
import * as vsTransactionBrave1WayModel from './models/vsTransactionBrave1Way.js';
import * as vsMasterBrave2WayModel from './models/vsMasterBrave2Way.js';
import * as vsTransactionBrave2WayModel from './models/vsTransactionBrave2Way.js';
import * as vsMasterBrave3WayModel from './models/vsMasterBrave3Way.js';
import * as vsTransactionBrave3WayModel from './models/vsTransactionBrave3Way.js';
import * as memberRankTransactionModel from './models/memberRankTransaction.js';
import * as resetPasswordTokenModel from './models/resetPasswordToken.js';
import * as mainComissionDevidendRequestModel from './models/mainCommissionDevidendRequest.js';

function UsersModule(app) {
  this.app = app;

  this.initModels = function () {
    this.app.models.members = membersModel;
    this.app.models.wallets = walletsModel;
    this.app.models.vsMasterPurchases = vsMasterPurchaseModel;
    this.app.models.vsMasterTickets = vsMasterTicketModel;
    this.app.models.vsTransactionPurchases = vsTransactionPurchaseModel;
    this.app.models.vsTransactionTickets = vsTransactionTicketModel;
    this.app.models.masterMatchingBonusRequest = masterMatchingBonusRequestModel;
    this.app.models.transactionMatchingBonusRequest = transactionMatchingBonusRequestModel;
    this.app.models.masterCommissionRequest = masterCommissionRequestModel;
    this.app.models.transactionCommissionRequest = transactionCommissionRequestModel;
    this.app.models.masterRankCommissionRequest = masterRankCommissionRequestModel;
    this.app.models.transactionRankCommissionRequest = transactionRankCommissionRequestModel;
    this.app.models.masterCashRewardRequest = masterCashRewardRequestModel;
    this.app.models.transactionCashRewardRequest = transactionCashRewardRequestModel;
    this.app.models.prognosedCashRewardRequestModel = prognosedCashRewardRequestModel;
    this.app.models.masterWithdrawRequest = masterWithdrawRequestModel;
    this.app.models.transactionWithdrawRequest = transactionWithdrawRequestModel;
    this.app.models.vsMasterBrave1Way = vsMasterBrave1WayModel;
    this.app.models.vsTransactionBrave1Way = vsTransactionBrave1WayModel;
    this.app.models.vsMasterBrave2Way = vsMasterBrave2WayModel;
    this.app.models.vsTransactionBrave2Way = vsTransactionBrave2WayModel;
    this.app.models.vsMasterBrave3Way = vsMasterBrave3WayModel;
    this.app.models.vsTransactionBrave3Way = vsTransactionBrave3WayModel;
    this.app.models.memberRankTransaction = memberRankTransactionModel;
    this.app.models.resetPasswordToken = resetPasswordTokenModel;
    this.app.models.mainComissionDevidendRequest = mainComissionDevidendRequestModel;
  };

  this.initRoutes = function () {
    this.app.use('/api/auth', routesAuth);
    this.app.use('/api/vs_purchase', routesVsPurchase);
    this.app.use('/api/member', routesMember);
    this.app.use('/api/matching_bonus_request', routesMatchingBonusRequest);
    this.app.use('/api/commission_request', routesCommissionRequest);
    this.app.use('/api/rank_commission_request', routesRankCommissionRequest);
    this.app.use('/api/cash_reward_request', routesCashRewardRequest);
    this.app.use('/api/withdraw_request', routesWithdrawRequest);
    this.app.use('/api/ticket_requests', routesTicketRequest);
    this.app.use('/api/brave1way_requests', routesBrave1WayRequest);
    this.app.use('/api/brave2way_requests', routesBrave2WayRequest);
    this.app.use('/api/brave3way_requests', routesBrave3WayRequest);
    this.app.use('/api/main_commission_devidend_request', mainCommissionDevidendRequest);
  };
}

export default UsersModule;